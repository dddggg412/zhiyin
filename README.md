# 知音编辑器

#### 介绍
使用中文编程语言（知语言）基于WIN32 API开发的开源文本编辑器,体积小、速度快、性能卓越。为中文编程提供一款更好的源代码编辑器。
- Q群：500558920
- 网址：www.880.xin

#### 软件架构
纯WIN32API+scintilla

#### 运行截图
![输入图片说明](src/%E8%BF%90%E8%A1%8C%E6%88%AA%E5%9B%BE/%E7%9F%A5%E9%9F%B3%E7%BC%96%E8%BE%91V1.0.00.JPG)
#### 安装教程

1.  解压即可使用

#### 使用说明

1.  目前是初始版，后面会逐步优化升级。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
