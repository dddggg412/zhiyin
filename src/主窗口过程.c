#include "framework.h"

int		    全_缩放重置 = 0 ;

BOOL		全_文件树栏悬停正在调整大小吗 = FALSE ;
BOOL		全_文件树栏正在调整大小吗 = FALSE ;
BOOL		全_选项卡页面正在移动吗 = FALSE ;
int		    全_选项卡页移动自 = 0 ;
POINT		g_rectLMouseDown = { 0 } ;
BOOL		全_函数列表悬停正在调整大小吗 = FALSE ;
BOOL		全_函数列表正在调整大小吗 = FALSE ;
BOOL		全_树状图悬停正在调整大小吗 = FALSE ;
BOOL		全_树状图正在调整大小吗 = FALSE ;
BOOL		全_Sql查询结果编辑悬停正在调整大小吗 = FALSE ;
BOOL		全_Sql查询结果编辑正在调整大小吗 = FALSE ;
BOOL		全_正在调整Sql查询结果列表视图悬停的大小吗 = FALSE ;
BOOL		全_正在调整Sql查询结果列表视图的大小吗 = FALSE ;

int 主窗口过程之前( MSG *p_信息 )
{
	NMHDR		*lpnmhdr = NULL;
	int		nret = 0 ;

	if( p_信息->message == WM_SYSKEYDOWN && 49 <= p_信息->wParam && p_信息->wParam <= 57 && (p_信息->lParam&(1<<29)) )
	{
		struct 选项卡页面S	*选项卡页节点 = 以索引选择选项卡页面( (unsigned int)(p_信息->wParam) - 49 ) ;
		if( 选项卡页节点 )return 1;
	}

	if( p_信息->message == WM_MOUSEMOVE )
	{
		WPARAM wParam ;
		LPARAM lParam ;
		POINT pt ;
		wParam = p_信息->wParam ;
		lParam = p_信息->lParam ;
		pt.x = LOWORD(lParam) ;
		pt.y = HIWORD(lParam) ;
		if( p_信息->hwnd != 全_主窗口句柄 )
		{
			ClientToScreen( p_信息->hwnd , & pt );
			ScreenToClient( 全_主窗口句柄 , & pt );
			lParam = MAKELONG( pt.x , pt.y ) ;
		}
		wParam = MAKELONG( IDM_MOUSEMOVE , 0 ) ;
		PostMessage( 全_主窗口句柄 , WM_COMMAND , wParam , lParam);
	}

	if( p_信息->message == WM_LBUTTONUP )
	{
		if( p_信息->hwnd != 全_主窗口句柄 )
		{
			PostMessage( 全_主窗口句柄 , WM_LBUTTONUP , p_信息->wParam , p_信息->lParam);
		}
	}

	if( p_信息->hwnd == 全_文件树句柄 )
	{
		if( p_信息->message == WM_RBUTTONDOWN )
		{
			POINT pt ;
			pt.x = LOWORD(p_信息->lParam) ;
			pt.y = HIWORD(p_信息->lParam) ;
			TVHITTESTINFO tvhti ;
			memset( & tvhti , 0x00 , sizeof(tvhti) );
			memcpy( & (tvhti.pt) , & pt , sizeof(POINT) );
			TreeView_HitTest( 全_文件树句柄 , & tvhti );
			struct 树视图数据S *tvd = 从HTREEITEM获取树状视图数据( tvhti.hItem ) ;
			if( tvd )
			{
				ClientToScreen( p_信息->hwnd , & pt );
				if( tvd->nImageIndex == 全_文件树映像驱动器 || tvd->nImageIndex == nFileTreeImageOpenFold || tvd->nImageIndex == nFileTreeImageClosedFold )
					TrackPopupMenu( g_hFileTreeDirectoryPopupMenu , 0 , pt.x , pt.y , 0 , p_信息->hwnd , NULL );
				else
					TrackPopupMenu( g_hFileTreeFilePopupMenu , 0 , pt.x , pt.y , 0 , p_信息->hwnd , NULL );
			}
			return 1;
		}
		else if( p_信息->message == WM_COMMAND && LOWORD(p_信息->wParam) == IDM_REFRESH_FILETREE )
		{
			RefreshFileTree();
			return 1;
		}
		else if( p_信息->message == WM_COMMAND && LOWORD(p_信息->wParam) == IDM_CREATE_SUB_DIRECTORY )
		{
			FileTreeCreateSubDirectory();
			return 1;
		}
		else if( p_信息->message == WM_COMMAND && LOWORD(p_信息->wParam) == IDM_RENAME_DIRECTORY )
		{
			FileTreeRenameDirectory();
			return 1;
		}
		else if( p_信息->message == WM_COMMAND && LOWORD(p_信息->wParam) == IDM_DELETE_DIRECTORY )
		{
			FileTreeDeleteDirectory();
			return 1;
		}
		else if( p_信息->message == WM_COMMAND && LOWORD(p_信息->wParam) == IDM_CREATE_FILE )
		{
			FileTreeCreateFile();
			return 1;
		}
		else if( p_信息->message == WM_COMMAND && LOWORD(p_信息->wParam) == IDM_COPY_FILE )
		{
			FileTreeCopyFile();
			return 1;
		}
		else if( p_信息->message == WM_COMMAND && LOWORD(p_信息->wParam) == IDM_RENAME_FILE )
		{
			FileTreeRenameFile();
			return 1;
		}
		else if( p_信息->message == WM_COMMAND && LOWORD(p_信息->wParam) == IDM_DELETE_FILE )
		{
			FileTreeDeleteFile();
			return 1;
		}
	}

	if( 全_当前选项卡页面节点 && p_信息->hwnd == 全_选项卡页面句柄 )
	{
		if( p_信息->message == WM_LBUTTONDOWN )
		{
			if( 全_选项卡页面正在移动吗 == FALSE )
			{
				int x = LOWORD(p_信息->lParam) ;
				int y = HIWORD(p_信息->lParam) ;

				int n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
				RECT rectTabPage ;
				全_选项卡页移动自 = -1 ;
				for( int n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
				{
					TabCtrl_GetItemRect( 全_选项卡页面句柄 , n选项卡页面索引 , & rectTabPage );
					if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
					{
						全_选项卡页移动自 = n选项卡页面索引 ;
						break;
					}
				}
				if( 全_选项卡页移动自 >= 0 )
				{
					SetCursor( LoadCursor(NULL,IDC_SIZEALL) );
					全_选项卡页面正在移动吗 = TRUE ;
					g_rectLMouseDown.x = x ;
					g_rectLMouseDown.y = y ;
				}
			}
		}
		else if( p_信息->message == WM_LBUTTONUP )
		{
			if( 全_选项卡页面正在移动吗 == TRUE )
			{
				int x = LOWORD(p_信息->lParam) ;
				int y = HIWORD(p_信息->lParam) ;

				if( abs(x-g_rectLMouseDown.x) > 拖动或单击鼠标移动阈值 || abs(y-g_rectLMouseDown.y) > 拖动或单击鼠标移动阈值 )
				{
					int n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
					RECT rectTabPage ;
					int nTabPageMoveTo = -1 ;
					for( int n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
					{
						TabCtrl_GetItemRect( 全_选项卡页面句柄 , n选项卡页面索引 , & rectTabPage );
						if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
						{
							nTabPageMoveTo = n选项卡页面索引 ;
							break;
						}
					}
					if( nTabPageMoveTo >= 0 )
					{
						TabCtrl_DeleteItem( 全_选项卡页面句柄 , 全_选项卡页移动自 );

						TCITEM		tci ;

						memset( & tci , 0x00 , sizeof(TCITEM) );
						tci.mask = TCIF_TEXT|TCIF_PARAM ;
						tci.pszText = 全_当前选项卡页面节点->acFilename ;
						tci.lParam = (LPARAM)全_当前选项卡页面节点 ;
						nret = TabCtrl_InsertItem( 全_选项卡页面句柄 , nTabPageMoveTo , & tci ) ;
						if( nret == -1 )
						{
							MessageBox(NULL, TEXT("TabCtrl_InsertItem失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
						}
						else
						{
							以索引选择选项卡页面( nTabPageMoveTo );
						}
					}
				}

				SetCursor( LoadCursor(NULL,IDC_ARROW) );
				全_选项卡页面正在移动吗 = FALSE ;
			}
		}
	}

	if( 全_当前选项卡页面节点 && p_信息->hwnd == 全_当前选项卡页面节点->Scintilla句柄 )
	{
		if( p_信息->message == WM_KEYDOWN )
		{
			if( 全_当前选项卡页面节点 && 全_当前选项卡页面节点->pst文档类型配置 && 全_当前选项卡页面节点->pst文档类型配置->pfuncOnKeyDown )
				全_当前选项卡页面节点->pst文档类型配置->pfuncOnKeyDown( 全_当前选项卡页面节点 , p_信息->wParam , (GetKeyState(VK_CONTROL)&0x8000)?VK_CONTROL:0 );
		}

		if( p_信息->message == WM_KEYDOWN )
		{
			if( 全_当前选项卡页面节点->bHexEditMode == TRUE )
			{
				nret = AdujstKeyOnHexEditMode( 全_当前选项卡页面节点 , p_信息 ) ;
				if( nret == 1 )
					return 1;
			}
		}

		if( p_信息->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_信息->lParam) ;
			pt.y = HIWORD(p_信息->lParam) ;
			ClientToScreen( p_信息->hwnd , & pt );
			TrackPopupMenu( 全_h编辑框弹出菜单 , 0 , pt.x , pt.y , 0 , 全_主窗口句柄 , NULL );
		}
	}

	if( 全_当前选项卡页面节点 && p_信息->hwnd == 全_当前选项卡页面节点->符号列表句柄 )
	{
		if( p_信息->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_信息->lParam) ;
			pt.y = HIWORD(p_信息->lParam) ;
			ClientToScreen( p_信息->hwnd , & pt );
			TrackPopupMenu( 全_h符号列表弹出菜单 , 0 , pt.x , pt.y , 0 , p_信息->hwnd , NULL );
			return 1;
		}
		else if( p_信息->message == WM_COMMAND && LOWORD(p_信息->wParam) == IDM_RELOAD_SYMBOLLIST )
		{
			if( 全_当前选项卡页面节点 && 全_当前选项卡页面节点->pst文档类型配置 && 全_当前选项卡页面节点->pst文档类型配置->pfuncOnReloadSymbolList )
				全_当前选项卡页面节点->pst文档类型配置->pfuncOnReloadSymbolList( 全_当前选项卡页面节点 );
			return 1;
		}
	}

	if( 全_当前选项卡页面节点 && p_信息->hwnd == 全_当前选项卡页面节点->符号树句柄 )
	{
		if( p_信息->message == WM_LBUTTONDBLCLK )
		{
			if( 全_当前选项卡页面节点->pst文档类型配置 && 全_当前选项卡页面节点->pst文档类型配置->pfuncOnDbClickSymbolTree )
				全_当前选项卡页面节点->pst文档类型配置->pfuncOnDbClickSymbolTree( 全_当前选项卡页面节点 );
			return 1;
		}
		else if( p_信息->message == WM_RBUTTONDOWN )
		{
			POINT pt ;
			pt.x = LOWORD(p_信息->lParam) ;
			pt.y = HIWORD(p_信息->lParam) ;
			ClientToScreen( p_信息->hwnd , & pt );
			TrackPopupMenu( 全_h符号树弹出菜单 , 0 , pt.x , pt.y , 0 , p_信息->hwnd , NULL );
			return 1;
		}
		else if( p_信息->message == WM_COMMAND && LOWORD(p_信息->wParam) == IDM_RELOAD_SYMBOLTREE )
		{
			if( 全_当前选项卡页面节点 && 全_当前选项卡页面节点->pst文档类型配置 && 全_当前选项卡页面节点->pst文档类型配置->pfuncOnReloadSymbolTree )
				全_当前选项卡页面节点->pst文档类型配置->pfuncOnReloadSymbolTree( 全_当前选项卡页面节点 );
			return 1;
		}
	}

	return 0;
}

int 发送消息之前( MSG *p_信息 )
{
	int		nret = 0 ;

	if( 全_当前选项卡页面节点 && p_信息->hwnd == 全_当前选项卡页面节点->Scintilla句柄 )
	{
		if( p_信息->message == WM_CHAR )
		{
			if( 全_当前选项卡页面节点->bHexEditMode == TRUE )
			{
				nret = AdujstCharOnHexEditMode( 全_当前选项卡页面节点 , p_信息 ) ;
				if( nret == 1 )
					return 1;
			}
		}
	}

	return 0;
}

int 主窗口过程之后( MSG *p_信息 )
{
	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	struct 选项卡页面S	*p = NULL ;
	TCITEM		tci ;

	if( 全_当前选项卡页面节点 && p_信息->hwnd == 全_选项卡页面句柄 )
	{
		if( p_信息->message == WM_RBUTTONDOWN )
		{
			int x = LOWORD(p_信息->lParam) ;
			int y = HIWORD(p_信息->lParam) ;
			int n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
			RECT rectTabPage , rectFileTreeBar ;
			for( n选项卡页面索引 = 0 ; n选项卡页面索引 < n选项卡页面数量 ; n选项卡页面索引++ )
			{
				int	x1 , x2 ;

				TabCtrl_GetItemRect( 全_选项卡页面句柄 , n选项卡页面索引 , & rectTabPage );
				x1 = rectTabPage.left ;
				x2 = rectTabPage.right;
				if( 全_显示文件树栏 == TRUE )
				{
					GetClientRect( 全_文件树栏句柄 , & rectFileTreeBar );
					x1 += rectFileTreeBar.right + 分割_宽度 ;
					x2 += rectFileTreeBar.right + 分割_宽度 ;
				}
				if( x1 < x && x < x2 && rectTabPage.top < y && y < rectTabPage.bottom )
				{
					以索引选择选项卡页面( n选项卡页面索引 );
					break;
				}
			}
		}
		else if( p_信息->message == WM_RBUTTONUP )
		{
			POINT pt ;
			pt.x = LOWORD(p_信息->lParam) ;
			pt.y = HIWORD(p_信息->lParam) ;
			ClientToScreen( p_信息->hwnd , & pt );
			TrackPopupMenu( 全_h选项卡页弹出菜单 , 0 , pt.x , pt.y , 0 , 全_主窗口句柄 , NULL );
		}
	}

	if( 全_当前选项卡页面节点 && p_信息->hwnd == 全_当前选项卡页面节点->Scintilla句柄 )
	{
		if( p_信息->message == WM_KEYDOWN || p_信息->message == WM_CHAR )
		{
			if( 全_当前选项卡页面节点->bHexEditMode == TRUE )
			{
				AdujstPositionOnHexEditMode( 全_当前选项卡页面节点 , p_信息 );
			}
		}
		else if( p_信息->message == WM_KEYUP )
		{
			int 当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
			UpdateNavigateBackNextTrace( 全_当前选项卡页面节点 , 当前文本字节位置 );

			if( 全_当前选项卡页面节点->pst文档类型配置 && 全_当前选项卡页面节点->pst文档类型配置->pfuncOnKeyUp && 全_当前选项卡页面节点->bHexEditMode == FALSE )
				全_当前选项卡页面节点->pst文档类型配置->pfuncOnKeyUp( 全_当前选项卡页面节点 , p_信息->wParam , p_信息->lParam );

			更新状态栏定位信息();
		}
	}

	if( p_信息->message == WM_LBUTTONUP )
	{
		n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
		for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
		{
			memset( & tci , 0x00 , sizeof(TCITEM) );
			tci.mask = TCIF_PARAM ;
			TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
			p = (struct 选项卡页面S *)(tci.lParam);
			if( p_信息->hwnd == p->Scintilla句柄 )
			{
				int 当前文本字节位置 = (int)p->pfuncScintilla( p->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
				AddNavigateBackNextTrace( p , 当前文本字节位置 );

				if( p && p->pst文档类型配置 && p->pst文档类型配置->pfuncOnKeyUp && p->bHexEditMode == FALSE )
					p->pst文档类型配置->pfuncOnKeyUp( p , p_信息->wParam , p_信息->lParam );

				更新状态栏定位信息();

				if( p->bHexEditMode == TRUE )
				{
					AdujstPositionOnHexEditMode( p , p_信息 );
				}
			}
		}
	}

	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		CenterWindow( hDlg , 全_主窗口句柄 );
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
#if 0
			char buf[1024];
			memset(buf,0x00,sizeof(buf));
			全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_STYLEGETFONT , STYLE_DEFAULT , (sptr_t)buf );
			InfoBox(buf);
#endif
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

//
//  函数: 主窗口过程(HWND, UINT, WPARAM, LPARAM)
//  目标: 处理主窗口的消息。
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT	- 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK 主窗口过程(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int		nWidth , nHeight ;
	int		wmId , wmNC ;
	HWND		wmWnd ;
	NMHDR		*lpnmhdr = NULL;
	SCNotification	*lpnotify = NULL ;
	NMTREEVIEW	*lpnmtv = NULL ;
	TOOLTIPTEXT	*pTTT = NULL ;
	struct 选项卡页面S	*选项卡页节点 = NULL ;
	PAINTSTRUCT	ps ;
	HDC		hdc ;

	int		nret = 0;

	if( message == WM_COMMAND )
	{
		wmId = LOWORD(wParam) ;
		if( IDM_FILE_OPEN_RECENTLY_HISTORY_BASE <= wmId && wmId <= IDM_FILE_OPEN_RECENTLY_HISTORY_BASE + 最近打开路径文件名最大数量M-1 )
		{
			if( 全_风格主配置.最近打开路径文件名[wmId-IDM_FILE_OPEN_RECENTLY_HISTORY_BASE][0] )
			{
				OpenFileDirectly( 全_风格主配置.最近打开路径文件名[wmId-IDM_FILE_OPEN_RECENTLY_HISTORY_BASE] );
			}
		}
		if( IDM_VIEW_SWITCH_STYLETHEME_BASE <= wmId && wmId <= IDM_VIEW_SWITCH_STYLETHEME_BASE + 视觉样式最大数量M-1 )
		{
			if( 全_风格主配置.最近打开路径文件名[wmId-IDM_VIEW_SWITCH_STYLETHEME_BASE][0] )
			{
				OnViewSwitchStyleTheme( wmId-IDM_VIEW_SWITCH_STYLETHEME_BASE );
			}
		}
		if( IDM_VIEW_SWITCH_FILETYPE_BASE <= wmId && wmId <= IDM_VIEW_SWITCH_FILETYPE_BASE + 视觉文件类型最大数量M-1 )
		{
			OnViewSwitchFileType( wmId-IDM_VIEW_SWITCH_FILETYPE_BASE );
		}
	}

	switch (message)
	{
	case WM_CREATE:
		nret = 创建新窗口( hWnd , wParam , lParam ) ;
		if( nret )
			PostQuitMessage(0);
		break;
	case WM_SIZE:
		nWidth = LOWORD( lParam );
		nHeight = HIWORD( lParam );
		重置窗口大小( hWnd , nWidth , nHeight );
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam) ;
		wmNC = HIWORD(wParam) ;
		wmWnd = (HWND)lParam ;

		// 分析菜单选择:
		if( wmNC == LBN_DBLCLK )
		{
			nWidth = 0 ;
			nHeight = 0 ;
		}

		if( 全_当前选项卡页面节点 && wmWnd == 全_当前选项卡页面节点->符号列表句柄 && wmNC == LBN_DBLCLK )
		{
			return OnSymbolListDbClick( 全_当前选项卡页面节点 );
		}
		else if( wmNC == BN_CLICKED && wmWnd == 全_h选项卡关闭按钮句柄 )
		{
			if( 全_h选项卡页关闭按钮节点 )
			{
				return 关闭文件时( 全_h选项卡页关闭按钮节点 );
			}
		}

		switch (wmId)
		{
		case IDM_FILE_NEW:
			文件_新建( 全_当前选项卡页面节点 );
			break;
		case IDM_FILE_OPEN:
			文件_打开();
			break;
		case IDM_FILE_CLEAN_OPEN_RECENTLY_HISTORY:
			清理最近打开文件的历史( 全_当前选项卡页面节点 );
			break;
		case IDM_FILE_SAVE:
			文件_保存( 全_当前选项卡页面节点 , FALSE );
			break;
		case IDM_FILE_SAVEAS:
			文件_另存为( 全_当前选项卡页面节点 );
			break;
		case IDM_FILE_SAVEALL:
			保存所有的文件();
			break;
		case IDM_FILE_CLOSE:
			关闭文件时( 全_当前选项卡页面节点 );
			break;
		case IDM_FILE_CLOSEALL:
			关闭所有文件时();
			break;
		case IDM_FILE_CLOSEALL_EXPECT_CURRENT_FILE:
			关闭除当前所有文件时( 全_当前选项卡页面节点 );
			break;
		case IDM_FILE_REMOTE_FILESERVERS:
			管理远程文件服务器();
			break;
		case IDM_CRATE_NEWFILE_ON_NEWBOOT:
			启动时没有打开文件则新建文件( 全_当前选项卡页面节点 );
			break;
		case IDM_FILE_OPENFILES_THAT_OPENNING_ON_EXIT:
			启动时打开退出时打开的文件( 全_当前选项卡页面节点 );
			break;
		case IDM_FILE_SETREADONLY_AFTER_OPEN:
			在打开文件后设置为只读( 全_当前选项卡页面节点 );
			break;
		case IDM_FILE_CHECK_UPDATE_WHERE_SELECTTABPAGE:
			检测文件变动( 全_当前选项卡页面节点 );
			break;
		case IDM_FILE_PROMPT_WHERE_AUTOUPDATE:
			文件变动自动重载提示( 全_当前选项卡页面节点 );
			break;
		case IDM_FILE_NEWFILE_WINDOWS_EOLS:
			设置新建文件换行符风格( 全_当前选项卡页面节点 , 0 );
			break;
		case IDM_FILE_NEWFILE_MAC_EOLS:
			设置新建文件换行符风格( 全_当前选项卡页面节点 , 1 );
			break;
		case IDM_FILE_NEWFILE_UNIX_EOLS:
			设置新建文件换行符风格( 全_当前选项卡页面节点 , 2 );
			break;
		case IDM_FILE_CONVERT_WINDOWS_EOLS:
			全文转换文件换行符风格( 全_当前选项卡页面节点 , 0 );
			break;
		case IDM_FILE_CONVERT_MAC_EOLS:
			全文转换文件换行符风格( 全_当前选项卡页面节点 , 1 );
			break;
		case IDM_FILE_CONVERT_UNIX_EOLS:
			全文转换文件换行符风格( 全_当前选项卡页面节点 , 2 );
			break;
		case IDM_FILE_NEWFILE_ENCODING_UTF8:
			设置新建文件字符编码( 全_当前选项卡页面节点 , 字符编码_UTF8 );
			break;
		case IDM_FILE_NEWFILE_ENCODING_GB18030:
			设置新建文件字符编码( 全_当前选项卡页面节点 , 字符编码_GBK );
			break;
		case IDM_FILE_NEWFILE_ENCODING_BIG5:
			设置新建文件字符编码( 全_当前选项卡页面节点 , 字符编码_BIG5 );
			break;
		case IDM_FILE_CONVERT_ENCODING_UTF8:
			全文转换文件字符编码( 全_当前选项卡页面节点 , 字符编码_UTF8 );
			break;
		case IDM_FILE_CONVERT_ENCODING_GB18030:
			全文转换文件字符编码( 全_当前选项卡页面节点 , 字符编码_GBK );
			break;
		case IDM_FILE_CONVERT_ENCODING_BIG5:
			全文转换文件字符编码( 全_当前选项卡页面节点 , 字符编码_BIG5 );
			break;
		case IDM_EXIT:
			nret = 检查所有文件保存() ;
			if( nret )
				break;
			DestroyWindow(hWnd);
			break;
		case IDM_EDIT_UNDO:
			撤销编辑( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_REDO:
			重做编辑( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_CUT:
			剪切文本( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_COPY:
			复制文本( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_PASTE:
			粘贴文本( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_DELETE:
			删除文本( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_CUTLINE:
			剪切行( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_CUTLINE_AND_PASTELINE:
			剪切行和粘贴行( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_COPYLINE:
			复制行( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_COPYLINE_AND_PASTELINE:
			复制行和粘贴行( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_COPY_FILENAME:
			复制文件名称( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_COPY_PATHNAME:
			复制路径名称( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_COPY_PATHFILENAME:
			复制路径文件名称( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_PASTELINE:
			粘贴行( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_PASTELINE_UPSTAIRS:
			往上粘贴行( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_DELETELINE:
			删除行( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_DELETEWHITECHARACTER_AT_LINEHEAD:
			删除行首空白字符( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_DELETEWHITECHARACTER_AT_LINETAIL:
			删除行尾空白字符( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_DELETEBLANKLINE:
			OnDeleteBlankLine( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_DELETEBLANKLINE_WITH_WHITECHARACTER:
			OnDeleteBlankLineWithWhiteCharacter( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_JOINLINE:
			OnJoinLine( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_LOWERCASE:
			OnLowerCaseEdit( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_UPPERCASE:
			OnUpperCaseEdit( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_ENABLE_AUTO_ADD_CLOSECHAR:
			OnEditEnableAutoAddCloseChar( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_ENABLE_AUTO_INDENTATION:
			OnEditEnableAutoIdentation( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_BASE64_ENCODING:
			OnEditBase64Encoding( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_BASE64_DECODING:
			OnEditBase64Decoding( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_MD5:
			OnEditMd5( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_SHA1:
			OnEditSha1( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_SHA256:
			OnEditSha256( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_3DES_CBC_ENCRYPTO:
			OnEdit3DesCbcEncrypto( 全_当前选项卡页面节点 );
			break;
		case IDM_EDIT_3DES_CBC_DECRYPTO:
			OnEdit3DesCbcDecrypto( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_FIND:
			OnSearchFind( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_FINDPREV:
			OnSearchFindPrev( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_FINDNEXT:
			OnSearchFindNext( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_FOUNDLIST:
			OnSearchFoundList( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_REPLACE:
			OnSearchReplace( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_SELECTALL:
			OnSelectAll( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_SELECTWORD:
			OnSelectWord( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_SELECTLINE:
			OnSelectLine( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_ADDSELECT_LEFT_CHARGROUP:
			OnAddSelectLeftCharGroup( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_ADDSELECT_RIGHT_CHARGROUP:
			OnAddSelectRightCharGroup( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_ADDSELECT_LEFT_WORD:
			OnAddSelectLeftWord( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_ADDSELECT_RIGHT_WORD:
			OnAddSelectRightWord( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_ADDSELECT_TOP_BLOCKFIRSTLINE:
			OnAddSelectTopBlockFirstLine( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_ADDSELECT_BOTTOM_BLOCKFIRSTLINE:
			OnAddSelectBottomBlockFirstLine( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_MOVE_LEFT_CHARGROUP:
			OnMoveLeftCharGroup( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_MOVE_RIGHT_CHARGROUP:
			OnMoveRightCharGroup( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_MOVE_LEFT_WORD:
			OnMoveLeftWord( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_MOVE_RIGHT_WORD:
			OnMoveRightWord( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_MOVE_TOP_BLOCKFIRSTLINE:
			OnMoveTopBlockFirstLine( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_MOVE_BOTTOM_BLOCKFIRSTLINE:
			OnMoveBottomBlockFirstLine( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_TOGGLE_BOOKMARK:
			OnSearchToggleBookmark( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_ADD_BOOKMARK:
			OnSearchAddBookmark( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_REMOVE_BOOKMARK:
			OnSearchRemoveBookmark( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_REMOVE_ALL_BOOKMARKS:
			OnSearchRemoveAllBookmarks( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_GOTO_PREV_BOOKMARK:
			OnSearchGotoPrevBookmark( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_GOTO_NEXT_BOOKMARK:
			OnSearchGotoNextBookmark( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_GOTO_PREV_BOOKMARK_IN_ALL_FILES:
			OnSearchGotoPrevBookmarkInAllFiles( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_GOTO_NEXT_BOOKMARK_IN_ALL_FILES:
			OnSearchGotoNextBookmarkInAllFiles( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_GOTOLINE:
			OnSearchGotoLine( 全_当前选项卡页面节点 );
			break;
		case IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE:
			OnSearchNavigateBackPrev_InThisFile();
			break;
		case IDM_SEARCH_NAVIGATEBACK_PREV_IN_ALL_FILES:
			OnSearchNavigateBackPrev_InAllFiles();
			break;
		case IDM_SEARCH_MULTISELECT_README:
			MessageBox( NULL , "使用快捷键Ctrl+MouseLButtonClick多位置定位、Ctrl+MouseLButtonDown+MouseMove+MouseLButtonUp多个文本块选择，支持多位置联动操作：输入、退格键、删除、复制等" , "即时帮助" , MB_ICONINFORMATION | MB_OK);
			break;
		case IDM_SEARCH_COLUMNSELECT_README:
			MessageBox( NULL , "使用快捷键Alt+MouseLButtonDown+MouseMove或Alt+Shift+Left/Right/Up/Down进行列选择，支持多行联动操作：输入、退格键、删除、复制等" , "即时帮助" , MB_ICONINFORMATION | MB_OK);
			break;
		case IDM_VIEW_FILETREE:
			OnViewFileTree( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_MODIFY_STYLETHEME:
			OnViewModifyThemeStyle();
			break;
		case IDM_VIEW_COPYNEW_STYLETHEME:
			OnViewCopyNewThemeStyle();
			break;
		case IDM_VIEW_HEXEDIT_MODE:
			OnViewHexEditMode( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_WRAPLINE_MODE:
			OnViewWrapLineMode( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_ENABLE_WINDOWS_VISUAL_STYLES:
			OnViewEnableWindowsVisualStyles();
			break;
		case IDM_VIEW_TAB_WIDTH:
			OnViewTabWidth( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_ONKEYDOWN_TAB_CONVERT_SPACES:
			OnViewOnKeydownTabConvertSpaces( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_LINENUMBER_VISIABLE:
			OnViewLineNumberVisiable( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_BOOKMARK_VISIABLE:
			OnViewBookmarkVisiable( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_WHITESPACE_VISIABLE:
			OnViewWhiteSpaceVisiable( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_NEWLINE_VISIABLE:
			OnViewNewLineVisiable( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_INDENTATIONGUIDES_VISIABLE:
			OnViewIndentationGuidesVisiable( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_ZOOMOUT:
			OnViewZoomOut( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_ZOOMIN:
			OnViewZoomIn( 全_当前选项卡页面节点 );
			break;
		case IDM_VIEW_ZOOMRESET:
			OnViewZoomReset( 全_当前选项卡页面节点 );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_VISIABLE:
			OnSourceCodeBlockFoldVisiable( 全_当前选项卡页面节点 );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_TOGGLE:
			OnSourceCodeBlockFoldToggle( 全_当前选项卡页面节点 );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_CONTRACT:
			OnSourceCodeBlockFoldContract( 全_当前选项卡页面节点 );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_EXPAND:
			OnSourceCodeBlockFoldExpand( 全_当前选项卡页面节点 );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_CONTRACTALL:
			OnSourceCodeBlockFoldContractAll( 全_当前选项卡页面节点 );
			break;
		case IDM_SOURCECODE_BLOCKFOLD_EXPANDALL:
			OnSourceCodeBlockFoldExpandAll( 全_当前选项卡页面节点 );
			break;
		case IDM_SOURCECODE_GOTODEF:
			if( 全_当前选项卡页面节点 && 全_当前选项卡页面节点->pst文档类型配置 && 全_当前选项卡页面节点->pst文档类型配置->pfuncOnKeyDown )
				全_当前选项卡页面节点->pst文档类型配置->pfuncOnKeyDown( 全_当前选项卡页面节点 , VK_F11 , lParam );
			break;
		case IDM_SOURCECODE_SET_RELOAD_SYMBOLLIST_OR_TREE_INTERVAL:
			OnSetReloadSymbolListOrTreeInterval( 全_当前选项卡页面节点 );
			break;
		case IDM_SOURCECODE_ENABLE_AUTOCOMPLETEDSHOW:
			OnSourceCodeEnableAutoCompletedShow( 全_当前选项卡页面节点 );
			break;
		case IDM_SOURCECODE_AUTOCOMPLETEDSHOW_AFTER_INPUT_CHARACTERS:
			OnSourceCodeAutoCompletedShowAfterInputCharacters( 全_当前选项卡页面节点 );
			break;
		case IDM_SOURCECODE_ENABLE_CALLTIPSHOW:
			OnSourceCodeEnableCallTipShow( 全_当前选项卡页面节点 );
			break;
		case IDM_DATABASE_INSERT_DATABASE_CONNECTION_CONFIG:
			OnInsertDataBaseConnectionConfig( 全_当前选项卡页面节点 );
			break;
		case IDM_DATABASE_EXECUTE_SQL:
		case IDM_DATABASE_EXECUTE_REDIS_COMMAND:
			if( 全_当前选项卡页面节点->pst文档类型配置 && 全_当前选项卡页面节点->pst文档类型配置->pfuncOnKeyDown )
				全_当前选项卡页面节点->pst文档类型配置->pfuncOnKeyDown( 全_当前选项卡页面节点 , VK_F5 , 0 );
			break;
		case IDM_DATABASE_AUTO_SELECT_CURRENT_SQL_AND_EXECUTE:
		case IDM_DATABASE_AUTO_SELECT_CURRENT_REDIS_COMMAND_AND_EXECUTE:
			if( 全_当前选项卡页面节点->pst文档类型配置 && 全_当前选项卡页面节点->pst文档类型配置->pfuncOnKeyDown )
				全_当前选项卡页面节点->pst文档类型配置->pfuncOnKeyDown( 全_当前选项卡页面节点 , VK_F5 , VK_CONTROL );
			break;
		case IDM_DATABASE_INSERT_REDIS_CONNECTION_CONFIG:
			OnInsertRedisConnectionConfig( 全_当前选项卡页面节点 );
			break;
		case IDM_ENV_FILE_POPUPMENU:
			OnEnvFilePopupMenu();
			break;
		case IDM_ENV_DIRECTORY_POPUPMENU:
			OnEnvDirectoryPopupMenu();
			break;
		case IDM_ENV_SET_PROCESSFILE_CMD:
			OnEnvSetProcessFileCommand();
			break;
		case IDM_ENV_EXECUTE_PROCESSFILE_CMD:
			OnEnvExecuteProcessFileCommand( 全_当前选项卡页面节点 );
			break;
		case IDM_ENV_SET_PROCESSTEXT_CMD:
			OnEnvSetProcessTextCommand();
			break;
		case IDM_ENV_EXECUTE_PROCESSTEXT_CMD:
			OnEnvExecuteProcessTextCommand( 全_当前选项卡页面节点 );
			break;
		case IDM_CHANGELOG:
		{
			char	更改日志路径文件名[ MAX_PATH ] ;
			memset( 更改日志路径文件名 , 0x00 , sizeof(更改日志路径文件名) );
			snprintf( 更改日志路径文件名 , sizeof(更改日志路径文件名)-1 , "%s\\ChangeLog-CN" , 全_模块路径名 );
			OpenFileDirectly( 更改日志路径文件名 );
		}
		break;
		case IDM_ABOUT:
			DialogBox(全_当前实例, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_MOUSEMOVE:
			if( 全_显示文件树栏 == TRUE )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( 全_文件树栏正在调整大小吗 == TRUE )
				{
					SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( 全_文件树栏矩形.top <= y && y <= 全_文件树栏矩形.bottom && 全_文件树栏矩形.right < x && x < 全_选项卡页矩形.left )
				{
					SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					if( 全_文件树栏悬停正在调整大小吗 == FALSE )
						全_文件树栏悬停正在调整大小吗 = TRUE ;
				}
				else
				{
					if( 全_文件树栏悬停正在调整大小吗 == TRUE )
						全_文件树栏悬停正在调整大小吗 = FALSE ;
				}

				if( 全_文件树栏正在调整大小吗 == TRUE )
				{
					全_风格主配置.文件树栏宽度 = x - 全_文件树栏矩形.left - 分割_宽度/2 ;
					if( 全_风格主配置.文件树栏宽度 < 符号树栏_宽度_最小 )
						全_风格主配置.文件树栏宽度 = 符号树栏_宽度_最小 ;

					更新所有窗口( 全_主窗口句柄 );
				}
			}

			if( 全_当前选项卡页面节点 && 全_选项卡页面正在移动吗 == TRUE )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				int n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
				RECT rectTabPage ;
				for( int n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
				{
					TabCtrl_GetItemRect( 全_选项卡页面句柄 , n选项卡页面索引 , & rectTabPage );
					if( rectTabPage.left < x && x < rectTabPage.right && rectTabPage.top < y && y < rectTabPage.bottom )
					{
						SetCursor( LoadCursor(NULL,IDC_SIZEALL) );
					}
				}
			}

			if( 全_当前选项卡页面节点 && 全_当前选项卡页面节点->符号列表句柄 )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( 全_函数列表正在调整大小吗 == TRUE )
				{
					SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( 全_当前选项卡页面节点->符号列表矩形.top <= y && y <= 全_当前选项卡页面节点->符号列表矩形.bottom && 全_当前选项卡页面节点->Scintilla矩形.right < x && x < 全_当前选项卡页面节点->符号列表矩形.left )
				{
					SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					全_函数列表悬停正在调整大小吗 = TRUE ;
				}
				else
				{
					if( 全_函数列表悬停正在调整大小吗 == TRUE )
						全_函数列表悬停正在调整大小吗 = FALSE ;
				}

				if( 全_函数列表正在调整大小吗 == TRUE )
				{
					全_风格主配置.符号列表的宽度 = 全_当前选项卡页面节点->符号列表矩形.right - x - 分割_宽度/2 ;
					if( 全_风格主配置.符号列表的宽度 < 符号列表_宽度_最小 )
						全_风格主配置.符号列表的宽度 = 符号列表_宽度_最小 ;

					更新所有窗口( 全_主窗口句柄 );
				}
			}

			if( 全_当前选项卡页面节点 && 全_当前选项卡页面节点->符号树句柄 )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( 全_树状图正在调整大小吗 == TRUE )
				{
					SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
				}
				else if( 全_当前选项卡页面节点->符号树矩形.top <= y && y <= 全_当前选项卡页面节点->符号树矩形.bottom && 全_当前选项卡页面节点->Scintilla矩形.right < x && x < 全_当前选项卡页面节点->符号树矩形.left )
				{
					SetCursor( LoadCursor(NULL,IDC_SIZEWE) );
					全_树状图悬停正在调整大小吗 = TRUE ;
				}
				else
				{
					if( 全_树状图悬停正在调整大小吗 == TRUE )
						全_树状图悬停正在调整大小吗 = FALSE ;
				}

				if( 全_树状图正在调整大小吗 == TRUE )
				{
					全_风格主配置.符号树宽度 = 全_当前选项卡页面节点->符号树矩形.right - x - 分割_宽度/2 ;
					if( 全_风格主配置.符号树宽度 < 树视图_宽度_最小 )
						全_风格主配置.符号树宽度 = 树视图_宽度_最小 ;

					更新所有窗口( 全_主窗口句柄 );
				}
			}

			if( 全_当前选项卡页面节点 && 全_当前选项卡页面节点->hwndQueryResultEdit )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( 全_Sql查询结果编辑正在调整大小吗 == TRUE )
				{
					SetCursor( LoadCursor(NULL,IDC_SIZENS) );
				}
				else if( 全_当前选项卡页面节点->rectQueryResultEdit.left <= x && x <= 全_当前选项卡页面节点->rectQueryResultEdit.right && 全_当前选项卡页面节点->Scintilla矩形.bottom < y && y < 全_当前选项卡页面节点->rectQueryResultEdit.top )
				{
					SetCursor( LoadCursor(NULL,IDC_SIZENS) );
					全_Sql查询结果编辑悬停正在调整大小吗 = TRUE ;
				}
				else
				{
					if( 全_Sql查询结果编辑悬停正在调整大小吗 == TRUE )
						全_Sql查询结果编辑悬停正在调整大小吗 = FALSE ;
				}

				if( 全_Sql查询结果编辑正在调整大小吗 == TRUE )
				{
					全_风格主配置.Sql查询结果编辑框高度 = 全_当前选项卡页面节点->rectQueryResultEdit.bottom - y - 分割_宽度/2 ;
					if( 全_风格主配置.Sql查询结果编辑框高度 < SQL查询结果_列表视图_高度_最小 )
						全_风格主配置.Sql查询结果编辑框高度 = SQL查询结果_列表视图_高度_最小 ;

					更新所有窗口( 全_主窗口句柄 );
				}
			}

			if( 全_当前选项卡页面节点 && 全_当前选项卡页面节点->hwndQueryResultTable )
			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) ;

				if( 全_正在调整Sql查询结果列表视图的大小吗 == TRUE )
				{
					SetCursor( LoadCursor(NULL,IDC_SIZENS) );
				}
				else if( 全_当前选项卡页面节点->rectQueryResultListView.left <= x && x <= 全_当前选项卡页面节点->rectQueryResultListView.right && 全_当前选项卡页面节点->rectQueryResultEdit.bottom < y && y < 全_当前选项卡页面节点->rectQueryResultListView.top )
				{
					SetCursor( LoadCursor(NULL,IDC_SIZENS) );
					全_正在调整Sql查询结果列表视图悬停的大小吗 = TRUE ;
				}
				else
				{
					if( 全_正在调整Sql查询结果列表视图悬停的大小吗 == TRUE )
						全_正在调整Sql查询结果列表视图悬停的大小吗 = FALSE ;
				}

				if( 全_正在调整Sql查询结果列表视图的大小吗 == TRUE )
				{
					int	nListViewHeight = 全_当前选项卡页面节点->rectQueryResultListView.bottom - y - 分割_宽度/2 ;
					if( nListViewHeight >= SQL查询结果_列表视图_高度_最小 )
					{
						全_风格主配置.Sql查询结果列表视图高度 = nListViewHeight ;
					}

					更新所有窗口( 全_主窗口句柄 );
				}
			}

			{
				int x = LOWORD(lParam) ;
				int y = HIWORD(lParam) - 全_工具栏高度 ;
				RECT r窗口矩形 ;
				int n选项卡页面索引 ;

				GetClientRect( 全_主窗口句柄 , & r窗口矩形 );

				int n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
				RECT rectTabPage , rectFileTreeBar ;
				for( n选项卡页面索引 = 0 ; n选项卡页面索引 < n选项卡页面数量 ; n选项卡页面索引++ )
				{
					int	x1 , x2 ;

					TabCtrl_GetItemRect( 全_选项卡页面句柄 , n选项卡页面索引 , & rectTabPage );
					x1 = rectTabPage.left ;
					x2 = rectTabPage.right;
					if( 全_显示文件树栏 == TRUE )
					{
						GetClientRect( 全_文件树栏句柄 , & rectFileTreeBar );
						x1 += rectFileTreeBar.right + 分割_宽度 ;
						x2 += rectFileTreeBar.right + 分割_宽度 ;
					}
					if( x1 < x && x < x2 && rectTabPage.top < y && y < rectTabPage.bottom && x < r窗口矩形.right - 50 )
					{
						全_h选项卡页关闭按钮节点 = 以索引获取选项卡页面(n选项卡页面索引) ;
						SetWindowPos( 全_h选项卡关闭按钮句柄 , HWND_TOP , x2-选项卡关闭按钮_宽度+13 , rectTabPage.top+4+全_工具栏高度 , 选项卡关闭按钮_宽度 , 选项卡关闭按钮_高度 , SWP_SHOWWINDOW );
						ShowWindow( 全_h选项卡关闭按钮句柄 , SW_SHOW );
						InvalidateRect( 全_h选项卡关闭按钮句柄 , NULL , TRUE );
						UpdateWindow( 全_h选项卡关闭按钮句柄 );
						break;
					}
				}
				if( n选项卡页面索引 >= n选项卡页面数量 )
				{
					全_h选项卡页关闭按钮节点 = NULL ;
					SetWindowPos( 全_h选项卡关闭按钮句柄 , HWND_BOTTOM , 0 , 0 , 0 , 0 , SWP_HIDEWINDOW );
					ShowWindow( 全_h选项卡关闭按钮句柄 , SW_HIDE );
					InvalidateRect( 全_h选项卡关闭按钮句柄 , NULL , TRUE );
					UpdateWindow( 全_h选项卡关闭按钮句柄 );
				}
			}

			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}

		break;
	case WM_NOTIFY:
		lpnmhdr = (NMHDR*)lParam ;
		lpnotify = (SCNotification*)lParam ;
		lpnmtv = (NMTREEVIEW*)lParam ;
		pTTT = (TOOLTIPTEXT *)lParam;

		switch (lpnmhdr->code)
		{
		case SCN_CHARADDED:
			OnCharAdded( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) , lpnotify );
			break;
		case SCN_MODIFIED:
			if( lpnotify->linesAdded )
			{
				AutosetLineNumberMarginWidth( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) );
			}
			break;
		case TCN_SELCHANGE:
			选项卡选择改变时();
			break;
		case SCN_SAVEPOINTREACHED:
			OnSavePointReached( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) );
			break;
		case SCN_SAVEPOINTLEFT:
			OnSavePointLeft( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) );
			break;
		case SCN_MARGINCLICK:
			OnMarginClick( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) , lpnotify );
			break;
		case SCN_UPDATEUI:
			if( ( lpnotify->updated & SC_UPDATE_SELECTION ) )
			{
				OnEditorTextSelected( GetTabPageByScintilla(lpnotify->nmhdr.hwndFrom) );
				更新状态栏选择信息();
			}
			break;
		case TVN_ITEMEXPANDING:
			if( lpnmhdr->hwndFrom == 全_文件树句柄 )
			{
				文件树节点展开时执行( lpnmtv );
			}
			break;
		case NM_DBLCLK:
			if( lpnmhdr->hwndFrom == 全_文件树句柄 )
			{
				nret = 文件树节点被双击时执行() ;
				if( nret == 1 )
					return DefWindowProc(hWnd, message, wParam, lParam);
			}
			else if( 全_当前选项卡页面节点 && lpnmhdr->hwndFrom == 全_当前选项卡页面节点->符号树句柄 )
			{
				if( 全_当前选项卡页面节点->pst文档类型配置 && 全_当前选项卡页面节点->pst文档类型配置->pfuncOnDbClickSymbolTree )
					全_当前选项卡页面节点->pst文档类型配置->pfuncOnDbClickSymbolTree( 全_当前选项卡页面节点 );
				return 0;
			}
			break;
		case TTN_NEEDTEXT:
			选项卡页节点 = 以索引获取选项卡页面( (int)(pTTT->hdr.idFrom) ) ;
			if( 选项卡页节点 )
			{
				memset( pTTT->szText , 0x00 , sizeof(pTTT->szText) );
				if( (int)(pTTT->hdr.idFrom) <= 8 )
					snprintf( pTTT->szText , sizeof(pTTT->szText)-1 , "%.68s - (Alt+%d)" , 选项卡页节点->acPathFilename , (int)(pTTT->hdr.idFrom)+1 );
				else
					snprintf( pTTT->szText , sizeof(pTTT->szText)-1 , "%.68s" , 选项卡页节点->acPathFilename );
			}
			break;
		}

		break;
		/*
		case WM_DRAWITEM:
		{
		LPDRAWITEMSTRUCT pDIS = (LPDRAWITEMSTRUCT)lParam;
		if( pDIS->hwndItem == 全_选项卡页面句柄 )
		{
		}
		}
		break;
		case WM_MEASUREITEM:
		{
		LPDRAWITEMSTRUCT pDIS = (LPDRAWITEMSTRUCT)lParam;
		if( pDIS->hwndItem == 全_选项卡页面句柄 )
		{
		}
		}
		break;
		*/
	case WM_COPYDATA:
	{
		COPYDATASTRUCT	*cpd ;
		cpd = (COPYDATASTRUCT*)lParam ;
		char acPathFilename[MAX_PATH] = "" ;
		strncpy( acPathFilename , (char*)(cpd->lpData) , MIN(sizeof(acPathFilename)-1,cpd->cbData) );

		if( acPathFilename[strlen(acPathFilename)-1] != '\\' )
			直接打开文件( acPathFilename );
		else
			定位目录( acPathFilename );
	}

	/*
	{
		HWND hForeWnd = NULL;
		DWORD dwForeID;
		DWORD dwCurID;

		hForeWnd = GetForegroundWindow();
		dwCurID = GetCurrentThreadId();
		dwForeID = GetWindowThreadProcessId( hForeWnd, NULL );
		AttachThreadInput( dwCurID, dwForeID, TRUE);
		SetWindowPos( 全_主窗口句柄, HWND_TOP, 0,0,0,0, SWP_NOSIZE|SWP_NOMOVE );
		SetForegroundWindow( 全_主窗口句柄 );
		AttachThreadInput( dwCurID, dwForeID, FALSE);
	}
	*/
	SwitchToThisWindow( 全_主窗口句柄 , TRUE );

	break;
	case WM_PAINT:
		hdc = BeginPaint( hWnd, & ps );
		/*
		RECT rect ;
		GetClientRect( hWnd , & rect );
		FillRect( hdc , & rect , 全_笔刷常用控件深色 );
		*/
		EndPaint( hWnd, & ps);
		break;
	case WM_CTLCOLORBTN:
		if( (HWND)lParam == 全_h选项卡关闭按钮句柄 && 全_h选项卡页关闭按钮节点 )
		{
			HWND btn = (HWND)lParam ;
			HDC hdc = (HDC)wParam ;
			RECT rect;
			GetClientRect( btn , & rect );
			SetTextColor( hdc , GetSysColor(COLOR_BTNFACE) );
			SetBkColor( hdc , GetSysColor(COLOR_HIGHLIGHT) );
			SetBkMode( hdc , TRANSPARENT);
			DrawText( hdc , "X" , 1 , & rect , DT_CENTER|DT_VCENTER|DT_SINGLELINE );
			return (LRESULT)全_选项卡关闭按钮画刷;
		}
	case WM_CTLCOLORLISTBOX:
	case WM_CTLCOLOREDIT:
	{
		HDC hdc = (HDC)wParam ;
		SetTextColor( hdc , g_pstWindowTheme->stStyleTheme.text.color );
		SetBkColor( hdc , g_pstWindowTheme->stStyleTheme.text.bgcolor );
		SetBkMode( hdc , TRANSPARENT);
		return (LRESULT)全_笔刷常用控件深色;
	}
	break;
	case WM_ACTIVATE:
		if( 全_当前选项卡页面节点 )
		{
			if( hWnd == 全_主窗口句柄 )
			{
				选项卡选择改变时();
			}
		}
		break;
	case WM_LBUTTONDOWN:
		if( 全_文件树栏悬停正在调整大小吗 == TRUE )
			全_文件树栏正在调整大小吗 = TRUE ;

		if( 全_函数列表悬停正在调整大小吗 == TRUE )
			全_函数列表正在调整大小吗 = TRUE ;

		if( 全_树状图悬停正在调整大小吗 == TRUE )
			全_树状图正在调整大小吗 = TRUE ;

		if( 全_Sql查询结果编辑悬停正在调整大小吗 == TRUE )
			全_Sql查询结果编辑正在调整大小吗 = TRUE ;

		if( 全_正在调整Sql查询结果列表视图悬停的大小吗 == TRUE )
			全_正在调整Sql查询结果列表视图的大小吗 = TRUE ;

		break;
	case WM_LBUTTONUP:
		if( 全_文件树栏正在调整大小吗 == TRUE )
		{
			全_文件树栏正在调整大小吗 = FALSE ;
			更新所有窗口( 全_主窗口句柄 );
		}

		if( 全_函数列表正在调整大小吗 == TRUE )
		{
			全_函数列表正在调整大小吗 = FALSE ;
			更新所有窗口( 全_主窗口句柄 );
		}

		if( 全_树状图正在调整大小吗 == TRUE )
		{
			全_树状图正在调整大小吗 = FALSE ;
			更新所有窗口( 全_主窗口句柄 );
		}

		if( 全_Sql查询结果编辑悬停正在调整大小吗 == TRUE )
		{
			全_Sql查询结果编辑正在调整大小吗 = FALSE ;
			更新所有窗口( 全_主窗口句柄 );
		}

		if( 全_正在调整Sql查询结果列表视图悬停的大小吗 == TRUE )
		{
			全_正在调整Sql查询结果列表视图的大小吗 = FALSE ;
			更新所有窗口( 全_主窗口句柄 );
		}

		break;
	case WM_DROPFILES:
		if( wParam )
		{
			执行打开拖放文件( (HDROP)wParam );
		}
		break;
	case WM_CLOSE:
		if( hWnd == 全_主窗口句柄 )
		{
			if( 全_风格主配置.打开退出时打开的文件 == TRUE )
			{
				启动时填充所有打开的文件();

				memset( 全_风格主配置.启动时的活动文件 , 0x00 , sizeof(全_风格主配置.启动时的活动文件) );
				if( 全_当前选项卡页面节点 )
					strncpy( 全_风格主配置.启动时的活动文件 , 全_当前选项卡页面节点->acPathFilename , sizeof(全_风格主配置.启动时的活动文件)-1 );
			}

			nret = 检查所有文件保存() ;
			if( nret )
				break;
		}
		return DefWindowProc(hWnd, message, wParam, lParam);
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}


