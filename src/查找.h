#ifndef _SEARCH_
#define _SEARCH_

#include "framework.h"

struct NavigateBackNextTrace
{
	struct 选项卡页面S		*选项卡页节点 ;
	HWND			Scintilla句柄 ;
	int			nLastPos ;

	struct list_head	nodeNavigateBackNextTrace ;
};

extern HWND 查找框句柄 ;
extern HWND hwndEditBoxInSearchFind ;

extern HWND 已查框句柄 ;
extern HWND 搜索中已找到的列表框句柄 ;

extern HWND 替换对话框句柄 ;
extern HWND hwndFromEditBoxInSearchReplace ;
extern HWND hwndToEditBoxInSearchReplace ;

void JumpGotoLine( struct 选项卡页面S *选项卡页节点 , int nGotoLineNo , int 文本当前行号 );

void SyncReplaceOptionsToFindDialog();
void SyncReplaceFromTextToFindDialog();

INT_PTR CALLBACK SearchFindWndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK SearchReplaceWndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK SearchFoundWndProc(HWND, UINT, WPARAM, LPARAM);

int OnSearchFind( struct 选项卡页面S *选项卡页节点 );
int OnSearchFindPrev( struct 选项卡页面S *选项卡页节点 );
int OnSearchFindNext( struct 选项卡页面S *选项卡页节点 );
int OnSearchFoundList( struct 选项卡页面S *选项卡页节点 );
int OnSearchReplace( struct 选项卡页面S *选项卡页节点 );
int OnSelectAll( struct 选项卡页面S *选项卡页节点 );
int OnSelectWord( struct 选项卡页面S *选项卡页节点 );
int OnSelectLine( struct 选项卡页面S *选项卡页节点 );
int OnAddSelectLeftCharGroup( struct 选项卡页面S *选项卡页节点 );
int OnAddSelectRightCharGroup( struct 选项卡页面S *选项卡页节点 );
int OnAddSelectLeftWord( struct 选项卡页面S *选项卡页节点 );
int OnAddSelectRightWord( struct 选项卡页面S *选项卡页节点 );
int OnAddSelectTopBlockFirstLine( struct 选项卡页面S *选项卡页节点 );
int OnAddSelectBottomBlockFirstLine( struct 选项卡页面S *选项卡页节点 );

int OnMoveLeftCharGroup( struct 选项卡页面S *选项卡页节点 );
int OnMoveRightCharGroup( struct 选项卡页面S *选项卡页节点 );
int OnMoveLeftWord( struct 选项卡页面S *选项卡页节点 );
int OnMoveRightWord( struct 选项卡页面S *选项卡页节点 );
int OnMoveTopBlockFirstLine( struct 选项卡页面S *选项卡页节点 );
int OnMoveBottomBlockFirstLine( struct 选项卡页面S *选项卡页节点 );
int OnSearchGotoLine( struct 选项卡页面S *选项卡页节点 );

int OnSearchToggleBookmark( struct 选项卡页面S *选项卡页节点 );
int OnSearchAddBookmark( struct 选项卡页面S *选项卡页节点 );
int OnSearchRemoveBookmark( struct 选项卡页面S *选项卡页节点 );
int OnSearchRemoveAllBookmarks( struct 选项卡页面S *选项卡页节点 );
int OnSearchGotoPrevBookmark( struct 选项卡页面S *选项卡页节点 );
int OnSearchGotoNextBookmark( struct 选项卡页面S *选项卡页节点 );
int OnSearchGotoPrevBookmarkInAllFiles( struct 选项卡页面S *选项卡页节点 );
int OnSearchGotoNextBookmarkInAllFiles( struct 选项卡页面S *选项卡页节点 );

void 初始化导航返回下一个跟踪列表();
int AddNavigateBackNextTrace( struct 选项卡页面S *选项卡页节点 , int 当前文本字节位置 );
int UpdateNavigateBackNextTrace( struct 选项卡页面S *选项卡页节点 , int 当前文本字节位置 );
int OnSearchNavigateBackPrev_InThisFile();
int OnSearchNavigateBackPrev_InAllFiles();
void CleanNavigateBackNextTraceListByThisFile( struct 选项卡页面S *选项卡页节点 );

#endif
