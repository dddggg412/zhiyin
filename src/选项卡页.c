#include "framework.h"

HWND		全_选项卡页面句柄 ;
RECT		全_选项卡页矩形 = { 0 } ;
struct 选项卡页面S	*全_当前选项卡页面节点 = NULL ;
HMENU		全_h选项卡页弹出菜单 ;
HMENU		全_h编辑框弹出菜单 ;
HMENU		全_h符号列表弹出菜单 ;
HMENU		全_h符号树弹出菜单 ;
int		全_n选项卡高度 = 选项卡_默认高度 ;

HWND		全_h选项卡关闭按钮句柄 ;
struct 选项卡页面S	*全_h选项卡页关闭按钮节点 = NULL ;
/*
HBRUSH		g_brushCurrentTabCloseButton ;
*/
HBRUSH		全_选项卡关闭按钮画刷 ;

int 创建选项卡页( HWND hWnd )
{
	RECT		rect主客户端 ;
	// long		l控制主题 ;

	GetClientRect( hWnd , & rect主客户端 );
	memcpy( & 全_选项卡页矩形 , & rect主客户端 , sizeof(RECT) );
	if( 全_显示文件树栏 == FALSE )
	{
		全_选项卡页矩形.left = rect主客户端.left ;
		全_选项卡页矩形.right = rect主客户端.right ;
	}
	else
	{
		全_选项卡页矩形.left = 符号树栏_宽度_默认 ;
		全_选项卡页矩形.right = rect主客户端.right ;
	}
	全_选项卡页矩形.top = rect主客户端.top ;
	全_选项卡页矩形.bottom = rect主客户端.bottom ;
	// 全_选项卡页面句柄 = CreateWindow( WC_TABCONTROL , NULL , WS_CHILD|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|TCS_MULTILINE|TCS_BUTTONS|TCS_FLATBUTTONS|TCS_TOOLTIPS , 全_选项卡页矩形.left , 全_选项卡页矩形.top , 全_选项卡页矩形.right-全_选项卡页矩形.left , 全_选项卡页矩形.bottom-全_选项卡页矩形.top , hWnd , NULL , 全_当前实例 , NULL ) ;
	全_选项卡页面句柄 = CreateWindow( WC_TABCONTROL , NULL , WS_CHILD|TCS_TOOLTIPS|TCS_FOCUSONBUTTONDOWN , 全_选项卡页矩形.left , 全_选项卡页矩形.top , 全_选项卡页矩形.right-全_选项卡页矩形.left , 全_选项卡页矩形.bottom-全_选项卡页矩形.top , hWnd , NULL , 全_当前实例 , NULL ) ;
	if( 全_选项卡页面句柄 == NULL )
	{
		MessageBox(NULL, TEXT("不能创建TabControl控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	/*
	l控制主题 = GetWindowLong( 全_选项卡页面句柄 , GWL_STYLE ) ;
	l控制主题 &= ~TCS_RAGGEDRIGHT ;
	SetWindowLong( 全_选项卡页面句柄 , GWL_STYLE , l控制主题 );

	l控制主题 = TabCtrl_GetExtendedStyle( 全_选项卡页面句柄 ) ;
	l控制主题 &= ~TCS_EX_FLATSEPARATORS ;
	TabCtrl_SetExtendedStyle( 全_选项卡页面句柄 , l控制主题 );
	*/

	SendMessage( 全_选项卡页面句柄 , WM_SETFONT , (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);
	TabCtrl_SetPadding( 全_选项卡页面句柄 , 17 , 4 );

	ShowWindow( 全_选项卡页面句柄 , SW_SHOW);
	UpdateWindow( 全_选项卡页面句柄 );

	全_h选项卡页弹出菜单 = LoadMenu( 全_当前实例 , MAKEINTRESOURCE(IDR_TABPAGE_POPUPMENU) ) ;
	if( 全_h选项卡页弹出菜单 == NULL )
	{
		MessageBox(NULL, TEXT("不能创建文件选项卡右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	全_h选项卡页弹出菜单 = GetSubMenu( 全_h选项卡页弹出菜单 , 0 ) ;

	全_h编辑框弹出菜单 = LoadMenu( 全_当前实例 , MAKEINTRESOURCE(IDR_EDITOR_POPUPMENU) ) ;
	if( 全_h编辑框弹出菜单 == NULL )
	{
		MessageBox(NULL, TEXT("不能创建编辑区右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	全_h编辑框弹出菜单 = GetSubMenu( 全_h编辑框弹出菜单 , 0 ) ;

	全_h符号列表弹出菜单 = LoadMenu( 全_当前实例 , MAKEINTRESOURCE(IDR_SYMBOLLIST_POPUPMENU) ) ;
	if( 全_h符号列表弹出菜单 == NULL )
	{
		MessageBox(NULL, TEXT("不能创建函数列表右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	全_h符号列表弹出菜单 = GetSubMenu( 全_h符号列表弹出菜单 , 0 ) ;

	全_h符号树弹出菜单 = LoadMenu( 全_当前实例 , MAKEINTRESOURCE(IDR_SYMBOLTREE_POPUPMENU) ) ;
	if( 全_h符号树弹出菜单 == NULL )
	{
		MessageBox(NULL, TEXT("不能创建函数列表右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	全_h符号树弹出菜单 = GetSubMenu( 全_h符号树弹出菜单 , 0 ) ;

	HFONT hTabCloseButtonFont = CreateFont(-20, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, "Arial");

	全_h选项卡关闭按钮句柄 = CreateWindowEx( 0 , "BUTTON" , "X" , WS_CHILD|BS_FLAT|BS_OWNERDRAW , 0 , 0 , 0 , 0 , hWnd , NULL , 全_当前实例 , NULL ) ;
	// 全_h选项卡关闭按钮句柄 = CreateWindowEx( 0 , "BUTTON" , "X" , WS_CHILD|BS_OWNERDRAW , 0 , 0 , 0 , 0 , hWnd , NULL , 全_当前实例 , NULL ) ;
	if( 全_h选项卡关闭按钮句柄 == NULL )
	{
		MessageBox(NULL, TEXT("不能创建TabCloseButton控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	SendMessage( 全_h选项卡关闭按钮句柄 , WM_SETFONT , (WPARAM)hTabCloseButtonFont, 0);

	/*
	g_brushCurrentTabCloseButton = CreateSolidBrush( GetSysColor(COLOR_HIGHLIGHT) ) ;
	*/
	// 全_选项卡关闭按钮画刷 = CreateSolidBrush( GetSysColor(COLOR_BTNFACE) ) ;
	全_选项卡关闭按钮画刷 = CreateSolidBrush( GetSysColor(COLOR_HIGHLIGHT) ) ;

	ShowWindow( 全_h选项卡关闭按钮句柄 , SW_HIDE);
	UpdateWindow( 全_h选项卡关闭按钮句柄 );

	return 0;
}

void 调整选项卡页面()
{
	RECT		rect主客户端 ;

	GetClientRect( 全_主窗口句柄 , & rect主客户端 );

	if( 全_显示文件树栏 == FALSE )
	{
		全_文件树栏矩形.left = 0 ;
		全_文件树栏矩形.right = 0 ;
		全_文件树栏矩形.top = rect主客户端.top + 全_工具栏高度 ;
		全_文件树栏矩形.bottom = rect主客户端.bottom ;

		全_选项卡页矩形.left = rect主客户端.left ;
		全_选项卡页矩形.right = rect主客户端.right ;
		全_选项卡页矩形.top = rect主客户端.top + 全_工具栏高度 ;
		全_选项卡页矩形.bottom = rect主客户端.bottom - 全_状态栏高度 ;
	}
	else
	{
		全_文件树栏矩形.left = rect主客户端.left ;
		全_文件树栏矩形.right = rect主客户端.left + 全_风格主配置.文件树栏宽度 ;
		全_文件树栏矩形.top = rect主客户端.top + 全_工具栏高度 ;
		全_文件树栏矩形.bottom = rect主客户端.bottom - 全_状态栏高度 ;

		全_选项卡页矩形.left = 全_文件树栏矩形.right + 分割_宽度 ;
		全_选项卡页矩形.right = rect主客户端.right ;
		全_选项卡页矩形.top = rect主客户端.top + 全_工具栏高度 ;
		全_选项卡页矩形.bottom = rect主客户端.bottom - 全_状态栏高度 ;
	}

	return;
}

void 调整选项卡页面框( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点->符号列表句柄 )
	{
		选项卡页节点->Scintilla矩形.left = 全_选项卡页矩形.left + SCINTILLA_边框_左 ;
		选项卡页节点->Scintilla矩形.right = 全_选项卡页矩形.right - 符号列表_边框_左 - 全_风格主配置.符号列表的宽度 - 符号列表_边框_右 - SCINTILLA_边框_右 ;
		选项卡页节点->Scintilla矩形.top = 全_选项卡页矩形.top + 全_n选项卡高度 + SCINTILLA_边框_上 ;
		选项卡页节点->Scintilla矩形.bottom = 全_选项卡页矩形.bottom - SCINTILLA_边框_下 ;

		选项卡页节点->符号列表矩形.left = 选项卡页节点->Scintilla矩形.right + 分割_宽度 + 符号列表_边框_左 ;
		选项卡页节点->符号列表矩形.right = 全_选项卡页矩形.right - 符号列表_边框_右 ;
		选项卡页节点->符号列表矩形.top = 全_选项卡页矩形.top + 全_n选项卡高度 + 符号列表_边框_上 ;
		选项卡页节点->符号列表矩形.bottom = 全_选项卡页矩形.bottom - 符号列表_边框_下 ;

		选项卡页节点->符号树矩形.left = 全_选项卡页矩形.right ;
		选项卡页节点->符号树矩形.right = 全_选项卡页矩形.right ;
		选项卡页节点->符号树矩形.top = 全_选项卡页矩形.top + 全_n选项卡高度 ;
		选项卡页节点->符号树矩形.bottom = 全_选项卡页矩形.bottom ;
	}
	else if( 选项卡页节点->符号树句柄 )
	{
		选项卡页节点->Scintilla矩形.left = 全_选项卡页矩形.left + SCINTILLA_边框_左 ;
		选项卡页节点->Scintilla矩形.right = 全_选项卡页矩形.right - 符号树_边框_左 - 全_风格主配置.符号树宽度 - 符号树_边框_右 - SCINTILLA_边框_右 ;
		选项卡页节点->Scintilla矩形.top = 全_选项卡页矩形.top + 全_n选项卡高度 + SCINTILLA_边框_上 ;
		选项卡页节点->Scintilla矩形.bottom = 全_选项卡页矩形.bottom - SCINTILLA_边框_下 ;

		选项卡页节点->符号列表矩形.left = 全_选项卡页矩形.right ;
		选项卡页节点->符号列表矩形.right = 全_选项卡页矩形.right ;
		选项卡页节点->符号列表矩形.top = 全_选项卡页矩形.top + 全_n选项卡高度 ;
		选项卡页节点->符号列表矩形.bottom = 全_选项卡页矩形.bottom ;

		选项卡页节点->符号树矩形.left = 选项卡页节点->Scintilla矩形.right + 分割_宽度 + 符号树_边框_左 ;
		选项卡页节点->符号树矩形.right = 全_选项卡页矩形.right - 符号树_边框_右 ;
		选项卡页节点->符号树矩形.top = 全_选项卡页矩形.top + 全_n选项卡高度 + 符号树_边框_上 ;
		选项卡页节点->符号树矩形.bottom = 全_选项卡页矩形.bottom - 符号树_边框_下 ;
	}
	else
	{
		选项卡页节点->Scintilla矩形.left = 全_选项卡页矩形.left + SCINTILLA_边框_左 ;
		选项卡页节点->Scintilla矩形.right = 全_选项卡页矩形.right - SCINTILLA_边框_右  ;
		选项卡页节点->Scintilla矩形.top = 全_选项卡页矩形.top + 全_n选项卡高度 + SCINTILLA_边框_上 ;
		选项卡页节点->Scintilla矩形.bottom = 全_选项卡页矩形.bottom - SCINTILLA_边框_下 ;

		选项卡页节点->符号列表矩形.left = 全_选项卡页矩形.right ;
		选项卡页节点->符号列表矩形.right = 全_选项卡页矩形.right ;
		选项卡页节点->符号列表矩形.top = 全_选项卡页矩形.top + 全_n选项卡高度 ;
		选项卡页节点->符号列表矩形.bottom = 全_选项卡页矩形.bottom ;

		选项卡页节点->符号树矩形.left = 全_选项卡页矩形.right ;
		选项卡页节点->符号树矩形.right = 全_选项卡页矩形.right ;
		选项卡页节点->符号树矩形.top = 全_选项卡页矩形.top + 全_n选项卡高度 ;
		选项卡页节点->符号树矩形.bottom = 全_选项卡页矩形.bottom ;
	}

	if( 选项卡页节点->hwndQueryResultEdit || 选项卡页节点->hwndQueryResultTable )
	{
		选项卡页节点->Scintilla矩形.bottom -= 分割_宽度 + 全_风格主配置.Sql查询结果编辑框高度 + 分割_宽度 + 全_风格主配置.Sql查询结果列表视图高度 ;
		选项卡页节点->符号列表矩形.bottom -= 分割_宽度 + 全_风格主配置.Sql查询结果编辑框高度 + 分割_宽度 + 全_风格主配置.Sql查询结果列表视图高度 ;
		选项卡页节点->符号树矩形.bottom -= 分割_宽度 + 全_风格主配置.Sql查询结果编辑框高度 + 分割_宽度 + 全_风格主配置.Sql查询结果列表视图高度 ;

		选项卡页节点->rectQueryResultEdit.left = 全_选项卡页矩形.left + SCINTILLA_边框_左 ;
		选项卡页节点->rectQueryResultEdit.right = 全_选项卡页矩形.right - SCINTILLA_边框_右 ;
		选项卡页节点->rectQueryResultEdit.top = 选项卡页节点->Scintilla矩形.bottom + 分割_宽度 ;
		选项卡页节点->rectQueryResultEdit.bottom = 选项卡页节点->rectQueryResultEdit.top + 全_风格主配置.Sql查询结果编辑框高度 ;

		选项卡页节点->rectQueryResultListView.left = 全_选项卡页矩形.left + SCINTILLA_边框_左 ;
		选项卡页节点->rectQueryResultListView.right = 全_选项卡页矩形.right - SCINTILLA_边框_右 ;
		选项卡页节点->rectQueryResultListView.top = 选项卡页节点->rectQueryResultEdit.bottom + 分割_宽度 ;
		选项卡页节点->rectQueryResultListView.bottom = 选项卡页节点->rectQueryResultListView.top + 全_风格主配置.Sql查询结果列表视图高度 ;
	}
}

struct 选项卡页面S *AddTabPage( struct RemoteFileServer *pstRemoteFileServer , char *pcPathFilename , char *pcFilename , char *pcExtname )
{
	int		n选项卡页面数量 , n选项卡页面索引 ;
	struct 选项卡页面S	*选项卡页节点 = NULL ;
	TCITEM		tci ;
	int		nret = 0 ;

	if( pcPathFilename[0] )
	{
		n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
		for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
		{
			memset( & tci , 0x00 , sizeof(TCITEM) );
			tci.mask = TCIF_PARAM ;
			TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
			选项卡页节点 = (struct 选项卡页面S *)(tci.lParam);
			if( strcmp( 选项卡页节点->acPathFilename , pcPathFilename ) == 0 )
			{
				SelectTabPage( 选项卡页节点 , n选项卡页面索引 );
				return NULL;
			}
		}
	}

	选项卡页节点 = (struct 选项卡页面S *)malloc( sizeof(struct 选项卡页面S) ) ;
	if (选项卡页节点 == NULL)
	{
		MessageBox(NULL, TEXT("申请内存用于TABPAGE失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return NULL;
	}
	memset( 选项卡页节点 , 0x00 , sizeof(struct 选项卡页面S) );

	if( pstRemoteFileServer )
		memcpy( & (选项卡页节点->stRemoteFileServer) , pstRemoteFileServer , sizeof(struct RemoteFileServer) );
	strncpy( 选项卡页节点->acPathFilename , pcPathFilename , sizeof(选项卡页节点->acPathFilename)-1 );
	strncpy( 选项卡页节点->acFilename , pcFilename , sizeof(选项卡页节点->acFilename) );
	选项卡页节点->nFilenameLen = strlen(选项卡页节点->acFilename) ;
	strncpy( 选项卡页节点->p扩展名 , pcExtname , sizeof(选项卡页节点->p扩展名)-1 );
	strncpy( 选项卡页节点->acPathName , pcPathFilename , sizeof(选项卡页节点->acPathName)-1 ); 选项卡页节点->acPathName[strlen(选项卡页节点->acPathName)-选项卡页节点->nFilenameLen] = '\0' ;

	选项卡页节点->pst文档类型配置 = 获取文档类型配置( pcExtname ) ;

	n选项卡页面数量 = TabCtrl_GetItemCount(全_选项卡页面句柄) ;

	memset( & tci , 0x00 , sizeof(TCITEM) );
	tci.mask = TCIF_TEXT|TCIF_PARAM ;
	tci.pszText = 选项卡页节点->acFilename ;
	tci.lParam = (LPARAM)选项卡页节点 ;
	nret = TabCtrl_InsertItem( 全_选项卡页面句柄 , n选项卡页面数量 , & tci ) ;
	if( nret == -1 )
	{
		MessageBox(NULL, TEXT("TabCtrl_InsertItem失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return NULL;
	}

	nret = CreateScintillaControl( 选项卡页节点 ) ;
	if (nret)
	{
		MessageBox(NULL, TEXT("CreateScintillaControl失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return NULL;
	}

	// 以索引选择选项卡页面( n选项卡页面数量 );

	return 选项卡页节点;
}

int RemoveTabPage( struct 选项卡页面S *选项卡页节点 )
{
	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	struct 选项卡页面S	*p = NULL ;
	TCITEM		tci ;
	int		nret = 0 ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		if( p == 选项卡页节点 )
		{
			if( n选项卡页面数量 == 1 )
			{
				全_当前选项卡页面节点 = NULL ;
				SetWindowTitle( NULL );
			}
			else if( n选项卡页面索引 == 0 )
				以索引选择选项卡页面( n选项卡页面索引+1 );
			else if( n选项卡页面索引 == n选项卡页面数量 - 1 )
				以索引选择选项卡页面( n选项卡页面索引-1 );
			else if( n选项卡页面索引 < n选项卡页面数量 )
				以索引选择选项卡页面( n选项卡页面索引+1 );

			DestroyScintillaControl( 选项卡页节点 );

			DestroyWindow( 选项卡页节点->符号列表句柄 );

			TabCtrl_DeleteItem( 全_选项卡页面句柄 , n选项卡页面索引 );

			break;
		}
	}

	return 0;
}

void SetTabPageTitle( int nTabPageNo , char *title )
{
	TCITEM		tci ;

	memset( & tci , 0x00 , sizeof(TCITEM) );
	tci.mask = TCIF_TEXT ;
	tci.pszText = title ;
	TabCtrl_SetItem( 全_选项卡页面句柄 , nTabPageNo , & tci );
	InvalidateRect( 全_选项卡页面句柄 , NULL , TRUE );

	return;
}

void SelectTabPage( struct 选项卡页面S *选项卡页节点 , int n选项卡页面索引 )
{
	int		n选项卡页面数量 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	int		nret = 0 ;

	if( n选项卡页面索引 < 0 )
	{
		n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
		for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
		{
			memset( & tci , 0x00 , sizeof(TCITEM) );
			tci.mask = TCIF_PARAM ;
			TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
			p = (struct 选项卡页面S *)(tci.lParam);
			if( p == 选项卡页节点 )
				break;
		}
		if( n选项卡页面索引 >= n选项卡页面数量 )
			return;
	}

	TabCtrl_SetCurSel( 全_选项卡页面句柄 , n选项卡页面索引 );
	全_当前选项卡页面节点 = 选项卡页节点 ;

	SetWindowTitle( 全_当前选项卡页面节点->acPathFilename );

	SetCurrentTabPageHighLight( n选项卡页面索引 );

	if( 选项卡页节点->stRemoteFileServer.acNetworkAddress[0] == '\0' && 选项卡页节点->acPathFilename[0] && 全_风格主配置.检查被选中选项卡页面的更新 == TRUE && 选项卡页节点->bNotPromptWhereAutoUpdate == FALSE )
	{
		struct stat	statbuf ;

		nret = stat( 选项卡页节点->acPathFilename , & statbuf ) ;
		if( nret == -1 )
		{
			char	msg[100 + MAX_PATH];
			int	decision;
			_snprintf(msg, sizeof(msg)-1, "侦测到文件[%s]被改名或删除，是否保留 ?" , 选项卡页节点->acPathFilename);
			p = 全_当前选项卡页面节点 ; 全_当前选项卡页面节点 = NULL ;
			decision = MessageBox(NULL, msg, 全_应用程序名称, MB_YESNO);
			全_当前选项卡页面节点 = p ;
			if (decision == IDNO)
			{
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSAVEPOINT, 0, 0) ;
				关闭文件时( 选项卡页节点 );
				选项卡页节点 = 全_当前选项卡页面节点 ;
			}
			else if (decision == IDYES)
			{
				选项卡页节点->acPathFilename[0] = '\0' ;

				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_INSERTTEXT, 0, (sptr_t)"X" );
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_DELETERANGE, 0, 1 );
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_ENDUNDOACTION, 0, 0 );
			}
		}
		else if( statbuf.st_mtime > 选项卡页节点->st_mtime )
		{
			if( IsDocumentModified(选项卡页节点) == TRUE )
			{
				if( 全_风格主配置.提示自动更新 == TRUE )
				{
					p = 全_当前选项卡页面节点 ; 全_当前选项卡页面节点 = NULL ;
					int decision = MessageBox(NULL, "外部文件已发生变化，是否丢弃本次修改重新打开(Yes)，或始终无视外部变化(No)", "询问", MB_ICONQUESTION|MB_YESNO|MB_TASKMODAL);
					全_当前选项卡页面节点 = p ;
					if( decision == IDYES )
					{
						goto _GOTO_RELOAD_FILE_FORCELY;
					}
					else if( decision == IDNO )
					{
						选项卡页节点->bNotPromptWhereAutoUpdate = TRUE ;
					}
				}
				else
				{
					goto _GOTO_RELOAD_FILE_FORCELY;
				}
			}
			else if( IsDocumentModified(选项卡页节点) == FALSE )
			{
				if( 全_风格主配置.提示自动更新 == TRUE )
				{
					p = 全_当前选项卡页面节点 ; 全_当前选项卡页面节点 = NULL ;
					MessageBox(NULL, "外部文件已发生变化，重新加载文件", "信息", MB_OK|MB_TASKMODAL);
					全_当前选项卡页面节点 = p ;
				}

_GOTO_RELOAD_FILE_FORCELY :
                ;
				int		当前文本字节位置 ;
				int		nCurrentLine ;
				int		nMaxLine ;

				当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
				nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;

				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_CLEARALL, 0, 0 );
				nret = LoadFileDirectly( 选项卡页节点->acPathFilename , NULL , 选项卡页节点 ) ;
				if( nret )
					return;

				nMaxLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETLINECOUNT, 0, 0 ) ;
				if( nCurrentLine > nMaxLine-1 )
					nCurrentLine = nMaxLine-1 ;

				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETUNDOCOLLECTION, 1, 0);
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSAVEPOINT, 0, 0);

				JumpGotoLine( 选项卡页节点 , nCurrentLine , 0 );

				选项卡页节点->st_mtime = statbuf.st_mtime ;
			}
		}
	}

	// 重新计算各窗口大小
	更新所有窗口( 全_主窗口句柄 );

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	return;
}

struct 选项卡页面S *GetTabPageByScintilla( void *Scintilla句柄 )
{
	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	struct 选项卡页面S	*p = NULL ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		p = 以索引获取选项卡页面( n选项卡页面索引 ) ;
		if( p->Scintilla句柄 == Scintilla句柄 )
			return p;
	}

	return NULL;
}

struct 选项卡页面S *以索引获取选项卡页面( int n选项卡页面索引 )
{
	TCITEM		tci ;

	int n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	if( n选项卡页面索引 < 0 || n选项卡页面索引 >= n选项卡页面数量 )
		return NULL;

	memset( & tci , 0x00 , sizeof(TCITEM) );
	tci.mask = TCIF_PARAM ;
	TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
	return (struct 选项卡页面S *)(tci.lParam);
}

struct 选项卡页面S *以索引选择选项卡页面( int n选项卡页面索引 )
{
	struct 选项卡页面S	*p = NULL ;

	p = 以索引获取选项卡页面( n选项卡页面索引 ) ;
	if( p )
	{
		SelectTabPage( p , n选项卡页面索引 );
		return p;
	}

	return NULL;
}

void SetCurrentTabPageHighLight( int nCurrentTabPageIndex )
{
	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		if( n选项卡页面索引 == nCurrentTabPageIndex )
		{
			TabCtrl_HighlightItem(全_选项卡页面句柄,n选项卡页面索引,1.00);
		}
		else
		{
			TabCtrl_HighlightItem(全_选项卡页面句柄,n选项卡页面索引,0.00);
		}
	}

	return;
}

int 选项卡选择改变时()
{
	int nPageNo = TabCtrl_GetCurSel(全_选项卡页面句柄) ;
	以索引选择选项卡页面( nPageNo );

	return 0;
}

int OnSymbolListDbClick( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 && 选项卡页节点->pst文档类型配置 && 选项卡页节点->pst文档类型配置->pfuncOnDbClickSymbolList )
	{
		return 选项卡页节点->pst文档类型配置->pfuncOnDbClickSymbolList( 选项卡页节点 );
	}
	else
	{
		return -1;
	}
}

int 计算选项卡页面高度()
{
	int	top = -1 , bottom = -1 ;

	int n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	RECT rectTabPage ;
	for( int n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		TabCtrl_GetItemRect( 全_选项卡页面句柄 , n选项卡页面索引 , & rectTabPage );
		调试_日志("index[%d] top[%d] bottom[%d]",n选项卡页面索引,rectTabPage.top,rectTabPage.bottom)
		if( top == -1 || rectTabPage.top < top )
		{
			top = rectTabPage.top ;
		}
		if( bottom == -1 || rectTabPage.bottom > bottom )
		{
			bottom = rectTabPage.bottom ;
		}
	}

	if( top == -1 || bottom == -1 )
		全_n选项卡高度 = 选项卡_默认高度 ;
	else
		全_n选项卡高度 = bottom - top + 1 ;

	return 0;
}
