#include "framework.h"

uintptr_t		重新加载符号列表或树线程处理程序  ;

static int InitScintillaControlFold( struct 选项卡页面S *选项卡页节点 )
{
	// 启用折叠
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETPROPERTY ,(sptr_t)"fold" ,(sptr_t)"1" );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINTYPEN, 边框_折叠_索引 , SC_MARGIN_SYMBOL );//页边类型
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINMASKN, 边框_折叠_索引 , SC_MASK_FOLDERS ); //页边掩码
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINWIDTHN, 边框_折叠_索引 , (全_风格主配置.启用块折叠==TRUE?16:0) ); //页边宽度
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINSENSITIVEN, 边框_折叠_索引 , TRUE ); //响应鼠标消息

	// 折叠标签样式
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDER , SC_MARK_PIXMAP );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDEROPEN , SC_MARK_PIXMAP );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDEREND ,  SC_MARK_PIXMAP );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDEROPENMID , SC_MARK_PIXMAP );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDERMIDTAIL , SC_MARK_TCORNERCURVE );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDERSUB , SC_MARK_VLINE );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDEFINE , SC_MARKNUM_FOLDERTAIL , SC_MARK_LCORNERCURVE );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDEFINEPIXMAP, SC_MARKNUM_FOLDER , (sptr_t)plus_xpm );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDEFINEPIXMAP, SC_MARKNUM_FOLDEROPEN , (sptr_t)minus_xpm );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDEFINEPIXMAP, SC_MARKNUM_FOLDEREND , (sptr_t)plus_xpm );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDEFINEPIXMAP, SC_MARKNUM_FOLDEROPENMID , (sptr_t)minus_xpm );

	// 折叠标签颜色
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERSETBACK , SC_MARKNUM_FOLDERSUB , 0xa0a0a0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERSETBACK , SC_MARKNUM_FOLDERMIDTAIL , 0xa0a0a0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERSETBACK , SC_MARKNUM_FOLDERTAIL , 0xa0a0a0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETFOLDFLAGS, 16|4, 0); //如果折叠就在折叠行的上下各画一条横线

	return 0;
}

funcInitTabPageControlsBeforeLoadFile InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl ;
int InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl( struct 选项卡页面S *选项卡页节点 )
{
	int	nret = 0 ;

	HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font);

	nret = IfSymbolReqularExp_CreateSymbolListCtl( 选项卡页节点 , hFont ) ;
	if( nret )
	{
		DeleteObject( hFont );
		return nret;
	}

	return 0;
}

funcInitTabPageControlsBeforeLoadFile InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl ;
int InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl( struct 选项卡页面S *选项卡页节点 )
{
	int	nret = 0 ;

	HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font);

	nret = CreateSymbolTreeCtl( 选项卡页节点 , hFont ) ;
	if( nret )
	{
		DeleteObject( hFont );
		return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_CPP ;
int InitTabPageControlsAfterLoadFile_CPP( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( 选项卡页节点->pst文档类型配置->keywords2 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords2) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_WORD2 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords2.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD2 , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.preprocessor.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_CS ;
int InitTabPageControlsAfterLoadFile_CS( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.preprocessor.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_JAVA ;
int InitTabPageControlsAfterLoadFile_JAVA( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.preprocessor.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_GO ;
int InitTabPageControlsAfterLoadFile_GO( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.preprocessor.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_SWIFT ;
int InitTabPageControlsAfterLoadFile_SWIFT( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.preprocessor.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsBeforeLoadFile InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl_And_ResultEditCtl_And_ResultListCtl ;
int InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl_And_ResultEditCtl_And_ResultListCtl( struct 选项卡页面S *选项卡页节点 )
{
	int	nret = 0 ;

	HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font);

	nret = CreateSymbolTreeCtl( 选项卡页节点 , hFont ) ;
	if( nret )
	{
		DeleteObject( hFont );
		return nret;
	}

	nret = CreateQueryResultEditCtl( 选项卡页节点 , hFont ) ;
	if( nret )
	{
		DeleteObject( hFont );
		return nret;
	}

	nret = CreateQueryResultTableCtl( 选项卡页节点 , hFont ) ;
	if( nret )
	{
		DeleteObject( hFont );
		return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_SQL ;
int InitTabPageControlsAfterLoadFile_SQL( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_SQL , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SQL_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SQL_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SQL_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SQL_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SQL_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SQL_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SQL_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SQL_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SQL_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SQL_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SQL_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SQL_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SQL_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SQL_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SQL_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SQL_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SQL_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SQL_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SQL_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SQL_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SQL_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SQL_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SQL_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SQL_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SQL_COMMENTDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SQL_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SQL_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SQL_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	// 解析数据库连接配置
	if( 选项卡页节点->pst文档类型配置->pfuncParseFileConfigHeader )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncParseFileConfigHeader( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	/* 刷新右边符号树 */
	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolTree )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolTree( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_REDIS ;
int InitTabPageControlsAfterLoadFile_REDIS( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	// 解析REDIS连接配置
	if( 选项卡页节点->pst文档类型配置->pfuncParseFileConfigHeader )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncParseFileConfigHeader( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	// 连接REDIS服务端
	nret = ConnectToRedis( 选项卡页节点 ) ;
	if( nret < 0 )
		return nret;

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_PYTHON ;
int InitTabPageControlsAfterLoadFile_PYTHON( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_PYTHON , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_P_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_P_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_P_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_P_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( 选项卡页节点->pst文档类型配置->keywords2 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords2) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_P_WORD2 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords2.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_P_WORD2 , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_P_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_P_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_P_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_P_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_P_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_P_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_P_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_P_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_P_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_P_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_P_TRIPLE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_P_TRIPLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_P_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_P_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_P_TRIPLEDOUBLE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_P_TRIPLEDOUBLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_P_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_P_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_P_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_P_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_P_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_P_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_P_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_P_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_P_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_P_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_P_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_P_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_P_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_P_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_P_COMMENTBLOCK , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_P_COMMENTBLOCK , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_P_COMMENTBLOCK , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_P_COMMENTBLOCK , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_LUA ;
int InitTabPageControlsAfterLoadFile_LUA( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_LUA , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LUA_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LUA_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LUA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LUA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( 选项卡页节点->pst文档类型配置->keywords2 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords2) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LUA_WORD2 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords2.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LUA_WORD2 , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LUA_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LUA_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LUA_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LUA_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LUA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LUA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LUA_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LUA_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LUA_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LUA_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LUA_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LUA_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LUA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LUA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LUA_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LUA_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LUA_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LUA_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LUA_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LUA_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LUA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LUA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LUA_COMMENTDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LUA_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LUA_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LUA_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_PERL ;
int InitTabPageControlsAfterLoadFile_PERL( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_PERL , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PL_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PL_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PL_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PL_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PL_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PL_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PL_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PL_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PL_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PL_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PL_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PL_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PL_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PL_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PL_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PL_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PL_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PL_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PL_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PL_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PL_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PL_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PL_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PL_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_SH ;
int InitTabPageControlsAfterLoadFile_SH( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_BASH , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SH_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SH_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SH_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SH_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SH_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SH_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SH_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SH_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SH_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SH_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SH_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SH_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SH_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SH_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SH_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SH_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SH_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SH_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SH_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SH_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_SH_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_SH_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_SH_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_SH_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_RUST ;
int InitTabPageControlsAfterLoadFile_RUST( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_RUST , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RUST_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RUST_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RUST_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RUST_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( 选项卡页节点->pst文档类型配置->keywords2 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords2) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RUST_WORD2 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords2.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RUST_WORD2 , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RUST_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RUST_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RUST_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RUST_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RUST_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RUST_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RUST_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RUST_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RUST_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RUST_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RUST_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RUST_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RUST_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RUST_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RUST_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RUST_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RUST_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RUST_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RUST_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RUST_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RUST_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RUST_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RUST_COMMENTLINEDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RUST_COMMENTLINEDOC , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RUST_COMMENTLINEDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RUST_COMMENTLINEDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RUST_COMMENTBLOCK , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RUST_COMMENTBLOCK , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RUST_COMMENTBLOCK , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RUST_COMMENTBLOCK , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RUST_COMMENTBLOCKDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RUST_COMMENTBLOCKDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RUST_COMMENTBLOCKDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RUST_COMMENTBLOCKDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_RUBY ;
int InitTabPageControlsAfterLoadFile_RUBY( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_RUBY , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RB_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RB_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RB_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RB_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( 选项卡页节点->pst文档类型配置->keywords2 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords2) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RB_WORD_DEMOTED , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords2.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RB_WORD_DEMOTED , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RB_WORD_DEMOTED , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RB_WORD_DEMOTED , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RB_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RB_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RB_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RB_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RB_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RB_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RB_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RB_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RB_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RB_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RB_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RB_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RB_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RB_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RB_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RB_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_RB_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_RB_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_RB_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_RB_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_LISP ;
int InitTabPageControlsAfterLoadFile_LISP( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_LISP , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LISP_KEYWORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LISP_KEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LISP_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LISP_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LISP_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LISP_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LISP_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LISP_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LISP_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LISP_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LISP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LISP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LISP_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LISP_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LISP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LISP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LISP_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LISP_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LISP_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LISP_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_LISP_MULTI_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_LISP_MULTI_COMMENT , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_LISP_MULTI_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_LISP_MULTI_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_ASM ;
int InitTabPageControlsAfterLoadFile_ASM( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_ASM , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_ASM_CPUINSTRUCTION , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_ASM_CPUINSTRUCTION , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_ASM_CPUINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_ASM_CPUINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_ASM_MATHINSTRUCTION , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_ASM_MATHINSTRUCTION , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_ASM_MATHINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_ASM_MATHINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_ASM_REGISTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_ASM_REGISTER , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_ASM_REGISTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_ASM_REGISTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_ASM_DIRECTIVE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_ASM_DIRECTIVE , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_ASM_DIRECTIVE , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_ASM_DIRECTIVE , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_ASM_DIRECTIVEOPERAND , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_ASM_DIRECTIVEOPERAND , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_ASM_DIRECTIVEOPERAND , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_ASM_DIRECTIVEOPERAND , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_ASM_EXTINSTRUCTION , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_ASM_EXTINSTRUCTION , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_ASM_EXTINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_ASM_EXTINSTRUCTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_ASM_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_ASM_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_ASM_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_ASM_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_ASM_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_ASM_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_ASM_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_ASM_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_ASM_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_ASM_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_ASM_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_ASM_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_ASM_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_ASM_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_ASM_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_ASM_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_ASM_COMMENTDIRECTIVE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_ASM_COMMENTDIRECTIVE , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_ASM_COMMENTDIRECTIVE , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_ASM_COMMENTDIRECTIVE , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_COBOL ;
int InitTabPageControlsAfterLoadFile_COBOL( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_COBOL , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 5 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 5 , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 5 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 5 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 8 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 8 , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 8 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 8 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 16 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 16 , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 16 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 16 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 6 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 6 , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 7 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 7 , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 7 , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 7 , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 4 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 4 , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 10 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 10 , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 10 , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 10 , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 9 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.preprocessor.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 9 , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 9 , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 9 , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 2 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 2 , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 3 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 3 , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_HTML ;
int InitTabPageControlsAfterLoadFile_HTML( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_HTML , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_TAG , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_TAG , g_pstWindowTheme->stStyleTheme.tags.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_TAG , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_TAG , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_TAGUNKNOWN , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.unknowtags.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_TAGUNKNOWN , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_TAGUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_TAGUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_ATTRIBUTE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.attributes.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_ATTRIBUTE , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_ATTRIBUTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_ATTRIBUTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.unknowattributes.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_ATTRIBUTEUNKNOWN , g_pstWindowTheme->stStyleTheme.unknowattributes.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_ENTITY , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.entities.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_ENTITY , g_pstWindowTheme->stStyleTheme.entities.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_ENTITY , (sptr_t)(g_pstWindowTheme->stStyleTheme.entities.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_ENTITY , (sptr_t)(g_pstWindowTheme->stStyleTheme.entities.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_TAGEND , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tagends.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_TAGEND , g_pstWindowTheme->stStyleTheme.tagends.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_TAGEND , (sptr_t)(g_pstWindowTheme->stStyleTheme.tagends.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_TAGEND , (sptr_t)(g_pstWindowTheme->stStyleTheme.tagends.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_CDATA , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.cdata.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_CDATA , g_pstWindowTheme->stStyleTheme.cdata.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_CDATA , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_CDATA , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_DOUBLESTRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_DOUBLESTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_SINGLESTRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_SINGLESTRING , g_pstWindowTheme->stStyleTheme.character.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_QUESTION , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_QUESTION , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_SCRIPT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_SCRIPT , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_SCRIPT , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , SCE_H_SCRIPT , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_SCRIPT , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_ASP , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_ASP , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_ASP , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , SCE_H_ASP , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_ASP , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_ASPAT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_ASPAT , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_ASPAT , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , SCE_H_ASPAT , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_ASPAT , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );
	}

	/* JavaScript */
	if( 选项卡页节点->pst文档类型配置->keywords3 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords3) );

		for( int style = SCE_HJ_START ; style <= SCE_HJ_REGEX ; style++ )
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJ_START , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJ_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJ_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJ_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJ_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJ_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJ_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJ_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJ_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJ_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJ_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJ_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJ_COMMENTDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJ_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJ_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJ_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJ_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJ_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJ_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJ_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJ_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJ_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJ_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJ_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJ_KEYWORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJ_KEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJ_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJ_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJ_DOUBLESTRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJ_DOUBLESTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJ_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJ_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJ_SINGLESTRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJ_SINGLESTRING , g_pstWindowTheme->stStyleTheme.character.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJ_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJ_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJA_START , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJA_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJA_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJA_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJA_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJA_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJA_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJA_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJA_COMMENTDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJA_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJA_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJA_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJA_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJA_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJA_KEYWORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJA_KEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJA_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJA_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJA_DOUBLESTRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJA_DOUBLESTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJA_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJA_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HJA_SINGLESTRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HJA_SINGLESTRING , g_pstWindowTheme->stStyleTheme.character.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HJA_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HJA_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );
	}

	/* VBScript */
	if( 选项卡页节点->pst文档类型配置->keywords4 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 2 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords4) );

		for( int style = SCE_HB_START ; style <= SCE_HB_STRINGEOL ; style++ )
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HB_START , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HB_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HB_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HB_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HB_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HB_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HB_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HB_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HB_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HB_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HB_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HB_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HB_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HB_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HB_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HB_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HB_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HB_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HB_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HB_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		for( int style = SCE_HBA_START ; style <= SCE_HBA_STRINGEOL ; style++ )
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HBA_START , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HBA_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HBA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HBA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HBA_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HBA_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HBA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HBA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HBA_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HBA_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HBA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HBA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HBA_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HBA_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HBA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HBA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HBA_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HBA_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HBA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HBA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );
	}

	/* Python */
	if( 选项卡页节点->pst文档类型配置->keywords5 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 3 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords5) );

		for( int style = SCE_HP_START ; style <= SCE_HP_IDENTIFIER ; style++ )
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HP_START , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HP_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HP_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HP_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HP_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HP_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HP_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HP_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HP_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HP_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HP_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HP_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HP_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HP_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HP_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HP_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HP_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HP_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HP_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HP_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HP_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HP_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HP_TRIPLE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HP_TRIPLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HP_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HP_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HP_TRIPLEDOUBLE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HP_TRIPLEDOUBLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HP_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HP_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HP_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HP_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

		for( int style = SCE_HPHP_COMPLEX_VARIABLE ; style <= SCE_HPA_IDENTIFIER ; style++ )
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPA_START , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPA_START , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPA_START , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPA_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPA_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPA_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPA_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPA_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPA_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPA_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPA_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPA_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPA_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPA_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPA_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPA_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPA_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPA_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPA_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPA_TRIPLE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPA_TRIPLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPA_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPA_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPA_TRIPLEDOUBLE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPA_TRIPLEDOUBLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPA_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPA_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPA_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPA_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPA_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPA_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	}

	/* PHP */
	if( 选项卡页节点->pst文档类型配置->keywords6 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 4 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords6) );

		for( int style = SCE_HPHP_DEFAULT ; style <= SCE_HPHP_OPERATOR ; style++ )
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , style , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPHP_HSTRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_HSTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPHP_HSTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_HSTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPHP_SIMPLESTRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_SIMPLESTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPHP_SIMPLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_SIMPLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPHP_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPHP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPHP_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPHP_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPHP_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPHP_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPHP_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPHP_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_HPHP_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_HPHP_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_HPHP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_HPHP_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );
	}

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_CSS ;
int InitTabPageControlsAfterLoadFile_CSS( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_CSS , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
	}

	if( 选项卡页节点->pst文档类型配置->keywords2 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords2) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 1 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 1 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 2 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.unknowtags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 2 , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 3 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.attributes.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 3 , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 4 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 4 , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 6 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.attributes.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 6 , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 7 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.unknowattributes.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 7 , g_pstWindowTheme->stStyleTheme.unknowattributes.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 7 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 7 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 8 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.attributes.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 8 , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 8 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 8 , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 9 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 9 , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 9 , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 9 , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 13 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 13 , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 13 , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 13 , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , 14 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , 14 , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , 14 , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , 14 , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_JAVASCRIPT ;
int InitTabPageControlsAfterLoadFile_JAVASCRIPT( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_PREPROCESSOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.preprocessor.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_COMMENTDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_XML ;
int InitTabPageControlsAfterLoadFile_XML( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_XML , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_TAG , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_TAG , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_TAG , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_TAG , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_TAGUNKNOWN , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.unknowtags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_TAGUNKNOWN , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_TAGUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_TAGUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_ATTRIBUTE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.attributes.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_ATTRIBUTE , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_ATTRIBUTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_ATTRIBUTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.unknowattributes.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_ATTRIBUTEUNKNOWN , g_pstWindowTheme->stStyleTheme.unknowattributes.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_ATTRIBUTEUNKNOWN , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_ENTITY , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.entities.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_ENTITY , g_pstWindowTheme->stStyleTheme.entities.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_ENTITY , (sptr_t)(g_pstWindowTheme->stStyleTheme.entities.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_ENTITY , (sptr_t)(g_pstWindowTheme->stStyleTheme.entities.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_TAGEND , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tagends.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_TAGEND , g_pstWindowTheme->stStyleTheme.tagends.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_TAGEND , (sptr_t)(g_pstWindowTheme->stStyleTheme.tagends.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_TAGEND , (sptr_t)(g_pstWindowTheme->stStyleTheme.tagends.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_CDATA , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.cdata.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_CDATA , g_pstWindowTheme->stStyleTheme.cdata.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_CDATA , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_CDATA , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_DOUBLESTRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_DOUBLESTRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_DOUBLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_SINGLESTRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_SINGLESTRING , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_SINGLESTRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_H_QUESTION , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_H_QUESTION , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.aspsection.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_H_QUESTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolTree( 选项卡页节点 );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_JSON ;
int InitTabPageControlsAfterLoadFile_JSON( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_JSON , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_JSON_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_JSON_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_JSON_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_JSON_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_JSON_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_JSON_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_JSON_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_JSON_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_JSON_PROPERTYNAME , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.attributes.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_JSON_PROPERTYNAME , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_JSON_PROPERTYNAME , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_JSON_PROPERTYNAME , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_JSON_LINECOMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_JSON_LINECOMMENT , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_JSON_LINECOMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_JSON_LINECOMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_JSON_BLOCKCOMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_JSON_BLOCKCOMMENT , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_JSON_BLOCKCOMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_JSON_BLOCKCOMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_JSON_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_JSON_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_JSON_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_JSON_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_JSON_KEYWORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_JSON_KEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_JSON_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_JSON_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_JSON_LDKEYWORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_JSON_LDKEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_JSON_LDKEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_JSON_LDKEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_YAML_ERROR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_YAML_ERROR , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_YAML_ERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_YAML_ERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolTree( 选项卡页节点 );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_YAML ;
int InitTabPageControlsAfterLoadFile_YAML( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_YAML , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_YAML_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_YAML_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_YAML_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_YAML_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_YAML_IDENTIFIER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.attributes.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_YAML_IDENTIFIER , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_YAML_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_YAML_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_YAML_KEYWORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_YAML_KEYWORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_YAML_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_YAML_KEYWORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_YAML_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_YAML_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_YAML_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_YAML_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_YAML_TEXT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_YAML_TEXT , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_YAML_TEXT , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_YAML_TEXT , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_YAML_ERROR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_YAML_ERROR , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_YAML_ERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_YAML_ERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_YAML_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_YAML_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_YAML_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_YAML_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_MAKEFILE ;
int InitTabPageControlsAfterLoadFile_MAKEFILE( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_MAKEFILE , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MAKE_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MAKE_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MAKE_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MAKE_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MAKE_PREPROCESSOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.preprocessor.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MAKE_PREPROCESSOR , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MAKE_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MAKE_PREPROCESSOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MAKE_IDENTIFIER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.attributes.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MAKE_IDENTIFIER , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MAKE_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MAKE_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MAKE_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MAKE_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MAKE_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MAKE_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MAKE_TARGET , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MAKE_TARGET , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MAKE_TARGET , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MAKE_TARGET , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_CMAKE ;
int InitTabPageControlsAfterLoadFile_CMAKE( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_CMAKE , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
	}

	if( 选项卡页节点->pst文档类型配置->keywords2 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords2) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_STRINGDQ , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_STRINGDQ , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_STRINGDQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_STRINGDQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_STRINGLQ , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_STRINGLQ , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_STRINGLQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_STRINGLQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_STRINGRQ , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_STRINGRQ , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_STRINGRQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_STRINGRQ , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_COMMANDS , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_COMMANDS , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_COMMANDS , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_COMMANDS , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_PARAMETERS , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_PARAMETERS , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_PARAMETERS , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_PARAMETERS , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_VARIABLE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.attributes.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_VARIABLE , g_pstWindowTheme->stStyleTheme.attributes.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_VARIABLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_VARIABLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.attributes.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_IFDEFINEDEF , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.phpsection.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_IFDEFINEDEF , g_pstWindowTheme->stStyleTheme.phpsection.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_IFDEFINEDEF , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_IFDEFINEDEF , (sptr_t)(g_pstWindowTheme->stStyleTheme.phpsection.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_MACRODEF , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.unknowtags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_MACRODEF , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_MACRODEF , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_MACRODEF , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_STRINGVAR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.unknowattributes.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_STRINGVAR , g_pstWindowTheme->stStyleTheme.unknowattributes.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_STRINGVAR , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_STRINGVAR , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowattributes.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_CMAKE_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_CMAKE_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_CMAKE_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_CMAKE_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_MARKDOWN ;
int InitTabPageControlsAfterLoadFile_MARKDOWN( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_MARKDOWN , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_STRONG1 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_STRONG1 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_STRONG1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_STRONG1 , TRUE );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_STRONG2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_STRONG2 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_STRONG2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_STRONG2 , TRUE );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_EM1 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_EM1 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_EM1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_EM1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETITALIC , SCE_MARKDOWN_EM1 , TRUE );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_EM2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.font) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_EM2 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_EM2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_EM2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETITALIC , SCE_MARKDOWN_EM2 , TRUE );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER1 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER1 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER1 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER2 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER2 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER3 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER3 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER3 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER4 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER4 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER4 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER5 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER5 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER5 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER5 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HEADER6 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HEADER6 , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HEADER6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HEADER6 , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_PRECHAR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_PRECHAR , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_PRECHAR , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_PRECHAR , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_ULIST_ITEM , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_ULIST_ITEM , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_ULIST_ITEM , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_ULIST_ITEM , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_OLIST_ITEM , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_OLIST_ITEM , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_OLIST_ITEM , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_OLIST_ITEM , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_BLOCKQUOTE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_BLOCKQUOTE , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_BLOCKQUOTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_BLOCKQUOTE , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_STRIKEOUT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_STRIKEOUT , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_STRIKEOUT , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_STRIKEOUT , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETWEIGHT , SCE_MARKDOWN_STRIKEOUT , 1 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_HRULE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_HRULE , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_HRULE , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_HRULE , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETWEIGHT , SCE_MARKDOWN_HRULE , 999 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_LINK , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_LINK , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_LINK , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_LINK , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETWEIGHT , SCE_MARKDOWN_LINK , 99 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_CODE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_CODE , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_CODE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_CODE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_CODE2 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.cdata.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_CODE2 , g_pstWindowTheme->stStyleTheme.cdata.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_CODE2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_CODE2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.bold) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_MARKDOWN_CODEBK , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.cdata.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_MARKDOWN_CODEBK , g_pstWindowTheme->stStyleTheme.cdata.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_MARKDOWN_CODEBK , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_MARKDOWN_CODEBK , (sptr_t)(g_pstWindowTheme->stStyleTheme.cdata.bold) );

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_LOG ;
int InitTabPageControlsAfterLoadFile_LOG( struct 选项卡页面S *选项卡页节点 )
{
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_CPP , 0 );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	if( 选项卡页节点->pst文档类型配置->keywords2 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords2) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_C_WORD2 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.unknowtags.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_C_WORD2 , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_C_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );
	}

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_PROPERTIES ;
int InitTabPageControlsAfterLoadFile_PROPERTIES( struct 选项卡页面S *选项卡页节点 )
{
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_PROPERTIES , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PROPS_DEFAULT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.text.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PROPS_DEFAULT , g_pstWindowTheme->stStyleTheme.text.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PROPS_DEFAULT , (sptr_t)(g_pstWindowTheme->stStyleTheme.text.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PROPS_DEFAULT , (sptr_t)(g_pstWindowTheme->stStyleTheme.text.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PROPS_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PROPS_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PROPS_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PROPS_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PROPS_SECTION , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PROPS_SECTION , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PROPS_SECTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PROPS_SECTION , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PROPS_ASSIGNMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PROPS_ASSIGNMENT , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PROPS_ASSIGNMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PROPS_ASSIGNMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PROPS_DEFVAL , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.preprocessor.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PROPS_DEFVAL , g_pstWindowTheme->stStyleTheme.preprocessor.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PROPS_DEFVAL , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PROPS_DEFVAL , (sptr_t)(g_pstWindowTheme->stStyleTheme.preprocessor.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_PROPS_KEY , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_PROPS_KEY , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_PROPS_KEY , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_PROPS_KEY , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_BATCH ;
int InitTabPageControlsAfterLoadFile_BATCH( struct 选项卡页面S *选项卡页节点 )
{
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_BATCH , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_BAT_DEFAULT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.text.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_BAT_DEFAULT , g_pstWindowTheme->stStyleTheme.text.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_BAT_DEFAULT , (sptr_t)(g_pstWindowTheme->stStyleTheme.text.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_BAT_DEFAULT , (sptr_t)(g_pstWindowTheme->stStyleTheme.text.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_BAT_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.comment.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_BAT_COMMENT , g_pstWindowTheme->stStyleTheme.comment.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_BAT_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_BAT_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.comment.bold) );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_BAT_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_BAT_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_BAT_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_BAT_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_BAT_LABEL , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_BAT_LABEL , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_BAT_LABEL , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_BAT_LABEL , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_BAT_HIDE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.unknowtags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_BAT_HIDE , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_BAT_HIDE , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_BAT_HIDE , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords2) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_BAT_COMMAND , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords2.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_BAT_COMMAND , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_BAT_COMMAND , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_BAT_COMMAND , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_BAT_IDENTIFIER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_BAT_IDENTIFIER , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_BAT_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_BAT_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_BAT_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_BAT_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_BAT_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_BAT_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	return 0;
}

funcInitTabPageControlsAfterLoadFile InitTabPageControlsAfterLoadFile_NIM ;
int InitTabPageControlsAfterLoadFile_NIM( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0 ;

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETLEXER , SCLEX_NIM , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_DEFAULT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.text.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_DEFAULT , g_pstWindowTheme->stStyleTheme.text.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_DEFAULT , (sptr_t)(g_pstWindowTheme->stStyleTheme.text.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_DEFAULT , (sptr_t)(g_pstWindowTheme->stStyleTheme.text.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_COMMENT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentline.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_COMMENT , g_pstWindowTheme->stStyleTheme.commentline.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_COMMENT , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentline.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_COMMENTDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_COMMENTDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_COMMENTDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_COMMENTLINE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_COMMENTLINE , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_COMMENTLINE , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_COMMENTLINEDOC , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.commentdoc.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_COMMENTLINEDOC , g_pstWindowTheme->stStyleTheme.commentdoc.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_COMMENTLINEDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_COMMENTLINEDOC , (sptr_t)(g_pstWindowTheme->stStyleTheme.commentdoc.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_NUMBER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.number.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_NUMBER , g_pstWindowTheme->stStyleTheme.number.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_NUMBER , (sptr_t)(g_pstWindowTheme->stStyleTheme.number.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_STRING , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_STRING , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_STRING , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_CHARACTER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.character.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_CHARACTER , g_pstWindowTheme->stStyleTheme.character.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_CHARACTER , (sptr_t)(g_pstWindowTheme->stStyleTheme.character.bold) );

	if( 选项卡页节点->pst文档类型配置->keywords )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_WORD , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_WORD , g_pstWindowTheme->stStyleTheme.keywords.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_WORD , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords.bold) );
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_TRIPLE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_TRIPLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_TRIPLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_TRIPLEDOUBLE , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_TRIPLEDOUBLE , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_TRIPLEDOUBLE , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_FUNCNAME , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.tags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_FUNCNAME , g_pstWindowTheme->stStyleTheme.tags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_FUNCNAME , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_FUNCNAME , (sptr_t)(g_pstWindowTheme->stStyleTheme.tags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_STRINGEOL , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.string.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_STRINGEOL , g_pstWindowTheme->stStyleTheme.string.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_STRINGEOL , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_STRINGEOL , (sptr_t)(g_pstWindowTheme->stStyleTheme.string.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_NUMERROR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.unknowtags.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_NUMERROR , g_pstWindowTheme->stStyleTheme.unknowtags.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_NUMERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_NUMERROR , (sptr_t)(g_pstWindowTheme->stStyleTheme.unknowtags.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_OPERATOR , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.operatorr.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_OPERATOR , g_pstWindowTheme->stStyleTheme.operatorr.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_OPERATOR , (sptr_t)(g_pstWindowTheme->stStyleTheme.operatorr.bold) );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_NIM_IDENTIFIER , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.text.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_NIM_IDENTIFIER , g_pstWindowTheme->stStyleTheme.text.fontsize );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_NIM_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.text.color) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_NIM_IDENTIFIER , (sptr_t)(g_pstWindowTheme->stStyleTheme.text.bold) );

	/*
	if( 选项卡页节点->pst文档类型配置->keywords2 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETKEYWORDS , 1 , (sptr_t)(选项卡页节点->pst文档类型配置->keywords2) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , SCE_P_WORD2 , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.keywords2.font)) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , SCE_P_WORD2 , g_pstWindowTheme->stStyleTheme.keywords2.fontsize );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , SCE_P_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.color) );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , SCE_P_WORD2 , (sptr_t)(g_pstWindowTheme->stStyleTheme.keywords2.bold) );
	}
	*/

	// 折叠
	InitScintillaControlFold( 选项卡页节点 );

	if( 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncOnReloadSymbolList( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcParseFileConfigHeader ParseSqlFileConfigHeader ;
int ParseSqlFileConfigHeader( struct 选项卡页面S *选项卡页节点 )
{
	struct DatabaseConnectionConfig	stDatabaseConnectionConfig ;
	int				nFileLineNo ;
	int				nFileLineCount ;
	char				acFileLineBuffer[ 128 ] ;
	char				acConfigRemark[ 64 ] ;
	char				acConfigKey[ 64 ] ;
	char				acConfigEval[ 64 ] ;
	char				acConfigValue[ 64 ] ;
	BOOL				bInConfigSecion ;

	memset( & stDatabaseConnectionConfig , 0x00 , sizeof(struct DatabaseConnectionConfig) );
	bInConfigSecion = FALSE ;
	nFileLineCount = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	for( nFileLineNo = 0 ; nFileLineNo <= nFileLineCount ; nFileLineNo++ )
	{
		memset( acFileLineBuffer , 0x00 , sizeof(acFileLineBuffer) );
		GetTextByLine( 选项卡页节点 , nFileLineNo , acFileLineBuffer , sizeof(acFileLineBuffer) );

		if( bInConfigSecion == FALSE )
		{
			if( strncmp( acFileLineBuffer , BEGIN_DATABASE_CONNECTION_CONFIG , sizeof(BEGIN_DATABASE_CONNECTION_CONFIG)-1 ) == 0 )
			{
				bInConfigSecion = TRUE ;
			}
		}
		else
		{
			if( strncmp( acFileLineBuffer , END_DATABASE_CONNECTION_CONFIG , sizeof(END_DATABASE_CONNECTION_CONFIG)-1 ) == 0 )
			{
				if( stDatabaseConnectionConfig.dbtype[0] )
				{
					BOOL	bChangeFlag = FALSE ;

					if(
						( 选项卡页节点->stDatabaseConnectionConfig.dbtype[0] && strcmp( 选项卡页节点->stDatabaseConnectionConfig.dbtype , stDatabaseConnectionConfig.dbtype ) )
						||
						( 选项卡页节点->stDatabaseConnectionConfig.dbhost[0] && strcmp( 选项卡页节点->stDatabaseConnectionConfig.dbhost , stDatabaseConnectionConfig.dbhost ) )
						||
						( 选项卡页节点->stDatabaseConnectionConfig.dbport && 选项卡页节点->stDatabaseConnectionConfig.dbport != stDatabaseConnectionConfig.dbport )
						||
						( 选项卡页节点->stDatabaseConnectionConfig.dbuser[0] && strcmp( 选项卡页节点->stDatabaseConnectionConfig.dbuser , stDatabaseConnectionConfig.dbuser ) )
						||
						( 选项卡页节点->stDatabaseConnectionConfig.dbpass[0] && strcmp( 选项卡页节点->stDatabaseConnectionConfig.dbpass , stDatabaseConnectionConfig.dbpass ) )
						||
						( 选项卡页节点->stDatabaseConnectionConfig.dbname[0] && strcmp( 选项卡页节点->stDatabaseConnectionConfig.dbname , stDatabaseConnectionConfig.dbname ) )
					)
					{
						bChangeFlag = TRUE ;
					}

					if( stDatabaseConnectionConfig.dbpass[0] )
						stDatabaseConnectionConfig.bConfigDbPass = TRUE ;
					else
						stDatabaseConnectionConfig.bConfigDbPass = FALSE ;

					memcpy( & (选项卡页节点->stDatabaseConnectionConfig) , & stDatabaseConnectionConfig , sizeof(struct DatabaseConnectionConfig) );

					if( bChangeFlag == TRUE )
					{
						DisconnectFromDatabase( 选项卡页节点 );

						ConnectToDatabase( 选项卡页节点 );
					}
				}
				break;
			}

			memset( acConfigRemark , 0x00 , sizeof(acConfigRemark) );
			memset( acConfigKey , 0x00 , sizeof(acConfigKey) );
			memset( acConfigEval , 0x00 , sizeof(acConfigEval) );
			memset( acConfigValue , 0x00 , sizeof(acConfigValue) );
			sscanf( acFileLineBuffer , "%s%s%s%*[ ]%[^\n]" , acConfigRemark , acConfigKey , acConfigEval , acConfigValue );
			if( strcmp( acConfigRemark , "--" ) != 0 || strcmp( acConfigEval , ":" ) != 0 )
				continue;
			if( strcmp( acConfigKey , "DBTYPE" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbtype , acConfigValue , sizeof(stDatabaseConnectionConfig.dbtype)-1 );
			else if( strcmp( acConfigKey , "DBHOST" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbhost , acConfigValue , sizeof(stDatabaseConnectionConfig.dbhost)-1 );
			else if( strcmp( acConfigKey , "DBPORT" ) == 0 )
				stDatabaseConnectionConfig.dbport = atoi(acConfigValue) ;
			else if( strcmp( acConfigKey , "DBUSER" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbuser , acConfigValue , sizeof(stDatabaseConnectionConfig.dbuser)-1 );
			else if( strcmp( acConfigKey , "DBPASS" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbpass , acConfigValue , sizeof(stDatabaseConnectionConfig.dbpass)-1 );
			else if( strcmp( acConfigKey , "DBNAME" ) == 0 )
				strncpy( stDatabaseConnectionConfig.dbname , acConfigValue , sizeof(stDatabaseConnectionConfig.dbname)-1 );
		}
	}

	return 0;
}

funcParseFileConfigHeader ParseRedisFileConfigHeader ;
int ParseRedisFileConfigHeader( struct 选项卡页面S *选项卡页节点 )
{
	struct Redis连接配置	stRedisConnectionConfig ;
	int				nFileLineNo ;
	int				nFileLineCount ;
	char				acFileLineBuffer[ 128 ] ;
	char				acConfigRemark[ 64 ] ;
	char				acConfigKey[ 64 ] ;
	char				acConfigEval[ 64 ] ;
	char				acConfigValue[ 64 ] ;
	BOOL				bInConfigSecion ;

	memset( & stRedisConnectionConfig , 0x00 , sizeof(struct Redis连接配置) );
	bInConfigSecion = FALSE ;
	nFileLineCount = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	for( nFileLineNo = 0 ; nFileLineNo <= nFileLineCount ; nFileLineNo++ )
	{
		memset( acFileLineBuffer , 0x00 , sizeof(acFileLineBuffer) );
		GetTextByLine( 选项卡页节点 , nFileLineNo , acFileLineBuffer , sizeof(acFileLineBuffer) );

		if( bInConfigSecion == FALSE )
		{
			if( strncmp( acFileLineBuffer , BEGIN_REDIS_CONNECTION_CONFIG , sizeof(BEGIN_REDIS_CONNECTION_CONFIG)-1 ) == 0 )
			{
				bInConfigSecion = TRUE ;
			}
		}
		else
		{
			if( strncmp( acFileLineBuffer , END_REDIS_CONNECTION_CONFIG , sizeof(END_REDIS_CONNECTION_CONFIG)-1 ) == 0 )
			{
				if( stRedisConnectionConfig.host[0] )
				{
					BOOL	bChangeFlag = FALSE ;

					if(
						( 选项卡页节点->stRedisConnectionConfig.host[0] && strcmp(选项卡页节点->stRedisConnectionConfig.host,stRedisConnectionConfig.host) )
						||
						( 选项卡页节点->stRedisConnectionConfig.port && 选项卡页节点->stRedisConnectionConfig.port != stRedisConnectionConfig.port )
						||
						( 选项卡页节点->stRedisConnectionConfig.pass[0] && strcmp(选项卡页节点->stRedisConnectionConfig.pass,stRedisConnectionConfig.pass) )
						||
						( 选项卡页节点->stRedisConnectionConfig.dbsl[0] && strcmp(选项卡页节点->stRedisConnectionConfig.dbsl,stRedisConnectionConfig.dbsl) )
					)
					{
						bChangeFlag = TRUE ;
					}

					memcpy( & (选项卡页节点->stRedisConnectionConfig) , & stRedisConnectionConfig , sizeof(struct Redis连接配置) );

					if( bChangeFlag == TRUE )
					{
						DisconnectFromRedis( 选项卡页节点 );

						ConnectToRedis( 选项卡页节点 );
					}
				}
				break;
			}

			memset( acConfigRemark , 0x00 , sizeof(acConfigRemark) );
			memset( acConfigKey , 0x00 , sizeof(acConfigKey) );
			memset( acConfigEval , 0x00 , sizeof(acConfigEval) );
			memset( acConfigValue , 0x00 , sizeof(acConfigValue) );
			sscanf( acFileLineBuffer , "%s%s%s%s" , acConfigRemark , acConfigKey , acConfigEval , acConfigValue );
			if( strcmp( acConfigRemark , "--" ) != 0 || strcmp( acConfigEval , ":" ) != 0 )
				continue;
			if( strcmp( acConfigKey , "HOST" ) == 0 )
				strncpy( stRedisConnectionConfig.host , acConfigValue , sizeof(stRedisConnectionConfig.host)-1 );
			else if( strcmp( acConfigKey , "PORT" ) == 0 )
				stRedisConnectionConfig.port = atoi(acConfigValue) ;
			else if( strcmp( acConfigKey , "PASS" ) == 0 )
				strncpy( stRedisConnectionConfig.pass , acConfigValue , sizeof(stRedisConnectionConfig.pass)-1 );
			else if( strcmp( acConfigKey , "DBSL" ) == 0 )
				strncpy( stRedisConnectionConfig.dbsl , acConfigValue , sizeof(stRedisConnectionConfig.dbsl)-1 );
		}
	}

	return 0;
}

int OnCharAdded_AutoAddClosedCharacter( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	/* 自动补全关闭符号 */
	if( lpnotify->ch == '(' && 全_风格主配置.启用自动添加闭合字符 )
	{
		int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)")" ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOPOS , (sptr_t)当前文本字节位置 , 0 );
	}
	else if( lpnotify->ch == '[' && 全_风格主配置.启用自动添加闭合字符 )
	{
		int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)"]" ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOPOS , (sptr_t)当前文本字节位置 , 0 );
	}
	else if( lpnotify->ch == '{' && 全_风格主配置.启用自动添加闭合字符 )
	{
		int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)"}" ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOPOS , (sptr_t)当前文本字节位置 , 0 );
	}
	/*
	else if( lpnotify->ch == '<' && 全_风格主配置.启用自动添加闭合字符 )
	{
	int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)">" ) ;
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOPOS , (sptr_t)当前文本字节位置 , 0 );
	}
	*/
	else if( lpnotify->ch == '\'' && 全_风格主配置.启用自动添加闭合字符 )
	{
		int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		char chCurrentCharacter = (char)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , 当前文本字节位置 , 0 ) ;
		char chPrevPrevCharacter = 0 ;
		if( 当前文本字节位置 >= 2 )
		{
			chPrevPrevCharacter = (char)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , 当前文本字节位置-2 , 0 ) ;
		}
		if( chPrevPrevCharacter == '\'' || chCurrentCharacter == '\'' )
		{
			;
		}
		else
		{
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)"'" ) ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOPOS , (sptr_t)当前文本字节位置 , 0 );
		}
	}
	else if( lpnotify->ch == '"' && 全_风格主配置.启用自动添加闭合字符 )
	{
		int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		char chCurrentCharacter = (char)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , 当前文本字节位置 , 0 ) ;
		char chPrevPrevCharacter = 0 ;
		if( 当前文本字节位置 >= 2 )
		{
			chPrevPrevCharacter = (char)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , 当前文本字节位置-2 , 0 ) ;
		}
		if( chPrevPrevCharacter == '"' || chCurrentCharacter == '"' )
		{
			;
		}
		else
		{
			int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)"\"" ) ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOPOS , (sptr_t)当前文本字节位置 , 0 );
		}
	}

	return 0;
}

int OnCharAdded_AutoAddClosedCharacter_AngleBracket( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	/* 自动补全关闭符号 */
	if( lpnotify->ch == '<' && 全_风格主配置.启用自动添加闭合字符 )
	{
		int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)">" ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOPOS , (sptr_t)当前文本字节位置 , 0 );
	}

	return 0;
}

int OnCharAdded_AutoIdentation( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	/* 自动缩进 */
	if( lpnotify->ch == '\n' && 全_风格主配置.启用自动识别 )
	{
		int	当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		int	nCurrnetLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_LINEFROMPOSITION , 当前文本字节位置 , 0 ) ;
		int	nPrevLinePos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_POSITIONFROMLINE , nCurrnetLine-1 , 0 ) ;
		char	acPrevLineIndentation[ 64 ] ;
		int	nPrevLineIndentationLen = 0 ;
		int	nPos = nPrevLinePos ;
		BOOL	bCollectIndentation = TRUE ;
		int	nLineTailChar = 0 ;

		memset( acPrevLineIndentation , 0x00 , sizeof(acPrevLineIndentation) );
		for( int ch = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , nPos , 0 ) ; ch != '\n' ; nPos++ , ch = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , nPos , 0 ) )
		{
			if( bCollectIndentation == TRUE )
			{
				if( strchr( " \t" , ch ) && nPrevLineIndentationLen <= sizeof(acPrevLineIndentation)-1 )
				{
					acPrevLineIndentation[nPrevLineIndentationLen] = ch ;
					nPrevLineIndentationLen++;
					continue;
				}
				else
				{
					bCollectIndentation = FALSE ;
				}
			}

			if( ! strchr( " \t\r\n" , ch ) )
				nLineTailChar = ch ;
		}

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acPrevLineIndentation ) ;
		if( nLineTailChar && strchr( "([{<" , nLineTailChar ) )
		{
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)"\t" ) ;

			当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			if( strchr( ")]}>" , (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , 当前文本字节位置 , 0 ) ) )
			{
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)(选项卡页节点->acEndOfLine) ) ;
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acPrevLineIndentation ) ;
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOPOS , (sptr_t)当前文本字节位置 , 0 );
			}
		}
	}

	return 0;
}

int OnCharAdded_CallTipsOnInputParenthesis( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	char		acWordBuffer[ 64 ] ;

	int		nret = 0 ;

	if( lpnotify->ch == '(' && 全_风格主配置.启用调用提示显示 )
	{
		/* 函数原型提示 */
		if( 选项卡页节点->pst文档类型配置 &&  ! RB_EMPTY_ROOT(&(选项卡页节点->pst文档类型配置->stCallTipShowTree.treeCallTipShow)) )
		{
			int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			int nStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_WORDSTARTPOSITION , 当前文本字节位置-1 , TRUE ) ;
			int nEndPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_WORDENDPOSITION , 当前文本字节位置-1 , TRUE ) ;
			if( nEndPos-nStartPos >= sizeof(acWordBuffer)-1 )
				nEndPos = nStartPos + sizeof(acWordBuffer)-1 ;

			struct Sci_TextRange	tr ;
			memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
			tr.chrg.cpMin = nStartPos;
			tr.chrg.cpMax = nEndPos;
			tr.lpstrText = acWordBuffer;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXTRANGE , 0 , (sptr_t)(&tr) );

			struct CallTipShowNode ctsQuery ;
			memset( & ctsQuery , 0x00 , sizeof(struct CallTipShowNode) );
			ctsQuery.acCallTipFuncName = acWordBuffer ;
			struct CallTipShowNode *p_ctsLocation = QueryCallTipShowTreeNode( & (选项卡页节点->pst文档类型配置->stCallTipShowTree) , & ctsQuery ) ;
			if( p_ctsLocation )
			{
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CALLTIPSHOW , 当前文本字节位置 , (sptr_t)(p_ctsLocation->acCallTipFuncDesc) );
			}
			else
			{
				if( 选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_AUTOCACTIVE , 0 , 0 ) )
					选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CALLTIPCANCEL , 0 , 0 );
			}
		}
	}
	else if( lpnotify->ch == ',' && 全_风格主配置.启用调用提示显示 )
	{
		/* 函数原型提示 */
		if( 选项卡页节点->pst文档类型配置 && ! RB_EMPTY_ROOT(&(选项卡页节点->pst文档类型配置->stCallTipShowTree.treeCallTipShow)) )
		{
			int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) - 1 ;
			int nPos = 当前文本字节位置 ;
			int ch ;
			for( ch = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , nPos , 0 )
				; nPos >= 0 && ch != '('
				; nPos-- , ch = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , nPos , 0 ) )
				;
			if( ch == '(' && nPos >= 0 )
			{
				int nStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_WORDSTARTPOSITION , nPos-1 , TRUE ) ;
				int nEndPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_WORDENDPOSITION , nPos-1 , TRUE ) ;
				if( nEndPos-nStartPos >= sizeof(acWordBuffer)-1 )
					nEndPos = nStartPos + sizeof(acWordBuffer)-1 ;

				struct Sci_TextRange	tr ;
				memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
				tr.chrg.cpMin = nStartPos;
				tr.chrg.cpMax = nEndPos;
				tr.lpstrText = acWordBuffer;
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXTRANGE , 0 , (sptr_t)(&tr) );

				struct CallTipShowNode ctsQuery ;
				memset( & ctsQuery , 0x00 , sizeof(struct CallTipShowNode) );
				ctsQuery.acCallTipFuncName = acWordBuffer ;
				struct CallTipShowNode *p_ctsLocation = QueryCallTipShowTreeNode( & (选项卡页节点->pst文档类型配置->stCallTipShowTree) , & ctsQuery ) ;
				if( p_ctsLocation )
				{
					选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CALLTIPSHOW , 当前文本字节位置 , (sptr_t)(p_ctsLocation->acCallTipFuncDesc) );
				}
				else
				{
					if( 选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_AUTOCACTIVE , 0 , 0 ) )
						选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CALLTIPCANCEL , 0 , 0 );
				}
			}
			else
			{
				if( 选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_AUTOCACTIVE , 0 , 0 ) )
					选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CALLTIPCANCEL , 0 , 0 );
			}
		}
	}

	return 0;
}

int OnCharAdded_CallTipsOnInputSpaceLookbackFromOneCharacter( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify , char chFromOneCharacter , BOOL bUpperCase )
{
	char		acWordBuffer[ 64 ] ;

	int		nret = 0 ;

	if( lpnotify->ch == ' ' && 全_风格主配置.启用调用提示显示 )
	{
		/* 函数原型提示 */
		if( 选项卡页节点->pst文档类型配置 &&  ! RB_EMPTY_ROOT(&(选项卡页节点->pst文档类型配置->stCallTipShowTree.treeCallTipShow)) )
		{
			int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			int nStartPos = 当前文本字节位置 ;
			int nEndPos  ;
			int ch ;

			while(1)
			{
				nStartPos--;
				if( nStartPos < 0 )
				{
					nStartPos = 0 ;
					break;
				}

				ch = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , nStartPos , 0 ) ;
				if( ch == chFromOneCharacter )
				{
					nStartPos++;
					break;
				}
			}

			while(1)
			{
				ch = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , nStartPos , 0 ) ;
				if( ! strchr( " \t\r\n\f" , ch ) )
				{
					break;
				}

				nStartPos++;
				if( nStartPos >= 当前文本字节位置 )
				{
					break;
				}
			}

			nEndPos = nStartPos + 1 ;
			while(1)
			{
				ch = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , nEndPos , 0 ) ;
				if( strchr( " \t\r\n\f" , ch ) )
				{
					break;
				}

				nEndPos++;
				if( nEndPos >= 当前文本字节位置 )
				{
					break;
				}
			}

			struct Sci_TextRange	tr ;
			memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
			tr.chrg.cpMin = nStartPos;
			tr.chrg.cpMax = nEndPos;
			tr.lpstrText = acWordBuffer;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXTRANGE , 0 , (sptr_t)(&tr) );

			if( bUpperCase == TRUE )
				ToUpperString( acWordBuffer );

			struct CallTipShowNode ctsQuery ;
			memset( & ctsQuery , 0x00 , sizeof(struct CallTipShowNode) );
			ctsQuery.acCallTipFuncName = acWordBuffer ;
			struct CallTipShowNode *p_ctsLocation = QueryCallTipShowTreeNode( & (选项卡页节点->pst文档类型配置->stCallTipShowTree) , & ctsQuery ) ;
			if( p_ctsLocation )
			{
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CALLTIPSHOW , 当前文本字节位置 , (sptr_t)(p_ctsLocation->acCallTipFuncDesc) );
			}
			else
			{
				if( 选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_AUTOCACTIVE , 0 , 0 ) )
					选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CALLTIPCANCEL , 0 , 0 );
			}
		}
	}
	else if( lpnotify->ch == '\n' && 全_风格主配置.启用调用提示显示 )
	{
		if( 选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_AUTOCACTIVE , 0 , 0 ) )
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CALLTIPCANCEL , 0 , 0 );
	}

	return 0;
}

int OnCharAdded_AutoCompletedShow( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	char		acWordBuffer[ 64 ] ;

	int		nret = 0 ;

	if( -1 <= lpnotify->ch && lpnotify->ch <= 255 && isprint(lpnotify->ch) && lpnotify->ch != ' ' && lpnotify->ch != '\t' && 全_风格主配置.启用自动完成显示 )
	{
		/* 自动完成提示 */
		if( 选项卡页节点->pst文档类型配置 && ! RB_EMPTY_ROOT(&(选项卡页节点->pst文档类型配置->stAutoCompletedShowTree.treeAutoCompletedShow)) )
		{
			int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			int nStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_WORDSTARTPOSITION , 当前文本字节位置-1 , TRUE ) ;
			int nEndPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_WORDENDPOSITION , 当前文本字节位置-1 , TRUE ) ;
			if( nEndPos-nStartPos >= sizeof(acWordBuffer)-1 )
				nEndPos = nStartPos + sizeof(acWordBuffer)-1 ;
			if( nEndPos-nStartPos >= 全_风格主配置.输入字符后自动完成显示 )
			{
				struct Sci_TextRange	tr ;
				memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
				tr.chrg.cpMin = nStartPos;
				tr.chrg.cpMax = nEndPos;
				tr.lpstrText = acWordBuffer;
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXTRANGE , 0 , (sptr_t)(&tr) );
				if( acWordBuffer[0] )
				{
					struct AutoCompletedShowNode acsQuery ;
					memset( & acsQuery , 0x00 , sizeof(struct AutoCompletedShowNode) );
					acsQuery.acAutoCompletedString = acWordBuffer ;
					struct AutoCompletedShowNode *p_acsLocation = QueryAutoCompletedShowTreeNodePartly( & (选项卡页节点->pst文档类型配置->stAutoCompletedShowTree) , & acsQuery ) ;
					if( p_acsLocation )
					{
						struct AutoCompletedShowNode *p_acsStart = NULL ;
						struct AutoCompletedShowNode *p_acsEnd = NULL ;

						struct rb_node *p_nodeFirst = rb_first(&(选项卡页节点->pst文档类型配置->stAutoCompletedShowTree.treeAutoCompletedShow)) ;
						struct rb_node *p_nodeLast = rb_last(&(选项卡页节点->pst文档类型配置->stAutoCompletedShowTree.treeAutoCompletedShow)) ;

						struct rb_node *p_nodePrev = rb_prev(&(p_acsLocation->nodeAutoCompletedShow)) ;
						struct rb_node *p_nodeNext = rb_next(&(p_acsLocation->nodeAutoCompletedShow)) ;
						if( p_nodePrev && p_nodeNext )
						{
							struct AutoCompletedShowNode *p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
							p_acsStart = p_acsLocation ;
							while(1)
							{
								nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsPrev ) ;
								if( nret )
									break;
								else
									p_acsStart = p_acsPrev ;

								if( p_nodePrev == p_nodeFirst )
									break;
								p_nodePrev = rb_prev(p_nodePrev) ;
								p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
							}

							struct AutoCompletedShowNode *p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
							p_acsEnd = p_acsLocation ;
							while(1)
							{
								nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsNext ) ;
								if( nret )
									break;
								else
									p_acsEnd = p_acsNext ;

								if( p_nodeNext == p_nodeLast )
									break;
								p_nodeNext = rb_next(p_nodeNext) ;
								p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
							}

							选项卡页节点->pst文档类型配置->nAutoCompletedShowStartPos = p_acsStart->begin ;
							选项卡页节点->pst文档类型配置->nAutoCompletedShowEndPos = p_acsEnd->end ;

							sprintf( 选项卡页节点->pst文档类型配置->acAutoCompletedShowBuffer , "%.*s" , 选项卡页节点->pst文档类型配置->nAutoCompletedShowEndPos - 选项卡页节点->pst文档类型配置->nAutoCompletedShowStartPos - 1 , 选项卡页节点->pst文档类型配置->acAutoCompletedBuffer + 选项卡页节点->pst文档类型配置->nAutoCompletedShowStartPos );
							选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_AUTOCSHOW , 当前文本字节位置-nStartPos , (sptr_t)(选项卡页节点->pst文档类型配置->acAutoCompletedShowBuffer) );
						}
					}
				}
			}
		}
	}

	return 0;
}

int OnCharAdded_AutoCompletedShow_HTML( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	char		acWordBuffer[ 64 ] ;

	int		nret = 0 ;

	if( -1 <= lpnotify->ch && lpnotify->ch <= 255 && isprint(lpnotify->ch) && lpnotify->ch != '\t' && 全_风格主配置.启用自动完成显示 )
	{
		/* 自动完成提示 */
		if( 选项卡页节点->pst文档类型配置 && ! RB_EMPTY_ROOT(&(选项卡页节点->pst文档类型配置->stAutoCompletedShowTree.treeAutoCompletedShow)) )
		{
			if( lpnotify->ch == '<' )
			{
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_AUTOCSHOW , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->acAutoCompletedBuffer) );
			}
			else if( lpnotify->ch == ' ' )
			{
				int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				int nPos ;
				int ch ;
				int chPrev ;

				ch = 0 ;
				for( nPos = 当前文本字节位置 - 1 ; nPos >= 0 ; nPos-- )
				{
					chPrev = ch ;
					ch = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , nPos , 0 ) ;
					if( ch == '<' || ch == '>' )
						break;
				}
				if( nPos >= 0 && ch == '<' && chPrev != '?' && chPrev != '%' )
				{
					选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_AUTOCSHOW , 0 , (sptr_t)(选项卡页节点->pst文档类型配置->acAutoCompletedBuffer2) );
				}
			}
			else if( isalpha(lpnotify->ch) )
			{
				int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				int nPos ;
				int ch ;
				int chPrev ;

				int nStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_WORDSTARTPOSITION , 当前文本字节位置-1 , TRUE ) ;
				int nEndPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_WORDENDPOSITION , 当前文本字节位置-1 , TRUE ) ;
				if( nEndPos-nStartPos >= sizeof(acWordBuffer)-1 )
					nEndPos = nStartPos + sizeof(acWordBuffer)-1 ;

				if( nStartPos > 0 && nEndPos-nStartPos >= 全_风格主配置.输入字符后自动完成显示 )
				{
					BOOL	bIsAttribute = FALSE ;

					ch = 0 ;
					for( nPos = 当前文本字节位置 ; nPos >= 0 ; nPos-- )
					{
						chPrev = ch ;
						ch = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , nPos , 0 ) ;
						if( ch == ' ' )
							bIsAttribute = TRUE ;
						else if( ch == '<' || ch == '>' )
							break;
					}
					if( nPos >= 0 )
					{
						if( ch == '<' && chPrev != '?' && chPrev != '%' )
						{
							if( bIsAttribute == FALSE )
							{
								struct Sci_TextRange	tr ;
								memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
								tr.chrg.cpMin = nStartPos;
								tr.chrg.cpMax = nEndPos;
								tr.lpstrText = acWordBuffer;
								选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXTRANGE , 0 , (sptr_t)(&tr) );
								if( acWordBuffer[0] )
								{
									struct AutoCompletedShowNode acsQuery ;
									memset( & acsQuery , 0x00 , sizeof(struct AutoCompletedShowNode) );
									acsQuery.acAutoCompletedString = acWordBuffer ;
									struct AutoCompletedShowNode *p_acsLocation = QueryAutoCompletedShowTreeNodePartly( & (选项卡页节点->pst文档类型配置->stAutoCompletedShowTree) , & acsQuery ) ;
									if( p_acsLocation )
									{
										struct AutoCompletedShowNode *p_acsStart = NULL ;
										struct AutoCompletedShowNode *p_acsEnd = NULL ;

										struct rb_node *p_nodeFirst = rb_first(&(选项卡页节点->pst文档类型配置->stAutoCompletedShowTree.treeAutoCompletedShow)) ;
										struct rb_node *p_nodeLast = rb_last(&(选项卡页节点->pst文档类型配置->stAutoCompletedShowTree.treeAutoCompletedShow)) ;

										struct rb_node *p_nodePrev = rb_prev(&(p_acsLocation->nodeAutoCompletedShow)) ;
										struct rb_node *p_nodeNext = rb_next(&(p_acsLocation->nodeAutoCompletedShow)) ;
										if( p_nodePrev && p_nodeNext )
										{
											struct AutoCompletedShowNode *p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											p_acsStart = p_acsLocation ;
											while(1)
											{
												nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsPrev ) ;
												if( nret )
													break;
												else
													p_acsStart = p_acsPrev ;

												if( p_nodePrev == p_nodeFirst )
													break;
												p_nodePrev = rb_prev(p_nodePrev) ;
												p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											}

											struct AutoCompletedShowNode *p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											p_acsEnd = p_acsLocation ;
											while(1)
											{
												nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsNext ) ;
												if( nret )
													break;
												else
													p_acsEnd = p_acsNext ;

												if( p_nodeNext == p_nodeLast )
													break;
												p_nodeNext = rb_next(p_nodeNext) ;
												p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											}

											选项卡页节点->pst文档类型配置->nAutoCompletedShowStartPos = p_acsStart->begin ;
											选项卡页节点->pst文档类型配置->nAutoCompletedShowEndPos = p_acsEnd->end ;

											sprintf( 选项卡页节点->pst文档类型配置->acAutoCompletedShowBuffer , "%.*s" , 选项卡页节点->pst文档类型配置->nAutoCompletedShowEndPos - 选项卡页节点->pst文档类型配置->nAutoCompletedShowStartPos - 1 , 选项卡页节点->pst文档类型配置->acAutoCompletedBuffer + 选项卡页节点->pst文档类型配置->nAutoCompletedShowStartPos );
											选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_AUTOCSHOW , 当前文本字节位置-nStartPos , (sptr_t)(选项卡页节点->pst文档类型配置->acAutoCompletedShowBuffer) );
										}
									}
								}
							}
							else
							{
								struct Sci_TextRange	tr ;
								memset( acWordBuffer , 0x00 , sizeof(acWordBuffer) );
								tr.chrg.cpMin = nStartPos;
								tr.chrg.cpMax = nEndPos;
								tr.lpstrText = acWordBuffer;
								选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXTRANGE , 0 , (sptr_t)(&tr) );
								if( acWordBuffer[0] )
								{
									struct AutoCompletedShowNode acsQuery ;
									memset( & acsQuery , 0x00 , sizeof(struct AutoCompletedShowNode) );
									acsQuery.acAutoCompletedString = acWordBuffer ;
									struct AutoCompletedShowNode *p_acsLocation = QueryAutoCompletedShowTreeNodePartly( & (选项卡页节点->pst文档类型配置->stAutoCompletedShowTree2) , & acsQuery ) ;
									if( p_acsLocation )
									{
										struct AutoCompletedShowNode *p_acsStart = NULL ;
										struct AutoCompletedShowNode *p_acsEnd = NULL ;

										struct rb_node *p_nodeFirst = rb_first(&(选项卡页节点->pst文档类型配置->stAutoCompletedShowTree2.treeAutoCompletedShow)) ;
										struct rb_node *p_nodeLast = rb_last(&(选项卡页节点->pst文档类型配置->stAutoCompletedShowTree2.treeAutoCompletedShow)) ;

										struct rb_node *p_nodePrev = rb_prev(&(p_acsLocation->nodeAutoCompletedShow)) ;
										struct rb_node *p_nodeNext = rb_next(&(p_acsLocation->nodeAutoCompletedShow)) ;
										if( p_nodePrev && p_nodeNext )
										{
											struct AutoCompletedShowNode *p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											p_acsStart = p_acsLocation ;
											while(1)
											{
												nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsPrev ) ;
												if( nret )
													break;
												else
													p_acsStart = p_acsPrev ;

												if( p_nodePrev == p_nodeFirst )
													break;
												p_nodePrev = rb_prev(p_nodePrev) ;
												p_acsPrev = rb_entry(p_nodePrev,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											}

											struct AutoCompletedShowNode *p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											p_acsEnd = p_acsLocation ;
											while(1)
											{
												nret = CompareAutoCompletedShowTreeNodePartlyEntry( & acsQuery , p_acsNext ) ;
												if( nret )
													break;
												else
													p_acsEnd = p_acsNext ;

												if( p_nodeNext == p_nodeLast )
													break;
												p_nodeNext = rb_next(p_nodeNext) ;
												p_acsNext = rb_entry(p_nodeNext,struct AutoCompletedShowNode,nodeAutoCompletedShow) ;
											}

											选项卡页节点->pst文档类型配置->nAutoCompletedShowStartPos2 = p_acsStart->begin ;
											选项卡页节点->pst文档类型配置->nAutoCompletedShowEndPos2 = p_acsEnd->end ;

											sprintf( 选项卡页节点->pst文档类型配置->acAutoCompletedShowBuffer2 , "%.*s" , 选项卡页节点->pst文档类型配置->nAutoCompletedShowEndPos2 - 选项卡页节点->pst文档类型配置->nAutoCompletedShowStartPos2 - 1 , 选项卡页节点->pst文档类型配置->acAutoCompletedBuffer2 + 选项卡页节点->pst文档类型配置->nAutoCompletedShowStartPos2 );
											选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_AUTOCSHOW , 当前文本字节位置-nStartPos , (sptr_t)(选项卡页节点->pst文档类型配置->acAutoCompletedShowBuffer2) );
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return 0;
}

funcOnCharAdded OnCharAdded_CPP_like ;
int OnCharAdded_CPP_like( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_CallTipsOnInputParenthesis( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_SQL_like ;
int OnCharAdded_SQL_like( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_CallTipsOnInputSpaceLookbackFromOneCharacter( 选项卡页节点 , lpnotify , ';' , TRUE ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_REDIS_like ;
int OnCharAdded_REDIS_like( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_CallTipsOnInputSpaceLookbackFromOneCharacter( 选项卡页节点 , lpnotify , '\n' , TRUE ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_HTML_like ;
int OnCharAdded_HTML_like( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter_AngleBracket( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow_HTML( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_XML_like ;
int OnCharAdded_XML_like( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter_AngleBracket( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_CSS_like ;
int OnCharAdded_CSS_like( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_JSON_like ;
int OnCharAdded_JSON_like( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_YAML_like ;
int OnCharAdded_YAML_like( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_MAKEFILE_like ;
int OnCharAdded_MAKEFILE_like( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_CMAKE_like ;
int OnCharAdded_CMAKE_like( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoCompletedShow( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

funcOnCharAdded OnCharAdded_MARKDOWN_like ;
int OnCharAdded_MARKDOWN_like( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	nret = OnCharAdded_AutoIdentation( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	nret = OnCharAdded_AutoAddClosedCharacter( 选项卡页节点 , lpnotify ) ;
	if( nret )
		return nret;

	return 0;
}

int OnKeyDown_BraceHighLight( struct 选项卡页面S *选项卡页节点 )
{
	int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	char chCurrentCharacter = (char)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCHARAT , 当前文本字节位置 , 0 ) ;

	if( chCurrentCharacter && strchr( "()[]{}<>" , chCurrentCharacter ) )
	{
		int nMatchPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_BRACEMATCH , 当前文本字节位置 , 0 ) ;
		if( nMatchPos != -1 )
		{
			int nStyle = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETSTYLEAT , 当前文本字节位置 , 0 );
			int nForeColor = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLEGETFORE , nStyle , 0 );
			int nBackColor = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLEGETBACK , nStyle , 0 );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , STYLE_BRACELIGHT , nBackColor );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , STYLE_BRACELIGHT , nForeColor );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , STYLE_BRACELIGHT , TRUE );

			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_BRACEHIGHLIGHT , 当前文本字节位置 , nMatchPos ) ;
		}
		else
		{
			int nStyle = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETSTYLEAT , 当前文本字节位置 , 0 );
			int nForeColor = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLEGETFORE , nStyle , 0 );
			int nBackColor = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLEGETBACK , nStyle , 0 );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , STYLE_BRACEBAD , nForeColor );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , STYLE_BRACEBAD , nBackColor );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETITALIC , STYLE_BRACEBAD , TRUE );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETUNDERLINE , STYLE_BRACEBAD , TRUE );

			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_BRACEBADLIGHT , 当前文本字节位置 , 0 ) ;
		}
	}
	else
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_BRACEBADLIGHT , INVALID_POSITION , INVALID_POSITION ) ;
	}

	return 0;
}

funcOnKeyDown OnKeyDown_GotoSymbolDefineLine ;
int OnKeyDown_GotoSymbolDefineLine( struct 选项卡页面S *选项卡页节点 , WPARAM wParam , LPARAM lParam )
{
	int		nret = 0 ;

	if( wParam == VK_F11 )
	{
		nret = GetCurrentWordAndJumpGotoLine( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

static void SelectSqlStatement(struct 选项卡页面S *选项卡页节点 )
{
	int	当前文本字节位置 ;
	int	文本总字节数 ;
	int	nStartPos ;
	int	nEndPos ;

	当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	文本总字节数 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETTEXTLENGTH, 0, 0 ) ;

	for( nStartPos = 当前文本字节位置 ; nStartPos >= 0 ; nStartPos-- )
	{
		if( ! strchr( "\r\n" , (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETCHARAT, nStartPos, 0 ) ) )
			break;
	}
	if( (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETCHARAT, nStartPos, 0 ) == ';' )
		nStartPos--;
	nEndPos = nStartPos ;

	for( ; nStartPos >= 0 ; nStartPos-- )
	{
		if( (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETCHARAT, nStartPos, 0 ) == ';' )
			break;
	}
	if( nStartPos < 0 )
		nStartPos = 0 ;
	else
		nStartPos++;

	for( ; nEndPos < 文本总字节数 ; nEndPos++ )
	{
		if( (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETCHARAT, nEndPos, 0 ) == ';' )
			break;
	}
	if( nEndPos >= 文本总字节数 )
		nEndPos = 文本总字节数 - 1 ;
	/*
	else
	nEndPos--;
	*/

	全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_SETSEL, nStartPos, nEndPos ) ;

	return;
}

funcOnKeyDown OnKeyDown_SQL ;
int OnKeyDown_SQL( struct 选项卡页面S *选项卡页节点 , WPARAM wParam , LPARAM lParam )
{
	int		nret = 0 ;

	if( wParam == VK_F5 )
	{
		if( lParam == VK_CONTROL )
		{
			SelectSqlStatement( 选项卡页节点 );
		}

		nret = ExecuteSqlQuery( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcOnKeyDown OnKeyDown_REDIS ;
int OnKeyDown_REDIS( struct 选项卡页面S *选项卡页节点 , WPARAM wParam , LPARAM lParam )
{
	int		nret = 0 ;

	if( wParam == VK_F5 )
	{
		if( lParam == VK_CONTROL )
		{
			SelectSqlStatement( 选项卡页节点 );
		}

		nret = ExecuteRedisQuery_REDIS( 选项卡页节点 ) ;
		if( nret )
			return nret;
	}

	return 0;
}

funcOnKeyUp OnKeyUp_GENERAL ;
int OnKeyUp_GENERAL( struct 选项卡页面S *选项卡页节点 , WPARAM wParam , LPARAM lParam )
{
	OnKeyDown_BraceHighLight( 选项卡页节点 );

	return 0;
}

funcOnReloadSymbolList OnReloadSymbolList_WithReqularExp ;
int OnReloadSymbolList_WithReqularExp( struct 选项卡页面S *选项卡页节点 )
{
	int nret = ReloadSymbolList_WithReqularExp( 选项卡页节点 );
	return nret;
}

funcOnDbClickSymbolList OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine ;
int OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine( struct 选项卡页面S *选项卡页节点 )
{
	return GetSymbolListItemAndJumpGotoLine( 选项卡页节点 );
}

funcOnReloadSymbolTree OnReloadSymbolTree_SQL ;
int OnReloadSymbolTree_SQL( struct 选项卡页面S *选项卡页节点 )
{
	int nret = ReloadSymbolTree_SQL( 选项卡页节点 );
	return nret;
}

funcOnReloadSymbolTree OnReloadSymbolTree_XML ;
int OnReloadSymbolTree_XML( struct 选项卡页面S *选项卡页节点 )
{
	HANDLE 互斥锁 = CreateMutex( NULL , TRUE , "RELOAD_SYMBOLLIST_OR_SYMBOLTREE_MUTEX" ) ;
	if( ! 互斥锁 )
		return -1;

	int nret = ReloadSymbolTree_XML( 选项卡页节点 );

	CloseHandle(互斥锁);

	return nret;
}

funcOnReloadSymbolTree OnReloadSymbolTree_JSON ;
int OnReloadSymbolTree_JSON( struct 选项卡页面S *选项卡页节点 )
{
	HANDLE 互斥锁 = CreateMutex( NULL , TRUE , "RELOAD_SYMBOLLIST_OR_SYMBOLTREE_MUTEX" ) ;
	if( ! 互斥锁 )
		return -1;

	int nret = ReloadSymbolTree_JSON( 选项卡页节点 );

	CloseHandle(互斥锁);

	return nret;
}

funcOnDbClickSymbolTree OnDbClickSymbolTree_SQL ;
int OnDbClickSymbolTree_SQL( struct 选项卡页面S *选项卡页节点 )
{
	return GetSymbolTreeItemAndAddTextToEditor( 选项卡页节点 );
}

funcOnDbClickSymbolTree OnDbClickSymbolTree_JSON ;
int OnDbClickSymbolTree_JSON( struct 选项卡页面S *选项卡页节点 )
{
	return GetSymbolTreeItemAndGotoEditorPos( 选项卡页节点 );
}

funcOnDbClickSymbolTree OnDbClickSymbolTree_REDIS ;
int OnDbClickSymbolTree_REDIS( struct 选项卡页面S *选项卡页节点 )
{
	return GetSymbolTreeItemAndAddTextToEditor( 选项卡页节点 );
}

funcCleanTabPageControls CleanTabPageControls_AllControls ;
int CleanTabPageControls_AllControls( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点->pcreSymbolRe )
	{
		pcre_free( 选项卡页节点->pcreSymbolRe ); 选项卡页节点->pcreSymbolRe = NULL ;
	}

	if( 选项卡页节点->符号列表句柄 )
	{
		DestroyWindow( 选项卡页节点->符号列表句柄 ); 选项卡页节点->符号列表句柄 = NULL ;
	}

	if( 选项卡页节点->符号树句柄 )
	{
		DestroyWindow( 选项卡页节点->符号树句柄 ); 选项卡页节点->符号树句柄 = NULL ;
	}

	if( 选项卡页节点->hwndQueryResultEdit )
	{
		DestroyWindow( 选项卡页节点->hwndQueryResultEdit ); 选项卡页节点->hwndQueryResultEdit = NULL ;
	}

	if( 选项卡页节点->hwndQueryResultTable )
	{
		DestroyWindow( 选项卡页节点->hwndQueryResultTable ); 选项卡页节点->hwndQueryResultTable = NULL ;
	}

	return 0;
}

struct 文档类型配置S	全_文档类型配置[] =
	{
		{ DOCTYPE_TXT , NULL , ";*.txt;" , "文本文件" , NULL , NULL , NULL , NULL , NULL , OnCharAdded_AutoIdentation , NULL , NULL , NULL , NULL , NULL } ,
		{ DOCTYPE_CPP , "doctype_CPP.conf" , ";*.h;*.hh;*.hpp;*.c;*.cc;*.cpp;*.cxx;*.rc;*.rc2;*.dlg;" , "C/C++文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_CPP , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_CS , "doctype_CS.conf" , ";*.cs;" , "C#文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_CS , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_JAVA , "doctype_JAVA.conf" , ";*.java;*.jad;*.pde;" , "Java文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_JAVA , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_GO , "doctype_GO.conf" , ";*.go;" , "Go文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_GO , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_SWIFT , "doctype_SWIFT.conf" , ";*.swift;" , "Swift文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_SWIFT , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_SQL , "doctype_SQL.conf" , ";*.sql;" , "SQL命令文件" , InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl_And_ResultEditCtl_And_ResultListCtl , InitTabPageControlsAfterLoadFile_SQL , ParseSqlFileConfigHeader , OnKeyDown_SQL , OnKeyUp_GENERAL , OnCharAdded_SQL_like , NULL , NULL , OnReloadSymbolTree_SQL , OnDbClickSymbolTree_SQL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_REDIS , "doctype_REDIS.conf" , ";*.redis;" , "Redis命令文件" , InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl , InitTabPageControlsAfterLoadFile_REDIS , ParseRedisFileConfigHeader , OnKeyDown_REDIS , OnKeyUp_GENERAL , OnCharAdded_REDIS_like , NULL , OnDbClickSymbolTree_REDIS , NULL , OnDbClickSymbolTree_REDIS , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_PYTHON , "doctype_PYTHON.conf" , ";*.py;*.pyw;*.pyx;*.pxd;*.pxi;" , "Python文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_PYTHON , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_LUA , "doctype_LUA.conf" , ";*.lua;" , "Lua文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_LUA , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_PERL , "doctype_PERL.conf" , ";*.pl;*.perl;" , "Perl文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_PERL , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_SH , "doctype_SH.conf" , ";*.sh;" , "Shell文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_SH , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_RUST , "doctype_RUST.conf" , ";*.rs;" , "Rust文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_RUST , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_RUBY , "doctype_RUBY.conf" , ";*.rb;" , "Ruby文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_RUBY , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_LISP , "doctype_LISP.conf" , ";*.lsp;" , "Lisp文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_LISP , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_ASM , "doctype_ASM.conf" , ";*.asm;" , "汇编文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_ASM , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_COBOL , "doctype_COBOL.conf" , ";*.cobol;*.cob;" , "Cobol文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_COBOL , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_HTML , "doctype_HTML.conf" , ";*.html;*.htm;*.shtml;*.xhtml;*.phtml;*.htt;*.htd;*.hta;*.asp;*.php;" , "HTML文件" , NULL , InitTabPageControlsAfterLoadFile_HTML , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_HTML_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_CSS , "doctype_CSS.conf" , ";*.css;" , "CSS文件" , NULL , InitTabPageControlsAfterLoadFile_CSS , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CSS_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_JAVASCRIPT , "doctype_JAVASCRIPT.conf" , ";*.js;*.es;*.ts;*.jse;*.jsm;*.mjs;*.qs;" , "JavaScript文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_JAVASCRIPT , NULL , OnKeyDown_GotoSymbolDefineLine , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_XML , "doctype_XML.conf" , ";*.xml;*.xsl;*.svg;*.xul;*.xsd;*.dtd;*.xslt;*.axl;*.xrc;*.rdf;" , "XML文件" , InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl , InitTabPageControlsAfterLoadFile_XML , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_XML_like , NULL , NULL , OnReloadSymbolTree_XML , OnDbClickSymbolTree_JSON , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_JSON , "doctype_JSON.conf" , ";*.json;*.eslintrc;*.jshintrc;*.jsonld;*.ipynb;*.babelrc;*.prettierrc;*.stylelintrc;*.jsonc;*.jscop;" , "JSON文件" , InitTabPageControlsBeforeLoadFile_CreateSymbolTreeCtl , InitTabPageControlsAfterLoadFile_JSON , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_JSON_like , NULL , NULL , OnReloadSymbolTree_JSON , OnDbClickSymbolTree_JSON , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_YAML , "doctype_YAML.conf" , ";*.yaml;*.yml;*.clang-format;*.clang-tidy;*.mir;*.apinotes;*.ifs;" , "YAML文件" , NULL , InitTabPageControlsAfterLoadFile_YAML , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_YAML_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_MAKEFILE , "doctype_MAKEFILE.conf" , ";makefile;Makefile;Makefile.gcc;*.mk;*.mak;configure;" , "Makefile文件" , NULL , InitTabPageControlsAfterLoadFile_MAKEFILE , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_MAKEFILE_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_CMAKE , "doctype_CMAKE.conf" , ";CMakeLists.txt;*.cmake;*.cmake.in;*.ctest;*.ctest.in;" , "CMake文件" , NULL , InitTabPageControlsAfterLoadFile_CMAKE , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CMAKE_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_MARKDOWN , "doctype_MARKDOWN.conf" , ";*.md;*.markdown;" , "Markdown文件" , NULL , InitTabPageControlsAfterLoadFile_MARKDOWN , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_MARKDOWN_like , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_LOG , "doctype_LOG.conf" , ";*.log;" , "Log文件" , NULL , InitTabPageControlsAfterLoadFile_LOG , NULL , NULL , NULL , NULL , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_PROPERTIES , "doctype_PROPERTIES.conf" , ";*.properties;*.ini;*.inf;*.cfg;*.cnf;*.conf;" , "Properties文件" , NULL , InitTabPageControlsAfterLoadFile_PROPERTIES , NULL , NULL , NULL , NULL , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_BATCH , "doctype_BATCH.conf" , ";*.bat;*.cmd;*.nt;" , "批处理文件" , NULL , InitTabPageControlsAfterLoadFile_BATCH , NULL , NULL , NULL , NULL , NULL , NULL , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_NIM , "doctype_NIM.conf" , ";*.nim;" , "Nim文件" , InitTabPageControlsBeforeLoadFile_IfSymbolReqularExp_CreateSymbolListCtl , InitTabPageControlsAfterLoadFile_NIM , NULL , NULL , OnKeyUp_GENERAL , OnCharAdded_CPP_like , OnReloadSymbolList_WithReqularExp , OnDbClickSymbolList_GetSymbolListItemAndJumpGotoLine , NULL , NULL , CleanTabPageControls_AllControls } ,
		{ DOCTYPE_END }
	} ;

struct 文档类型配置S *获取文档类型配置( char *pcExtname )
{
	char			p扩展名[ 1+_MAX_EXT+1 ] ;
	struct 文档类型配置S	*pst文档类型配置 = NULL ;

	memset( p扩展名 , 0x00 , sizeof(p扩展名) );
	_snprintf( p扩展名 , sizeof(p扩展名)-1 , ";*%s;" , pcExtname );
	for( pst文档类型配置 = 全_文档类型配置 ; pst文档类型配置->n文档的类型 != DOCTYPE_END ; pst文档类型配置++ )
	{
		if( strstr( pst文档类型配置->p文件扩展名 , p扩展名 ) )
			return pst文档类型配置;
	}

	return NULL;
}

#define 所支持的_文件的全部类型	"全部文件类型"
#define 所有_文件类型描述		"所有文件"
#define 所有文件_扩展名		"*.*"

char *获取文件对话框筛选器指针()
{
	size_t			文件对话框筛选器大小 ;
	struct 文档类型配置S	*pst文档类型配置 = NULL ;
	static char		*文件对话框的筛选器 = NULL ;
	char			*p = NULL ;

	if( 文件对话框的筛选器 )
		return 文件对话框的筛选器;

	文件对话框筛选器大小 = strlen(所支持的_文件的全部类型)+1+1+1 + 1 + 1 ;
	for( pst文档类型配置 = 全_文档类型配置 ; pst文档类型配置->n文档的类型 != DOCTYPE_END ; pst文档类型配置++ )
	{
		文件对话框筛选器大小 += strlen(pst文档类型配置->p文件扩展名+1) + strlen(pst文档类型配置->p文件扩展名+1) ;
	}
	for( pst文档类型配置 = 全_文档类型配置 ; pst文档类型配置->p文件扩展名 ; pst文档类型配置++ )
	{
		文件对话框筛选器大小 += strlen(pst文档类型配置->p文件类型描述)+1+1+(strlen(pst文档类型配置->p文件扩展名)-2)+1 + 1 + (strlen(pst文档类型配置->p文件扩展名)-2) + 1 ;
	}
	文件对话框筛选器大小 += strlen(所有_文件类型描述)+1+1+strlen(所有文件_扩展名)+1 + 1 + strlen(所有文件_扩展名) + 1 ;

	文件对话框筛选器大小++;

	文件对话框的筛选器 = (char*)malloc( 文件对话框筛选器大小 ) ;
	if( 文件对话框的筛选器 == NULL )
		return NULL;
	memset( 文件对话框的筛选器 , 0x00 , 文件对话框筛选器大小 );

	p = 文件对话框的筛选器 ;
	p += sprintf( p , "%s (" , 所支持的_文件的全部类型 ) ;
	for( pst文档类型配置 = 全_文档类型配置 ; pst文档类型配置->n文档的类型 != DOCTYPE_END ; pst文档类型配置++ )
	{
		p += sprintf( p , "%s" , pst文档类型配置->p文件扩展名+1 ) ;
	}
	p += sprintf( p , ")" ) ;
	p++;
	for( pst文档类型配置 = 全_文档类型配置 ; pst文档类型配置->n文档的类型 != DOCTYPE_END ; pst文档类型配置++ )
	{
		p += sprintf( p , "%s" , pst文档类型配置->p文件扩展名+1 ) ;
	}
	p++;
	for( pst文档类型配置 = 全_文档类型配置 ; pst文档类型配置->n文档的类型 != DOCTYPE_END ; pst文档类型配置++ )
	{
		p += sprintf( p , "%s (%.*s)" , pst文档类型配置->p文件类型描述 , (int)strlen(pst文档类型配置->p文件扩展名)-2 , pst文档类型配置->p文件扩展名+1 ) ;
		p++;
		p += sprintf( p , "%.*s" , (int)strlen(pst文档类型配置->p文件扩展名)-2 , pst文档类型配置->p文件扩展名+1 ) ;
		p++;
	}
	p += sprintf( p , "%s (%s)" , 所有_文件类型描述 , 所有文件_扩展名 ) ;
	p++;
	p += sprintf( p , "%s" , 所有文件_扩展名 ) ;
	p++;

	p++;

	if( p-文件对话框的筛选器 != 文件对话框筛选器大小 )
		return NULL;

	return 文件对话框的筛选器;
}

int BuildAutoCompletedShowTree( struct 文档类型配置S *pstFileExtnameMapper , char *word2 )
{
	char				*acWord2Dup = NULL ;
	char				*p1 = NULL ;
	struct AutoCompletedShowNode	*pstAutoCompletedShowNode = NULL ;

	acWord2Dup = _strdup(word2) ;
	if( acWord2Dup == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存用以存放自动完成单词临时缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -4;
	}

	p1 = strtok( acWord2Dup , " " ) ;
	while( p1 )
	{
		pstAutoCompletedShowNode = (struct AutoCompletedShowNode *)malloc( sizeof(struct AutoCompletedShowNode) ) ;
		if( pstAutoCompletedShowNode == NULL )
		{
			MessageBox(NULL, TEXT("不能分配内存用以存放自动完成显示结构"), TEXT("错误"), MB_ICONERROR | MB_OK);
			free( acWord2Dup );
			return -4;
		}
		memset( pstAutoCompletedShowNode , 0x00 , sizeof(struct AutoCompletedShowNode) );

		pstAutoCompletedShowNode->acAutoCompletedString = _strdup( p1 ) ;

		pstFileExtnameMapper->sAutoCompletedBufferSize += strlen(pstAutoCompletedShowNode->acAutoCompletedString)+1 ;

		LinkAutoCompletedShowTreeNode( & (pstFileExtnameMapper->stAutoCompletedShowTree) , pstAutoCompletedShowNode );

		p1 = strtok( NULL , " " ) ;
	}

	pstFileExtnameMapper->sAutoCompletedBufferSize++;

	free( acWord2Dup );

	return 0;
}

int ExpandAutoCompletedShowTreeToBuffer( struct 文档类型配置S *pstFileExtnameMapper )
{
	if( pstFileExtnameMapper->acAutoCompletedBuffer )
		free( pstFileExtnameMapper->acAutoCompletedBuffer );
	pstFileExtnameMapper->acAutoCompletedBuffer = (char*)malloc( pstFileExtnameMapper->sAutoCompletedBufferSize ) ;
	if( pstFileExtnameMapper->acAutoCompletedBuffer == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存用以存放自动完成缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -13;
	}
	memset( pstFileExtnameMapper->acAutoCompletedBuffer , 0x00 , pstFileExtnameMapper->sAutoCompletedBufferSize );

	if( pstFileExtnameMapper->acAutoCompletedShowBuffer )
		free( pstFileExtnameMapper->acAutoCompletedShowBuffer );
	pstFileExtnameMapper->acAutoCompletedShowBuffer = (char*)malloc( pstFileExtnameMapper->sAutoCompletedBufferSize ) ;
	if( pstFileExtnameMapper->acAutoCompletedShowBuffer == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存用以存放自动完成显示缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -13;
	}
	memset( pstFileExtnameMapper->acAutoCompletedShowBuffer , 0x00 , pstFileExtnameMapper->sAutoCompletedBufferSize );

	struct rb_node			*nodeTravel = NULL ;
	struct rb_node			*nodeLast = NULL ;
	struct AutoCompletedShowNode	*pstAutoCompletedShowNode = NULL ;
	char				*p = pstFileExtnameMapper->acAutoCompletedBuffer ;
	int				offset = 0 ;
	int				len ;

	nodeTravel = rb_first( & (pstFileExtnameMapper->stAutoCompletedShowTree.treeAutoCompletedShow) ) ;
	nodeLast = rb_last( & (pstFileExtnameMapper->stAutoCompletedShowTree.treeAutoCompletedShow) ) ;
	while( nodeTravel )
	{
		pstAutoCompletedShowNode = rb_entry( nodeTravel , struct AutoCompletedShowNode , nodeAutoCompletedShow ) ;

		len = sprintf( p , "%s " , pstAutoCompletedShowNode->acAutoCompletedString );

		pstAutoCompletedShowNode->begin = offset ;
		pstAutoCompletedShowNode->end = offset + len ;

		p += len ;
		offset += len ;

		if( nodeTravel == nodeLast )
			break;
		nodeTravel = rb_next( nodeTravel ) ;
	}

	return 0;
}

int BuildAutoCompletedShowTree2( struct 文档类型配置S *pstFileExtnameMapper , char *word2 )
{
	char				*acWord2Dup2 = NULL ;
	char				*p1 = NULL ;
	struct AutoCompletedShowNode	*pstAutoCompletedShowNode2 = NULL ;

	acWord2Dup2 = _strdup(word2) ;
	if( acWord2Dup2 == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存用以存放自动完成单词临时缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -4;
	}

	p1 = strtok( acWord2Dup2 , " " ) ;
	while( p1 )
	{
		pstAutoCompletedShowNode2 = (struct AutoCompletedShowNode *)malloc( sizeof(struct AutoCompletedShowNode) ) ;
		if( pstAutoCompletedShowNode2 == NULL )
		{
			MessageBox(NULL, TEXT("不能分配内存用以存放自动完成显示结构"), TEXT("错误"), MB_ICONERROR | MB_OK);
			free( acWord2Dup2 );
			return -4;
		}
		memset( pstAutoCompletedShowNode2 , 0x00 , sizeof(struct AutoCompletedShowNode) );

		pstAutoCompletedShowNode2->acAutoCompletedString = _strdup( p1 ) ;

		pstFileExtnameMapper->sAutoCompletedBufferSize2 += strlen(pstAutoCompletedShowNode2->acAutoCompletedString)+1 ;

		LinkAutoCompletedShowTreeNode( & (pstFileExtnameMapper->stAutoCompletedShowTree2) , pstAutoCompletedShowNode2 );

		p1 = strtok( NULL , " " ) ;
	}

	pstFileExtnameMapper->sAutoCompletedBufferSize2++;

	free( acWord2Dup2 );

	return 0;
}

int ExpandAutoCompletedShowTreeToBuffer2( struct 文档类型配置S *pstFileExtnameMapper )
{
	if( pstFileExtnameMapper->acAutoCompletedBuffer2 )
		free( pstFileExtnameMapper->acAutoCompletedBuffer2 );
	pstFileExtnameMapper->acAutoCompletedBuffer2 = (char*)malloc( pstFileExtnameMapper->sAutoCompletedBufferSize2 ) ;
	if( pstFileExtnameMapper->acAutoCompletedBuffer2 == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存用以存放自动完成缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -13;
	}
	memset( pstFileExtnameMapper->acAutoCompletedBuffer2 , 0x00 , pstFileExtnameMapper->sAutoCompletedBufferSize );

	if( pstFileExtnameMapper->acAutoCompletedShowBuffer2 )
		free( pstFileExtnameMapper->acAutoCompletedShowBuffer2 );
	pstFileExtnameMapper->acAutoCompletedShowBuffer2 = (char*)malloc( pstFileExtnameMapper->sAutoCompletedBufferSize2 ) ;
	if( pstFileExtnameMapper->acAutoCompletedShowBuffer2 == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存用以存放自动完成显示缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -13;
	}
	memset( pstFileExtnameMapper->acAutoCompletedShowBuffer2 , 0x00 , pstFileExtnameMapper->sAutoCompletedBufferSize2 );

	struct rb_node			*nodeTravel2 = NULL ;
	struct rb_node			*nodeLast2 = NULL ;
	struct AutoCompletedShowNode	*pstAutoCompletedShowNode2 = NULL ;
	char				*p = pstFileExtnameMapper->acAutoCompletedBuffer2 ;
	int				offset = 0 ;
	int				len ;

	nodeTravel2 = rb_first( & (pstFileExtnameMapper->stAutoCompletedShowTree2.treeAutoCompletedShow) ) ;
	nodeLast2 = rb_last( & (pstFileExtnameMapper->stAutoCompletedShowTree2.treeAutoCompletedShow) ) ;
	while( nodeTravel2 )
	{
		pstAutoCompletedShowNode2 = rb_entry( nodeTravel2 , struct AutoCompletedShowNode , nodeAutoCompletedShow ) ;

		len = sprintf( p , "%s " , pstAutoCompletedShowNode2->acAutoCompletedString );

		pstAutoCompletedShowNode2->begin = offset ;
		pstAutoCompletedShowNode2->end = offset + len ;

		p += len ;
		offset += len ;

		if( nodeTravel2 == nodeLast2 )
			break;
		nodeTravel2 = rb_next( nodeTravel2 ) ;
	}

	return 0;
}

int LoadLexerConfigFile( const char *filename , struct 文档类型配置S *pstFileExtnameMapper , char *filebuf )
{
	char				pathfilename[ MAX_PATH ] ;
	FILE				*fp = NULL ;
	char				*pcItemName = NULL ;
	char				*pcItemValue = NULL ;
	char				*p1 = NULL ;
	char				*pcResult = NULL ;
	struct CallTipShowNode		*pstCallTipShowNode = NULL ;
	int				nret = 0 ;

	memset( pathfilename , 0x00 , sizeof(pathfilename) );
	_snprintf( pathfilename , sizeof(pathfilename)-1 , "%s\\conf\\%s" , 全_模块路径名 , pstFileExtnameMapper->p文件类型的配置文件名 );
	fp = fopen( pathfilename , "r" ) ;
	if( fp == NULL )
		return 1;

	while(1)
	{
		memset( filebuf , 0x00 , FILEBUF_MAX );
		if( fgets( filebuf , FILEBUF_MAX-1 , fp ) == NULL )
			break;

		nret = DecomposeConfigFileBuffer( filebuf , & pcItemName , & pcItemValue ) ;
		if( nret > 0 )
			continue;
		else if( nret < 0 )
			return nret;

		if( strcmp( pcItemName , "keywords.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "keywords2.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords2 = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords2 == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "keywords3.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords3 = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords3 == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "keywords4.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords4 = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords4 == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "keywords5.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords5 = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords5 == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "keywords6.set" ) == 0 )
		{
			pstFileExtnameMapper->keywords6 = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->keywords6 == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "autocomplete.set" ) == 0 )
		{
			pstFileExtnameMapper->autocomplete_set = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->autocomplete_set == NULL )
			{
				MessageBox(NULL, TEXT("不能分配内存用以存放autocomplete.set"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}

			DestroyAutoCompletedShowTree( & (pstFileExtnameMapper->stAutoCompletedShowTree) );
			pstFileExtnameMapper->sAutoCompletedBufferSize = 0 ;

			nret = BuildAutoCompletedShowTree( pstFileExtnameMapper , pstFileExtnameMapper->autocomplete_set ) ;
			if( nret )
				return nret;

			nret = ExpandAutoCompletedShowTreeToBuffer( pstFileExtnameMapper ) ;
			if( nret )
				return nret;
		}
		else if( strcmp( pcItemName , "autocomplete2.set" ) == 0 )
		{
			pstFileExtnameMapper->autocomplete2_set = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->autocomplete2_set == NULL )
			{
				MessageBox(NULL, TEXT("不能分配内存用以存放autocomplete.set"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}

			DestroyAutoCompletedShowTree( & (pstFileExtnameMapper->stAutoCompletedShowTree2) );
			pstFileExtnameMapper->sAutoCompletedBufferSize2 = 0 ;

			nret = BuildAutoCompletedShowTree2( pstFileExtnameMapper , pstFileExtnameMapper->autocomplete2_set ) ;
			if( nret )
				return nret;

			nret = ExpandAutoCompletedShowTreeToBuffer2( pstFileExtnameMapper ) ;
			if( nret )
				return nret;
		}
		else if( strcmp( pcItemName , "calltip.add" ) == 0 )
		{
			pstCallTipShowNode = (struct CallTipShowNode *)malloc( sizeof(struct CallTipShowNode) ) ;
			if( pstCallTipShowNode == NULL )
				return -4;
			memset( pstCallTipShowNode , 0x00 , sizeof(struct CallTipShowNode) );

			p1 = strchr( pcItemValue , '|' ) ;
			if( p1 == NULL )
				continue;

			(*p1) = '\0' ;
			pstCallTipShowNode->acCallTipFuncName = _strdup( pcItemValue ) ;
			pstCallTipShowNode->acCallTipFuncDesc = _strdup( p1+1 ) ;
			FoldNewLineString( pstCallTipShowNode->acCallTipFuncDesc );

			LinkCallTipShowTreeNode( & (pstFileExtnameMapper->stCallTipShowTree) , pstCallTipShowNode );
		}
		else if( strcmp( pcItemName , "symbol_reqular_exp" ) == 0 )
		{
			pstFileExtnameMapper->acSymbolReqularExp = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->acSymbolReqularExp == NULL )
				return -3;
		}
		else if( strcmp( pcItemName , "file.extnames" ) == 0 )
		{
			pstFileExtnameMapper->p文件扩展名 = _strdup( pcItemValue ) ;
			if( pstFileExtnameMapper->p文件扩展名 == NULL )
				return -3;
		}
	}

	return 0;
}

int BeginReloadSymbolListOrTreeThread()
{
	if( 重新加载符号列表或树线程处理程序 == NULL )
	{
		重新加载符号列表或树线程处理程序 = _beginthread( 重新加载符号列表或树线程条目 , 0 , NULL ) ;
		if( 重新加载符号列表或树线程处理程序 == -1L )
		{
			重新加载符号列表或树线程处理程序  ;
			错误框( "创建定时刷新符号列表或符号树线程失败" );
			return -1;
		}
	}

	return 0;
}

void 重新加载符号列表或树线程条目( void *param )
{
	int		nret = 0 ;

	while(1)
	{
		HANDLE 互斥锁 = CreateMutex( NULL , TRUE , "RELOAD_SYMBOLLIST_OR_SYMBOLTREE_MUTEX" ) ;
		if( 互斥锁 )
		{
			if( 全_当前选项卡页面节点 && 全_当前选项卡页面节点->pst文档类型配置 )
			{
				if( 全_当前选项卡页面节点->pst文档类型配置->pfuncOnReloadSymbolList )
				{
					全_当前选项卡页面节点->pst文档类型配置->pfuncOnReloadSymbolList( 全_当前选项卡页面节点 );
				}

				if( 全_当前选项卡页面节点->pst文档类型配置->pfuncOnReloadSymbolTree )
				{
					全_当前选项卡页面节点->pst文档类型配置->pfuncOnReloadSymbolTree( 全_当前选项卡页面节点 );
				}

				if( 全_当前选项卡页面节点->pst文档类型配置->n文档的类型 == DOCTYPE_SQL
					|| 全_当前选项卡页面节点->pst文档类型配置->n文档的类型 == DOCTYPE_XML
					|| 全_当前选项卡页面节点->pst文档类型配置->n文档的类型 == DOCTYPE_JSON )
				{
					CloseHandle(互斥锁);
					break;
				}
			}

			CloseHandle(互斥锁);
		}

		if( 全_风格主配置.重新加载符号列表或树间隔 <= 0 )
			break;

		Sleep( 全_风格主配置.重新加载符号列表或树间隔*1000 );
	}

	重新加载符号列表或树线程处理程序 = 0 ;
	_endthread();
}

void 更新文件类型菜单()
{
	HMENU		hRootMenu ;
	HMENU		hMenu_VIEW ;
	HMENU		hMenu_FILETYPE ;
	int		count ;
	int		index ;

	BOOL		bret ;

	hRootMenu = GetMenu(全_主窗口句柄) ;
	hMenu_VIEW = GetSubMenu( hRootMenu , 3 ) ;
	hMenu_FILETYPE = GetSubMenu( hMenu_VIEW , 2 ) ;
	count = GetMenuItemCount( hMenu_FILETYPE ) ;
	for( index = 0 ; index < count ; index++ )
	{
		bret = DeleteMenu( hMenu_FILETYPE , 0 , MF_BYPOSITION );
	}

	struct 文档类型配置S	*pst文档类型配置 = NULL ;

	for( index = 0 , pst文档类型配置 = 全_文档类型配置 ; pst文档类型配置->n文档的类型 != DOCTYPE_END ; index++ , pst文档类型配置++ )
	{
		bret = AppendMenu( hMenu_FILETYPE , MF_POPUP|MF_STRING , IDM_VIEW_SWITCH_FILETYPE_BASE+index , pst文档类型配置->p文件类型描述 );
	}

	return;
}
