#include "framework.h"

struct WindowTheme		g_astWindowTheme[ 视觉样式最大数量M ] ;
int				g_nWindowThemeCount = 0 ;
struct WindowTheme		*g_pstWindowTheme = NULL ;
 struct StyleTheme		g_stStyleTheme = { 0 } ;
static struct StyleTheme	g_stStyleThemeDialogData = { 0 } ;

HBRUSH				全_笔刷常用控件深色 ;

HWND		hwndLineNumberStatic ;
HFONT		fontLineNumberStatic ;

HWND		hwndFoldMarginStatic ;
HFONT		fontFoldMarginStatic ;

HWND		hwndTextStatic ;
HFONT		fontTextStatic ;

HWND		hwndCaretLineStatic ;
HFONT		fontCaretLineStatic ;

HWND		hwndIndicatorStatic ;
HFONT		fontIndicatorStatic ;

HWND		hwndKeywordStatic ;
HFONT		fontKeywordStatic ;

HWND		hwndKeyword2Static ;
HFONT		fontKeyword2Static ;

HWND		hwndStringStatic ;
HFONT		fontStringStatic ;

HWND		hwndCharacterStatic ;
HFONT		fontCharacterStatic ;

HWND		hwndNumberStatic ;
HFONT		fontNumberStatic ;

HWND		hwndOperatorrStatic ;
HFONT		fontOperatorrStatic ;

HWND		hwndPreprocessorStatic ;
HFONT		fontPreprocessorStatic ;

HWND		hwndCommentStatic ;
HFONT		fontCommentStatic ;

HWND		hwndCommentLineStatic ;
HFONT		fontCommentLineStatic ;

HWND		hwndCommentDocStatic ;
HFONT		fontCommentDocStatic ;

HWND		hwndTagsStatic ;
HFONT		fontTagsStatic ;

HWND		hwndUnknowTagsStatic ;
HFONT		fontUnknowTagsStatic ;

HWND		hwndAttributesStatic ;
HFONT		fontAttributesStatic ;

HWND		hwndUnknowAttributesStatic ;
HFONT		fontUnknowAttributesStatic ;

HWND		hwndEntitiesStatic ;
HFONT		fontEntitiesStatic ;

HWND		hwndTagEndsStatic ;
HFONT		fontTagEndsStatic ;

HWND		hwndCDataStatic ;
HFONT		fontCDataStatic ;

HWND		hwndPhpSectionStatic ;
HFONT		fontPhpSectionStatic ;

HWND		hwndAspSectionStatic ;
HFONT		fontAspSectionStatic ;

/*
#define SET_STYLETHEME_DEFAULT(_st_memb_,_font_name_,_font_size_,_color_,_bgcolor_,_bold_) \
	strcpy( pstStyleTheme->_st_memb_.font , _font_name_ ); \
	pstStyleTheme->_st_memb_.fontsize = _font_size_ ; \
	pstStyleTheme->_st_memb_.color = _color_ ; \
	pstStyleTheme->_st_memb_.bgcolor = _bgcolor_ ; \
	pstStyleTheme->_st_memb_.bold = _bold_ ; \

void SetStyleThemeDefault( struct StyleTheme *pstStyleTheme )
{
	SET_STYLETHEME_DEFAULT( linenumber , "Courier New" , 10 , 0x00FFFFFF , 0x00888888 , FALSE )
	SET_STYLETHEME_DEFAULT( foldmargin , "Courier New" , 10 , 0x00666666 , 0x00666666 , FALSE )

	SET_STYLETHEME_DEFAULT( text , "Courier New" , 10 , 0x00FFFFFF , 0x00444444 , FALSE )
	SET_STYLETHEME_DEFAULT( caretline , "Courier New" , 10 , 0x0 , 0x00C08080 , FALSE )
	SET_STYLETHEME_DEFAULT( indicator , "Courier New" , 10 , 0x00FFFFFF , 0x00EEEEEE , FALSE )

	SET_STYLETHEME_DEFAULT( keywords , "Courier New" , 10 , 0x00FF8000 , 0x0 , TRUE )
	SET_STYLETHEME_DEFAULT( keywords2 , "Courier New" , 10 , 0x00C08000 , 0x0 , TRUE )
	SET_STYLETHEME_DEFAULT( string , "Courier New" , 10 , 0x00C080FF , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( character , "Courier New" , 10 , 0x00400080 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( number , "Courier New" , 10 , 0x00C080FF , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( operatorr , "Courier New" , 10 , 0x00B0B0B0 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( preprocessor , "Courier New" , 10 , 0x000000FF , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( comment , "Courier New" , 10 , 0x0000FF00 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( commentline , "Courier New" , 10 , 0x0000FF00 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( commentdoc , "Courier New" , 10 , 0x0000FF00 , 0x0 , FALSE )

	SET_STYLETHEME_DEFAULT( tags , "Courier New" , 10 , 0x00FF8000 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( unknowtags , "Courier New" , 10 , 0x00804000 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( attributes , "Courier New" , 10 , 0x0080FF80 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( unknowattributes , "Courier New" , 10 , 0x00808040 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( entities , "Courier New" , 10 , 0x00800080 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( tagends , "Courier New" , 10 , 0x00000080 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( cdata , "Courier New" , 10 , 0x00008000 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( phpsection , "Courier New" , 10 , 0x00FF0000 , 0x0 , FALSE )
	SET_STYLETHEME_DEFAULT( aspsection , "Courier New" , 10 , 0x00FFEFBF , 0x0 , FALSE )

	return;
}
*/

#define LOAD_STYLETHEME(_st_memb_) \
	{ \
		if( strcmp( pcItemName , #_st_memb_ ".font" ) == 0 ) \
		{ \
			if( pcItemValue[0] ) \
				strncpy( pstWindowTheme->stStyleTheme._st_memb_.font , pcItemValue , sizeof(pstWindowTheme->stStyleTheme._st_memb_.font)-1 ) ; \
		} \
		else if( strcmp( pcItemName , #_st_memb_ ".fontsize" ) == 0 ) \
		{ \
			if( pcItemValue[0] ) \
				pstWindowTheme->stStyleTheme._st_memb_.fontsize = atoi( pcItemValue ) ; \
		} \
		else if( strcmp( pcItemName , #_st_memb_ ".color" ) == 0 ) \
		{ \
			pcResult = NULL ; \
			pstWindowTheme->stStyleTheme._st_memb_.color = strtol( pcItemValue , & pcResult , 16 ) ; \
			if( (*pcResult) ) \
				return -11; \
		} \
		else if( strcmp( pcItemName , #_st_memb_ ".bgcolor" ) == 0 ) \
		{ \
			pcResult = NULL ; \
			pstWindowTheme->stStyleTheme._st_memb_.bgcolor = strtol( pcItemValue , & pcResult , 16 ) ; \
			if( (*pcResult) ) \
				return -11; \
		} \
		else if( strcmp( pcItemName , #_st_memb_ ".bold" ) == 0 ) \
		{ \
			if( _stricmp( pcItemValue , "TRUE" ) == 0 ) \
				pstWindowTheme->stStyleTheme._st_memb_.bold = TRUE ; \
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 ) \
				pstWindowTheme->stStyleTheme._st_memb_.bold = FALSE ; \
			else \
				return -12; \
		} \
	} \

int LoadStyleThemeConfigFile( struct WindowTheme *pstWindowTheme , char *filebuf )
{
	FILE				*fp = NULL ;
	char				*pcItemName = NULL ;
	char				*pcItemValue = NULL ;
	char				*p1 = NULL ;
	char				*pcResult = NULL ;
	struct CallTipShowNode		*pstCallTipShowNode = NULL ;
	int				nret = 0 ;

	fp = fopen( pstWindowTheme->acPathFilename , "r" ) ;
	if( fp == NULL )
		return -2;

	while(1)
	{
		memset( filebuf , 0x00 , FILEBUF_MAX );
		if( fgets( filebuf , FILEBUF_MAX-1 , fp ) == NULL )
			break;

		nret = DecomposeConfigFileBuffer( filebuf , & pcItemName , & pcItemValue ) ;
		if( nret > 0 )
			continue;
		else if( nret < 0 )
			return nret;

		LOAD_STYLETHEME(linenumber)
		LOAD_STYLETHEME(foldmargin)

		LOAD_STYLETHEME(text)
		LOAD_STYLETHEME(caretline)
		LOAD_STYLETHEME(indicator)

		LOAD_STYLETHEME(keywords)
		LOAD_STYLETHEME(keywords2)
		LOAD_STYLETHEME(string)
		LOAD_STYLETHEME(character)
		LOAD_STYLETHEME(number)
		LOAD_STYLETHEME(operatorr)
		LOAD_STYLETHEME(preprocessor)
		LOAD_STYLETHEME(comment)
		LOAD_STYLETHEME(commentline)
		LOAD_STYLETHEME(commentdoc)

		LOAD_STYLETHEME(tags)
		LOAD_STYLETHEME(unknowtags)
		LOAD_STYLETHEME(attributes)
		LOAD_STYLETHEME(unknowattributes)
		LOAD_STYLETHEME(entities)
		LOAD_STYLETHEME(tagends)
		LOAD_STYLETHEME(cdata)
		LOAD_STYLETHEME(phpsection)
		LOAD_STYLETHEME(aspsection)
	}

	return 0;
}

void 更新窗口主题菜单()
{
	HMENU		hRootMenu ;
	HMENU		hMenu_VIEW ;
	HMENU		hMenu_THEMES ;
	int		count ;
	int		index ;

	BOOL		bret ;

	hRootMenu = GetMenu(全_主窗口句柄) ;
	hMenu_VIEW = GetSubMenu( hRootMenu , 3 ) ;
	hMenu_THEMES = GetSubMenu( hMenu_VIEW , 4 ) ;
	count = GetMenuItemCount( hMenu_THEMES ) ;
	for( index = 0 ; index < count ; index++ )
	{
		bret = DeleteMenu( hMenu_THEMES , 0 , MF_BYPOSITION );
	}

	for( index = 0 ; index < g_nWindowThemeCount ; index++ )
	{
		bret = AppendMenu( hMenu_THEMES , MF_POPUP|MF_STRING , IDM_VIEW_SWITCH_STYLETHEME_BASE+index , g_astWindowTheme[index].acThemeName );
	}

	return;
}

int LoadAllStyleThemeConfigFiles( char *filebuf )
{
	char		acFindPathFilename[ MAX_PATH ] ;
	WIN32_FIND_DATA	stFindFileData ;
	HANDLE		hFindFile ;
	int		nFileIndex ;

	int		nret = 0 ;

	memset( g_astWindowTheme , 0x00 , sizeof(g_astWindowTheme) );

	snprintf( g_astWindowTheme[0].acPathFilename , sizeof(g_astWindowTheme[0].acPathFilename)-1 , "%s\\conf\\styletheme.conf" , 全_模块路径名 );
	strcpy( g_astWindowTheme[0].acThemeName , "(DEFAULT)" );
	nret = LoadStyleThemeConfigFile( &(g_astWindowTheme[0]) , filebuf ) ;
	if( nret )
		return nret;

	nFileIndex = 0 ;

	memset( acFindPathFilename , 0x00 , sizeof(acFindPathFilename) );
	snprintf( acFindPathFilename , sizeof(acFindPathFilename)-1 , "%s\\conf\\styletheme_*.conf" , 全_模块路径名 );
	hFindFile = FindFirstFile( acFindPathFilename , & stFindFileData ) ;
	if( hFindFile == INVALID_HANDLE_VALUE )
		return 0;

	do
	{
		if( strcmp(stFindFileData.cFileName,".") == 0 || strcmp(stFindFileData.cFileName,"..") == 0 )
			continue;

		if( stFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			;
		}
		else
		{
			nFileIndex++;
			if( nFileIndex >= 视觉样式最大数量M )
				break;

			snprintf( g_astWindowTheme[nFileIndex].acPathFilename , sizeof(g_astWindowTheme[nFileIndex].acPathFilename)-1 , "%s\\conf\\%s" , 全_模块路径名 , stFindFileData.cFileName );
			sscanf( stFindFileData.cFileName+(sizeof("styletheme_")-1) , "%[^.]" , g_astWindowTheme[nFileIndex].acThemeName );
			nret = LoadStyleThemeConfigFile( &(g_astWindowTheme[nFileIndex]) , filebuf ) ;
			if( nret )
				return nret;

			if( strcmp( 全_风格主配置.窗口的主题 , g_astWindowTheme[nFileIndex].acThemeName ) == 0 )
				g_pstWindowTheme = & (g_astWindowTheme[nFileIndex]) ;
		}
	}
	while( FindNextFile( hFindFile , & stFindFileData ) );

	FindClose( hFindFile );

	nFileIndex++;
	g_nWindowThemeCount = nFileIndex ;

	if( g_pstWindowTheme == NULL )
		g_pstWindowTheme = & (g_astWindowTheme[0]) ;

	全_笔刷常用控件深色 = CreateSolidBrush( g_pstWindowTheme->stStyleTheme.text.bgcolor ) ;

	return 0;
}

#define SAVE_STYLETHEME(_st_memb_) \
	fprintf( fp , #_st_memb_ ".font = " ); ExpandConfigStringItemValue( fp , g_pstWindowTheme->stStyleTheme._st_memb_.font );\
	fprintf( fp , #_st_memb_ ".fontsize = %d\n" , g_pstWindowTheme->stStyleTheme._st_memb_.fontsize ); \
	fprintf( fp , #_st_memb_ ".color = 0x%08X\n" , g_pstWindowTheme->stStyleTheme._st_memb_.color ); \
	fprintf( fp , #_st_memb_ ".bgcolor = 0x%08X\n" , g_pstWindowTheme->stStyleTheme._st_memb_.bgcolor ); \
	if( g_pstWindowTheme->stStyleTheme._st_memb_.bold == TRUE ) \
		fprintf( fp , #_st_memb_ ".bold = TRUE\n" ); \
	else if( g_pstWindowTheme->stStyleTheme._st_memb_.bold == FALSE ) \
		fprintf( fp , #_st_memb_ ".bold = FALSE\n" ); \

int 保存样式主题配置文件()
{
	FILE		*fp = NULL ;

	fp = fopen( g_pstWindowTheme->acPathFilename , "w" ) ;
	if( fp == NULL )
		return -1;

	SAVE_STYLETHEME(linenumber)
	SAVE_STYLETHEME(foldmargin)

	SAVE_STYLETHEME(text)
	SAVE_STYLETHEME(caretline)
	SAVE_STYLETHEME(indicator)

	SAVE_STYLETHEME(keywords)
	SAVE_STYLETHEME(keywords2)
	SAVE_STYLETHEME(string)
	SAVE_STYLETHEME(character)
	SAVE_STYLETHEME(number)
	SAVE_STYLETHEME(operatorr)
	SAVE_STYLETHEME(preprocessor)
	SAVE_STYLETHEME(comment)
	SAVE_STYLETHEME(commentline)
	SAVE_STYLETHEME(commentdoc)

	SAVE_STYLETHEME(tags)
	SAVE_STYLETHEME(unknowtags)
	SAVE_STYLETHEME(attributes)
	SAVE_STYLETHEME(unknowattributes)
	SAVE_STYLETHEME(entities)
	SAVE_STYLETHEME(tagends)
	SAVE_STYLETHEME(cdata)
	SAVE_STYLETHEME(phpsection)
	SAVE_STYLETHEME(aspsection)

	fclose( fp );

	return 0;
}

int CopyNewThemeStyle( char *acStyleTheme )
{
	memset( & (g_astWindowTheme[g_nWindowThemeCount]) , 0x00 , sizeof(struct WindowTheme) );
	snprintf( g_astWindowTheme[g_nWindowThemeCount].acPathFilename , sizeof(g_astWindowTheme[g_nWindowThemeCount].acPathFilename)-1 , "%s\\conf\\styletheme_%s.conf" , 全_模块路径名 , acStyleTheme );
	strncpy( g_astWindowTheme[g_nWindowThemeCount].acThemeName , acStyleTheme , sizeof(g_astWindowTheme[g_nWindowThemeCount].acThemeName)-1 );
	memcpy( & (g_astWindowTheme[g_nWindowThemeCount].stStyleTheme) , & (g_pstWindowTheme->stStyleTheme) , sizeof(struct StyleTheme) );

	FILE *fp = fopen( g_astWindowTheme[g_nWindowThemeCount].acPathFilename , "r" ) ;
	if( fp )
	{
		错误框( "主题方案配置文件[%s]已存在" , g_astWindowTheme[g_nWindowThemeCount].acPathFilename );
		fclose( fp );
		return -1;
	}

	g_pstWindowTheme = & (g_astWindowTheme[g_nWindowThemeCount]) ;

	g_nWindowThemeCount++;

	保存样式主题配置文件();

	更新窗口主题菜单();

	OnViewModifyThemeStyle();

	return 0;
}

static int ChooseStyleFont( char *font , int *fontsize , BOOL *bold )
{
	CHOOSEFONT		cf ;
	LOGFONT			lf ;
	BOOL			bret ;

	memset( & lf , 0x00 , sizeof(LOGFONT) );
	strcpy( lf.lfFaceName , font );
	lf.lfWeight = (*bold)?FW_BOLD:FW_NORMAL ;
	lf.lfHeight = -MulDiv( (*fontsize) , GetDeviceCaps(GetDC(全_主窗口句柄),LOGPIXELSY) , 72 ) ;

	memset( & cf , 0x00 , sizeof(CHOOSEFONT) );
	cf.lStructSize = sizeof(CHOOSEFONT) ;
	cf.lpLogFont = & lf ;
	cf.Flags = CF_INITTOLOGFONTSTRUCT ;
	cf.nFontType = SCREEN_FONTTYPE ;
	bret = ChooseFont( & cf ) ;
	if( bret == FALSE )
		return 1;

	strcpy( font , lf.lfFaceName );
	(*fontsize) = cf.iPointSize / 10 ;
	(*bold) = (lf.lfWeight==FW_BOLD) ;

	return 0;
}

int ChooseStyleTextColor( HWND hwnd , int *color )
{
	CHOOSECOLOR	cc ;
	COLORREF	cr ;
	COLORREF	crs[16] ;
	BOOL		bret ;

	memset( & cr , 0x00 , sizeof(COLORREF) );
	cr = *(color) ;
	memset( & crs , 0x00 , sizeof(crs) );

	cc.lStructSize = sizeof(CHOOSECOLOR) ;
	cc.hwndOwner = hwnd ;
	cc.rgbResult = cr ;
	cc.lpCustColors = (LPDWORD) crs ;
	cc.Flags = CC_RGBINIT ;
	bret = ChooseColor( & cc ) ;
	if( bret == TRUE )
	{
		(*color) = cc.rgbResult ;
	}

	return 0;
}

#define CREATE_STYLETHEME_FONT(_st_memb_,_idc_static_,_hwnd_handle_name_,_font_handle_name_) \
	_hwnd_handle_name_ = GetDlgItem( hDlg , _idc_static_ ) ; \
	_font_handle_name_ = CreateFont(-g_stStyleThemeDialogData._st_memb_.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_stStyleThemeDialogData._st_memb_.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_stStyleThemeDialogData._st_memb_.font); \
	SendMessage( _hwnd_handle_name_ , WM_SETFONT , (WPARAM)_font_handle_name_ , FALSE ); \

#define SET_STATIC_TEXTCOLOR(_hwnd_static_,_st_memb_) \
	if( (HWND)lParam == _hwnd_static_ ) \
	{ \
		SetTextColor( (HDC)wParam , g_stStyleThemeDialogData._st_memb_.color ); \
	} \

#define SYNC_FONT(_st_memb_,_hwnd_handle_name_,_font_handle_name_) \
	strcpy( g_stStyleThemeDialogData._st_memb_.font , g_stStyleThemeDialogData.text.font ); \
	g_stStyleThemeDialogData._st_memb_.fontsize = g_stStyleThemeDialogData.text.fontsize ; \
	DeleteObject( _font_handle_name_ ); \
	_font_handle_name_ = CreateFont(-g_stStyleThemeDialogData._st_memb_.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_stStyleThemeDialogData._st_memb_.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_stStyleThemeDialogData._st_memb_.font); \
	SendMessage( _hwnd_handle_name_ , WM_SETFONT , (WPARAM)_font_handle_name_ , FALSE ); \
	InvalidateRect( _hwnd_handle_name_ , NULL , TRUE ); \

#define PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(_st_memb_,_hwnd_handle_name_,_font_handle_name_,_idc_setfont_button_,_idc_settextcolor_button_) \
	if( wmId == _idc_setfont_button_ ) \
	{ \
		ChooseStyleFont( g_stStyleThemeDialogData._st_memb_.font , & (g_stStyleThemeDialogData._st_memb_.fontsize) , & (g_stStyleThemeDialogData._st_memb_.bold) ); \
		DeleteObject( _font_handle_name_ ); \
		_font_handle_name_ = CreateFont(-g_stStyleThemeDialogData._st_memb_.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_stStyleThemeDialogData._st_memb_.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_stStyleThemeDialogData._st_memb_.font); \
		SendMessage( _hwnd_handle_name_ , WM_SETFONT , (WPARAM)_font_handle_name_ , FALSE ); \
		InvalidateRect( _hwnd_handle_name_ , NULL , TRUE ); \
	} \
	else if( wmId == _idc_settextcolor_button_ ) \
	{ \
		ChooseStyleTextColor( hDlg , & (g_stStyleThemeDialogData._st_memb_.color) ); \
		InvalidateRect( _hwnd_handle_name_ , NULL , TRUE ); \
	} \

INT_PTR CALLBACK StyleThemeWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	int		wmId ;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		memcpy( & g_stStyleThemeDialogData , & (g_pstWindowTheme->stStyleTheme) , sizeof(struct StyleTheme) );

		CREATE_STYLETHEME_FONT(linenumber,IDC_LINENUMBER_STATIC,hwndLineNumberStatic,fontLineNumberStatic)
		CREATE_STYLETHEME_FONT(foldmargin,IDC_FOLDMARGIN_STATIC,hwndFoldMarginStatic,fontFoldMarginStatic)

		CREATE_STYLETHEME_FONT(text,IDC_TEXT_STATIC,hwndTextStatic,fontTextStatic)
		CREATE_STYLETHEME_FONT(caretline,IDC_CARETLINE_STATIC,hwndCaretLineStatic,fontCaretLineStatic)
		CREATE_STYLETHEME_FONT(indicator,IDC_INDICATOR_STATIC,hwndIndicatorStatic,fontIndicatorStatic)

		CREATE_STYLETHEME_FONT(keywords,IDC_KEYWORDS_STATIC,hwndKeywordStatic,fontKeywordStatic)
		CREATE_STYLETHEME_FONT(keywords2,IDC_KEYWORDS2_STATIC,hwndKeyword2Static,fontKeyword2Static)
		CREATE_STYLETHEME_FONT(string,IDC_STRING_STATIC,hwndStringStatic,fontStringStatic)
		CREATE_STYLETHEME_FONT(character,IDC_CHARACTER_STATIC,hwndCharacterStatic,fontCharacterStatic)
		CREATE_STYLETHEME_FONT(number,IDC_NUMBER_STATIC,hwndNumberStatic,fontNumberStatic)
		CREATE_STYLETHEME_FONT(operatorr,IDC_OPERATORR_STATIC,hwndOperatorrStatic,fontOperatorrStatic)
		CREATE_STYLETHEME_FONT(preprocessor,IDC_PREPROCESSOR_STATIC,hwndPreprocessorStatic,fontPreprocessorStatic)
		CREATE_STYLETHEME_FONT(comment,IDC_COMMENT_STATIC,hwndCommentStatic,fontCommentStatic)
		CREATE_STYLETHEME_FONT(commentline,IDC_COMMENTLINE_STATIC,hwndCommentLineStatic,fontCommentLineStatic)
		CREATE_STYLETHEME_FONT(commentdoc,IDC_COMMENTDOC_STATIC,hwndCommentDocStatic,fontCommentDocStatic)

		CREATE_STYLETHEME_FONT(tags,IDC_TAGS_STATIC,hwndTagsStatic,fontTagsStatic)
		CREATE_STYLETHEME_FONT(unknowtags,IDC_UNKNOWTAGS_STATIC,hwndUnknowTagsStatic,fontUnknowTagsStatic)
		CREATE_STYLETHEME_FONT(attributes,IDC_ATTRIBUTES_STATIC,hwndAttributesStatic,fontAttributesStatic)
		CREATE_STYLETHEME_FONT(unknowattributes,IDC_UNKNOWATTRIBUTES_STATIC,hwndUnknowAttributesStatic,fontUnknowAttributesStatic)
		CREATE_STYLETHEME_FONT(entities,IDC_ENTITIES_STATIC,hwndEntitiesStatic,fontEntitiesStatic)
		CREATE_STYLETHEME_FONT(tagends,IDC_TAGENDS_STATIC,hwndTagEndsStatic,fontTagEndsStatic)
		CREATE_STYLETHEME_FONT(cdata,IDC_CDATA_STATIC,hwndCDataStatic,fontCDataStatic)
		CREATE_STYLETHEME_FONT(phpsection,IDC_PHPSECTION_STATIC,hwndPhpSectionStatic,fontPhpSectionStatic)
		CREATE_STYLETHEME_FONT(aspsection,IDC_ASPSECTION_STATIC,hwndAspSectionStatic,fontAspSectionStatic)

		CenterWindow( hDlg , 全_主窗口句柄 );

		return (INT_PTR)TRUE;

	case WM_CTLCOLORSTATIC:
		SetTextColor( (HDC)wParam , GetSysColor(COLOR_WINDOWTEXT) );
		SetBkColor( (HDC)wParam , GetSysColor(COLOR_BTNFACE) );

		if( (HWND)lParam == hwndLineNumberStatic )
		{
			SetTextColor( (HDC)wParam , g_stStyleThemeDialogData.linenumber.color );
			SetBkColor( (HDC)wParam , g_stStyleThemeDialogData.linenumber.bgcolor );
			HBRUSH brush = CreateSolidBrush( g_stStyleThemeDialogData.linenumber.bgcolor ) ;
			return (INT_PTR)brush;
		}
		else if( (HWND)lParam == hwndFoldMarginStatic )
		{
			SetTextColor( (HDC)wParam , g_stStyleThemeDialogData.foldmargin.color );
			SetBkColor( (HDC)wParam , g_stStyleThemeDialogData.foldmargin.bgcolor );
			HBRUSH brush = CreateSolidBrush( g_stStyleThemeDialogData.foldmargin.bgcolor ) ;
			return (INT_PTR)brush;
		}
		else if( (HWND)lParam == hwndTextStatic )
		{
			SetTextColor( (HDC)wParam , g_stStyleThemeDialogData.text.color );
			SetBkColor( (HDC)wParam , g_stStyleThemeDialogData.text.bgcolor );
			HBRUSH brush = CreateSolidBrush( g_stStyleThemeDialogData.text.bgcolor ) ;
			return (INT_PTR)brush;
		}
		else if( (HWND)lParam == hwndCaretLineStatic )
		{
			SetTextColor( (HDC)wParam , g_stStyleThemeDialogData.text.color );
			SetBkColor( (HDC)wParam , g_stStyleThemeDialogData.caretline.bgcolor );
			HBRUSH brush = CreateSolidBrush( g_stStyleThemeDialogData.caretline.bgcolor ) ;
			return (INT_PTR)brush;
		}
		else if( (HWND)lParam == hwndIndicatorStatic )
		{
			SetTextColor( (HDC)wParam , g_stStyleThemeDialogData.text.color );
			SetBkColor( (HDC)wParam , g_stStyleThemeDialogData.indicator.bgcolor );
			HBRUSH brush = CreateSolidBrush( g_stStyleThemeDialogData.indicator.bgcolor ) ;
			return (INT_PTR)brush;
		}

		else SET_STATIC_TEXTCOLOR( hwndKeywordStatic , keywords )
		else SET_STATIC_TEXTCOLOR( hwndKeyword2Static , keywords2 )
		else SET_STATIC_TEXTCOLOR( hwndStringStatic , string )
		else SET_STATIC_TEXTCOLOR( hwndCharacterStatic , character )
		else SET_STATIC_TEXTCOLOR( hwndNumberStatic , number )
		else SET_STATIC_TEXTCOLOR( hwndOperatorrStatic , operatorr )
		else SET_STATIC_TEXTCOLOR( hwndPreprocessorStatic , preprocessor )
		else SET_STATIC_TEXTCOLOR( hwndCommentStatic , comment )
		else SET_STATIC_TEXTCOLOR( hwndCommentLineStatic , commentline )
		else SET_STATIC_TEXTCOLOR( hwndCommentDocStatic , commentdoc )

		else SET_STATIC_TEXTCOLOR( hwndTagsStatic , tags )
		else SET_STATIC_TEXTCOLOR( hwndUnknowTagsStatic , unknowtags )
		else SET_STATIC_TEXTCOLOR( hwndAttributesStatic , attributes )
		else SET_STATIC_TEXTCOLOR( hwndUnknowAttributesStatic , unknowattributes )
		else SET_STATIC_TEXTCOLOR( hwndEntitiesStatic , entities )
		else SET_STATIC_TEXTCOLOR( hwndTagEndsStatic , tagends )
		else SET_STATIC_TEXTCOLOR( hwndCDataStatic , cdata )
		else SET_STATIC_TEXTCOLOR( hwndPhpSectionStatic , phpsection )
		else SET_STATIC_TEXTCOLOR( hwndAspSectionStatic , aspsection )

		// SetWindowLong( hDlg , DWL_MSGRESULT , (LONG)TRUE );
		return (LRESULT)GetSysColorBrush(COLOR_BTNFACE);
	case WM_COMMAND:
		wmId = LOWORD(wParam) ;

		if( wmId == IDC_SETTEXTCOLOR_LINENUMBER_BUTTON ) \
		{
			ChooseStyleTextColor( hDlg , & (g_stStyleThemeDialogData.linenumber.color) );
			InvalidateRect( hwndLineNumberStatic , NULL , TRUE );
		}
		else if( wmId == IDC_SETBGCOLOR_LINENUMBER_BUTTON ) \
		{
			ChooseStyleTextColor( hDlg , & (g_stStyleThemeDialogData.linenumber.bgcolor) );
			InvalidateRect( hwndLineNumberStatic , NULL , TRUE );
		}
		else if( wmId == IDC_SETTEXTCOLOR_FOLDMARGIN_BUTTON ) \
		{
			ChooseStyleTextColor( hDlg , & (g_stStyleThemeDialogData.foldmargin.color) );
			InvalidateRect( hwndFoldMarginStatic , NULL , TRUE );
		}
		else if( wmId == IDC_SETBGCOLOR_FOLDMARGIN_BUTTON ) \
		{
			ChooseStyleTextColor( hDlg , & (g_stStyleThemeDialogData.foldmargin.bgcolor) );
			InvalidateRect( hwndFoldMarginStatic , NULL , TRUE );
		}
		else if( wmId == IDC_SETFONT_TEXT_BUTTON )
		{
			ChooseStyleFont( g_stStyleThemeDialogData.text.font , & (g_stStyleThemeDialogData.text.fontsize) , & (g_stStyleThemeDialogData.text.bold) );

			SYNC_FONT(text,hwndTextStatic,fontTextStatic)
			SYNC_FONT(caretline,hwndCaretLineStatic,fontCaretLineStatic)
			SYNC_FONT(indicator,hwndIndicatorStatic,fontIndicatorStatic)

			SYNC_FONT(keywords,hwndKeywordStatic,fontKeywordStatic)
			SYNC_FONT(keywords2,hwndKeyword2Static,fontKeyword2Static)
			SYNC_FONT(string,hwndStringStatic,fontStringStatic)
			SYNC_FONT(character,hwndCharacterStatic,fontCharacterStatic)
			SYNC_FONT(number,hwndNumberStatic,fontNumberStatic)
			SYNC_FONT(operatorr,hwndOperatorrStatic,fontOperatorrStatic)
			SYNC_FONT(preprocessor,hwndPreprocessorStatic,fontPreprocessorStatic)
			SYNC_FONT(comment,hwndCommentStatic,fontCommentStatic)
			SYNC_FONT(commentline,hwndCommentLineStatic,fontCommentLineStatic)
			SYNC_FONT(commentdoc,hwndCommentDocStatic,fontCommentDocStatic)

			SYNC_FONT(tags,hwndTagsStatic,fontTagsStatic)
			SYNC_FONT(unknowtags,hwndUnknowTagsStatic,fontUnknowTagsStatic)
			SYNC_FONT(attributes,hwndAttributesStatic,fontAttributesStatic)
			SYNC_FONT(unknowattributes,hwndUnknowAttributesStatic,fontUnknowAttributesStatic)
			SYNC_FONT(entities,hwndEntitiesStatic,fontEntitiesStatic)
			SYNC_FONT(tagends,hwndTagEndsStatic,fontTagEndsStatic)
			SYNC_FONT(cdata,hwndCDataStatic,fontCDataStatic)
			SYNC_FONT(phpsection,hwndPhpSectionStatic,fontPhpSectionStatic)
			SYNC_FONT(aspsection,hwndAspSectionStatic,fontAspSectionStatic)
		}
		else if( wmId == IDC_SETTEXTCOLOR_TEXT_BUTTON )
		{
			ChooseStyleTextColor( hDlg , & (g_stStyleThemeDialogData.text.color) );
			InvalidateRect( hwndTextStatic , NULL , TRUE );
		}
		else if( wmId == IDC_SETBGCOLOR_TEXT_BUTTON )
		{
			ChooseStyleTextColor( hDlg , & (g_stStyleThemeDialogData.text.bgcolor) );
			InvalidateRect( hwndCaretLineStatic , NULL , TRUE );
		}
		else if( wmId == IDC_SETBGCOLOR_CARETLINE_BUTTON )
		{
			ChooseStyleTextColor( hDlg , & (g_stStyleThemeDialogData.caretline.bgcolor) );
			InvalidateRect( hwndCaretLineStatic , NULL , TRUE );
		}
		else if( wmId == IDC_SETBGCOLOR_INDICATOR_BUTTON )
		{
			ChooseStyleTextColor( hDlg , & (g_stStyleThemeDialogData.indicator.bgcolor) );
			InvalidateRect( hwndIndicatorStatic , NULL , TRUE );
		}

		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(keywords,hwndKeywordStatic,fontKeywordStatic,IDC_SETFONT_KEYWORDS_BUTTON,IDC_SETTEXTCOLOR_KEYWORDS_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(keywords2,hwndKeyword2Static,fontKeyword2Static,IDC_SETFONT_KEYWORDS2_BUTTON,IDC_SETTEXTCOLOR_KEYWORDS2_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(string,hwndStringStatic,fontStringStatic,IDC_SETFONT_STRING_BUTTON,IDC_SETTEXTCOLOR_STRING_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(character,hwndCharacterStatic,fontCharacterStatic,IDC_SETFONT_CHARACTER_BUTTON,IDC_SETTEXTCOLOR_CHARACTER_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(number,hwndNumberStatic,fontNumberStatic,IDC_SETFONT_NUMBER_BUTTON,IDC_SETTEXTCOLOR_NUMBER_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(operatorr,hwndOperatorrStatic,fontOperatorrStatic,IDC_SETFONT_OPERATORR_BUTTON,IDC_SETTEXTCOLOR_OPERATORR_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(preprocessor,hwndPreprocessorStatic,fontPreprocessorStatic,IDC_SETFONT_PREPROCESSOR_BUTTON,IDC_SETTEXTCOLOR_PREPROCESSOR_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(comment,hwndCommentStatic,fontCommentStatic,IDC_SETFONT_COMMENT_BUTTON,IDC_SETTEXTCOLOR_COMMENT_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(commentline,hwndCommentLineStatic,fontCommentLineStatic,IDC_SETFONT_COMMENTLINE_BUTTON,IDC_SETTEXTCOLOR_COMMENTLINE_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(commentdoc,hwndCommentDocStatic,fontCommentDocStatic,IDC_SETFONT_COMMENTDOC_BUTTON,IDC_SETTEXTCOLOR_COMMENTDOC_BUTTON)

		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(tags,hwndTagsStatic,fontTagsStatic,IDC_SETFONT_TAGS_BUTTON,IDC_SETTEXTCOLOR_TAGS_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(unknowtags,hwndUnknowTagsStatic,fontUnknowTagsStatic,IDC_SETFONT_UNKNOWTAGS_BUTTON,IDC_SETTEXTCOLOR_UNKNOWTAGS_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(attributes,hwndAttributesStatic,fontAttributesStatic,IDC_SETFONT_ATTRIBUTES_BUTTON,IDC_SETTEXTCOLOR_ATTRIBUTES_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(unknowattributes,hwndUnknowAttributesStatic,fontUnknowAttributesStatic,IDC_SETFONT_UNKNOWATTRIBUTES_BUTTON,IDC_SETTEXTCOLOR_UNKNOWATTRIBUTES_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(entities,hwndEntitiesStatic,fontEntitiesStatic,IDC_SETFONT_ENTITIES_BUTTON,IDC_SETTEXTCOLOR_ENTITIES_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(tagends,hwndTagEndsStatic,fontTagEndsStatic,IDC_SETFONT_TAGENDS_BUTTON,IDC_SETTEXTCOLOR_TAGENDS_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(cdata,hwndCDataStatic,fontCDataStatic,IDC_SETFONT_CDATA_BUTTON,IDC_SETTEXTCOLOR_CDATA_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(phpsection,hwndPhpSectionStatic,fontPhpSectionStatic,IDC_SETFONT_PHPSECTION_BUTTON,IDC_SETTEXTCOLOR_PHPSECTION_BUTTON)
		else PROCESS_STYLETHEME_FONT_OR_COLOR_MESSAGE(aspsection,hwndAspSectionStatic,fontAspSectionStatic,IDC_SETFONT_ASPSECTION_BUTTON,IDC_SETTEXTCOLOR_ASPSECTION_BUTTON)

		/*
		else if( LOWORD(wParam) == IDC_SETDEFAULT_BUTTON )
		{
			SetStyleThemeDefault( & g_stStyleThemeDialogData );
			InvalidateRect( hDlg , NULL , TRUE );
		}
		*/
		else if( LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL )
		{
			DeleteObject( fontLineNumberStatic );
			DeleteObject( fontFoldMarginStatic );

			DeleteObject( fontTextStatic );
			DeleteObject( fontCaretLineStatic );
			DeleteObject( fontTextStatic );
			DeleteObject( fontCaretLineStatic );
			DeleteObject( fontIndicatorStatic );

			DeleteObject( fontKeywordStatic );
			DeleteObject( fontKeyword2Static );
			DeleteObject( fontStringStatic );
			DeleteObject( fontCharacterStatic );
			DeleteObject( fontNumberStatic );
			DeleteObject( fontOperatorrStatic );
			DeleteObject( fontPreprocessorStatic );
			DeleteObject( fontCommentStatic );
			DeleteObject( fontCommentLineStatic );
			DeleteObject( fontCommentDocStatic );

			DeleteObject( fontTagsStatic );
			DeleteObject( fontUnknowTagsStatic );
			DeleteObject( fontAttributesStatic );
			DeleteObject( fontUnknowAttributesStatic );
			DeleteObject( fontEntitiesStatic );
			DeleteObject( fontTagEndsStatic );
			DeleteObject( fontCDataStatic );
			DeleteObject( fontPhpSectionStatic );
			DeleteObject( fontAspSectionStatic );

			memcpy( & (g_pstWindowTheme->stStyleTheme) , & g_stStyleThemeDialogData , sizeof(struct StyleTheme) );

			EndDialog(hDlg, LOWORD(wParam));

			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
