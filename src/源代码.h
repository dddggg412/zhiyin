#ifndef _VIEW_
#define _VIEW_

#include "framework.h"

int OnSourceCodeBlockFoldVisiable( struct 选项卡页面S *选项卡页节点 );
int OnSourceCodeBlockFoldToggle( struct 选项卡页面S *选项卡页节点 );
int OnSourceCodeBlockFoldContract( struct 选项卡页面S *选项卡页节点 );
int OnSourceCodeBlockFoldExpand( struct 选项卡页面S *选项卡页节点 );
int OnSourceCodeBlockFoldContractAll( struct 选项卡页面S *选项卡页节点 );
int OnSourceCodeBlockFoldExpandAll( struct 选项卡页面S *选项卡页节点 );

int OnSetReloadSymbolListOrTreeInterval( struct 选项卡页面S *选项卡页节点 );

int OnSourceCodeEnableAutoCompletedShow( struct 选项卡页面S *选项卡页节点 );
int OnSourceCodeAutoCompletedShowAfterInputCharacters( struct 选项卡页面S *选项卡页节点 );
int OnSourceCodeEnableCallTipShow( struct 选项卡页面S *选项卡页节点 );

#define	BEGIN_DATABASE_CONNECTION_CONFIG	"-- EDITULTRA BEGIN DATABASE CONNECTION CONFIG"
#define END_DATABASE_CONNECTION_CONFIG		"-- EDITULTRA END DATABASE CONNECTION CONFIG"

int OnInsertDataBaseConnectionConfig( struct 选项卡页面S *选项卡页节点 );

#define	BEGIN_REDIS_CONNECTION_CONFIG	"-- EDITULTRA BEGIN REDIS CONNECTION CONFIG"
#define END_REDIS_CONNECTION_CONFIG	"-- EDITULTRA END REDIS CONNECTION CONFIG"

int OnInsertRedisConnectionConfig( struct 选项卡页面S *选项卡页节点 );

#endif
