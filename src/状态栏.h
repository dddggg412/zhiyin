#ifndef _STATUSBAR_
#define _STATUSBAR_

#include "framework.h"

extern HWND		全_状态栏句柄 ;
extern int		全_状态栏高度 ;

void 设置状态栏进度信息( const char *format , ... );
void 清除状态栏进度信息();

void 更新状态栏进度信息();
void 更新状态栏定位信息();
void 更新状态栏换行符模式信息();
void 更新状态栏字符编码信息();
void 更新状态栏选择信息();
void 更新状态栏( HWND hWnd );

#endif
