#include "framework.h"

int IfSymbolReqularExp_CreateSymbolListCtl( struct 选项卡页面S *选项卡页节点 , HFONT hFont )
{
	if( 选项卡页节点->pst文档类型配置->acSymbolReqularExp )
	{
		if( 选项卡页节点->符号列表句柄 )
		{
			DestroyWindow( 选项卡页节点->符号列表句柄 );
		}

		选项卡页节点->符号列表句柄 = CreateWindow( "listbox" , NULL , WS_CHILD|WS_VSCROLL|LBS_NOTIFY|LBS_USETABSTOPS , 选项卡页节点->符号列表矩形.left , 选项卡页节点->符号列表矩形.top , 选项卡页节点->符号列表矩形.right-选项卡页节点->符号列表矩形.left , 选项卡页节点->符号列表矩形.bottom-选项卡页节点->符号列表矩形.top , 全_主窗口句柄 , NULL , 全_当前实例 , NULL ) ;
		if( 选项卡页节点->符号列表句柄 == NULL )
		{
			MessageBox(NULL, TEXT("不能创建符号列表控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		SendMessage( 选项卡页节点->符号列表句柄 , WM_SETFONT , (WPARAM)hFont, 0);

		SendMessage( 选项卡页节点->符号列表句柄 , TVM_SETTEXTCOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.color );
		SendMessage( 选项卡页节点->符号列表句柄 , TVM_SETLINECOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.color );
		SendMessage( 选项卡页节点->符号列表句柄 , TVM_SETBKCOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.bgcolor );

		// 预编译符号列表
		if( 选项卡页节点->pcreSymbolRe )
			pcre_free( 选项卡页节点->pcreSymbolRe );
		const char *error_desc = NULL ;
		int error_offset ;
		选项卡页节点->pcreSymbolRe = pcre_compile( 选项卡页节点->pst文档类型配置->acSymbolReqularExp , PCRE_MULTILINE , & error_desc , & error_offset , NULL ) ;
		if( 选项卡页节点->pcreSymbolRe == NULL )
		{
			MessageBox(NULL, "不能解析符号正则表达式", TEXT("错误"), MB_ICONERROR | MB_OK);
			return 1;
		}
	}

	return 0;
}

int ReloadSymbolList_WithReqularExp( struct 选项卡页面S *选项卡页节点 )
{
	int		nFileSize ;
	char		*acFileBuffer = NULL ;
	char		*p1 = NULL ;
	char		*p2 = NULL ;
	char		*p3 = NULL ;
	int		nLineNo ;

	ListBox_ResetContent( 选项卡页节点->符号列表句柄 );

	nFileSize = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
	acFileBuffer = (char*)malloc( nFileSize + 1 ) ;
	if( acFileBuffer == NULL )
		return -1;
	memset( acFileBuffer , 0x00 , nFileSize + 1 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXT , (sptr_t)(nFileSize+1) , (sptr_t)acFileBuffer ) ;

	p1 = acFileBuffer ;
	nLineNo = 0 ;
	while(1)
	{
		for( p3 = p1 ; *(p3) && *(p3) != '\n' ; p3++ )
			;
		if( *(p3) == '\0' )
			break;
		*(p3) = '\0' ;

		p2 = p3 ;
		if( p2 > p1 && *(p2-1) == '\r' )
		{
			p2--;
			*(p2) = '\0' ;
		}

		int	nSymbolOvector[ PATTERN_OVECCOUNT ] ;
		int	nSymbolOvectorCount ;
		int	tmp = 0 ;
		nSymbolOvectorCount = pcre_exec( 选项卡页节点->pcreSymbolRe , NULL , p1 , (int)(p2-p1) , 0 , 0 , nSymbolOvector , PATTERN_OVECCOUNT ) ;
		if( nSymbolOvectorCount > 0 )
		{
			memmove( p1 , p1+nSymbolOvector[2] , nSymbolOvector[3]-nSymbolOvector[2] );
			*(p1+nSymbolOvector[3]-nSymbolOvector[2]) = '\0' ;

			if( STRCMP( p1 , != , "if" ) )
			{
				int nListBoxItemIndex = ListBox_AddString( 选项卡页节点->符号列表句柄 , p1 ) ;
				ListBox_SetItemData( 选项卡页节点->符号列表句柄 , nListBoxItemIndex , (LPARAM)nLineNo );
			}
		}

		p1 = p3 + 1 ;
		nLineNo++;
	}

	free( acFileBuffer );

	return 0;
}

int GetCurrentWordAndJumpGotoLine( struct 选项卡页面S *选项卡页节点 )
{
	int		当前文本字节位置 ;
	int		nCurrentWordStartPos ;
	int		nCurrentWordEndPos ;
	char		acCurrentWord[ 256 ] ;

	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentWordStartPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDSTARTPOSITION, 当前文本字节位置, TRUE ) ;
	nCurrentWordEndPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDENDPOSITION, 当前文本字节位置, TRUE ) ;
	if( nCurrentWordEndPos - nCurrentWordStartPos > sizeof(acCurrentWord)-1 )
		nCurrentWordEndPos = nCurrentWordStartPos + sizeof(acCurrentWord)-1 ;
	memset( acCurrentWord , 0x00 , sizeof(acCurrentWord) );
	GetTextByRange( 全_当前选项卡页面节点 , nCurrentWordStartPos , nCurrentWordEndPos , acCurrentWord );

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		if( p->pst文档类型配置->n文档的类型 == DOCTYPE_CPP )
		{
			int nFindIndex = ListBox_FindString( p->符号列表句柄 , 0 , acCurrentWord ) ;
			if( nFindIndex != LB_ERR )
			{
				if( p != 选项卡页节点 )
					以索引选择选项卡页面( n选项卡页面索引 );
				int nGotoLineNo = (int)SendMessage( p->符号列表句柄 , LB_GETITEMDATA , nFindIndex , 0 ) ;
				AddNavigateBackNextTrace( p , 当前文本字节位置 );
				JumpGotoLine( p , nGotoLineNo , 0 );
				break;
			}
		}
	}

	return 0;
}

int GetSymbolListItemAndJumpGotoLine( struct 选项卡页面S *选项卡页节点 )
{
	int nListBoxItemNo = (int)SendMessage( 选项卡页节点->符号列表句柄 , LB_GETCURSEL , 0 , 0 ) ;
	int nGotoLineNo = (int)SendMessage( 选项卡页节点->符号列表句柄 , LB_GETITEMDATA , nListBoxItemNo , 0 ) ;
	int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	int 文本当前行号 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_LINEFROMPOSITION , 当前文本字节位置 , 0 ) ;
	AddNavigateBackNextTrace( 全_当前选项卡页面节点 , 当前文本字节位置 );
	JumpGotoLine( 选项卡页节点 , nGotoLineNo , 文本当前行号 );

	return 0;
}
