#ifndef _SYMBOLLISTCTL_
#define _SYMBOLLISTCTL_

#include "framework.h"

int IfSymbolReqularExp_CreateSymbolListCtl( struct 选项卡页面S *选项卡页节点 , HFONT hFont );
int ReloadSymbolList_WithReqularExp( struct 选项卡页面S *选项卡页节点 );
int GetCurrentWordAndJumpGotoLine( struct 选项卡页面S *选项卡页节点 );
int GetSymbolListItemAndJumpGotoLine( struct 选项卡页面S *选项卡页节点 );

#endif
