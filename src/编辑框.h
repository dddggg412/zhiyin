#ifndef _EDIT_
#define _EDIT_

#include "framework.h"

int 撤销编辑( struct 选项卡页面S *选项卡页节点 );
int 重做编辑( struct 选项卡页面S *选项卡页节点 );

int 剪切文本( struct 选项卡页面S *选项卡页节点 );
int 复制文本( struct 选项卡页面S *选项卡页节点 );
int 粘贴文本( struct 选项卡页面S *选项卡页节点 );
int 删除文本( struct 选项卡页面S *选项卡页节点 );
int 剪切行( struct 选项卡页面S *选项卡页节点 );
int 剪切行和粘贴行( struct 选项卡页面S *选项卡页节点 );
int 复制行( struct 选项卡页面S *选项卡页节点 );
int 复制行和粘贴行( struct 选项卡页面S *选项卡页节点 );
int 复制文件名称( struct 选项卡页面S *选项卡页节点 );
int 复制路径名称( struct 选项卡页面S *选项卡页节点 );
int 复制路径文件名称( struct 选项卡页面S *选项卡页节点 );
int 粘贴行( struct 选项卡页面S *选项卡页节点 );
int 往上粘贴行( struct 选项卡页面S *选项卡页节点 );
int 删除行( struct 选项卡页面S *选项卡页节点 );
int 删除行首空白字符( struct 选项卡页面S *选项卡页节点 );
int 删除行尾空白字符( struct 选项卡页面S *选项卡页节点 );
int OnDeleteBlankLine( struct 选项卡页面S *选项卡页节点 );
int OnDeleteBlankLineWithWhiteCharacter( struct 选项卡页面S *选项卡页节点 );

int OnJoinLine( struct 选项卡页面S *选项卡页节点 );

int OnLowerCaseEdit( struct 选项卡页面S *选项卡页节点 );
int OnUpperCaseEdit( struct 选项卡页面S *选项卡页节点 );

int OnEditEnableAutoAddCloseChar( struct 选项卡页面S *选项卡页节点 );
int OnEditEnableAutoIdentation( struct 选项卡页面S *选项卡页节点 );

int OnEditBase64Encoding( struct 选项卡页面S *选项卡页节点 );
int OnEditBase64Decoding( struct 选项卡页面S *选项卡页节点 );
int OnEditMd5( struct 选项卡页面S *选项卡页节点 );
int OnEditSha1( struct 选项卡页面S *选项卡页节点 );
int OnEditSha256( struct 选项卡页面S *选项卡页节点 );
int OnEdit3DesCbcEncrypto( struct 选项卡页面S *选项卡页节点 );
int OnEdit3DesCbcDecrypto( struct 选项卡页面S *选项卡页节点 );

#endif
