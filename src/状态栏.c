#include "framework.h"

char		全_状态栏进度信息[ 256 ] = "" ;
HWND		全_状态栏句柄 = NULL ;
int		全_状态栏高度 = 0 ;

void 设置状态栏进度信息( const char *format , ... )
{
	va_list		valist ;

	memset( 全_状态栏进度信息 , 0x00 , sizeof(全_状态栏进度信息) );
	va_start( valist , format );
	snprintf( 全_状态栏进度信息 , sizeof(全_状态栏进度信息)-1 , format , valist );
	va_end( valist );

	更新状态栏进度信息();

	return;
}

void 清除状态栏进度信息()
{
	memset( 全_状态栏进度信息 , 0x00 , sizeof(全_状态栏进度信息) );

	更新状态栏进度信息();

	return;
}

void 更新状态栏进度信息()
{
	SendMessage( 全_状态栏句柄 , SB_SETTEXT , (WPARAM)状态栏项_正在处理_索引M , (LPARAM)全_状态栏进度信息 );

	return;
}

void 更新状态栏定位信息()
{
	char	buf[ 1024 ] ;

	if( 全_当前选项卡页面节点 == NULL )
	{
		SendMessage( 全_状态栏句柄 , SB_SETTEXT , (WPARAM)状态栏项_位置_索引M , (LPARAM)"" );
		return ;
	}

	int 当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	int 文本当前行号 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_LINEFROMPOSITION , 当前文本字节位置 , 0 ) ;
	int 当前行第一个字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_POSITIONFROMLINE , 文本当前行号 , 0 ) ;
	int 当前行结束符字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETLINEENDPOSITION , 文本当前行号 , 0 ) ;
	int 文本总字节数/*空格和行结束符也包括在内*/ = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;
	int 文本总行数 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	memset( buf , 0x00 , sizeof(buf) );
	snprintf( buf , sizeof(buf)-1 , "列:%d/%d    行:%d/%d    偏移量:%d/%d"
		, 当前文本字节位置-当前行第一个字节位置+1 , 当前行结束符字节位置-当前行第一个字节位置+1
		, 文本当前行号+1 , 文本总行数
		, 当前文本字节位置 , 文本总字节数 );
	SendMessage( 全_状态栏句柄 , SB_SETTEXT , (WPARAM)状态栏项_位置_索引M , (LPARAM)buf );

	return;
}

void 更新状态栏换行符模式信息()
{
	char	buf[ 1024 ] ;

	if( 全_当前选项卡页面节点 == NULL )
	{
		SendMessage( 全_状态栏句柄 , SB_SETTEXT , (WPARAM)状态栏项_换行符_索引M , (LPARAM)"" );
		return ;
	}

	memset( buf , 0x00 , sizeof(buf) );
	int n换行符模式 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETEOLMODE , 0 , 0 ) ;
	if( n换行符模式 == 0 )
		strcpy( buf , "换行符模式:DOS" );
	else if( n换行符模式 == 1 )
		strcpy( buf , "换行符模式:MAC" );
	else if( n换行符模式 == 2 )
		strcpy( buf , "换行符模式:UNIX/Linux" );
	else
		strcpy( buf , "未知换行符模式" );
	SendMessage( 全_状态栏句柄 , SB_SETTEXT , (WPARAM)状态栏项_换行符_索引M , (LPARAM)buf );

	return;
}

void 更新状态栏字符编码信息()
{
	char	buf[ 1024 ] ;

	if( 全_当前选项卡页面节点 == NULL )
	{
		SendMessage( 全_状态栏句柄 , SB_SETTEXT , (WPARAM)状态栏项_编码_索引M , (LPARAM)"" );
		return ;
	}

	memset( buf , 0x00 , sizeof(buf) );
	if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
		strcpy( buf , "字符编码:UTF8" );
	else if( 全_当前选项卡页面节点->nCodePage == 字符编码_GBK )
		strcpy( buf , "字符编码:GBK" );
	else if( 全_当前选项卡页面节点->nCodePage == 字符编码_BIG5 )
		strcpy( buf , "字符编码:BIG5" );
	else
		strcpy( buf , "未知字符编码" );
	SendMessage( 全_状态栏句柄 , SB_SETTEXT , (WPARAM)状态栏项_编码_索引M , (LPARAM)buf );

	return;
}

void 更新状态栏选择信息()
{
	int	nSelectionStart ;
	int	nSelectionEnd ;
	char	buf[ 1024 ] ;

	if( 全_当前选项卡页面节点 == NULL )
	{
		SendMessage( 全_状态栏句柄 , SB_SETTEXT , (WPARAM)状态栏项_选择文本_索引M , (LPARAM)"" );
		return ;
	}

	nSelectionStart = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
	nSelectionEnd = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETSELECTIONEND , 0 , 0 ) ;

	memset( buf , 0x00 , sizeof(buf) );
	snprintf( buf , sizeof(buf)-1 , "选择文本长度:%d" , nSelectionEnd-nSelectionStart );
	SendMessage( 全_状态栏句柄 , SB_SETTEXT , (WPARAM)状态栏项_选择文本_索引M , (LPARAM)buf );

	return;
}

void 更新状态栏( HWND hWnd )
{
	RECT	r窗口矩形 ;
	int	状态栏分栏右边框[ 状态栏项数量M ] ;

	GetClientRect( hWnd , & r窗口矩形 );

	SetWindowPos ( 全_状态栏句柄 , HWND_TOP ,  r窗口矩形.left , r窗口矩形.bottom-全_状态栏高度 , r窗口矩形.right-r窗口矩形.left , r窗口矩形.bottom , SWP_SHOWWINDOW );

	状态栏分栏右边框[4] = r窗口矩形.right ;
	状态栏分栏右边框[3] = 状态栏分栏右边框[4] - 150 ;
	状态栏分栏右边框[2] = 状态栏分栏右边框[3] - 150 ;
	状态栏分栏右边框[1] = 状态栏分栏右边框[2] - 150 ;
	状态栏分栏右边框[0] = 状态栏分栏右边框[1] - 300 ;
	SendMessage( 全_状态栏句柄 , SB_SETPARTS , 状态栏项数量M , (LPARAM)(LPINT)状态栏分栏右边框 );

	更新状态栏进度信息();
	更新状态栏定位信息();
	更新状态栏换行符模式信息();
	更新状态栏字符编码信息();
	更新状态栏选择信息();

	// ShowWindow( 全_状态栏句柄 , SW_SHOW );
	InvalidateRect( 全_状态栏句柄 , NULL , TRUE );
	UpdateWindow( 全_状态栏句柄 );

	return;
}
