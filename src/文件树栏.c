#include "framework.h"

HWND	全_文件树栏句柄 = NULL ;
BOOL	全_显示文件树栏 = FALSE ;
RECT	全_文件树栏矩形 = { 0 } ;

HWND	全_文件树句柄 = NULL ;

HMENU	g_hFileTreeFilePopupMenu ;
HMENU	g_hFileTreeDirectoryPopupMenu ;

int	全_文件树映像驱动器 ;
int	nFileTreeImageOpenFold ;
int	nFileTreeImageClosedFold ;
int	nFileTreeImageTextFile ;
int	nFileTreeImageGeneralFile ;
int	nFileTreeImageExecFile ;

static BOOL LoadTreeViewImageLists(HWND hwnd)
{
	HIMAGELIST himl;  // handle to image list
	HBITMAP hbmp;     // handle to bitmap

	if ((himl = ImageList_Create(16, 16, ILC_COLOR, 6, 0)) == NULL)
		return FALSE;

	// Add the open file, closed file, and document bitmaps.
	hbmp = LoadBitmap(全_当前实例, MAKEINTRESOURCE(IDB_DRIVE));
	全_文件树映像驱动器 = ImageList_Add(himl, hbmp, (HBITMAP)NULL);
	DeleteObject(hbmp);

	hbmp = LoadBitmap(全_当前实例, MAKEINTRESOURCE(IDB_OPENFOLD));
	nFileTreeImageOpenFold = ImageList_Add(himl, hbmp, (HBITMAP)NULL);
	DeleteObject(hbmp);

	hbmp = LoadBitmap(全_当前实例, MAKEINTRESOURCE(IDB_CLSDFOLD));
	nFileTreeImageClosedFold = ImageList_Add(himl, hbmp, (HBITMAP)NULL);
	DeleteObject(hbmp);

	hbmp = LoadBitmap(全_当前实例, MAKEINTRESOURCE(IDB_TXT));
	nFileTreeImageGeneralFile = ImageList_Add(himl, hbmp, (HBITMAP)NULL);
	DeleteObject(hbmp);

	hbmp = LoadBitmap(全_当前实例, MAKEINTRESOURCE(IDB_DOC));
	nFileTreeImageTextFile = ImageList_Add(himl, hbmp, (HBITMAP)NULL);
	DeleteObject(hbmp);

	hbmp = LoadBitmap(全_当前实例, MAKEINTRESOURCE(IDB_EXE));
	nFileTreeImageExecFile = ImageList_Add(himl, hbmp, (HBITMAP)NULL);
	DeleteObject(hbmp);

	// Fail if not all of the images were added.
	if (ImageList_GetImageCount(himl) < 6)
		return FALSE;

	// Associate the image list with the tree-view control.
	TreeView_SetImageList(hwnd, himl, TVSIL_NORMAL);

	return TRUE;
}

int 创建文件树栏( HWND hWnd )
{
	RECT		rect主客户端 ;
	TCITEM		tci ;

	BOOL		bret ;
	int		nret ;

	INITCOMMONCONTROLSEX icex ;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX) ;
	icex.dwICC = ICC_COOL_CLASSES | ICC_BAR_CLASSES ;
	bret = InitCommonControlsEx( & icex ) ;
	if (bret != TRUE)
	{
		MessageBox(NULL, TEXT("不能注册文件树ReBar控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	GetClientRect( hWnd , & rect主客户端 );
	memcpy( & 全_文件树栏矩形 , & rect主客户端 , sizeof(RECT) );
	全_文件树栏矩形.left = rect主客户端.left ;
	全_文件树栏矩形.right = 全_文件树栏矩形.left + 符号树栏_宽度_默认 ;
	全_文件树栏矩形.top = rect主客户端.top ;
	全_文件树栏矩形.bottom = rect主客户端.bottom ;
	全_文件树栏句柄 = CreateWindow( WC_TABCONTROL , NULL , WS_CHILD , 全_文件树栏矩形.left , 全_文件树栏矩形.top , 全_文件树栏矩形.right - 全_文件树栏矩形.left , 全_文件树栏矩形.bottom-全_文件树栏矩形.top , hWnd , NULL , 全_当前实例 , NULL ) ;
	if( 全_文件树栏句柄 == NULL )
	{
		MessageBox(NULL, TEXT("不能创建TabControl控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -2;
	}

	SendMessage( 全_文件树栏句柄 , WM_SETFONT , (WPARAM)GetStockObject(DEFAULT_GUI_FONT), 0);
	TabCtrl_SetPadding( 全_文件树栏句柄 , 20 , 5 );

	tci.mask = TCIF_TEXT ;
	tci.pszText = (char*)"文件资源管理器" ;
	nret = TabCtrl_InsertItem( 全_文件树栏句柄 , 0 , & tci ) ;
	if( nret == -1 )
	{
		MessageBox(NULL, TEXT("TabCtrl_InsertItem失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return 0;
	}

	// hwndFileTree = CreateWindowEx( WS_EX_STATICEDGE , WC_TREEVIEW , NULL , WS_CHILD|TVS_HASLINES|TVS_HASBUTTONS|TVS_LINESATROOT|WS_TABSTOP , rectFileTreeBar.left , rectFileTreeBar.top+选项卡_默认高度 , rectFileTreeBar.right-rectFileTreeBar.left , rectFileTreeBar.bottom-rectFileTreeBar.top-选项卡_默认高度 , hWnd , NULL , 全_当前实例 , NULL ) ;
	全_文件树句柄 = CreateWindow( WC_TREEVIEW , NULL , WS_CHILD|TVS_HASLINES|TVS_HASBUTTONS|TVS_LINESATROOT|WS_TABSTOP|TVS_SHOWSELALWAYS , 全_文件树栏矩形.left , 全_文件树栏矩形.top+选项卡_默认高度 , 全_文件树栏矩形.right-全_文件树栏矩形.left , 全_文件树栏矩形.bottom-全_文件树栏矩形.top-选项卡_默认高度 , hWnd , NULL , 全_当前实例 , NULL ) ;
	if( 全_文件树句柄 == NULL )
	{
		MessageBox(NULL, TEXT("不能创建文件树TreeView控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -2;
	}

	/*
	SendMessage( hwndFileTree , TVM_SETTEXTCOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.color );
	SendMessage( hwndFileTree , TVM_SETLINECOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.color );
	SendMessage( hwndFileTree , TVM_SETBKCOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.bgcolor );
	*/

	LoadTreeViewImageLists( 全_文件树句柄 );

	LoadDrivesToFileTree( 全_文件树句柄 );

	LoadRemoteDriversToFileTree( 全_文件树句柄 );

	ShowWindow( 全_文件树栏句柄 , SW_HIDE);
	UpdateWindow( 全_文件树栏句柄 );
	ShowWindow( 全_文件树句柄 , SW_HIDE);
	UpdateWindow( 全_文件树句柄 );

	g_hFileTreeDirectoryPopupMenu = LoadMenu( 全_当前实例 , MAKEINTRESOURCE(IDR_FILETREE_DIRECTORY_POPUPMENU) ) ;
	if( g_hFileTreeDirectoryPopupMenu == NULL )
	{
		MessageBox(NULL, TEXT("不能创建目录树右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	g_hFileTreeDirectoryPopupMenu = GetSubMenu( g_hFileTreeDirectoryPopupMenu , 0 ) ;

	g_hFileTreeFilePopupMenu = LoadMenu( 全_当前实例 , MAKEINTRESOURCE(IDR_FILETREE_FILE_POPUPMENU) ) ;
	if( g_hFileTreeFilePopupMenu == NULL )
	{
		MessageBox(NULL, TEXT("不能创建文件树右键弹出菜单"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	g_hFileTreeFilePopupMenu = GetSubMenu( g_hFileTreeFilePopupMenu , 0 ) ;

	HMENU hMenu = GetMenu( 全_主窗口句柄 ) ;
	CheckMenuItem(hMenu, IDM_VIEW_FILETREE, MF_UNCHECKED);

	return 0;
}

void 调整文件树框( RECT *rectFileTreeBar , RECT *文件树矩形 )
{
	文件树矩形->left = 文件树_边框_左 ;
	文件树矩形->right = rectFileTreeBar->right - 文件树_边框_右 ;
	文件树矩形->top = rectFileTreeBar->top + 选项卡_默认高度 + 文件树_边框_上 ;
	文件树矩形->bottom = rectFileTreeBar->bottom - 文件树_边框_下 ;
	return;
}

static struct 树视图数据S *AllocTreeViewData( struct RemoteFileServer *pstRemoteFileServer , char *acPathName , char *acPathFilename , char *acFilename , BOOL bIsLoadedCompleted , int nImageIndex )
{
	struct 树视图数据S	*tvd = NULL ;

	tvd = (struct 树视图数据S *)malloc( sizeof(struct 树视图数据S) ) ;
	if( tvd == NULL )
		return NULL;
	memset( tvd , 0x00 , sizeof(struct 树视图数据S) );

	tvd->pstRemoteFileServer = pstRemoteFileServer ;
	strcpy( tvd->acPathName , acPathName );
	strcpy( tvd->acPathFilename , acPathFilename );
	strcpy( tvd->acFilename , acFilename );
	tvd->bIsLoadedCompleted = bIsLoadedCompleted ;
	tvd->nImageIndex = nImageIndex ;

	return tvd;
}

struct 树视图数据S *从HTREEITEM获取树状视图数据( HTREEITEM hti )
{
	TVITEM		tviChild ;
	BOOL		bret ;

	memset( & tviChild , 0x00 , sizeof(TVITEM) );
	tviChild.mask = TVIF_HANDLE ;
	tviChild.hItem = hti ;
	bret = TreeView_GetItem( 全_文件树句柄 , & tviChild ) ;
	if( bret != TRUE )
		return NULL;

	struct 树视图数据S *tvd = (struct 树视图数据S *)(tviChild.lParam) ;
	return tvd;
}

struct 树视图数据S *AddFileTreeNode( HWND hwnd , HTREEITEM parent , int nImageIndex , struct RemoteFileServer *pstRemoteFileServer , char *acPathName , char *acPathFilename , char *acFilename , BOOL bIsLoadedCompleted )
{
	TVITEM			tvi ;
	TVINSERTSTRUCT		tvis;
	struct 树视图数据S	*tvd = NULL ;

	tvd = AllocTreeViewData( pstRemoteFileServer , acPathName , acPathFilename , acFilename , bIsLoadedCompleted , nImageIndex ) ;
	if( tvd == NULL )
		return NULL;

	tvi.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_DI_SETITEM | TVIF_PARAM ;
	tvi.iImage = tvi.iSelectedImage = nImageIndex ;
	tvi.pszText = acFilename ;
	tvi.lParam = (LPARAM)tvd ;
	tvis.hParent = parent ;
	tvis.item = tvi;
	tvis.hInsertAfter = (parent==TVI_ROOT?TVI_LAST:TVI_SORT) ;
	tvd->hti = TreeView_InsertItem( hwnd , & tvis ) ;

	return tvd;
}

int AppendFileTreeNodeChildren( HWND hwnd , struct 树视图数据S *tvdParent )
{
	char			acFindPathFilename[ MAX_PATH+1 ] ;
	WIN32_FIND_DATA		stFindFileData ;
	HANDLE			hFindFile ;
	char			acPathFilename[ MAX_PATH ] ;
	struct 树视图数据S	*tvd = NULL ;

	sprintf( acFindPathFilename , "%s/*" , tvdParent->acPathFilename );
	hFindFile = FindFirstFile( acFindPathFilename , & stFindFileData ) ;
	if( hFindFile == INVALID_HANDLE_VALUE )
		return 0;

	do
	{
		if( strcmp(stFindFileData.cFileName,".") == 0 || strcmp(stFindFileData.cFileName,"..") == 0 )
			continue;

		if( stFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
		{
			_snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s/%s" , tvdParent->acPathFilename , stFindFileData.cFileName ) ;
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageClosedFold , NULL , tvdParent->acPathFilename , acPathFilename , stFindFileData.cFileName , FALSE ) ;
			if( tvd == NULL )
			{
				FindClose( hFindFile );
				return -1;
			}
		}
		else
		{
			_snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s/%s" , tvdParent->acPathFilename , stFindFileData.cFileName ) ;
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageTextFile , NULL , tvdParent->acPathFilename , acPathFilename , stFindFileData.cFileName , TRUE ) ;
			if( tvd == NULL )
			{
				FindClose( hFindFile );
				return -1;
			}
		}
	}
	while( FindNextFile( hFindFile , & stFindFileData ) );

	FindClose( hFindFile );

	tvdParent->bIsLoadedCompleted = TRUE ;

	return 0;
}

int LoadDrivesToFileTree( HWND hwnd )
{
	char			acDrivenames[ MAX_PATH ] ;
	char			*pcDrivename = NULL ;
	size_t			nDriveNameLength ;
	char			*p = NULL ;
	struct 树视图数据S	*tvd = NULL ;
	int			nret = 0 ;

	GetLogicalDriveStrings( sizeof(acDrivenames)-1 , acDrivenames ) ;
	pcDrivename = acDrivenames ;
	while( pcDrivename[0] )
	{
		nDriveNameLength = strlen(pcDrivename) ;

		p = strchr( pcDrivename , '\\' ) ;
		if( p )
			*(p) = '\0' ;
		tvd = AddFileTreeNode( hwnd , TVI_ROOT , 全_文件树映像驱动器 , NULL , pcDrivename , pcDrivename , pcDrivename , FALSE ) ;
		if( tvd == NULL )
			return -1;

		nret = AppendFileTreeNodeChildren( hwnd , tvd ) ;
		if( nret )
			break;

		pcDrivename += nDriveNameLength + 1 ;
	}

	return 0;
}

int 加载目录到文件树( HWND hwnd , char *pcPathname )
{
	char			*p = NULL ;
	struct 树视图数据S	*tvd = NULL ;
	int			nret = 0 ;

	p = strrchr( pcPathname , '\\' ) ;
	if( p )
		*(p) = '\0' ;
	p = strrchr( pcPathname , '\\' ) ;
	if( p )
		*(p) = '\0' ;
	tvd = AddFileTreeNode( hwnd , TVI_ROOT , nFileTreeImageOpenFold , NULL , pcPathname , pcPathname , pcPathname , FALSE ) ;
	if( tvd == NULL )
		return -1;

	nret = AppendFileTreeNodeChildren( hwnd , tvd ) ;
	if( nret )
		return -2;

	全_显示文件树栏 = TRUE ;
	TreeView_SelectItem( 全_文件树句柄 , tvd->hti );
	SendMessage( 全_文件树句柄 , WM_SETFOCUS , 0 , 0 );
	更新所有窗口( 全_主窗口句柄 );

	return 0;
}

static size_t ReadRemoteDirectory(void *buffer, size_t size, size_t nmemb, void *stream)
{
	struct RemoteFileBuffer	*rfb = (struct RemoteFileBuffer *)stream ;
	size_t			nTransferLen ;
	size_t			nReadLen ;

	nReadLen = nTransferLen = size * nmemb ;
	if( nReadLen > rfb->remain_len )
		nReadLen = rfb->remain_len ;

	memcpy( rfb->buf + rfb->str_len , buffer , nReadLen );
	rfb->str_len += nReadLen ;
	rfb->remain_len -= nReadLen ;

	return nTransferLen;
}

int AppendRemoteFileTreeNodeChildren( HWND hwnd , struct 树视图数据S *tvdParent , CURL *curl )
{
	struct RemoteFileBuffer	*rfb = NULL ;
	char			userpwd[ 256 ] ;
	CURLcode		res ;
	char			*p1 = NULL ;
	char			*p2 = NULL ;
	char			chFileType ;
	char			acPathFilename[ MAX_PATH ] ;
	char			acFilename[ MAX_PATH ] ;
	struct 树视图数据S	*tvd = NULL ;
	int			nret = 0 ;

	if( tvdParent->pstRemoteFileServer->acLoginPass[0] == '\0' )
	{
		nret = InputBox( 全_主窗口句柄 , "请输入用户密码：" , "输入窗口" , 0 , tvdParent->pstRemoteFileServer->acLoginPass , sizeof(tvdParent->pstRemoteFileServer->acLoginPass)-1 ) ;
		if( nret == IDOK )
		{
			if( tvdParent->pstRemoteFileServer->acLoginPass[0] == '\0' )
			{
				return 1;
			}
		}
		else if( nret == IDCANCEL )
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	rfb = (struct RemoteFileBuffer *)malloc( sizeof(struct RemoteFileBuffer) ) ;
	if( rfb == NULL )
	{
		MessageBox(NULL, TEXT("不能申请内存以存放远程文件列表缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return 1;
	}
	memset( rfb , 0x00 , sizeof(struct RemoteFileBuffer) );
	rfb->remain_len = REMOTE_FILE_BUFFER_SIZE_DEFAULT - 2 ;

	memset( userpwd , 0x00 , sizeof(userpwd) );
	snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdParent->pstRemoteFileServer->acLoginUser , tvdParent->pstRemoteFileServer->acLoginPass );
	curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
	curl_easy_setopt( curl , CURLOPT_CUSTOMREQUEST , "ls" );
	curl_easy_setopt( curl , CURLOPT_WRITEDATA , rfb );
	curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
	res = curl_easy_perform( curl ) ;
	if( res == CURLE_REMOTE_ACCESS_DENIED )
	{
		free( rfb );
		return 0;
	}
	else if( res != CURLE_OK )
	{
		错误框( "连接远程文件服务器失败[%s]" , curl_easy_strerror(res) );
		free( rfb );
		if( tvdParent->pstRemoteFileServer->bConfigLoginPass == FALSE )
			memset( tvdParent->pstRemoteFileServer->acLoginPass , 0x00 , sizeof(tvdParent->pstRemoteFileServer->acLoginPass) );
		return -3;
	}

	调试_日志( "rfb->str_len[%d] ->buf[%.*s] " , rfb->str_len , rfb->str_len,rfb->buf )
	DEBUGHEXLOGC( rfb->buf , (long)(rfb->str_len) , "rfb->str_len[%d] ->remain_len[%d]" , rfb->str_len , rfb->remain_len )
	p1 = rfb->buf ;
	while( (*p1) )
	{
		p2 = strchr( p1+1 , '\n' ) ;
		if( p2 )
		{
			(*p2) = '\0' ;
			if( p2-1 > p1 && *(p2-1) == '\r' )
				*(p2-1) = '\0' ;
		}
		else
		{
			p2 = p1 + strlen(p1) ;
		}

		chFileType = (*p1) ;

		for( int n = 0 ; n < 8 ; n++ )
		{
			/* 搜索直到非空格 */
			for( ; (*p1) ; p1++ )
				if( (*p1) == ' ' )
					break;
			if( (*p1) == '\0' )
				break;

			/* 搜索直到空格 */
			for( ; (*p1) ; p1++ )
				if( (*p1) != ' ' )
					break;
			if( (*p1) == '\0' )
				break;
		}
		if( (*p1) == ' ' )
		{
			p1 = p2 + 1 ;
			continue;
		}
		memset( acFilename , 0x00 , sizeof(acFilename) );
		strncpy( acFilename , p1 , sizeof(acFilename)-1 );

		if( strcmp(acFilename,".") == 0 || strcmp(acFilename,"..") == 0 )
		{
			p1 = p2 + 1 ;
			continue;
		}

		if( chFileType == 'd' )
		{
			memset( acPathFilename , 0x00 , sizeof(acPathFilename) );
			snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s%s/" , tvdParent->acPathFilename , acFilename );
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageClosedFold , tvdParent->pstRemoteFileServer , tvdParent->acPathFilename , acPathFilename , acFilename , FALSE ) ;
			if( tvd == NULL )
			{
				free( rfb );
				return -1;
			}
		}
		else if( chFileType == '-' )
		{
			memset( acPathFilename , 0x00 , sizeof(acPathFilename) );
			snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s%s" , tvdParent->acPathFilename , acFilename );
			tvd = AddFileTreeNode( hwnd , tvdParent->hti , nFileTreeImageTextFile , tvdParent->pstRemoteFileServer , tvdParent->acPathFilename , acPathFilename , acFilename , TRUE ) ;
			if( tvd == NULL )
			{
				free( rfb );
				return -1;
			}
		}

		p1 = p2 + 1 ;
	}

	free( rfb );

	tvdParent->bIsLoadedCompleted = TRUE ;

	return 0;
}

int LoadRemoteDriverToFileTree( HWND hwnd , struct RemoteFileServer *pnodeRemoteFileServer )
{
	char			acPathFilename[ MAX_PATH ] ;
	struct 树视图数据S	*tvd = NULL ;

	memset( acPathFilename , 0x00 , sizeof(acPathFilename) );
	if( pnodeRemoteFileServer->nAccessArea == 0 )
		snprintf( acPathFilename , sizeof(acPathFilename)-1 , "sftp://%s:%d/~/" , pnodeRemoteFileServer->acNetworkAddress , pnodeRemoteFileServer->nNetworkPort );
	else
		snprintf( acPathFilename , sizeof(acPathFilename)-1 , "sftp://%s:%d/" , pnodeRemoteFileServer->acNetworkAddress , pnodeRemoteFileServer->nNetworkPort );
	tvd = AddFileTreeNode( hwnd , TVI_ROOT , 全_文件树映像驱动器 , pnodeRemoteFileServer , acPathFilename , acPathFilename , pnodeRemoteFileServer->acFileServerName , FALSE ) ;
	if( tvd == NULL )
		return -1;

	return 0;
}

int LoadRemoteDriversToFileTree( HWND hwnd )
{
	struct RemoteFileServer	*pnodeRemoteFileServer = NULL ;
	int			nret = 0 ;

	list_for_each_entry( pnodeRemoteFileServer , & 远程文件服务器列表 , struct RemoteFileServer , nodeRemoteFileServer )
	{
		LoadRemoteDriverToFileTree( hwnd , pnodeRemoteFileServer );
	}

	return 0;
}

int UpdateFileTreeNodeChildren( HWND hwnd , HTREEITEM htiParent )
{
	struct 树视图数据S	*tvdParent = NULL ;
	HTREEITEM		htiChild ;
	TVITEM			tviChild ;
	BOOL			bret ;

	int			nret = 0 ;

	SetCursor( LoadCursor(NULL,IDC_WAIT) );

	CURL			*curl = NULL ;
	char			url[ 1024 ] ;

	tvdParent = 从HTREEITEM获取树状视图数据( htiParent ) ;
	if( tvdParent->pstRemoteFileServer )
	{
		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		curl_easy_setopt( curl , CURLOPT_WRITEFUNCTION , ReadRemoteDirectory );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
	}

	if( tvdParent->nImageIndex == 全_文件树映像驱动器 && tvdParent->pstRemoteFileServer && tvdParent->bIsLoadedCompleted == FALSE )
	{
		curl_easy_setopt( curl , CURLOPT_URL , tvdParent->acPathFilename );
		nret = AppendRemoteFileTreeNodeChildren( hwnd , tvdParent , curl ) ;
		if( nret )
			return nret;
	}

	htiChild = TreeView_GetChild( hwnd , htiParent ) ;
	while( htiChild )
	{
		tviChild.mask = TVIF_HANDLE ;
		tviChild.hItem = htiChild ;
		bret = TreeView_GetItem( hwnd , & tviChild ) ;
		if( bret != TRUE )
		{
			SetCursor( LoadCursor(NULL,IDC_ARROW) );
			if( tvdParent->pstRemoteFileServer )
				curl_easy_cleanup( curl );
			return -1;
		}

		struct 树视图数据S *tvdChild = (struct 树视图数据S *)(tviChild.lParam) ;
		if( tvdChild->bIsLoadedCompleted == FALSE )
		{
			if( tvdChild->pstRemoteFileServer == NULL )
			{
				nret = AppendFileTreeNodeChildren( hwnd , tvdChild ) ;
			}
			else
			{
				memset( url , 0x00 , sizeof(url) );
				strncpy( url , tvdChild->acPathFilename , sizeof(url)-1 );
				curl_easy_setopt( curl , CURLOPT_URL , url );

				nret = AppendRemoteFileTreeNodeChildren( hwnd , tvdChild , curl ) ;
			}
			if( nret )
			{
				SetCursor( LoadCursor(NULL,IDC_ARROW) );
				if( tvdChild->pstRemoteFileServer )
					curl_easy_cleanup( curl );
				return nret;
			}
		}

		htiChild = TreeView_GetNextSibling( hwnd , htiChild ) ;
	}

	if( tvdParent->pstRemoteFileServer )
		curl_easy_cleanup( curl );

	SetCursor( LoadCursor(NULL,IDC_ARROW) );

	return 0;
}

int 文件树节点展开时执行( NMTREEVIEW *lpnmtv )
{
	if( lpnmtv->action != 2 )
		return 0;

	UpdateFileTreeNodeChildren( 全_文件树句柄 , lpnmtv->itemNew.hItem );

	return 0;
}

int 文件树节点被双击时执行()
{
	HTREEITEM		hti ;
	struct 文档类型配置S	*pst文档类型配置 ;
	char			p扩展名[ MAX_PATH ] ;

	int			nret ;

	hti = TreeView_GetSelection( 全_文件树句柄 ) ;
	if( hti == NULL )
		return 0;
	struct 树视图数据S *tvd = 从HTREEITEM获取树状视图数据( hti ) ;
	if( tvd->bIsLoadedCompleted == FALSE )
		return UpdateFileTreeNodeChildren( 全_文件树句柄 , hti ) ;
	if( tvd->nImageIndex != nFileTreeImageTextFile )
		return 1;

	SplitPathFilename( tvd->acPathFilename , NULL , NULL , NULL , NULL , p扩展名 , NULL , NULL );
	pst文档类型配置 = 获取文档类型配置( p扩展名 ) ;
	if( pst文档类型配置 == NULL )
	{
		int ID = MessageBox( NULL , "该文件不在文本编辑器支持文件类型中，是否继续打开？" , "文件类型警告" , MB_ICONWARNING|MB_YESNO  ) ;
		if( ID == IDNO )
			return 0;
	}

	if( tvd->pstRemoteFileServer == NULL )
		nret = OpenFileDirectly( tvd->acPathFilename ) ;
	else
		nret = OpenRemoteFileDirectly( tvd->pstRemoteFileServer , tvd->acPathFilename ) ;
	if( nret )
		return nret;

	return 0;
}

int RefreshFileTree()
{
	HTREEITEM		htiSelect ;
	struct 树视图数据S	*tvd = NULL ;
	struct 树视图数据S	tvdSelect ;
	HTREEITEM		htiParent ;
	struct 树视图数据S	*tvdParent = NULL ;
	HTREEITEM		htiFirstChild ;
	HTREEITEM		htiChild ;
	TVITEM			tviChild ;

	BOOL			bret ;

	int			nret ;

	htiSelect = TreeView_GetSelection( 全_文件树句柄 ) ;
	if( htiSelect == NULL )
		return 0;
	tvd = 从HTREEITEM获取树状视图数据( htiSelect ) ;
	memcpy( & tvdSelect , tvd , sizeof(struct 树视图数据S) );

	htiParent = TreeView_GetParent( 全_文件树句柄 , htiSelect ) ;
	if( htiParent == NULL )
		return 0;
	tvdParent = 从HTREEITEM获取树状视图数据( htiParent ) ;
	while(1)
	{
		htiFirstChild = TreeView_GetChild( 全_文件树句柄 , htiParent ) ;
		if( htiFirstChild == NULL )
			break;
		tvd = 从HTREEITEM获取树状视图数据( htiFirstChild ) ;
		free( tvd );

		TreeView_DeleteItem( 全_文件树句柄 , htiFirstChild );
	}

	if( tvdParent->pstRemoteFileServer == NULL )
	{
		nret = AppendFileTreeNodeChildren( 全_文件树句柄 , tvdParent ) ;
		if( nret )
			return nret;
	}
	else
	{
		CURL			*curl = NULL ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		curl_easy_setopt( curl , CURLOPT_WRITEFUNCTION , ReadRemoteDirectory );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );

		curl_easy_setopt( curl , CURLOPT_URL , tvdParent->acPathFilename );

		nret = AppendRemoteFileTreeNodeChildren( 全_文件树句柄 , tvdParent , curl ) ;
		if( nret )
		{
			curl_easy_cleanup( curl );
			return -1;
		}

		curl_easy_cleanup( curl );
	}

	nret = UpdateFileTreeNodeChildren( 全_文件树句柄 , htiParent ) ;
	if( nret )
		return nret;

	htiChild = TreeView_GetChild( 全_文件树句柄 , htiParent ) ;
	while( htiChild )
	{
		tviChild.mask = TVIF_HANDLE ;
		tviChild.hItem = htiChild ;
		bret = TreeView_GetItem( 全_文件树句柄 , & tviChild ) ;
		if( bret != TRUE )
			return -1;

		struct 树视图数据S *tvdChild = (struct 树视图数据S *)(tviChild.lParam) ;
		if( strcmp(tvdChild->acPathFilename,tvdSelect.acPathFilename) == 0 && strcmp(tvdChild->acFilename,tvdSelect.acFilename) == 0 )
		{
			TreeView_SelectItem( 全_文件树句柄 , htiChild );
			break;
		}

		htiChild = TreeView_GetNextSibling( 全_文件树句柄 , htiChild ) ;
	}

	return 0;
}

int FileTreeRenameDirectory()
{
	HTREEITEM		htiSelect ;
	struct 树视图数据S	*tvdSelect = NULL ;
	char			acOldDirectoryName[ MAX_PATH ] ;
	char			acNewDirectoryName[ MAX_PATH ] ;
	char			acNewDirectoryPathName[ MAX_PATH ] ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( 全_文件树句柄 ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = 从HTREEITEM获取树状视图数据( htiSelect ) ;

	memset( acOldDirectoryName , 0x00 , sizeof(acOldDirectoryName) );
	strncpy( acOldDirectoryName , tvdSelect->acFilename , sizeof(acOldDirectoryName)-1 );
	strcpy( acNewDirectoryName , acOldDirectoryName );
	nret = InputBox( 全_主窗口句柄 , "请输入新目录名：" , "输入窗口" , 0 , acNewDirectoryName , sizeof(acNewDirectoryName)-1 ) ;
	if( nret == IDOK )
	{
		;
	}
	else if( nret == IDCANCEL )
	{
		return 1;
	}
	else
	{
		return -1;
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		snprintf( acNewDirectoryPathName , sizeof(acNewDirectoryPathName)-1 , "%s/%s" , tvdSelect->acPathName , acNewDirectoryName );
		BOOL bret = MoveFile( tvdSelect->acPathFilename , acNewDirectoryPathName ) ;
		if( bret == FALSE )
		{
			错误框( "目录[%s]改名[%s]失败，请检查目录是否非空" , acOldDirectoryName , acNewDirectoryName );
			return -1;
		}
		else
		{
			TVITEM	tvi ;
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			tvi.mask = TVIF_HANDLE|TVIF_TEXT ;
			tvi.hItem = htiSelect ;
			tvi.pszText = acNewDirectoryName ;
			TreeView_SetItem( 全_文件树句柄 , & tvi );

			strncpy( tvdSelect->acFilename , acNewDirectoryName , sizeof(tvdSelect->acFilename)-1 );
			memset( tvdSelect->acPathFilename , 0x00 , sizeof(tvdSelect->acPathFilename) );
			snprintf( tvdSelect->acPathFilename , sizeof(tvdSelect->acPathFilename)-1 , "%s/%s" , tvdSelect->acPathName , tvdSelect->acFilename );
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			url[ MAX_PATH ] ;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		snprintf( url , sizeof(url)-1 , "%s" , tvdSelect->acPathName );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_NOBODY , 1L );
		char *p = NULL ;
		p = strchr( tvdSelect->acPathName , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
			p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rename %s%s %s%s" , p+1,tvdSelect->acFilename , p+1,acNewDirectoryName );
		}
		else
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rename /%s%s /%s%s" , p+1,tvdSelect->acFilename , p+1,acNewDirectoryName );
		}
		headerlist = curl_slist_append( headerlist , postquote );
		curl_easy_setopt( curl , CURLOPT_POSTQUOTE , headerlist );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			错误框( "目录[%s]改名[%s]失败[%d][%s]，请检查目录是否非空" , tvdSelect->acFilename , acNewDirectoryName , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			TVITEM	tvi ;
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			tvi.mask = TVIF_HANDLE|TVIF_TEXT ;
			tvi.hItem = htiSelect ;
			tvi.pszText = acNewDirectoryName ;
			TreeView_SetItem( 全_文件树句柄 , & tvi );

			strncpy( tvdSelect->acFilename , acNewDirectoryName , sizeof(tvdSelect->acFilename)-1 );
			memset( tvdSelect->acPathFilename , 0x00 , sizeof(tvdSelect->acPathFilename) );
			snprintf( tvdSelect->acPathFilename , sizeof(tvdSelect->acPathFilename)-1 , "%s%s/" , tvdSelect->acPathName , tvdSelect->acFilename );
		}

		curl_easy_cleanup( curl );
	}

	InvalidateRect( 全_文件树句柄 , NULL , TRUE );
	UpdateWindow( 全_文件树句柄 );

	return 0;
}

int FileTreeDeleteDirectory()
{
	HTREEITEM		htiSelect ;
	struct 树视图数据S	*tvdSelect = NULL ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( 全_文件树句柄 ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = 从HTREEITEM获取树状视图数据( htiSelect ) ;

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		BOOL bret = RemoveDirectory( tvdSelect->acPathFilename ) ;
		if( bret == FALSE )
		{
			错误框( "删除目录[%s]失败，请检查目录是否非空" , tvdSelect->acPathFilename );
			return -1;
		}
		else
		{
			TreeView_DeleteItem( 全_文件树句柄 , htiSelect );
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		curl_easy_setopt( curl , CURLOPT_URL , tvdSelect->acPathName );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_NOBODY , 1L );
		char *p = NULL ;
		p = strchr( tvdSelect->acPathFilename , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
			p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rmdir %.*s" , strlen(p+1)-1 , p+1 );
		}
		else
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rmdir /%.*s" , strlen(p+1)-1 , p+1 );
		}
		headerlist = curl_slist_append( headerlist , postquote );
		curl_easy_setopt( curl , CURLOPT_POSTQUOTE , headerlist );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			错误框( "删除目录[%s]失败[%d][%s]，请检查目录是否非空" , tvdSelect->acPathFilename , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			TreeView_DeleteItem( 全_文件树句柄 , htiSelect );
		}

		curl_easy_cleanup( curl );
	}

	InvalidateRect( 全_文件树句柄 , NULL , TRUE );
	UpdateWindow( 全_文件树句柄 );

	return 0;
}

int FileTreeCreateSubDirectory()
{
	HTREEITEM		htiSelect ;
	struct 树视图数据S	*tvdSelect = NULL ;
	char			acDirectoryName[ MAX_PATH ] ;
	char			acPathDirectoryName[ MAX_PATH ] ;
	char			acPathName[ MAX_PATH ] ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( 全_文件树句柄 ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = 从HTREEITEM获取树状视图数据( htiSelect ) ;

	memset( acDirectoryName , 0x00 , sizeof(acDirectoryName) );
	nret = InputBox( 全_主窗口句柄 , "请输入子目录名：" , "输入窗口" , 0 , acDirectoryName , sizeof(acDirectoryName)-1 ) ;
	if( nret == IDOK )
	{
		;
	}
	else if( nret == IDCANCEL )
	{
		return 1;
	}
	else
	{
		return -1;
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		snprintf( acPathDirectoryName , sizeof(acPathDirectoryName)-1 , "%s\\%s" , tvdSelect->acPathFilename , acDirectoryName );
		strcpy( acPathName , tvdSelect->acPathFilename );

		BOOL bret = CreateDirectory( acPathDirectoryName , NULL ) ;
		if( bret == FALSE )
		{
			错误框( "创建目录[%s]失败" , acPathDirectoryName );
			return -1;
		}
		else
		{
			AddFileTreeNode( 全_文件树句柄 , htiSelect , nFileTreeImageClosedFold , NULL , acPathName , acPathDirectoryName , acDirectoryName , FALSE );
			TreeView_Expand( 全_文件树句柄 , htiSelect , TVE_EXPAND );
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			url[ MAX_PATH ] ;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		snprintf( acPathDirectoryName , sizeof(acPathDirectoryName)-1 , "%s%s/" , tvdSelect->acPathFilename , acDirectoryName );
		strcpy( acPathName , tvdSelect->acPathFilename );

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		snprintf( url , sizeof(url)-1 , "%s" , tvdSelect->acPathFilename );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_NOBODY , 1L );
		memset( postquote , 0x00 , sizeof(postquote) );
		char *p = NULL ;
		p = strchr( tvdSelect->acPathFilename , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
			p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "mkdir %s%s" , p+1 , acDirectoryName );
		}
		else
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "mkdir /%s%s" , p+1 , acDirectoryName );
		}
		headerlist = curl_slist_append( headerlist , postquote );
		curl_easy_setopt( curl , CURLOPT_POSTQUOTE , headerlist );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			错误框( "创建子目录[%s]失败[%d][%s]" , acPathDirectoryName , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			AddFileTreeNode( 全_文件树句柄 , htiSelect , nFileTreeImageClosedFold , tvdSelect->pstRemoteFileServer , acPathName , acPathDirectoryName , acDirectoryName , FALSE );
		}

		curl_easy_cleanup( curl );
	}

	InvalidateRect( 全_文件树句柄 , NULL , TRUE );
	UpdateWindow( 全_文件树句柄 );

	return 0;
}

static size_t WriteRemoteFile(void *buffer, size_t size, size_t nmemb, void *stream)
{
	return 0;
}

int FileTreeCreateFile()
{
	HTREEITEM		htiSelect ;
	struct 树视图数据S	*tvdSelect = NULL ;
	char			acFileName[ MAX_PATH ] ;
	char			acPathFilename[ MAX_PATH ] ;
	char			acPathName[ MAX_PATH ] ;
	struct 树视图数据S	*tvdCreate = NULL ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( 全_文件树句柄 ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = 从HTREEITEM获取树状视图数据( htiSelect ) ;

	memset( acFileName , 0x00 , sizeof(acFileName) );
	nret = InputBox( 全_主窗口句柄 , "请输入文件名：" , "输入窗口" , 0 , acFileName , sizeof(acFileName)-1 ) ;
	if( nret == IDOK )
	{
		;
	}
	else if( nret == IDCANCEL )
	{
		return 1;
	}
	else
	{
		return -1;
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s/%s" , tvdSelect->acPathFilename , acFileName );
		strcpy( acPathName , tvdSelect->acPathFilename );

		FILE *fp = fopen( acPathFilename , "r" ) ;
		if( fp )
		{
			fclose( fp );
			错误框( "文件[%s]已存在" , acPathFilename );
			return -1;
		}

		fp = fopen( acPathFilename , "w" ) ;
		if( fp == NULL )
		{
			错误框( "创建文件[%s]失败" , acPathFilename );
			return -1;
		}
		else
		{
			fclose( fp );
			tvdCreate = AddFileTreeNode( 全_文件树句柄 , htiSelect , nFileTreeImageTextFile , NULL , acPathName , acPathFilename , acFileName , TRUE ) ;
			/*
			TreeView_SelectItem( 全_文件树句柄 , tvdCreate->hti );
			文件树节点被双击时执行();
			*/
			TreeView_Expand( 全_文件树句柄 , htiSelect , TVE_EXPAND );
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			url[ MAX_PATH ] ;
		char			userpwd[ 256 ] ;

		snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s%s" , tvdSelect->acPathFilename , acFileName );
		strcpy( acPathName , tvdSelect->acPathFilename );

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		snprintf( url , sizeof(url)-1 , "%s%s" , tvdSelect->acPathFilename , acFileName );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_READFUNCTION , WriteRemoteFile );
		curl_easy_setopt( curl , CURLOPT_UPLOAD , 1L );
		curl_easy_setopt( curl , CURLOPT_READDATA , NULL );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			错误框( "创建文件[%s]失败[%d][%s]" , acPathFilename , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			tvdCreate = AddFileTreeNode( 全_文件树句柄 , htiSelect , nFileTreeImageTextFile , tvdSelect->pstRemoteFileServer , acPathName , acPathFilename , acFileName , TRUE ) ;
			TreeView_SelectItem( 全_文件树句柄 , tvdCreate->hti );
			文件树节点被双击时执行();
		}

		curl_easy_cleanup( curl );
	}

	InvalidateRect( 全_文件树句柄 , NULL , TRUE );
	UpdateWindow( 全_文件树句柄 );

	return 0;
}

int FileTreeCopyFile()
{
	HTREEITEM		htiSelect ;
	struct 树视图数据S	*tvdSelect = NULL ;
	char			acNewFileName[ MAX_PATH ] ;
	char			acNewPathFilename[ MAX_PATH ] ;
	struct 树视图数据S	*tvdCreate = NULL ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( 全_文件树句柄 ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = 从HTREEITEM获取树状视图数据( htiSelect ) ;

	strcpy( acNewFileName , tvdSelect->acFilename );
	nret = InputBox( 全_主窗口句柄 , "请输入文件名：" , "输入窗口" , 0 , acNewFileName , sizeof(acNewFileName)-1 ) ;
	if( nret == IDOK )
	{
		;
	}
	else if( nret == IDCANCEL )
	{
		return 1;
	}
	else
	{
		return -1;
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		snprintf( acNewPathFilename , sizeof(acNewPathFilename)-1 , "%s/%s" , tvdSelect->acPathName , acNewFileName );

		BOOL bret = CopyFile( tvdSelect->acPathFilename , acNewPathFilename , TRUE ) ;
		if( bret == FALSE )
		{
			错误框( "复制文件[%s]失败" , acNewPathFilename );
			return -1;
		}
		else
		{
			tvdCreate = AddFileTreeNode( 全_文件树句柄 , TreeView_GetParent(全_文件树句柄,htiSelect) , nFileTreeImageTextFile , NULL , tvdSelect->acPathName , acNewPathFilename , acNewFileName , TRUE ) ;
			TreeView_SelectItem( 全_文件树句柄 , tvdCreate->hti );
		}
	}
	else
	{
#if 0
		CURL			*curl = NULL ;
		CURLcode		res;
		char			url[ MAX_PATH ] ;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		snprintf( acNewPathFilename , sizeof(acNewPathFilename)-1 , "%s%s" , tvdSelect->acPathName , acNewFileName );
		// strcpy( acPathName , tvdSelect->acPathFilename );


		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		strncpy( url , 选项卡页节点->acPathFilename , sizeof(url)-1 );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , 选项卡页节点->stRemoteFileServer.acLoginUser , 选项卡页节点->stRemoteFileServer.acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		curl_easy_setopt( curl , CURLOPT_UPLOAD , 1L );
		curl_easy_setopt( curl , CURLOPT_READFUNCTION , WriteRemoteFile );
		curl_easy_setopt( curl , CURLOPT_READDATA , 选项卡页节点 );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			MessageBox(NULL, TEXT("连接远程文件服务器失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
			curl_easy_cleanup( curl );
			return -3;
		}
		else
		{
			curl_easy_cleanup( curl );
		}
#endif
		错误框( "暂时不支持远程SFTP复制文件" );
	}

	InvalidateRect( 全_文件树句柄 , NULL , TRUE );
	UpdateWindow( 全_文件树句柄 );

	return 0;
}

int FileTreeRenameFile()
{
	HTREEITEM		htiSelect ;
	struct 树视图数据S	*tvdSelect = NULL ;
	char			acOldFileName[ MAX_PATH ] ;
	char			acNewFileName[ MAX_PATH ] ;
	char			acNewPathFileName[ MAX_PATH ] ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( 全_文件树句柄 ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = 从HTREEITEM获取树状视图数据( htiSelect ) ;

	memset( acOldFileName , 0x00 , sizeof(acOldFileName) );
	strncpy( acOldFileName , tvdSelect->acFilename , sizeof(acOldFileName)-1 );
	strcpy( acNewFileName , acOldFileName );
	nret = InputBox( 全_主窗口句柄 , "请输入新文件名：" , "输入窗口" , 0 , acNewFileName , sizeof(acNewFileName)-1 ) ;
	if( nret == IDOK )
	{
		;
	}
	else if( nret == IDCANCEL )
	{
		return 1;
	}
	else
	{
		return -1;
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		snprintf( acNewPathFileName , sizeof(acNewPathFileName)-1 , "%s/%s" , tvdSelect->acPathName , acNewFileName );
		BOOL bret = MoveFile( tvdSelect->acPathFilename , acNewPathFileName ) ;
		if( bret == FALSE )
		{
			错误框( "目录[%s]改名[%s]失败，请检查目录是否非空" , acOldFileName , acNewFileName );
			return -1;
		}
		else
		{
			TVITEM	tvi ;
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			tvi.mask = TVIF_HANDLE|TVIF_TEXT ;
			tvi.hItem = htiSelect ;
			tvi.pszText = acNewFileName ;
			TreeView_SetItem( 全_文件树句柄 , & tvi );

			strncpy( tvdSelect->acFilename , acNewFileName , sizeof(tvdSelect->acFilename)-1 );
			memset( tvdSelect->acPathFilename , 0x00 , sizeof(tvdSelect->acPathFilename) );
			snprintf( tvdSelect->acPathFilename , sizeof(tvdSelect->acPathFilename)-1 , "%s/%s" , tvdSelect->acPathName , tvdSelect->acFilename );
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			url[ MAX_PATH ] ;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		snprintf( url , sizeof(url)-1 , "%s" , tvdSelect->acPathName );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_NOBODY , 1L );
		char *p = NULL ;
		p = strchr( tvdSelect->acPathName , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
			p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rename %s%s %s%s" , p+1,tvdSelect->acFilename , p+1,acNewFileName );
		}
		else
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rename /%s%s /%s%s" , p+1,tvdSelect->acFilename , p+1,acNewFileName );
		}
		headerlist = curl_slist_append( headerlist , postquote );
		curl_easy_setopt( curl , CURLOPT_POSTQUOTE , headerlist );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			错误框( "文件[%s]改名[%s]失败[%d][%s]" , tvdSelect->acFilename , acNewFileName , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			TVITEM	tvi ;
			memset( & tvi , 0x00 , sizeof(TVITEM) );
			tvi.mask = TVIF_HANDLE|TVIF_TEXT ;
			tvi.hItem = htiSelect ;
			tvi.pszText = acNewFileName ;
			TreeView_SetItem( 全_文件树句柄 , & tvi );

			strncpy( tvdSelect->acFilename , acNewFileName , sizeof(tvdSelect->acFilename)-1 );
			memset( tvdSelect->acPathFilename , 0x00 , sizeof(tvdSelect->acPathFilename) );
			snprintf( tvdSelect->acPathFilename , sizeof(tvdSelect->acPathFilename)-1 , "%s%s" , tvdSelect->acPathName , tvdSelect->acFilename );
		}

		curl_easy_cleanup( curl );
	}

	InvalidateRect( 全_文件树句柄 , NULL , TRUE );
	UpdateWindow( 全_文件树句柄 );

	return 0;
}

int FileTreeDeleteFile()
{
	HTREEITEM		htiSelect ;
	struct 树视图数据S	*tvdSelect = NULL ;
	int			n选项卡页面数量 , n选项卡页面索引 ;
	struct 选项卡页面S		*选项卡页节点 = NULL ;
	TCITEM			tci ;
	int			nret = 0 ;

	htiSelect = TreeView_GetSelection( 全_文件树句柄 ) ;
	if( htiSelect == NULL )
		return 0;
	tvdSelect = 从HTREEITEM获取树状视图数据( htiSelect ) ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		选项卡页节点 = (struct 选项卡页面S *)(tci.lParam);
		if( strcmp( 选项卡页节点->acPathFilename , tvdSelect->acPathFilename ) == 0 )
		{
			nret = 关闭文件时( 选项卡页节点 ) ;
			if( nret )
				return 0;
			break;
		}
	}

	if( tvdSelect->pstRemoteFileServer == NULL )
	{
		BOOL bret = DeleteFile( tvdSelect->acPathFilename ) ;
		if( bret == FALSE )
		{
			错误框( "删除文件[%s]失败" , tvdSelect->acPathFilename );
			return -1;
		}
		else
		{
			TreeView_DeleteItem( 全_文件树句柄 , htiSelect );
		}
	}
	else
	{
		CURL			*curl = NULL ;
		CURLcode		res;
		char			postquote[ MAX_PATH ] ;
		struct curl_slist	*headerlist = NULL;
		char			userpwd[ 256 ] ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -2;
		}

		curl_easy_setopt( curl , CURLOPT_URL , tvdSelect->acPathName );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , tvdSelect->pstRemoteFileServer->acLoginUser , tvdSelect->pstRemoteFileServer->acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_NOBODY , 1L );
		char *p = NULL ;
		p = strchr( tvdSelect->acPathFilename , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
			p = strchr( p+1 , '/' ) ;
		if( tvdSelect->pstRemoteFileServer->nAccessArea == 0 )
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rm %.*s" , strlen(p+1) , p+1 );
		}
		else
		{
			memset( postquote , 0x00 , sizeof(postquote) );
			snprintf( postquote , sizeof(postquote)-1 , "rm /%.*s" , strlen(p+1) , p+1 );
		}
		headerlist = curl_slist_append( headerlist , postquote );
		curl_easy_setopt( curl , CURLOPT_POSTQUOTE , headerlist );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( res != CURLE_OK )
		{
			错误框( "删除文件[%s]失败[%d][%s]" , tvdSelect->acPathFilename , res , curl_easy_strerror(res) );
			curl_easy_cleanup( curl );
			return -1;
		}
		else
		{
			TreeView_DeleteItem( 全_文件树句柄 , htiSelect );
		}

		curl_easy_cleanup( curl );
	}

	InvalidateRect( 全_文件树句柄 , NULL , TRUE );
	UpdateWindow( 全_文件树句柄 );

	return 0;
}

int 定位目录( char *acPathName )
{
	char			*acPathNameDup = NULL ;
	char			*pcDirectoryName = NULL ;
	HTREEITEM		hti = NULL , htiChild = NULL ;
	struct 树视图数据S	*tvd = NULL ;
	int			nret = 0 ;
	BOOL			bret = TRUE ;

	acPathNameDup = _strdup(acPathName) ;
	if( acPathNameDup == NULL )
		return -1;

	htiChild = TreeView_GetRoot( 全_文件树句柄 ) ;
	pcDirectoryName = strtok( acPathNameDup , "\\/" ) ;
	while( pcDirectoryName )
	{
		hti = htiChild ;

		do
		{
			tvd = 从HTREEITEM获取树状视图数据( hti ) ;
			if( tvd == NULL )
			{
				break;
			}
			else if( tvd->bIsLoadedCompleted == FALSE )
			{
				nret = AppendFileTreeNodeChildren( 全_文件树句柄 , tvd ) ;
				if( nret )
				{
					free( acPathNameDup );
					return nret;
				}
			}
			if( strcmp(tvd->acFilename,pcDirectoryName) == 0 )
				break;

			hti = TreeView_GetNextSibling( 全_文件树句柄 , hti ) ;
		}
		while( hti );
		if( hti == NULL )
			break;

		htiChild = TreeView_GetChild( 全_文件树句柄 , hti ) ;
		pcDirectoryName = strtok( NULL , "\\/" ) ;
	}
	if( pcDirectoryName == NULL )
	{
		全_显示文件树栏 = TRUE ;
		TreeView_SelectItem( 全_文件树句柄 , hti );
		SendMessage( 全_文件树句柄 , WM_SETFOCUS , 0 , 0 );
		更新所有窗口( 全_主窗口句柄 );
	}

	free( acPathNameDup );

	return 0;
}
