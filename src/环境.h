#ifndef _ENV_
#define _ENV_

#include "framework.h"

extern BOOL 全_已选择环境文件弹出菜单 ;
extern BOOL 全_已选择环境目录弹出菜单 ;

int OnEnvFilePopupMenu();
int OnEnvDirectoryPopupMenu();

int OnEnvSetProcessFileCommand();
int OnEnvExecuteProcessFileCommand( struct 选项卡页面S *选项卡页节点 );
int OnEnvSetProcessTextCommand();
int OnEnvExecuteProcessTextCommand( struct 选项卡页面S *选项卡页节点 );

#endif
