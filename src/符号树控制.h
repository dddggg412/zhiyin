#ifndef _SYMBOLTREECTL_
#define _SYMBOLTREECTL_

#include "framework.h"

int CreateSymbolTreeCtl( struct 选项卡页面S *选项卡页节点 , HFONT hFont );
int GetSymbolTreeItemAndAddTextToEditor( struct 选项卡页面S *选项卡页节点 );

/*
 * SQL
 */

int ReloadSymbolTree_SQL( struct 选项卡页面S *选项卡页节点 );

/*
 * Redis
 */

typedef redisContext *funcRedisConnectWithTimeout(const char *ip, int port, const struct timeval tv);
typedef void funcRedisFree(redisContext *c);
typedef void *funcRedisCommand(redisContext *c, const char *format, ...);
typedef void funcFreeReplyObject(void *reply);

struct Redis库函数
{
	HMODULE				hmod_hiredis_dll ;
	funcRedisConnectWithTimeout	*pfuncRedisConnectWithTimeout ;
	funcRedisFree			*pfuncRedisFree ;
	funcRedisCommand		*pfuncRedisCommand ;
	funcFreeReplyObject		*pfuncFreeReplyObject ;
};

extern struct Redis库函数	stRedisLibraryFunctions ;

struct Redis连接句柄
{
	redisContext		*ctx ;
};

struct Redis连接配置
{
	char	host[ 40 ] ;
	int	port ;
	char	pass[ 64 ] ;
	char	dbsl[ 20 ] ;
};

int ParseRedisFileConfigHeader( struct 选项卡页面S *选项卡页节点 );
int DisconnectFromRedis( struct 选项卡页面S *选项卡页节点 );
int ConnectToRedis( struct 选项卡页面S *选项卡页节点 );
int ExecuteRedisQuery_REDIS( struct 选项卡页面S *选项卡页节点 );

int ReloadSymbolTree_XML( struct 选项卡页面S *选项卡页节点 );
int ReloadSymbolTree_JSON( struct 选项卡页面S *选项卡页节点 );
int GetSymbolTreeItemAndGotoEditorPos( struct 选项卡页面S *选项卡页节点 );

#endif
