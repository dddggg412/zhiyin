
#ifndef _H_FRAMEWORK_
#define _H_FRAMEWORK_

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN			 // 从 Windows 头文件中排除极少使用的内容
// Windows 头文件
#include <windows.h>
#include <richedit.h>
#include <Commdlg.h>
#include <commctrl.h>
#include <windowsx.h>
#include <shellapi.h>
#include <process.h>
#include <uxtheme.h>
// C 运行时头文件
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <wchar.h>
#include <string.h>


static char 关于知语言编辑器[]=
		"知语言编辑器\n"
		"版本：0.0.1\n"
		"版权(C) 2023-2023 位中原\n"
		"Q群：500558920\n"
		"网址：www.880.xin\n"
		"仓库：https://gitee.com/zhi-yu-yan/zhiyin.git\n"
		"——————————————————————\n"
		"中文编程语言（知语言）开发的开源文本编辑器体积小、\n"
		"速度快、性能卓越。为中文编程提供一款更好的源代码\n"
		"编辑器。\n"
		;

int inet_pton (int af, const char *cp, void *buf);
char *strtok_s( char *strToken, const char *strDelimit, char **buf);
#pragma comment(linker,"\"/manifestdependency:type='win32' \
	name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
	processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


#include "列表.h"
#include "红黑树.h"

#include "日志.h"

#include "fasterjson.h"
#include "fasterxml.h"

#include "./三方/pthread/pthread.h"
#include "./三方/scintilla/scintilla.h"
#include "./三方/scintilla/SciLexer.h"

#include "./三方/pcre/pcre.h"

#include "./三方/curl/curl.h"

#include "./三方/openssl/sha.h"
#include "./三方/openssl/aes.h"
#include "./三方/openssl/evp.h"
#include "./三方/openssl/md5.h"
#include "./三方/openssl/des.h"

#include "./三方/iconv/iconv.h"

#include "./三方/mysql/mysql.h"

#include "./三方/oracle/oci.h"

#include "./三方/hiredis/hiredis.h"

#include "./三方/sqlite3/sqlite3.h"

#include "./三方/thc_hydra/libpq-fe.h"

#include "输入框.h"

#define 启动时打开文件最大数量M		20
#define 最近打开路径文件名最大数量M	20
#define 视觉样式最大数量M		20
#define 视觉文件类型最大数量M			100
#define 状态栏项数量M			5
#define 状态栏项_正在处理_索引M	0
#define 状态栏项_位置_索引M	1
#define 状态栏项_换行符_索引M	2
#define 状态栏项_编码_索引M	3
#define 状态栏项_选择文本_索引M	4

#include "resource.h"
#include "工具栏.h"
#include "公用.h"
#include "编码.h"
#include "远程文件服务器.h"
#include "样式主题.h"
#include "符号列表控制.h"
#include "符号树控制.h"
#include "查询结果编辑控制.h"
#include "查询结果表控制.h"
#include "zhiyin.h"
#include "配置.h"
#include "文件树栏.h"
#include "选项卡页.h"
#include "编辑框控制.h"
#include "文件.h"
#include "编辑框.h"
#include "查找.h"
#include "视图.h"
#include "环境.h"
#include "源代码.h"
#include "文档类型.h"
#include "菜单.h"
#include "状态栏.h"
#include "主窗口过程.h"

#include "minus.xpm"
#include "plus.xpm"

#define 错误_无法打开文件	-101
#define 错误_正则表达式_没发现	-131

#define 选项卡_默认高度		21

#define 拖动或单击鼠标移动阈值		2

#define 文件树_边框_左		2
#define 文件树_边框_右		2
#define 文件树_边框_上		2
#define 文件树_边框_下		2

#define SCINTILLA_边框_左		2
#define SCINTILLA_边框_右		2
#define SCINTILLA_边框_上		4
#define SCINTILLA_边框_下		2

#define 边框_行号_索引		0
#define 边框_行号_宽度		30

#define 边框_书签_索引		1
#define 边框_书签_宽度		16
#define 边框_书签_MASKN		SC_MARK_BOOKMARK
#define 边框_书签_值		0

#define 边框_折叠_索引		2
#define 边框_折叠_宽度		16

#define 符号列表_边框_左		2
#define 符号列表_边框_右		2
#define 符号列表_边框_上		2
#define 符号列表_边框_下	    2

#define 符号树_边框_左		2
#define 符号树_边框_右		2
#define 符号树_边框_上		2
#define 符号树_边框_下	2

#define 符号树栏_宽度_默认	250
#define 符号树栏_宽度_最小		100

#define 符号列表_宽度_默认	300
#define 符号列表_宽度_最小		100

#define 符号树_宽度_默认		300
#define 树视图_宽度_最小		100

#define SQL查询结果_编辑_高度_默认	80
#define SQL查询结果_编辑_高度_最小		40

#define SQL查询结果_列表视图_高度_默认	260
#define SQL查询结果_列表视图_高度_最小	40

#define 分割_宽度			6

#define PATTERN_OVECCOUNT		30

#define 字符_宽度			8

#define 空格_大小			3

#define 编辑框_选项卡_宽度_默认	8

#define FONTSIZE_ZOOMOUT_ADJUSTVALUE	4
#define FONTSIZE_EDITOR_ADJUSTVALUE	0

#define MAX_TRACE_COUNT			100

#define 选项卡关闭按钮_宽度		选项卡_默认高度-8
#define 选项卡关闭按钮_高度		选项卡_默认高度-8

extern struct 主配置S	全_风格主配置 ;
extern struct 文档类型配置S		全_文档类型配置[] ;
extern struct list_head			远程文件服务器列表 ;

extern HBRUSH		全_笔刷常用控件深色 ;

#endif
