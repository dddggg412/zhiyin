#include "framework.h"

struct list_head	listNavigateBackNextTrace ;
int			nMaxNavigateBackNextTraceCount = 0 ;

HWND			查找框句柄 ;
HWND			hwndEditBoxInSearchFind ;

HWND			已查框句柄 ;
HWND			搜索中已找到的列表框句柄 ;

HWND			替换对话框句柄 ;
HWND			hwndFromEditBoxInSearchReplace ;
HWND			hwndToEditBoxInSearchReplace ;

void JumpGotoLine( struct 选项卡页面S *选项卡页节点 , int nGotoLineNo , int 文本当前行号 )
{
	int nLineCount = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	int nScreenLineCount = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_LINESONSCREEN , 0 , 0 ) ;
	if( nGotoLineNo > 文本当前行号 )
	{
		int nJumpLineNo = nGotoLineNo + nScreenLineCount / 2 ;
		if( nJumpLineNo > nLineCount )
			nJumpLineNo = nLineCount ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOLINE , nJumpLineNo , 0 );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOLINE , nGotoLineNo , 0 );
	}
	else
	{
		int nJumpLineNo = nGotoLineNo - nScreenLineCount / 2 ;
		if( nJumpLineNo < 1 )
			nJumpLineNo = 1 ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOLINE , nJumpLineNo , 0 );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOLINE , nGotoLineNo , 0 );
	}

	return;
}

static size_t PrepareSearchFlags( HWND hwndSearch )
{
	size_t		flags = 0 ;

	if( IsDlgButtonChecked( hwndSearch , IDC_OPTIONS_WHOLEWORD ) )
		flags |= SCFIND_WHOLEWORD ;
	if( IsDlgButtonChecked( hwndSearch , IDC_OPTIONS_MATCHCASE ) )
		flags |= SCFIND_MATCHCASE ;
	if( IsDlgButtonChecked( hwndSearch , IDC_OPTIONS_WORDSTART ) )
		flags |= SCFIND_WORDSTART ;

	if( IsDlgButtonChecked( hwndSearch , IDC_TEXTTYPE_REGEXP ) )
		flags |= SCFIND_REGEXP ;
	/*
	else if( IsDlgButtonChecked( hwndSearch , IDC_TEXTTYPE_POSIXREGEXP ) )
		flags |= SCFIND_POSIX ;
	*/

	return flags;
}

static int 调整CurrentPosBeforeFindPrev()
{
	size_t nSelectStartPos=(size_t)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 );
	size_t nSelectEndPos=(size_t)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETSELECTIONEND , 0 , 0 );
	int 当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	if( nSelectEndPos != nSelectStartPos )
	{
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_SETCURRENTPOS , 当前文本字节位置-1 , 0 ) ;
	}
	return 当前文本字节位置;
}

static int 调整CurrentPosBeforeFindNext()
{
	int nSelectStartPos=(int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 );
	int nSelectEndPos=(int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETSELECTIONEND , 0 , 0 );
	int 当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	if( nSelectEndPos != nSelectStartPos )
	{
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_SETCURRENTPOS , 当前文本字节位置+1 , 0 ) ;
	}

	return 当前文本字节位置;
}

static char *GetWindowString( HWND hWnd )
{
	int	nTextLen ;
	char	*pcText = NULL ;

	nTextLen = (int)SendMessage( hWnd , WM_GETTEXTLENGTH , 0 , 0 ) ;
	if( nTextLen == 0 )
		return NULL;
	pcText = (char*)malloc( nTextLen+1) ;
	if( pcText == NULL )
		return NULL;
	memset( pcText , 0x00 , nTextLen+1 );
	SendMessage( hWnd , WM_GETTEXT , nTextLen+1 , (LPARAM)pcText );

	return pcText;
}

static void SetWindowString( HWND hWnd , char *pcText )
{
	SendMessage( hWnd , WM_SETTEXT , -1 , (LPARAM)pcText );
}

void SyncReplaceOptionsToFindDialog()
{
	CheckDlgButton( 查找框句柄 , IDC_TEXTTYPE_GENERAL , IsDlgButtonChecked( 替换对话框句柄 , IDC_TEXTTYPE_GENERAL ) );
	CheckDlgButton( 查找框句柄 , IDC_TEXTTYPE_REGEXP , IsDlgButtonChecked( 替换对话框句柄 , IDC_TEXTTYPE_REGEXP ) );

	CheckDlgButton( 查找框句柄 , IDC_AREATYPE_THISFILE , IsDlgButtonChecked( 替换对话框句柄 , IDC_AREATYPE_THISFILE ) );
	CheckDlgButton( 查找框句柄 , IDC_AREATYPE_ALLOPENEDFILES , IsDlgButtonChecked( 替换对话框句柄 , IDC_AREATYPE_ALLOPENEDFILES ) );

	CheckDlgButton( 查找框句柄 , IDC_OPTIONS_WHOLEWORD , IsDlgButtonChecked( 替换对话框句柄 , IDC_OPTIONS_WHOLEWORD ) );
	CheckDlgButton( 查找框句柄 , IDC_OPTIONS_MATCHCASE , IsDlgButtonChecked( 替换对话框句柄 , IDC_OPTIONS_MATCHCASE ) );
	CheckDlgButton( 查找框句柄 , IDC_OPTIONS_WORDSTART , IsDlgButtonChecked( 替换对话框句柄 , IDC_OPTIONS_WORDSTART ) );

	CheckDlgButton( 查找框句柄 , IDC_SEARCHLOCATE_TO_TOP_OR_BOTTOM , IsDlgButtonChecked( 替换对话框句柄 , IDC_SEARCHLOCATE_TO_TOP_OR_BOTTOM ) );
	CheckDlgButton( 查找框句柄 , IDC_SEARCHLOCATE_LOOP , IsDlgButtonChecked( 替换对话框句柄 , IDC_SEARCHLOCATE_LOOP ) );

	return;
}

void SyncReplaceFromTextToFindDialog()
{
	char *pcReplaceDialogFromText = GetWindowString( hwndFromEditBoxInSearchReplace ) ;
	SendMessage( hwndEditBoxInSearchFind , WM_SETTEXT , 0 , (LPARAM)pcReplaceDialogFromText );
	free( pcReplaceDialogFromText );

	return;
}

static int GrepSomethingInCurrentDirectory( HWND hwnd , struct 选项卡页面S *选项卡页节点 , char *pcFindDialogText )
{
	int	found = 0 ;

	if( 选项卡页节点->stRemoteFileServer.acNetworkAddress[0] )
	{
		WarnBox( "该功能不支持在远程文件目录中搜索" );
		CheckRadioButton( hwnd , IDC_AREATYPE_THISFILE , IDC_AREATYPE_CURRENTDIRECTORY , IDC_AREATYPE_ALLOPENEDFILES );
		return 错误_正则表达式_没发现;
	}

	char			acFindPathFilename[ MAX_PATH ] ;
	WIN32_FIND_DATA		stFindFileData ;
	HANDLE			hFindFile ;
	char			acPathFilename[ MAX_PATH ] ;
	struct 文档类型配置S	*pst文档类型配置 = NULL ;
	char			acExtName[ MAX_PATH ] ;

	memset( acFindPathFilename , 0x00 , sizeof(acFindPathFilename) );
	snprintf( acFindPathFilename , sizeof(acFindPathFilename)-1 , "%s*.*" , 选项卡页节点->acPathName );

	hFindFile = FindFirstFile( acFindPathFilename , & stFindFileData ) ;
	if( hFindFile == INVALID_HANDLE_VALUE )
		return 0;

	do
	{
		if( strcmp(stFindFileData.cFileName,".") == 0 || strcmp(stFindFileData.cFileName,"..") == 0 )
			continue;

		if( stFindFileData.dwFileAttributes & FILE_ATTRIBUTE_ARCHIVE )
		{
			snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s%s" , 选项卡页节点->acPathName , stFindFileData.cFileName ) ;
			memset( acExtName , 0x00 , sizeof(acExtName) );
			SplitPathFilename( acPathFilename , NULL , NULL , NULL , NULL , acExtName , NULL , NULL );
			pst文档类型配置 = 获取文档类型配置( acExtName ) ;
			if( pst文档类型配置 )
			{
				char *file_buf = filedup( acPathFilename , NULL ) ;
				if( file_buf )
				{
					if( strstr( file_buf , pcFindDialogText ) )
					{
						OpenFileDirectly( acPathFilename );
						found = 1 ;
					}
				}
			}
		}
	}
	while( FindNextFile( hFindFile , & stFindFileData ) );

	FindClose( hFindFile );

	if( found )
	{
		SelectTabPage( 选项卡页节点 , -1 );
		return 0;
	}
	else
	{
		return 错误_正则表达式_没发现;
	}
}

INT_PTR CALLBACK SearchFindWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	int		nret = 0 ;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if ( LOWORD(wParam) == IDCANCEL)
		{
			ShowWindow( hDlg , SW_HIDE );
			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_FINDPREV )
		{
			size_t	nFindFlags = PrepareSearchFlags( 查找框句柄 ) ;
			BOOL	bLoopFind = IsDlgButtonChecked( 查找框句柄 , IDC_SEARCHLOCATE_LOOP ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcFindDialogText = GetWindowString( hwndEditBoxInSearchFind ) ;
			if( pcFindDialogText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcFindDialogText) ;
				if( tmp == NULL )
				{
					free( pcFindDialogText );
					break;
				}
				pcFindDialogText = tmp ;
			}

			if( IsDlgButtonChecked( 查找框句柄 , IDC_AREATYPE_CURRENTDIRECTORY ) )
			{
				GrepSomethingInCurrentDirectory( 查找框句柄 , 全_当前选项卡页面节点 , pcFindDialogText );
				CheckRadioButton( 查找框句柄 , IDC_AREATYPE_THISFILE , IDC_AREATYPE_CURRENTDIRECTORY , IDC_AREATYPE_ALLOPENEDFILES );
				free( pcEditorSelectionText );
				free( pcFindDialogText );
				return (INT_PTR)TRUE;
			}

			if( IsDlgButtonChecked( 查找框句柄 , IDC_AREATYPE_THISFILE ) )
			{
				struct Sci_TextToFind	ft ;
				int 当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				int 文本总字节数 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) - 1 ;
				int nStartPos = 当前文本字节位置 - 1 ;
				int nEndPos = 0 ;
_GOTO_RETRY_FINDPREV_IN_THISFILE:
				memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = nEndPos ;
				ft.lpstrText = pcFindDialogText ;
				int nFoundPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFoundPos >= 0 )
				{
					全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
				else
				{
					if( 当前文本字节位置 < 文本总字节数 && bLoopFind == TRUE )
					{
						nStartPos = 文本总字节数 ;
						nEndPos = 当前文本字节位置 ;
						bLoopFind = FALSE ;
						goto _GOTO_RETRY_FINDPREV_IN_THISFILE;
					}
					else
					{
						InfoBox( "搜到顶了" );
					}
				}
			}
			else
			{
				int		n选项卡页面数量 ;
				int		nCurrentTabPageIndex ;
				TCITEM		tci ;
				struct 选项卡页面S	*p = NULL ;

				n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
				for( nCurrentTabPageIndex = 0; nCurrentTabPageIndex < n选项卡页面数量; nCurrentTabPageIndex++ )
				{
					memset( & tci , 0x00 , sizeof(TCITEM) );
					tci.mask = TCIF_PARAM ;
					TabCtrl_GetItem( 全_选项卡页面句柄 , nCurrentTabPageIndex , & tci );
					p = (struct 选项卡页面S *)(tci.lParam);
					if( p == 全_当前选项卡页面节点 )
					{
						struct Sci_TextToFind	ft ;
						int 当前文本字节位置 = (int)p->pfuncScintilla( p->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
						int 文本总字节数 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) - 1 ;
						int nStartPos = 当前文本字节位置 - 1 ;
						int nEndPos = 0 ;
						memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
						ft.chrg.cpMin = nStartPos ;
						ft.chrg.cpMax = nEndPos ;
						ft.lpstrText = pcFindDialogText ;
						int nFoundPos = (int)p->pfuncScintilla( p->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
						if( nFoundPos >= 0 )
						{
							p->pfuncScintilla( p->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
						}
						else
						{
							int nPrevTabPageIndex = nCurrentTabPageIndex - 1 ;
							for( ; ; nPrevTabPageIndex-- )
							{
								if( bLoopFind == FALSE && nPrevTabPageIndex < 0 )
								{
									InfoBox( "搜到顶了" );
									break;
								}

								if( nPrevTabPageIndex < 0 )
									nPrevTabPageIndex = n选项卡页面数量 - 1 ;

								memset( & tci , 0x00 , sizeof(TCITEM) );
								tci.mask = TCIF_PARAM ;
								TabCtrl_GetItem( 全_选项卡页面句柄 , nPrevTabPageIndex , & tci );
								p = (struct 选项卡页面S *)(tci.lParam);

								memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
								ft.chrg.cpMin = (int)p->pfuncScintilla( p->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) - 1 ;
								if( nPrevTabPageIndex != nCurrentTabPageIndex )
									ft.chrg.cpMax = 0 ;
								else
									ft.chrg.cpMax = 当前文本字节位置 ;
								ft.lpstrText = pcFindDialogText ;
								nFoundPos = (int)p->pfuncScintilla( p->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
								if( nFoundPos >= 0 )
								{
									以索引选择选项卡页面( nPrevTabPageIndex );
									p->pfuncScintilla( p->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
									break;
								}
								else
								{
									if( nPrevTabPageIndex == nCurrentTabPageIndex )
									{
										InfoBox( "搜到底了" );
										break;
									}
								}
							}
						}
						break;
					}
				}
			}

			free( pcEditorSelectionText );
			free( pcFindDialogText );

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_FINDNEXT )
		{
			size_t	nFindFlags = PrepareSearchFlags( 查找框句柄 ) ;
			BOOL	bLoopFind = IsDlgButtonChecked( 查找框句柄 , IDC_SEARCHLOCATE_LOOP ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcFindDialogText = GetWindowString( hwndEditBoxInSearchFind ) ;
			if( pcFindDialogText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcFindDialogText) ;
				if( tmp == NULL )
				{
					free( pcFindDialogText );
					break;
				}
				pcFindDialogText = tmp ;
			}

			if( IsDlgButtonChecked( 查找框句柄 , IDC_AREATYPE_CURRENTDIRECTORY ) )
			{
				GrepSomethingInCurrentDirectory( 查找框句柄 , 全_当前选项卡页面节点 , pcFindDialogText );
				CheckRadioButton( 查找框句柄 , IDC_AREATYPE_THISFILE , IDC_AREATYPE_CURRENTDIRECTORY , IDC_AREATYPE_ALLOPENEDFILES );
				free( pcEditorSelectionText );
				free( pcFindDialogText );
				return (INT_PTR)TRUE;
			}

			if( IsDlgButtonChecked( 查找框句柄 , IDC_AREATYPE_THISFILE ) )
			{
				struct Sci_TextToFind	ft ;
				int 当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				int 文本总字节数 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) - 1 ;
				int nStartPos = 当前文本字节位置 + 1 ;
				int nEndPos = 文本总字节数 ;
_GOTO_RETRY_FINDNEXT_IN_THISFILE:
				memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = nEndPos ;
				ft.lpstrText = pcFindDialogText ;
				int nFoundPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFoundPos >= 0 )
				{
					全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
				else
				{
					if( 当前文本字节位置 > 0 && bLoopFind == TRUE )
					{
						nStartPos = 0 ;
						nEndPos = 当前文本字节位置 ;
						bLoopFind = FALSE ;
						goto _GOTO_RETRY_FINDNEXT_IN_THISFILE;
					}
					else
					{
						InfoBox( "搜到底了" );
					}
				}
			}
			else
			{
				int		n选项卡页面数量 ;
				int		nCurrentTabPageIndex ;
				TCITEM		tci ;
				struct 选项卡页面S	*p = NULL ;

				n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
				for( nCurrentTabPageIndex = 0; nCurrentTabPageIndex < n选项卡页面数量; nCurrentTabPageIndex++ )
				{
					memset( & tci , 0x00 , sizeof(TCITEM) );
					tci.mask = TCIF_PARAM ;
					TabCtrl_GetItem( 全_选项卡页面句柄 , nCurrentTabPageIndex , & tci );
					p = (struct 选项卡页面S *)(tci.lParam);
					if( p == 全_当前选项卡页面节点 )
					{
						struct Sci_TextToFind	ft ;
						int 当前文本字节位置 = (int)p->pfuncScintilla( p->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
						int 文本总字节数 = (int)p->pfuncScintilla( p->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;
						int nStartPos = 当前文本字节位置 + 1 ;
						int nEndPos = 文本总字节数 ;
						memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
						ft.chrg.cpMin = nStartPos ;
						ft.chrg.cpMax = nEndPos ;
						ft.lpstrText = pcFindDialogText ;
						int nFoundPos = (int)p->pfuncScintilla( p->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
						if( nFoundPos >= 0 )
						{
							p->pfuncScintilla( p->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
						}
						else
						{
							int nNextTabPageIndex = nCurrentTabPageIndex + 1 ;
							for( ; ; nNextTabPageIndex++ )
							{
								if( bLoopFind == FALSE && nNextTabPageIndex >= n选项卡页面数量 )
								{
									InfoBox( "搜到底了" );
									break;
								}

								if( nNextTabPageIndex == n选项卡页面数量 )
									nNextTabPageIndex = 0 ;

								memset( & tci , 0x00 , sizeof(TCITEM) );
								tci.mask = TCIF_PARAM ;
								TabCtrl_GetItem( 全_选项卡页面句柄 , nNextTabPageIndex , & tci );
								p = (struct 选项卡页面S *)(tci.lParam);

								memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
								ft.chrg.cpMin = 0 ;
								if( nNextTabPageIndex != nCurrentTabPageIndex )
									ft.chrg.cpMax = (int)p->pfuncScintilla( p->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) - 1 ;
								else
									ft.chrg.cpMax = 当前文本字节位置 ;
								ft.lpstrText = pcFindDialogText ;
								nFoundPos = (int)p->pfuncScintilla( p->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
								if( nFoundPos >= 0 )
								{
									以索引选择选项卡页面( nNextTabPageIndex );
									p->pfuncScintilla( p->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
									break;
								}
								else
								{
									if( nNextTabPageIndex == nCurrentTabPageIndex )
									{
										InfoBox( "搜到底了" );
										break;
									}
								}
							}
						}
						break;
					}
				}
			}

			free( pcEditorSelectionText );
			free( pcFindDialogText );

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_FOUNDLIST )
		{
			if( IsDlgButtonChecked( 查找框句柄 , IDC_AREATYPE_THISFILE ) == FALSE )
			{
				InfoBox( "目前只支持当前文件处理" );
				break;
			}

			size_t	nFindFlags = PrepareSearchFlags( 查找框句柄 ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcFindDialogText = GetWindowString( hwndEditBoxInSearchFind ) ;
			if( pcFindDialogText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcFindDialogText) ;
				if( tmp == NULL )
				{
					free( pcFindDialogText );
					break;
				}
				pcFindDialogText = tmp ;
			}

			int 当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			int nCurrentLine = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_LINEFROMPOSITION , 当前文本字节位置 , 0 ) ;

			int nTextTotalLength = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
			char *pcText = (char*)malloc( nTextTotalLength + 1 ) ;
			if( pcText == NULL )
				return -1;
			memset( pcText , 0x00 , nTextTotalLength + 1 );
			全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETTEXT , (sptr_t)(nTextTotalLength+1) , (sptr_t)pcText ) ;

			ListBox_ResetContent( 搜索中已找到的列表框句柄 );

			int linenumber = -1 ;
			char *linestart = pcText ;
			char *lineend = strchr( linestart , '\n' ) ;
			char *p = NULL ;
			int nCurrentListIndex = -1 ;
			char acAddString[ 4096 ] ;
			while( linestart )
			{
				if( lineend )
					(*lineend) = '\0' ;
				int linelen = (int)strlen(linestart) ;
				if( linestart[linelen-1] == '\r' )
					linestart[linelen-1] = '\0' ;
				linelen--;

				for( p = linestart ; (*p) ; p++ )
				{
					if( (*p) == '\t' )
						(*p) = ' ' ;
				}

				linenumber++;

				if( strstr( linestart , pcFindDialogText ) )
				{
					memset( acAddString , 0x00 , sizeof(acAddString) );
					if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
					{
						char *tmp = StrdupUtf8ToGb(linestart) ;
						if( tmp == NULL )
						{
							break;
						}
						snprintf( acAddString , sizeof(acAddString)-1 , "%d: %s" , linenumber+1 , tmp );
						free( tmp );
					}
					else
					{
						snprintf( acAddString , sizeof(acAddString)-1 , "%d: %s" , linenumber+1 , linestart );
					}

					int nListBoxItemIndex = ListBox_AddString( 搜索中已找到的列表框句柄 , acAddString ) ;
					ListBox_SetItemData( 搜索中已找到的列表框句柄 , nListBoxItemIndex , (LPARAM)linenumber );
					if( linenumber == nCurrentLine )
						nCurrentListIndex = ListBox_GetCount( 搜索中已找到的列表框句柄 ) - 1 ;
				}

				if( lineend == NULL )
					break;
				linestart = lineend + 1 ;
				lineend = strchr( linestart , '\n' ) ;
			}

			if( nCurrentListIndex >= 0 )
				ListBox_SetCurSel( 搜索中已找到的列表框句柄 , nCurrentListIndex );

			free( pcText );

			free( pcEditorSelectionText );
			free( pcFindDialogText );

			if( ListBox_GetCount(搜索中已找到的列表框句柄) > 0 )
			{
				CenterWindow( 已查框句柄 , 全_主窗口句柄 );
				ShowWindow( 已查框句柄 , SW_SHOW );
				UpdateWindow( 已查框句柄 );
			}
			else
			{
				错误框( "没有匹配的行" );
			}

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_FOUNDCOUNT )
		{
			size_t	nFindFlags = PrepareSearchFlags( 查找框句柄 ) ;
			BOOL	bLoopFind = IsDlgButtonChecked( 查找框句柄 , IDC_SEARCHLOCATE_LOOP ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcFindDialogText = GetWindowString( hwndEditBoxInSearchFind ) ;
			if( pcFindDialogText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcFindDialogText) ;
				if( tmp == NULL )
				{
					free( pcFindDialogText );
					break;
				}
				pcFindDialogText = tmp ;
			}

			int nFoundCount = 0 ;

			struct Sci_TextToFind	ft ;
			int 文本总字节数 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) - 1 ;
			int nStartPos = 0 ;
			int nEndPos = 文本总字节数 ;

			while(1)
			{
				memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = nEndPos ;
				ft.lpstrText = pcFindDialogText ;
				int nFoundPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFoundPos >= 0 )
				{
					nFoundCount++;
					nStartPos = nFoundPos + 1 ;
				}
				else
				{
					InfoBox( "本文件中共出现了%d次" , nFoundCount );
					break;
				}
			}

			free( pcEditorSelectionText );
			free( pcFindDialogText );

			return (INT_PTR)TRUE;
		}
		else
		{
			return (INT_PTR)TRUE;
		}
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK SearchReplaceWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if ( LOWORD(wParam) == IDCANCEL)
		{
			ShowWindow( hDlg , SW_HIDE );
			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_REPLACEPREV )
		{
			size_t nFindFlags = PrepareSearchFlags( 替换对话框句柄 ) ;
			char *pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char *pcReplaceDialogFromText = GetWindowString( hwndFromEditBoxInSearchReplace ) ;
			if( pcReplaceDialogFromText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogFromText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogFromText );
					break;
				}
				pcReplaceDialogFromText = tmp ;
			}
			char *pcReplaceDialogToText = GetWindowString( hwndToEditBoxInSearchReplace ) ;
			if( pcReplaceDialogToText == NULL )
			{
				pcReplaceDialogToText = _strdup("") ;
			}
			if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogToText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogToText );
					break;
				}
				pcReplaceDialogToText = tmp ;
			}

			if( pcEditorSelectionText == NULL || ( pcEditorSelectionText && ( strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 || strcmp( pcEditorSelectionText , pcReplaceDialogToText ) == 0 ) ) )
			{
				if( pcEditorSelectionText && strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 )
				{
					全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );
				}

				SyncReplaceOptionsToFindDialog();
				SyncReplaceFromTextToFindDialog();
				SendMessage( 查找框句柄 , WM_COMMAND , ID_FINDPREV , 0 );
			}
			else
			{
				struct Sci_TextToFind	ft ;
				int nStartPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
				memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = 0 ;
				ft.lpstrText = pcReplaceDialogFromText ;
				int nFindPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFindPos >= 0 )
				{
					全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}

			free( pcReplaceDialogFromText );
			free( pcReplaceDialogToText );
			free( pcEditorSelectionText );

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_REPLACENEXT )
		{
			size_t nFindFlags = PrepareSearchFlags( 替换对话框句柄 ) ;
			char *pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char *pcReplaceDialogFromText = GetWindowString( hwndFromEditBoxInSearchReplace ) ;
			if( pcReplaceDialogFromText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogFromText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogFromText );
					break;
				}
				pcReplaceDialogFromText = tmp ;
			}
			char *pcReplaceDialogToText = GetWindowString( hwndToEditBoxInSearchReplace ) ;
			if( pcReplaceDialogToText == NULL )
			{
				pcReplaceDialogToText = _strdup("") ;
			}
			if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogToText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogToText );
					break;
				}
				pcReplaceDialogToText = tmp ;
			}

			if( pcEditorSelectionText == NULL || ( pcEditorSelectionText && ( strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 || strcmp( pcEditorSelectionText , pcReplaceDialogToText ) == 0 ) ) )
			{
				if( pcEditorSelectionText && strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 )
				{
					全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );
				}

				SyncReplaceOptionsToFindDialog();
				SyncReplaceFromTextToFindDialog();
				SendMessage( 查找框句柄 , WM_COMMAND , ID_FINDNEXT , 0 );
			}
			else
			{
				struct Sci_TextToFind	ft ;
				int nStartPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
				int nEndPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETSELECTIONEND , 0 , 0 ) ;
				memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = nEndPos ;
				ft.lpstrText = pcReplaceDialogFromText ;
				int	nFindPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFindPos >= 0 )
				{
					全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}

			free( pcReplaceDialogFromText );
			free( pcReplaceDialogToText );
			free( pcEditorSelectionText );

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_REPLACEALL )
		{
			size_t nFindFlags = PrepareSearchFlags( 替换对话框句柄 ) ;
			char *pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char *pcReplaceDialogFromText = GetWindowString( hwndFromEditBoxInSearchReplace ) ;
			if( pcReplaceDialogFromText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogFromText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogFromText );
					break;
				}
				pcReplaceDialogFromText = tmp ;
			}
			char *pcReplaceDialogToText = GetWindowString( hwndToEditBoxInSearchReplace ) ;
			if( pcReplaceDialogToText == NULL )
			{
				pcReplaceDialogToText = _strdup("") ;
			}
			if( 全_当前选项卡页面节点->nCodePage == 字符编码_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogToText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogToText );
					break;
				}
				pcReplaceDialogToText = tmp ;
			}

			if( pcEditorSelectionText == NULL || ( pcEditorSelectionText && ( strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 || strcmp( pcEditorSelectionText , pcReplaceDialogToText ) == 0 ) ) )
			{
				struct Sci_TextToFind	ft ;
				int nStartPos ;

				全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_BEGINUNDOACTION , 0 , 0 );

				if( IsDlgButtonChecked( 替换对话框句柄 , IDC_AREATYPE_THISFILE ) )
				{
					nStartPos = 0 ;
					while(1)
					{
						memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
						ft.chrg.cpMin = nStartPos ;
						ft.chrg.cpMax = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;
						ft.lpstrText = pcReplaceDialogFromText ;
						int nFindPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
						if( nFindPos >= 0 )
						{
							全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
							全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );

							nStartPos = nFindPos + (int)strlen(pcReplaceDialogToText) ;
						}
						else
						{
							break;
						}
					}
				}
				else
				{
					int		n选项卡页面数量 ;
					int		n选项卡页面索引 ;
					TCITEM		tci ;
					struct 选项卡页面S	*p = NULL ;

					n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
					for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
					{
						memset( & tci , 0x00 , sizeof(TCITEM) );
						tci.mask = TCIF_PARAM ;
						TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
						p = (struct 选项卡页面S *)(tci.lParam);

						nStartPos = 0 ;
						while(1)
						{
							memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
							ft.chrg.cpMin = nStartPos ;
							ft.chrg.cpMax = (int)p->pfuncScintilla( p->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;
							ft.lpstrText = pcReplaceDialogFromText ;
							int nFindPos = (int)p->pfuncScintilla( p->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
							if( nFindPos >= 0 )
							{
								p->pfuncScintilla( p->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
								p->pfuncScintilla( p->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );

								nStartPos = nFindPos + 1 ;
							}
							else
							{
								break;
							}
						}
					}
				}

				全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_ENDUNDOACTION , 0 , 0 );
			}
			else
			{
				struct Sci_TextToFind	ft ;
				int nStartPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
				int nEndPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETSELECTIONEND , 0 , 0 ) ;
				int nFileEndPos = nEndPos ;
				int nDeltaLen = (int)strlen(pcReplaceDialogToText) - (int)strlen(pcReplaceDialogFromText) ;
				if( nDeltaLen < 0 )
					nDeltaLen = 0 ;

				全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_BEGINUNDOACTION , 0 , 0 );

				while(1)
				{
					memset( & ft , 0x00 , sizeof(struct Sci_TextToFind) );
					ft.chrg.cpMin = nStartPos ;
					ft.chrg.cpMax = nEndPos ;
					ft.lpstrText = pcReplaceDialogFromText ;
					int nFindPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
					if( nFindPos >= 0 )
					{
						全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
						全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );

						nStartPos = nFindPos + 1 ;
						nEndPos += nDeltaLen ;
						if( nEndPos > nFileEndPos )
							nEndPos = nFileEndPos ;
					}
					else
					{
						break;
					}
				}

				全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_ENDUNDOACTION , 0 , 0 );
			}

			free( pcReplaceDialogFromText );
			free( pcReplaceDialogToText );
			free( pcEditorSelectionText );
		}
		else
		{
			return (INT_PTR)TRUE;
		}
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK SearchFoundWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if( LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL )
		{
			ShowWindow( hDlg , SW_HIDE );
			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_COPYTO_CLIPBOARD )
		{
			int nItemCount = ListBox_GetCount( 搜索中已找到的列表框句柄 ) ;
			if( nItemCount > 0 )
			{
				int nItemIndex ;
				int nTotalLength = 0 ;
				for( nItemIndex = 0 ; nItemIndex < nItemCount ; nItemIndex++ )
				{
					nTotalLength += ListBox_GetTextLen( 搜索中已找到的列表框句柄 , nItemIndex ) ;
					nTotalLength += 2 ;
				}

				HGLOBAL hgl = (char*)GlobalAlloc( GMEM_MOVEABLE , nTotalLength + 1 ) ;
				char *acTotalText = (char*)GlobalLock(hgl) ;
				if( acTotalText == NULL )
				{
					错误框( "不能分配内存用以存放复制到剪贴板的临时缓冲区" );
					return (INT_PTR)TRUE;
				}
				memset( acTotalText , 0x00 , nTotalLength + 1 );
				nTotalLength = 0 ;
				for( nItemIndex = 0 ; nItemIndex < nItemCount ; nItemIndex++ )
				{
					ListBox_GetText( 搜索中已找到的列表框句柄 , nItemIndex , acTotalText+nTotalLength );
					nTotalLength += ListBox_GetTextLen( 搜索中已找到的列表框句柄 , nItemIndex ) ;
					memcpy( acTotalText+nTotalLength , "\r\n" , 2 );
					nTotalLength += 2 ;
				}
				GlobalUnlock(hgl);

				OpenClipboard( 全_主窗口句柄 );
				EmptyClipboard();
				SetClipboardData( CF_TEXT , acTotalText );
				CloseClipboard();
			}
		}
		else if( LOWORD(wParam) == IDC_FOUNDLIST && HIWORD(wParam) == LBN_DBLCLK )
		{
			int nListBoxItemNo = (int)SendMessage( 搜索中已找到的列表框句柄 , LB_GETCURSEL , 0 , 0 ) ;
			int nGotoLineNo = (int)SendMessage( 搜索中已找到的列表框句柄 , LB_GETITEMDATA , nListBoxItemNo , 0 ) ;
			int 当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			int 文本当前行号 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_LINEFROMPOSITION , 当前文本字节位置 , 0 ) ;
			JumpGotoLine( 全_当前选项卡页面节点 , nGotoLineNo , 文本当前行号 );

			return (INT_PTR)TRUE;
		}
		else
		{
			return (INT_PTR)TRUE;
		}
	}
	return (INT_PTR)FALSE;
}

int OnSearchFind( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		CopyEditorSelectionToWnd( 选项卡页节点 , hwndEditBoxInSearchFind );

		SendMessage( 替换对话框句柄 , DM_SETDEFID , ID_FINDNEXT , 0 );
		SetFocus( hwndEditBoxInSearchFind );

		CenterWindow( 查找框句柄 , 全_主窗口句柄 );
		ShowWindow( 查找框句柄 , SW_SHOW );
	}

	return 0;
}

int OnSearchFindPrev( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		CopyEditorSelectionToWnd( 选项卡页节点 , hwndEditBoxInSearchFind );

		SendMessage( 查找框句柄 , WM_COMMAND , ID_FINDPREV , 0 );
	}

	return 0;
}

int OnSearchFindNext( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		CopyEditorSelectionToWnd( 选项卡页节点 , hwndEditBoxInSearchFind );

		SendMessage( 查找框句柄 , WM_COMMAND , ID_FINDNEXT , 0 );
	}

	return 0;
}

int OnSearchFoundList( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		CopyEditorSelectionToWnd( 选项卡页节点 , hwndEditBoxInSearchFind );

		SendMessage( 查找框句柄 , WM_COMMAND , ID_FOUNDLIST , 0 );
	}

	return 0;
}

int OnSearchReplace( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		CopyEditorSelectionToWnd( 选项卡页节点 , hwndFromEditBoxInSearchReplace );

		SendMessage( 替换对话框句柄 , DM_SETDEFID , ID_REPLACENEXT , 0 );
		SetFocus( hwndFromEditBoxInSearchReplace );

		CenterWindow( 替换对话框句柄 , 全_主窗口句柄 );
		ShowWindow( 替换对话框句柄 , SW_SHOW );
	}

	return 0;
}

int OnSelectAll( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_SELECTALL, 0, 0 );
	}

	return 0;
}

int OnSelectWord( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		int	当前文本字节位置 ;
		int	nCurrentWordStartPos ;
		int	nCurrentWordEndPos ;

		当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentWordStartPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDSTARTPOSITION, 当前文本字节位置, TRUE ) ;
		nCurrentWordEndPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDENDPOSITION, 当前文本字节位置, TRUE ) ;
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_SETSEL, nCurrentWordStartPos, nCurrentWordEndPos ) ;
	}

	return 0;
}

int OnSelectLine( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		int	当前文本字节位置 ;
		int	nCurrentLine ;
		int	nCurrentLineStartPos ;
		int	nCurrentLineEndPos ;
		int	n换行符模式 ;

		当前文本字节位置 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
		nCurrentLineStartPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
		nCurrentLineEndPos = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETLINEENDPOSITION, nCurrentLine, 0 ) ;
		n换行符模式 = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETEOLMODE, 0, 0 ) ;
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_SETSEL, nCurrentLineStartPos, nCurrentLineEndPos+(n换行符模式==0?2:1) ) ;
	}

	return 0;
}

int OnAddSelectLeftCharGroup( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDPARTLEFTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectRightCharGroup( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDPARTRIGHTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectLeftWord( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDLEFTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectRightWord( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDRIGHTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectTopBlockFirstLine( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_PARAUPEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectBottomBlockFirstLine( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_PARADOWNEXTEND, 0, 0 );
	return 0;
}

int OnMoveLeftCharGroup( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDPARTLEFT, 0, 0 );
	return 0;
}

int OnMoveRightCharGroup( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDPARTRIGHT, 0, 0 );
	return 0;
}

int OnMoveLeftWord( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDLEFT, 0, 0 );
	return 0;
}

int OnMoveRightWord( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_WORDRIGHT, 0, 0 );
	return 0;
}

int OnMoveTopBlockFirstLine( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_PARAUP, 0, 0 );
	return 0;
}

int OnMoveBottomBlockFirstLine( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_PARADOWN, 0, 0 );
	return 0;
}

int OnSearchGotoLine( struct 选项卡页面S *选项卡页节点 )
{
	char		LineNo[ 20 ] ;

	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	memset( LineNo , 0x00 , sizeof(LineNo) );
	strcpy( LineNo , "0" );
	nret = InputBox( 全_主窗口句柄 , "请输入行号：" , "输入窗口" , 0 , LineNo , sizeof(LineNo)-1 ) ;
	if( nret == IDOK )
	{
		int	nGotoLine ;
		int	当前文本字节位置 ;
		int	nCurrentLine ;

		nGotoLine = atoi(LineNo) ;

		当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;

		JumpGotoLine( 选项卡页节点 , nGotoLine-1 , nCurrentLine );
	}

	return 0;
}

int OnSearchToggleBookmark( struct 选项卡页面S *选项卡页节点 )
{
	int	当前文本字节位置 ;
	int	nCurrentLine ;
	int	nMarkNumber ;

	当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
	nMarkNumber = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERGET , nCurrentLine , 0 ) ;
	if( nMarkNumber == 边框_书签_值 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERADD , nCurrentLine , 边框_书签_值 );
	}
	else
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDELETE , nCurrentLine , 边框_书签_值 );
	}

	return 0;
}

int OnSearchAddBookmark( struct 选项卡页面S *选项卡页节点 )
{
	int	当前文本字节位置 ;
	int	nCurrentLine ;

	当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERADD , nCurrentLine , 边框_书签_值 );

	return 0;
}

int OnSearchRemoveBookmark( struct 选项卡页面S *选项卡页节点 )
{
	int	当前文本字节位置 ;
	int	nCurrentLine ;

	当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERDELETE , nCurrentLine , 边框_书签_值 );

	return 0;
}

int OnSearchRemoveAllBookmarks( struct 选项卡页面S *选项卡页节点 )
{
	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_MARKERDELETEALL , 边框_书签_值 , 0 );
	}

	return 0;
}

int OnSearchGotoPrevBookmark( struct 选项卡页面S *选项卡页节点 )
{
	int	当前文本字节位置 ;
	int	nCurrentLine ;
	int	nFindLineNo ;

	if( 选项卡页节点 == NULL )
		return 0;

	当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
	nFindLineNo = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERPREVIOUS , nCurrentLine-1 , 边框_书签_MASKN ) ;
	if( nFindLineNo != -1 )
	{
		JumpGotoLine( 选项卡页节点 , nFindLineNo , nCurrentLine );
	}

	return 0;
}

int OnSearchGotoNextBookmark( struct 选项卡页面S *选项卡页节点 )
{
	int	当前文本字节位置 ;
	int	nCurrentLine ;
	int	nFindLineNo ;

	if( 选项卡页节点 == NULL )
		return 0;

	当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
	nFindLineNo = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_MARKERNEXT , nCurrentLine+1 , 边框_书签_MASKN ) ;
	if( nFindLineNo != -1 )
	{
		JumpGotoLine( 选项卡页节点 , nFindLineNo , nCurrentLine );
	}

	return 0;
}

int OnSearchGotoPrevBookmarkInAllFiles( struct 选项卡页面S *选项卡页节点 )
{
	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	int		当前文本字节位置 ;
	int		nCurrentLine ;
	int		nFindLineNo ;
	int		nMaxLine ;

	if( 选项卡页节点 == NULL )
		return 0;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		if( p == 选项卡页节点 )
		{
			当前文本字节位置 = (int)p->pfuncScintilla( p->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
			nCurrentLine = (int)p->pfuncScintilla( p->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
			nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERPREVIOUS , nCurrentLine-1 , 边框_书签_MASKN ) ;
			if( nFindLineNo != -1 )
			{
				JumpGotoLine( p , nFindLineNo , nCurrentLine );
				return 0;
			}
			else
			{
				for( n选项卡页面索引-- ; n选项卡页面索引 >= 0 ; n选项卡页面索引-- )
				{
					memset( & tci , 0x00 , sizeof(TCITEM) );
					tci.mask = TCIF_PARAM ;
					TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
					p = (struct 选项卡页面S *)(tci.lParam);

					nMaxLine = (int)p->pfuncScintilla( p->pScintilla, SCI_GETLINECOUNT, 0, 0 ) ;
					nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERPREVIOUS , nMaxLine-1 , 边框_书签_MASKN ) ;
					if( nFindLineNo != -1 )
					{
						以索引选择选项卡页面( n选项卡页面索引 );
						JumpGotoLine( p , nFindLineNo , nMaxLine );
						return 0;
					}
				}
				return 1;
			}
		}
	}

	return 0;
}

int OnSearchGotoNextBookmarkInAllFiles( struct 选项卡页面S *选项卡页节点 )
{
	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	int		当前文本字节位置 ;
	int		nCurrentLine ;
	int		nFindLineNo ;

	if( 选项卡页节点 == NULL )
		return 0;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		if( p == 选项卡页节点 )
		{
			当前文本字节位置 = (int)p->pfuncScintilla( p->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
			nCurrentLine = (int)p->pfuncScintilla( p->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
			nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERNEXT , nCurrentLine+1 , 边框_书签_MASKN ) ;
			if( nFindLineNo != -1 )
			{
				JumpGotoLine( p , nFindLineNo , nCurrentLine );
				return 0;
			}
			else
			{

				for( n选项卡页面索引++ ; n选项卡页面索引 < n选项卡页面数量 ; n选项卡页面索引++ )
				{
					memset( & tci , 0x00 , sizeof(TCITEM) );
					tci.mask = TCIF_PARAM ;
					TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
					p = (struct 选项卡页面S *)(tci.lParam);

					nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERNEXT , 0 , 边框_书签_MASKN ) ;
					if( nFindLineNo != -1 )
					{
						以索引选择选项卡页面( n选项卡页面索引 );
						JumpGotoLine( p , nFindLineNo , 0 );
						return 0;
					}
				}
				return 1;
			}
		}
	}

	return 0;
}

void 初始化导航返回下一个跟踪列表()
{
	初始化列表头( & listNavigateBackNextTrace );
	nMaxNavigateBackNextTraceCount = 0 ;

	return;
}

int AddNavigateBackNextTrace( struct 选项卡页面S *选项卡页节点 , int 当前文本字节位置 )
{
	if( nMaxNavigateBackNextTraceCount + 1 > MAX_TRACE_COUNT )
	{
		struct NavigateBackNextTrace *first = list_first_entry( & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace ) ;
		list_del( & (first->nodeNavigateBackNextTrace) );
		free( first );
		nMaxNavigateBackNextTraceCount--;
	}

	struct NavigateBackNextTrace *curr = (struct NavigateBackNextTrace*)malloc( sizeof(struct NavigateBackNextTrace) ) ;
	if( curr == NULL )
	{
		错误框( "申请内存失败用以存放导航退回结构" );
		return -2;
	}
	memset( curr , 0x00 , sizeof(struct NavigateBackNextTrace) );

	curr->选项卡页节点 = 选项卡页节点 ;
	curr->Scintilla句柄 = 选项卡页节点->Scintilla句柄 ;
	curr->nLastPos = 当前文本字节位置 ;

	list_add_tail( & (curr->nodeNavigateBackNextTrace) , & listNavigateBackNextTrace );
	nMaxNavigateBackNextTraceCount++;

	return 0;
}

int UpdateNavigateBackNextTrace( struct 选项卡页面S *选项卡页节点 , int 当前文本字节位置 )
{
	struct NavigateBackNextTrace *last = list_last_entry( & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace ) ;

	if( last && last->选项卡页节点 == 选项卡页节点 )
		last->nLastPos = 当前文本字节位置 ;

	return 0;
}

int OnSearchNavigateBackPrev_InThisFile()
{
	struct NavigateBackNextTrace	*curr = NULL ;
	struct NavigateBackNextTrace	*prev = NULL ;

	list_for_each_entry_safe_reverse( curr , prev , & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace )
	{
		if( curr->Scintilla句柄 != 全_当前选项卡页面节点->Scintilla句柄 )
			return 1;

		list_del( & (curr->nodeNavigateBackNextTrace) );
		free( curr );
		nMaxNavigateBackNextTraceCount--;

		if( & (prev->nodeNavigateBackNextTrace) != & listNavigateBackNextTrace )
		{
			if( prev->Scintilla句柄 == 全_当前选项卡页面节点->Scintilla句柄 )
			{
				int nTextLength = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETLENGTH, 0, 0 ) ;
				int nGotoPos ;
				if( prev->nLastPos > nTextLength - 1 )
					nGotoPos = nTextLength - 1 ;
				else
					nGotoPos = prev->nLastPos ;
				全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GOTOPOS , nGotoPos , 0 );
			}
		}

		return 0;
	}

	return 1;
}

int OnSearchNavigateBackPrev_InAllFiles()
{
	struct NavigateBackNextTrace	*curr = NULL ;
	struct NavigateBackNextTrace	*prev = NULL ;

	list_for_each_entry_safe_reverse( curr , prev , & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace )
	{
		if( curr->选项卡页节点 != 全_当前选项卡页面节点 )
		{
			SelectTabPage( curr->选项卡页节点 , -1 );
			return 0;
		}

		list_del( & (curr->nodeNavigateBackNextTrace) );
		free( curr );
		nMaxNavigateBackNextTraceCount--;

		if( & (prev->nodeNavigateBackNextTrace) != & listNavigateBackNextTrace )
		{
			if( prev->选项卡页节点 != 全_当前选项卡页面节点 )
			{
				SelectTabPage( prev->选项卡页节点 , -1 );
			}

			int nTextLength = (int)全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GETLENGTH, 0, 0 ) ;
			int nGotoPos ;
			if( prev->nLastPos > nTextLength - 1 )
				nGotoPos = nTextLength - 1 ;
			else
				nGotoPos = prev->nLastPos ;
			全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla , SCI_GOTOPOS , nGotoPos , 0 );
		}

		return 0;
	}

	return 1;
}

void CleanNavigateBackNextTraceListByThisFile( struct 选项卡页面S *选项卡页节点 )
{
	struct NavigateBackNextTrace	*curr = NULL ;
	struct NavigateBackNextTrace	*next = NULL ;

	list_for_each_entry_safe( curr , next , & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace )
	{
		if( curr->Scintilla句柄 == 选项卡页节点->Scintilla句柄 )
		{
			list_del( & (curr->nodeNavigateBackNextTrace) );
			free( curr );
			nMaxNavigateBackNextTraceCount--;
		}
	}

	return;
}

