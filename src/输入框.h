#ifndef _H_INPUTBOX_
#define _H_INPUTBOX_

#include <windows.h>

int InputBox( HWND hWndParent , LPCTSTR lpText , LPCTSTR lpCaption , UINT uType , LPTSTR lpInputBuf , UINT uMaxInputSize );

#endif
