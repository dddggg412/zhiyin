#ifndef _QUERYRESULTEDITCTL_
#define _QUERYRESULTEDITCTL_

#include "framework.h"

int CreateQueryResultEditCtl( struct 选项卡页面S *选项卡页节点 , HFONT hFont );

int AppendSqlQueryResultEditText( HWND hWnd , char *format , ... );

#endif
