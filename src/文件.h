#ifndef _FILE_
#define _FILE_

#include "framework.h"

int GetFileNameByOpenFileDialog( char* pcFilename, int nFilenameBufSize);
int GetFileNameBySaveFileDialog( char* pcFilename, int nFilenameBufSize);

int SplitPathFilename( char *acFullPathFilename , char *acDrivename , char *acPathname , char *acFilename , char *acMainFilename , char *acExtFilename , char *acDriveAndPathname , char *acPathFilename );

int 文件_新建( struct 选项卡页面S *选项卡页节点 );
int LoadFileDirectly( char *acFullPathFilename , FILE *fp , struct 选项卡页面S *选项卡页节点 );
int OpenFileDirectly( char *pcPathFilename );
int 直接打开文件( char *a文件名称 );
int 文件_打开();
int 清理最近打开文件的历史( struct 选项卡页面S *选项卡页节点 );
int 执行打开拖放文件( HDROP hDrop );
int OpenRemoteFileDirectly( struct RemoteFileServer *pstRemoteFileServer , char *acFullPathFilename );
int 文件_保存( struct 选项卡页面S *选项卡页节点 , BOOL bSaveAs );
int 文件_另存为( struct 选项卡页面S *选项卡页节点 );
int 保存所有的文件();
int 关闭文件时( struct 选项卡页面S *选项卡页节点 );
int 关闭所有文件时();
int 关闭除当前所有文件时( struct 选项卡页面S *选项卡页节点 );

int 启动时没有打开文件则新建文件( struct 选项卡页面S *选项卡页节点 );
int 启动时打开退出时打开的文件( struct 选项卡页面S *选项卡页节点 );
int 在打开文件后设置为只读( struct 选项卡页面S *选项卡页节点 );

int 启动时填充所有打开的文件();
int 检查所有文件保存();

int 检测文件变动( struct 选项卡页面S *选项卡页节点 );
int 文件变动自动重载提示( struct 选项卡页面S *选项卡页节点 );

int 设置新建文件换行符风格( struct 选项卡页面S *选项卡页节点 , int 新建文件Eols );
int 全文转换文件换行符风格( struct 选项卡页面S *选项卡页节点 , int nConvertEols );

int 设置新建文件字符编码( struct 选项卡页面S *选项卡页节点 , int 新建文件编码 );
int 全文转换文件字符编码( struct 选项卡页面S *选项卡页节点 , int nConvertEncoding );

int test_SplitPathFilename();

#endif
