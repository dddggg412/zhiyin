
#include "�����_ģ��.h"
#include "zhiyin.h"

LINK_RBTREENODE_STRING( LinkAutoCompletedShowTreeNode , struct AutoCompletedShowTree , treeAutoCompletedShow , struct AutoCompletedShowNode , nodeAutoCompletedShow , acAutoCompletedString )
UNLINK_RBTREENODE( UnlinkAutoCompletedShowTreeNode , struct AutoCompletedShowTree , treeAutoCompletedShow , struct AutoCompletedShowNode , nodeAutoCompletedShow )
funcCompareRbTreeNodeEntry CompareAutoCompletedShowTreeNodePartlyEntry;
int CompareAutoCompletedShowTreeNodePartlyEntry( void *pv1 , void *pv2 )
{
	struct AutoCompletedShowNode	*nodeQuery = (struct AutoCompletedShowNode *) pv1 ;
	struct AutoCompletedShowNode	*nodeTreeTravel = (struct AutoCompletedShowNode *) pv2 ;

	return strncmp( nodeQuery->acAutoCompletedString , nodeTreeTravel->acAutoCompletedString , strlen(nodeQuery->acAutoCompletedString) );
}
QUERY_RBTREENODE( QueryAutoCompletedShowTreeNodePartly , struct AutoCompletedShowTree , treeAutoCompletedShow , struct AutoCompletedShowNode , nodeAutoCompletedShow , & CompareAutoCompletedShowTreeNodePartlyEntry )
funcFreeRbTreeNodeEntry FreeAutoCompletedShowTreeNode ;
void FreeAutoCompletedShowTreeNode( void *pv )
{
	struct AutoCompletedShowNode	*node = (struct AutoCompletedShowNode *) pv ;

	if( node->acAutoCompletedString )
		free( node->acAutoCompletedString );

	free( node );
}
DESTROY_RBTREE( DestroyAutoCompletedShowTree , struct AutoCompletedShowTree , treeAutoCompletedShow , struct AutoCompletedShowNode , nodeAutoCompletedShow , & FreeAutoCompletedShowTreeNode )

LINK_RBTREENODE_STRING( LinkCallTipShowTreeNode , struct CallTipShowTree , treeCallTipShow , struct CallTipShowNode , nodeCallTipShow , acCallTipFuncName )
UNLINK_RBTREENODE( UnlinkCallTipShowTreeNode , struct CallTipShowTree , treeCallTipShow , struct CallTipShowNode , nodeCallTipShow )
QUERY_RBTREENODE_STRING( QueryCallTipShowTreeNode , struct CallTipShowTree , treeCallTipShow , struct CallTipShowNode , nodeCallTipShow , acCallTipFuncName )
DESTROY_RBTREE( DestroyCallTipShowTree , struct CallTipShowTree , treeCallTipShow , struct CallTipShowNode , nodeCallTipShow , NULL )
