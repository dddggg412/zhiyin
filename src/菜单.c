#include "framework.h"

void 推送最近打开路径文件名( char *acPathFilename )
{
	int	index ;
	int	start ;
	int	end ;

	for( index = 0 ; index < 最近打开路径文件名最大数量M ; index++ )
	{
		if( strcmp( 全_风格主配置.最近打开路径文件名[index] , acPathFilename ) == 0 )
			break;
	}
	if( index < 最近打开路径文件名最大数量M )
	{
		start = 0 ;
		end = index ;
	}
	else
	{
		start = 0 ;
		end = 最近打开路径文件名最大数量M - 1 ;
	}

	for( index = end - 1 ; index >= start ; index-- )
	{
		strcpy( 全_风格主配置.最近打开路径文件名[index+1] , 全_风格主配置.最近打开路径文件名[index] );
	}
	strncpy( 全_风格主配置.最近打开路径文件名[0] , acPathFilename , sizeof(全_风格主配置.最近打开路径文件名[0])-1 );

	return;
}

void 更新最近打开路径文件名()
{
	HMENU		hRootMenu ;
	HMENU		hMenu_FILE ;
	HMENU		hMenu_OPENRECENTLYFILES ;
	int		count ;
	int		index ;

	BOOL		bret ;

	hRootMenu = GetMenu(全_主窗口句柄) ;
	hMenu_FILE = GetSubMenu( hRootMenu , 0 ) ;
	hMenu_OPENRECENTLYFILES = GetSubMenu( hMenu_FILE , 2 ) ;
	count = GetMenuItemCount( hMenu_OPENRECENTLYFILES ) ;
	for( index = 0 ; index < count ; index++ )
	{
		bret = DeleteMenu( hMenu_OPENRECENTLYFILES , 0 , MF_BYPOSITION );
	}

	for( index = 0 ; index < 最近打开路径文件名最大数量M ; index++ )
	{
		if( 全_风格主配置.最近打开路径文件名[index][0] == '\0' )
			break;

		bret = AppendMenu( hMenu_OPENRECENTLYFILES , MF_POPUP|MF_STRING , IDM_FILE_OPEN_RECENTLY_HISTORY_BASE+index , 全_风格主配置.最近打开路径文件名[index] );
	}

	count = GetMenuItemCount( hMenu_OPENRECENTLYFILES ) ;
	if( count == 0 )
	{
		bret = AppendMenu( hMenu_OPENRECENTLYFILES , MF_POPUP|MF_STRING , IDM_FILE_OPEN_RECENTLY_HISTORY_BASE , "q" );
		SetMenuItemEnable( 全_主窗口句柄 , IDM_FILE_OPEN_RECENTLY_HISTORY_BASE , FALSE );
	}

	return;
}

void 已检查设置当前文件类型( HWND hWnd , struct 选项卡页面S *选项卡页节点 )
{
	struct 文档类型配置S	*pst文档类型配置 = NULL ;
	int			index ;

	for( index = 0 , pst文档类型配置 = 全_文档类型配置 ; pst文档类型配置->n文档的类型 != DOCTYPE_END ; index++ , pst文档类型配置++ )
	{
		设置菜单项已选择( hWnd , IDM_VIEW_SWITCH_FILETYPE_BASE+index , (选项卡页节点&&(选项卡页节点->pst文档类型配置==pst文档类型配置)) );
	}

	return;
}

void 设置当前窗口主题已检查( HWND hWnd )
{
	int	index ;

	for( index = 0 ; index < g_nWindowThemeCount ; index++ )
	{
		设置菜单项已选择( hWnd , IDM_VIEW_SWITCH_STYLETHEME_BASE+index , g_pstWindowTheme==&(g_astWindowTheme[index]) );
	}

	return;
}

void 设置视图选项卡宽度菜单文本( int nMenuId )
{
	MENUITEMINFO	mii ;
	char		*pStart = NULL ;
	char		*pEnd = NULL ;
	char		acMenuText[ 256 ] ;
	char		acNewMenuText[ 256 ] ;

	memset( & mii , 0x00 , sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO) ;
	mii.fMask = MIIM_STRING ;
	mii.dwTypeData = acMenuText ;
	mii.cch = sizeof(acMenuText) - 1 ;
	GetMenuItemInfo(GetMenu(全_主窗口句柄), nMenuId , FALSE, & mii );
	pStart = strchr( acMenuText , '[' ) ;
	if( pStart )
	{
		pEnd = strchr( pStart+1 , ']' ) ;
		if( pEnd )
		{
			memset( acNewMenuText , 0x00 , sizeof(acNewMenuText) );
			snprintf( acNewMenuText , sizeof(acNewMenuText)-1 , "%.*s%d%.*s" , pStart-acMenuText+1,acMenuText , 全_风格主配置.选项卡宽度 , strlen(pEnd)+1 , pEnd );
		}
	}
	mii.cch = (int)strlen(acNewMenuText) - 1 ;
	mii.dwTypeData = acNewMenuText ;
	SetMenuItemInfo(GetMenu(全_主窗口句柄), nMenuId , FALSE, & mii );

	return;
}

void 设置源代码自动完成输入字符后显示菜单文本( int nMenuId )
{
	MENUITEMINFO	mii ;
	char		*p = NULL ;
	char		acMenuText[ 256 ] ;

	memset( & mii , 0x00 , sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO) ;
	mii.fMask = MIIM_STRING ;
	mii.dwTypeData = acMenuText ;
	mii.cch = sizeof(acMenuText) - 1 ;
	GetMenuItemInfo(GetMenu(全_主窗口句柄), nMenuId , FALSE, & mii );
	p = strchr( acMenuText , '[' ) ;
	if( p )
	{
		*(p+1) = (全_风格主配置.输入字符后自动完成显示%10) + '0' ;
	}
	mii.cch = (int)strlen(acMenuText) - 1 ;
	SetMenuItemInfo(GetMenu(全_主窗口句柄), nMenuId , FALSE, & mii );

	return;
}

void 设置重新加载符号列表或树间隔菜单文本( int nMenuId )
{
	MENUITEMINFO	mii ;
	char		*pStart = NULL ;
	char		*pEnd = NULL ;
	char		acMenuText[ 256 ] ;
	char		acNewMenuText[ 256 ] ;

	memset( & mii , 0x00 , sizeof(MENUITEMINFO) );
	mii.cbSize = sizeof(MENUITEMINFO) ;
	mii.fMask = MIIM_STRING ;
	mii.dwTypeData = acMenuText ;
	mii.cch = sizeof(acMenuText) - 1 ;
	GetMenuItemInfo(GetMenu(全_主窗口句柄), nMenuId , FALSE, & mii );
	pStart = strchr( acMenuText , '[' ) ;
	if( pStart )
	{
		pEnd = strchr( pStart+1 , ']' ) ;
		if( pEnd )
		{
			memset( acNewMenuText , 0x00 , sizeof(acNewMenuText) );
			snprintf( acNewMenuText , sizeof(acNewMenuText)-1 , "%.*s%d%.*s" , pStart-acMenuText+1,acMenuText , 全_风格主配置.重新加载符号列表或树间隔 , strlen(pEnd)+1 , pEnd );
		}
	}
	mii.cch = (int)strlen(acNewMenuText) - 1 ;
	mii.dwTypeData = acNewMenuText ;
	SetMenuItemInfo(GetMenu(全_主窗口句柄), nMenuId , FALSE, & mii );

	return;
}

void 更新所有菜单( HWND hWnd , struct 选项卡页面S *选项卡页节点 )
{
	设置菜单项已选择( hWnd , IDM_CRATE_NEWFILE_ON_NEWBOOT , 全_风格主配置.在启动时创建新文件 );
	设置菜单项已选择( hWnd , IDM_FILE_OPENFILES_THAT_OPENNING_ON_EXIT , 全_风格主配置.打开退出时打开的文件 );
	设置菜单项已选择( hWnd , IDM_FILE_SETREADONLY_AFTER_OPEN , 全_风格主配置.打开文件后设置为只读 );
	更新最近打开路径文件名();
	设置菜单项已选择( hWnd , IDM_FILE_CHECK_UPDATE_WHERE_SELECTTABPAGE , 全_风格主配置.检查被选中选项卡页面的更新 );
	设置菜单项已选择( hWnd , IDM_FILE_PROMPT_WHERE_AUTOUPDATE , 全_风格主配置.提示自动更新 );
	设置菜单项已选择( hWnd , IDM_FILE_NEWFILE_WINDOWS_EOLS , (全_风格主配置.新建文件Eols==0) );
	设置菜单项已选择( hWnd , IDM_FILE_NEWFILE_MAC_EOLS , (全_风格主配置.新建文件Eols==1) );
	设置菜单项已选择( hWnd , IDM_FILE_NEWFILE_UNIX_EOLS , (全_风格主配置.新建文件Eols==2) );
	设置菜单项已选择( hWnd , IDM_FILE_NEWFILE_ENCODING_UTF8 , (全_风格主配置.新建文件编码==65001) );
	设置菜单项已选择( hWnd , IDM_FILE_NEWFILE_ENCODING_GB18030 , (全_风格主配置.新建文件编码==936) );
	设置菜单项已选择( hWnd , IDM_FILE_NEWFILE_ENCODING_BIG5 , (全_风格主配置.新建文件编码==950) );

	设置菜单项已选择( hWnd , IDM_EDIT_ENABLE_AUTO_ADD_CLOSECHAR , 全_风格主配置.启用自动添加闭合字符 );
	设置菜单项已选择( hWnd , IDM_EDIT_ENABLE_AUTO_INDENTATION , 全_风格主配置.启用自动识别 );

	已检查设置当前文件类型( hWnd , 选项卡页节点 );
	设置当前窗口主题已检查( hWnd );
	设置菜单项已选择( hWnd , IDM_VIEW_FILETREE , 全_显示文件树栏 );
	设置菜单项已选择( hWnd , IDM_VIEW_ENABLE_WINDOWS_VISUAL_STYLES , 全_风格主配置.启用窗口视觉样式 );
	设置视图选项卡宽度菜单文本( IDM_VIEW_TAB_WIDTH );
	设置菜单项已选择( hWnd , IDM_VIEW_ONKEYDOWN_TAB_CONVERT_SPACES , 全_风格主配置.按下Tab键转换为空格 );
	设置菜单项已选择( hWnd , IDM_VIEW_WRAPLINE_MODE , 全_风格主配置.换行模式 );
	设置菜单项已选择( hWnd , IDM_VIEW_LINENUMBER_VISIABLE , 全_风格主配置.启用行号 );
	设置菜单项已选择( hWnd , IDM_VIEW_BOOKMARK_VISIABLE , 全_风格主配置.启用书签 );
	设置菜单项已选择( hWnd , IDM_VIEW_WHITESPACE_VISIABLE , 全_风格主配置.启用空格符 );
	设置菜单项已选择( hWnd , IDM_VIEW_NEWLINE_VISIABLE , 全_风格主配置.新行可见 );
	设置菜单项已选择( hWnd , IDM_VIEW_INDENTATIONGUIDES_VISIABLE , 全_风格主配置.启用缩进辅助线 );

	设置重新加载符号列表或树间隔菜单文本( IDM_SOURCECODE_SET_RELOAD_SYMBOLLIST_OR_TREE_INTERVAL );
	设置菜单项已选择( hWnd , IDM_SOURCECODE_ENABLE_AUTOCOMPLETEDSHOW , 全_风格主配置.启用自动完成显示 );
	设置源代码自动完成输入字符后显示菜单文本( IDM_SOURCECODE_AUTOCOMPLETEDSHOW_AFTER_INPUT_CHARACTERS );
	设置菜单项已选择( hWnd , IDM_SOURCECODE_ENABLE_CALLTIPSHOW , 全_风格主配置.启用调用提示显示 );

	设置菜单项已选择( hWnd , IDM_SOURCECODE_BLOCKFOLD_VISIABLE , 全_风格主配置.启用块折叠 );

	if( 选项卡页节点 )
	{
		SetMenuItemEnable( hWnd , IDM_FILE_SAVE , 选项卡页节点 && IsDocumentModified(选项卡页节点) );
		SetMenuItemEnable( hWnd , IDM_FILE_SAVEAS , 选项卡页节点 && 选项卡页节点->acFilename[0] );
		设置菜单项已选择( hWnd , IDM_FILE_CONVERT_WINDOWS_EOLS , (选项卡页节点->pfuncScintilla(选项卡页节点->pScintilla,SCI_GETEOLMODE,0,0)==0) );
		设置菜单项已选择( hWnd , IDM_FILE_CONVERT_MAC_EOLS , (选项卡页节点->pfuncScintilla(选项卡页节点->pScintilla,SCI_GETEOLMODE,0,0)==1) );
		设置菜单项已选择( hWnd , IDM_FILE_CONVERT_UNIX_EOLS , (选项卡页节点->pfuncScintilla(选项卡页节点->pScintilla,SCI_GETEOLMODE,0,0)==2) );
		设置菜单项已选择( hWnd , IDM_FILE_CONVERT_ENCODING_UTF8 , (选项卡页节点->pfuncScintilla(选项卡页节点->pScintilla,SCI_GETCODEPAGE,0,0)==65001) );
		设置菜单项已选择( hWnd , IDM_FILE_CONVERT_ENCODING_GB18030 , (选项卡页节点->pfuncScintilla(选项卡页节点->pScintilla,SCI_GETCODEPAGE,0,0)==936) );
		设置菜单项已选择( hWnd , IDM_FILE_CONVERT_ENCODING_BIG5 , (选项卡页节点->pfuncScintilla(选项卡页节点->pScintilla,SCI_GETCODEPAGE,0,0)==950) );

		SetMenuItemEnable( hWnd , IDM_VIEW_HEXEDIT_MODE , TRUE );
		设置菜单项已选择( hWnd , IDM_VIEW_HEXEDIT_MODE , 选项卡页节点->bHexEditMode );
	}
	else
	{
		SetMenuItemEnable( hWnd , IDM_FILE_SAVE , FALSE );
		SetMenuItemEnable( hWnd , IDM_FILE_SAVEAS , FALSE );

		设置菜单项已选择( hWnd , IDM_FILE_CONVERT_WINDOWS_EOLS , FALSE );
		设置菜单项已选择( hWnd , IDM_FILE_CONVERT_MAC_EOLS , FALSE );
		设置菜单项已选择( hWnd , IDM_FILE_CONVERT_UNIX_EOLS , FALSE );

		SetMenuItemEnable( hWnd , IDM_VIEW_HEXEDIT_MODE , FALSE );
	}

	if( 选项卡页节点 == NULL )
	{
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_NEW , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_OPEN , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_SAVE , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_SAVEAS , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_SAVEALL , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_CLOSE , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_REMOTE_FILESERVERS , TRUE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_EDIT_UNDO , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_EDIT_REDO , FALSE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_EDIT_CUT , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_EDIT_COPY , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_EDIT_PASTE , FALSE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_FIND , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_FINDPREV , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_FINDNEXT , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_FOUNDLIST , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_REPLACE , FALSE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_TOGGLE_BOOKMARK , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_PREV_BOOKMARK , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_NEXT_BOOKMARK , FALSE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE , FALSE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_VIEW_FILETREE , TRUE );
		if( 全_显示文件树栏 )
		{
			SendMessage( 全_工具栏句柄 , TB_CHECKBUTTON , IDM_VIEW_FILETREE , TRUE );
		}
		else
		{
			SendMessage( 全_工具栏句柄 , TB_CHECKBUTTON , IDM_VIEW_FILETREE , FALSE );
		}

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_VIEW_MODIFY_STYLETHEME , TRUE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_VIEW_HEXEDIT_MODE , FALSE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_VIEW_ZOOMOUT , FALSE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_VIEW_ZOOMIN , FALSE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_ABOUT , TRUE );
	}
	else
	{
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_NEW , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_OPEN , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_SAVE , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_SAVEAS , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_SAVEALL , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_CLOSE , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_FILE_REMOTE_FILESERVERS , TRUE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_EDIT_UNDO , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_EDIT_REDO , TRUE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_EDIT_CUT , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_EDIT_COPY , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_EDIT_PASTE , TRUE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_FIND , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_FINDPREV , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_FINDNEXT , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_FOUNDLIST , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_REPLACE , TRUE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_TOGGLE_BOOKMARK , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_PREV_BOOKMARK , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_GOTO_NEXT_BOOKMARK , TRUE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE , TRUE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_VIEW_FILETREE , TRUE );
		if( 全_显示文件树栏 )
		{
			SendMessage( 全_工具栏句柄 , TB_CHECKBUTTON , IDM_VIEW_FILETREE , TRUE );
		}
		else
		{
			SendMessage( 全_工具栏句柄 , TB_CHECKBUTTON , IDM_VIEW_FILETREE , FALSE );
		}
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_VIEW_HEXEDIT_MODE , TRUE );
		if( 选项卡页节点->bHexEditMode )
		{
			SendMessage( 全_工具栏句柄 , TB_CHECKBUTTON , IDM_VIEW_HEXEDIT_MODE , TRUE );
		}
		else
		{
			SendMessage( 全_工具栏句柄 , TB_CHECKBUTTON , IDM_VIEW_HEXEDIT_MODE , FALSE );
		}

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_VIEW_MODIFY_STYLETHEME , TRUE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_VIEW_ZOOMOUT , TRUE );
		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_VIEW_ZOOMIN , TRUE );

		SendMessage( 全_工具栏句柄 , TB_ENABLEBUTTON , IDM_ABOUT , TRUE );
	}

	return;
}
