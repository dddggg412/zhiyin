/****************************************************************************************************
 * 名称：知音编译器
 * 描述：windows版本知语言编辑器。
 * 版权(C) 2023-2023 位中原
 * Q群：500558920
 * 网址：www.880.xin
 * 仓库：https://gitee.com/zhi-yu-yan/zhiyin.git
 ****************************************************************************************************/

#include "framework.h"

#define 最大字符串长度 100

struct 主配置S	    全_风格主配置 ;
HINSTANCE	    全_当前实例 = NULL ;
HWND		    全_主窗口句柄 = NULL ;
HWND		    全_工具栏句柄 = NULL ;
WCHAR		    全_标题栏文本[最大字符串长度];
WCHAR		    全_主窗口类名[最大字符串长度];
int		    全_工具栏高度 = 0 ;
int		    全_argc = 0 ;
char		    **全_argv = NULL ;
char		    全_应用程序名称[100] = "知音";
char		    全_窗口类名称[100] = "主窗口";
char		    全_模块文件名[ MAX_PATH ] ;
char		    全_模块路径名[ MAX_PATH ] ;

ATOM 注册窗口类(HINSTANCE 当前实例句柄)
{
	WNDCLASSEX wcex;
	wcex.cbSize         = sizeof(WNDCLASSEX);
	wcex.style		    = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= 主窗口过程;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= 当前实例句柄;
	wcex.hIcon		    = LoadIcon(当前实例句柄, MAKEINTRESOURCE(IDI_EDITULTRA));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW);
	wcex.lpszMenuName	= MAKEINTRESOURCEA(IDC_EUX);
	wcex.lpszClassName	= 全_窗口类名称;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	return RegisterClassEx(&wcex);
}



BOOL 初始化实例(HINSTANCE 当前实例句柄, int 窗口的实现方式)
{
	HFONT		h控制字体 ;
	long		l控制主题 ;
	RECT		r窗口矩形 ;

	if( 全_风格主配置.启用窗口视觉样式 == FALSE ){DWORD dwFlags = (STAP_ALLOW_CONTROLS | STAP_ALLOW_WEBCONTENT) ;SetThemeAppProperties( dwFlags );}

	INITCOMMONCONTROLSEX icex ;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX) ;
	icex.dwICC = ICC_COOL_CLASSES | ICC_BAR_CLASSES ;
	BOOL bret = InitCommonControlsEx( & icex ) ;
	if( bret != TRUE ){MessageBox(NULL, TEXT("不能注册通用控件"), TEXT("错误"), MB_ICONERROR | MB_OK);return -1;}

	h控制字体 = CreateFont(
			-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE,  /*字体的逻辑高度*/
			0,                                                                           /*字体的逻辑宽度*/
			0,                                                                           /*指定移位向量相对X轴的偏转角度*/
			0,                                                                           /*指定字符基线相对X轴的偏转角度*/
			(g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL),                /*设置字体粗细程度*/
			FALSE,                                                                       /*是否启用斜体*/
			FALSE,                                                                       /*是否启用下划线*/
			0,                                                                           /*是否启用删除线*/
			DEFAULT_CHARSET ,                                                            /*指定字符集*/
			OUT_DEFAULT_PRECIS,                                                          /*输出精度*/
			CLIP_DEFAULT_PRECIS,                                                         /*剪裁精度*/
			DEFAULT_QUALITY,                                                             /*输出质量*/
			DEFAULT_PITCH | FF_SWISS,                                                    /*字体族*/
			g_pstWindowTheme->stStyleTheme.text.font                                     /*字体名*/
			) ;

	全_当前实例 = 当前实例句柄; // 将实例句柄存储在全局变量中

	全_主窗口句柄 = CreateWindowEx(
			WS_EX_ACCEPTFILES,/*窗口的扩展风格*/
			全_窗口类名称,       /*指向注册类名的指针*/
			全_应用程序名称,      /*指向窗口名称的指针*/
			WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_MAXIMIZE | WS_CLIPCHILDREN, /*窗口风格*/
			CW_USEDEFAULT,    /*窗口的水平位置*/
			0,               /*窗口的垂直位置*/
			900,             /*窗口的宽度*/
			800,             /*窗口的高度*/
			NULL,            /*父窗口的句柄*/
			NULL,            /*菜单的句柄或是子窗口的标识符*/
			当前实例句柄,         /*应用程序实例的句柄*/
			NULL             /*指向窗口的创建数据*/
			);
	if (!全_主窗口句柄){return FALSE;}

	创建主窗工具栏(当前实例句柄);
	GetClientRect( 全_工具栏句柄 , & r窗口矩形 );
	全_工具栏高度 = r窗口矩形.bottom - r窗口矩形.top ;



	全_状态栏句柄 = CreateWindow( STATUSCLASSNAME , "" , SBS_SIZEGRIP|WS_CHILD|WS_VISIBLE , 0 , 0 , 0 , 0 , 全_主窗口句柄 , NULL , 当前实例句柄 , NULL ) ;
	GetClientRect( 全_状态栏句柄 , & r窗口矩形 );
	全_状态栏高度 = r窗口矩形.bottom - r窗口矩形.top ;

	查找框句柄 = CreateDialog( 当前实例句柄 ,MAKEINTRESOURCE(IDD_SEARCH_FIND_DIALOG), 全_主窗口句柄 , SearchFindWndProc ) ;
	if (!查找框句柄)
	{
		return FALSE;
	}
	CheckRadioButton( 查找框句柄 , IDC_TEXTTYPE_GENERAL , IDC_TEXTTYPE_POSIXREGEXP , IDC_TEXTTYPE_GENERAL );
	CheckRadioButton( 查找框句柄 , IDC_AREATYPE_THISFILE , IDC_AREATYPE_CURRENTDIRECTORY , IDC_AREATYPE_THISFILE );
	CheckDlgButton( 查找框句柄 , IDC_OPTIONS_WHOLEWORD , FALSE );
	CheckDlgButton( 查找框句柄 , IDC_OPTIONS_MATCHCASE , TRUE );
	CheckDlgButton( 查找框句柄 , IDC_OPTIONS_WORDSTART , FALSE );
	CheckRadioButton( 查找框句柄 , IDC_SEARCHLOCATE_TO_TOP_OR_BOTTOM , IDC_SEARCHLOCATE_LOOP , IDC_SEARCHLOCATE_LOOP );
	hwndEditBoxInSearchFind = GetDlgItem( 查找框句柄 , IDC_SEARCH_TEXT ) ;
	ShowWindow( 查找框句柄 , SW_HIDE );

	已查框句柄 = CreateDialog( 当前实例句柄 ,MAKEINTRESOURCE(IDD_SEARCH_FOUND_DIALOG), 全_主窗口句柄 , SearchFoundWndProc ) ;
	if (!已查框句柄)
	{
		return FALSE;
	}
	搜索中已找到的列表框句柄 = GetDlgItem( 已查框句柄 , IDC_FOUNDLIST ) ;
	SendMessage( 搜索中已找到的列表框句柄 , WM_SETFONT , (WPARAM)h控制字体, 0);
	l控制主题 = GetWindowLong( 搜索中已找到的列表框句柄 , GWL_STYLE ) ;
	l控制主题 |= LBS_NOTIFY|LBS_USETABSTOPS|LBS_HASSTRINGS ;
	SetWindowLong( 搜索中已找到的列表框句柄 , GWL_STYLE , l控制主题 );
	ShowWindow( 已查框句柄 , SW_HIDE );

	替换对话框句柄 = CreateDialog( 当前实例句柄 ,MAKEINTRESOURCE(IDD_SEARCH_REPLACE_DIALOG), 全_主窗口句柄 , SearchReplaceWndProc ) ;
	if (!替换对话框句柄)
	{
		return FALSE;
	}
	CheckRadioButton( 替换对话框句柄 , IDC_TEXTTYPE_GENERAL , IDC_TEXTTYPE_POSIXREGEXP , IDC_TEXTTYPE_GENERAL );
	CheckRadioButton( 替换对话框句柄 , IDC_AREATYPE_THISFILE , IDC_AREATYPE_ALLOPENEDFILES , IDC_AREATYPE_THISFILE );
	CheckDlgButton( 替换对话框句柄 , IDC_OPTIONS_WHOLEWORD , FALSE );
	CheckDlgButton( 替换对话框句柄 , IDC_OPTIONS_MATCHCASE , TRUE );
	CheckDlgButton( 替换对话框句柄 , IDC_OPTIONS_WORDSTART , FALSE );
	CheckRadioButton( 替换对话框句柄 , IDC_SEARCHLOCATE_TO_TOP_OR_BOTTOM , IDC_SEARCHLOCATE_LOOP , IDC_SEARCHLOCATE_LOOP );
	hwndFromEditBoxInSearchReplace = GetDlgItem( 替换对话框句柄 , IDC_SEARCH_FROMTEXT ) ;
	hwndToEditBoxInSearchReplace = GetDlgItem( 替换对话框句柄 , IDC_SEARCH_TOTEXT ) ;
	ShowWindow( 替换对话框句柄 , SW_HIDE );

	更新所有菜单( 全_主窗口句柄 , 全_当前选项卡页面节点 );
	更新所有窗口( 全_主窗口句柄 );
	ShowWindow(全_主窗口句柄, SW_SHOWMAXIMIZED);
	UpdateWindow(全_主窗口句柄);

	return TRUE;
}


int 创建新窗口( HWND hWnd , WPARAM wParam , LPARAM lParam )
{
	BOOL	bret ;
	int	nret ;
	long	lsret ; /*原来是：long 换成了 long*/
	HKEY	注册表键_ZhiYin ;

	//// EnableMenuItem(GetSubMenu(GetMenu(hwndMainClient),3), 0, MF_DISABLED | MF_GRAYED | MF_BYCOMMAND);

	INITCOMMONCONTROLSEX icex ;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX) ;
	icex.dwICC = ICC_TAB_CLASSES ;
	bret = InitCommonControlsEx( & icex ) ;
	if (bret != TRUE){MessageBox(NULL, TEXT("不能注册TabControl控件"), TEXT("错误"), MB_ICONERROR | MB_OK); return -1;}

	nret = 创建文件树栏( hWnd ) ;
	if( nret ) return nret;

	nret = 创建选项卡页( hWnd ) ;
	if( nret ) return nret;

	lsret = RegOpenKey( HKEY_CLASSES_ROOT , "*\\shell\\ZHIYIN" , & 注册表键_ZhiYin ) ;
	if( lsret == ERROR_SUCCESS )
	{
		设置菜单项已选择( hWnd, IDM_ENV_FILE_POPUPMENU, TRUE);
		全_已选择环境文件弹出菜单 = TRUE ;
		RegCloseKey( 注册表键_ZhiYin );
	}
	else
	{
		设置菜单项已选择( hWnd, IDM_ENV_FILE_POPUPMENU, FALSE);
		全_已选择环境文件弹出菜单 = FALSE ;
	}

	lsret = RegOpenKey( HKEY_CLASSES_ROOT , "Directory\\shell\\ZHIYIN" , & 注册表键_ZhiYin ) ;
	if( lsret == ERROR_SUCCESS )
	{
		设置菜单项已选择( hWnd, IDM_ENV_DIRECTORY_POPUPMENU, TRUE);
		全_已选择环境目录弹出菜单 = TRUE ;
		RegCloseKey( 注册表键_ZhiYin );
	}
	else
	{
		设置菜单项已选择( hWnd, IDM_ENV_DIRECTORY_POPUPMENU, FALSE);
		全_已选择环境目录弹出菜单 = FALSE ;
	}

	if( 全_风格主配置.显示文件树栏 == TRUE )
	{
		全_显示文件树栏 = TRUE ;
		全_文件树栏矩形.right = 全_风格主配置.文件树栏宽度 ;
		全_选项卡页矩形.left = 全_风格主配置.文件树栏宽度 + 分割_宽度 ;
	}

	return 0;
}

void 更新所有窗口( HWND hWnd )
{
	RECT	矩形调整 ;
	RECT	文件树矩形 ;

	计算选项卡页面高度();
	调整选项卡页面();
	SetWindowPos( 全_选项卡页面句柄 , HWND_TOP , 全_选项卡页矩形.left , 全_选项卡页矩形.top , 全_选项卡页矩形.right-全_选项卡页矩形.left , 全_选项卡页矩形.bottom-全_选项卡页矩形.top , SWP_SHOWWINDOW );
	memcpy( & 矩形调整 , & 全_选项卡页矩形 , sizeof(RECT) );
	TabCtrl_AdjustRect( 全_选项卡页面句柄 , FALSE , & 矩形调整 );

	计算选项卡页面高度();
	调整选项卡页面();

	if( 全_显示文件树栏 == TRUE )
	{
		SetWindowPos ( 全_文件树栏句柄 , HWND_TOP ,  全_文件树栏矩形.left , 全_文件树栏矩形.top , 全_文件树栏矩形.right-全_文件树栏矩形.left , 全_文件树栏矩形.bottom-全_文件树栏矩形.top , SWP_SHOWWINDOW );
		memcpy( & 矩形调整 , & 全_文件树栏矩形 , sizeof(RECT) );
		TabCtrl_AdjustRect( 全_文件树栏句柄 , FALSE , & 矩形调整 );
		ShowWindow( 全_文件树栏句柄 , SW_SHOW );
		UpdateWindow( 全_文件树栏句柄 );
	}
	else
	{
		SetWindowPos ( 全_文件树栏句柄 , 0 ,  0 , 0 , 0 , 0 , SWP_HIDEWINDOW );
		ShowWindow( 全_文件树栏句柄 , SW_HIDE );
		UpdateWindow( 全_文件树栏句柄 );
	}

	调整文件树框( & 全_文件树栏矩形 , & 文件树矩形 );

	if( 全_显示文件树栏 == TRUE )
	{
		SetWindowPos( 全_文件树句柄 , HWND_TOP , 文件树矩形.left , 文件树矩形.top , 文件树矩形.right-文件树矩形.left , 文件树矩形.bottom-文件树矩形.top , SWP_SHOWWINDOW );
		ShowWindow( 全_文件树句柄 , SW_SHOW );
		InvalidateRect( 全_文件树句柄 , NULL , TRUE );
		UpdateWindow( 全_文件树句柄 );
	}
	else
	{
		SetWindowPos( 全_文件树句柄 , 0 , 0 , 0 , 0 , 0 , SWP_HIDEWINDOW );
		ShowWindow( 全_文件树句柄 , SW_HIDE );
		UpdateWindow( 全_文件树句柄 );
	}

	SetWindowPos( 全_选项卡页面句柄 , HWND_TOP , 全_选项卡页矩形.left , 全_选项卡页矩形.top , 全_选项卡页矩形.right-全_选项卡页矩形.left , 全_选项卡页矩形.bottom-全_选项卡页矩形.top , SWP_SHOWWINDOW );
	memcpy( & 矩形调整 , & 全_选项卡页矩形 , sizeof(RECT) );
	TabCtrl_AdjustRect( 全_选项卡页面句柄 , FALSE , & 矩形调整 );
	/*
	ShowWindow( 全_选项卡页面句柄 , SW_SHOW);
	InvalidateRect( 全_选项卡页面句柄 , NULL , TRUE );
	UpdateWindow( 全_选项卡页面句柄 );
	*/

	int n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( int n选项卡页面索引 = 0 ; n选项卡页面索引 < n选项卡页面数量 ; n选项卡页面索引++ )
	{
		TCITEM tci ;
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		struct 选项卡页面S *选项卡页节点 = (struct 选项卡页面S *)(tci.lParam);
		if( 选项卡页节点 != 全_当前选项卡页面节点 )
		{
			SetWindowPos( 选项卡页节点->Scintilla句柄 , 0 , 选项卡页节点->Scintilla矩形.left , 选项卡页节点->Scintilla矩形.top , 选项卡页节点->Scintilla矩形.right-选项卡页节点->Scintilla矩形.left , 选项卡页节点->Scintilla矩形.bottom-选项卡页节点->Scintilla矩形.top , SWP_HIDEWINDOW );

			if( 选项卡页节点->符号列表句柄 )
			{
				SetWindowPos( 选项卡页节点->符号列表句柄 , 0 , 选项卡页节点->符号列表矩形.left , 选项卡页节点->符号列表矩形.top , 选项卡页节点->符号列表矩形.right-选项卡页节点->符号列表矩形.left , 选项卡页节点->符号列表矩形.bottom-选项卡页节点->符号列表矩形.top , SWP_HIDEWINDOW );
				/*
				ShowWindow( 选项卡页节点->符号列表句柄 , SW_HIDE);
				InvalidateRect( 选项卡页节点->符号列表句柄 , NULL , TRUE );
				UpdateWindow( 选项卡页节点->符号列表句柄 );
				*/
			}

			if( 选项卡页节点->符号树句柄 )
			{
				SetWindowPos( 选项卡页节点->符号树句柄 , 0 , 选项卡页节点->符号树矩形.left , 选项卡页节点->符号树矩形.top , 选项卡页节点->符号树矩形.right-选项卡页节点->符号树矩形.left , 选项卡页节点->符号树矩形.bottom-选项卡页节点->符号树矩形.top , SWP_HIDEWINDOW );
				/*
				ShowWindow( 选项卡页节点->符号树句柄 , SW_HIDE);
				InvalidateRect( 选项卡页节点->符号树句柄 , NULL , TRUE );
				UpdateWindow( 选项卡页节点->符号树句柄 );
				*/
			}

			if( 选项卡页节点->hwndQueryResultEdit )
			{
				/*
				ShowWindow( 选项卡页节点->hwndQueryResultEdit , SW_HIDE );
				UpdateWindow( 选项卡页节点->hwndQueryResultEdit );
				*/
			}

			if( 选项卡页节点->hwndQueryResultTable )
			{
				/*
				ShowWindow( 选项卡页节点->hwndQueryResultTable , SW_HIDE );
				UpdateWindow( 选项卡页节点->hwndQueryResultTable );
				*/
			}
		}
	}

	if( 全_当前选项卡页面节点 )
	{
		调整选项卡页面框( 全_当前选项卡页面节点 );

		SetWindowPos( 全_当前选项卡页面节点->Scintilla句柄 , HWND_TOP , 全_当前选项卡页面节点->Scintilla矩形.left , 全_当前选项卡页面节点->Scintilla矩形.top , 全_当前选项卡页面节点->Scintilla矩形.right-全_当前选项卡页面节点->Scintilla矩形.left , 全_当前选项卡页面节点->Scintilla矩形.bottom-全_当前选项卡页面节点->Scintilla矩形.top , SWP_SHOWWINDOW );
		InvalidateRect( 全_当前选项卡页面节点->Scintilla句柄 , NULL , TRUE );
		UpdateWindow( 全_当前选项卡页面节点->Scintilla句柄 );

		if( 全_当前选项卡页面节点->符号列表句柄 )
		{
			SetWindowPos( 全_当前选项卡页面节点->符号列表句柄 , HWND_TOP , 全_当前选项卡页面节点->符号列表矩形.left , 全_当前选项卡页面节点->符号列表矩形.top , 全_当前选项卡页面节点->符号列表矩形.right-全_当前选项卡页面节点->符号列表矩形.left , 全_当前选项卡页面节点->符号列表矩形.bottom-全_当前选项卡页面节点->符号列表矩形.top , SWP_SHOWWINDOW );
			ShowWindow( 全_当前选项卡页面节点->符号列表句柄 , SW_SHOW);
			InvalidateRect( 全_当前选项卡页面节点->符号列表句柄 , NULL , TRUE );
			UpdateWindow( 全_当前选项卡页面节点->符号列表句柄 );
		}

		if( 全_当前选项卡页面节点->符号树句柄 )
		{
			SetWindowPos( 全_当前选项卡页面节点->符号树句柄 , HWND_TOP , 全_当前选项卡页面节点->符号树矩形.left , 全_当前选项卡页面节点->符号树矩形.top , 全_当前选项卡页面节点->符号树矩形.right-全_当前选项卡页面节点->符号树矩形.left , 全_当前选项卡页面节点->符号树矩形.bottom-全_当前选项卡页面节点->符号树矩形.top , SWP_SHOWWINDOW );
			ShowWindow( 全_当前选项卡页面节点->符号树句柄 , SW_SHOW);
			InvalidateRect( 全_当前选项卡页面节点->符号树句柄 , NULL , TRUE );
			UpdateWindow( 全_当前选项卡页面节点->符号树句柄 );
		}

		if( 全_当前选项卡页面节点->hwndQueryResultEdit )
		{
			SetWindowPos( 全_当前选项卡页面节点->hwndQueryResultEdit , HWND_TOP , 全_当前选项卡页面节点->rectQueryResultEdit.left , 全_当前选项卡页面节点->rectQueryResultEdit.top , 全_当前选项卡页面节点->rectQueryResultEdit.right-全_当前选项卡页面节点->rectQueryResultEdit.left , 全_当前选项卡页面节点->rectQueryResultEdit.bottom-全_当前选项卡页面节点->rectQueryResultEdit.top , 全_当前选项卡页面节点->hwndQueryResultEdit?SWP_SHOWWINDOW:SWP_HIDEWINDOW );
			ShowWindow( 全_当前选项卡页面节点->hwndQueryResultEdit , SW_SHOW);
			InvalidateRect( 全_当前选项卡页面节点->hwndQueryResultEdit , NULL , TRUE );
			UpdateWindow( 全_当前选项卡页面节点->hwndQueryResultEdit );
		}

		if( 全_当前选项卡页面节点->hwndQueryResultTable )
		{
			SetWindowPos( 全_当前选项卡页面节点->hwndQueryResultTable , HWND_TOP , 全_当前选项卡页面节点->rectQueryResultListView.left , 全_当前选项卡页面节点->rectQueryResultListView.top , 全_当前选项卡页面节点->rectQueryResultListView.right-全_当前选项卡页面节点->rectQueryResultListView.left , 全_当前选项卡页面节点->rectQueryResultListView.bottom-全_当前选项卡页面节点->rectQueryResultListView.top , 全_当前选项卡页面节点->hwndQueryResultTable?SWP_SHOWWINDOW:SWP_HIDEWINDOW );
			ShowWindow( 全_当前选项卡页面节点->hwndQueryResultTable , SW_SHOW);
			InvalidateRect( 全_当前选项卡页面节点->hwndQueryResultTable , NULL , TRUE );
			UpdateWindow( 全_当前选项卡页面节点->hwndQueryResultTable );
		}

		SetFocus( 全_当前选项卡页面节点->Scintilla句柄 );
	}
	UpdateWindow( 全_工具栏句柄 );
	// InvalidateRect( g_hwndReBar , NULL , TRUE );
	// UpdateWindow( g_hwndReBar );
	更新状态栏( hWnd );
	// InvalidateRect( 全_主窗口句柄 , NULL , TRUE );
	// UpdateWindow( 全_主窗口句柄 );
	return;
}
void 重置窗口大小( HWND hWnd , int nWidth , int nHeight )
{
	if( 全_文件树栏句柄 == NULL || 全_选项卡页面句柄 == NULL )return;

	全_选项卡页矩形.right = 全_选项卡页矩形.left + nWidth ;
	全_选项卡页矩形.bottom = 全_选项卡页矩形.top + nHeight ;
	更新所有窗口( hWnd );
	return;
}



void *子线程1_程序更新(void *arg)/*arg:接收线程传递过来的参数*/
{
    printf("http://www.880.xin\n");
    return "程序更新成功。";
}
void* 子线程2_用户中心(void* arg)
{
    printf("http://www.880.xin/login\n");
    return "用户登录成功。";
}

int APIENTRY wWinMain(HINSTANCE 当前实例句柄,HINSTANCE 上一个实例,LPWSTR	命令行参数,int	窗口的实现方式)
{

	/**************************************************多线程-开始**************************************************/
    int 返回结果;
    pthread_t 子线程_程序更新, 子线程_用户中心;
    void* 线程_结果;

    返回结果 = pthread_create(
    		&子线程_程序更新,         /*一个线程变量名，被创建线程的标识*/
    		NULL,            /*线程的属性指针*/
			子线程1_程序更新, /*被创建线程的程序代码*/
			NULL             /*传给"子线程1_要执行的函数"的参数（程序代码的参数）*/
			);
    if (返回结果 != 0) {printf("程序更新线程创建失败！");return 0;}

    返回结果 = pthread_create(&子线程_用户中心, NULL, 子线程2_用户中心, NULL);
    if (返回结果 != 0) {printf("用户中心登录线程创建失败！");return 0;}

    /* 等待一个线程的结束,线程间同步的操作
     * 第一个参数：指定要等待的线程ID
     * 第二个参数：用户定义的指针，用来存储被等待线程的返回值。如果是NULL，则返回值被忽略。
    */
    返回结果 = pthread_join(子线程_程序更新, &线程_结果);
    //输出线程执行完毕后返回的数据
    printf("%s\n", (char*)线程_结果);

    返回结果 = pthread_join(子线程_用户中心, &线程_结果);
    printf("%s\n", (char*)线程_结果);

	/**************************************************多线程-结束**************************************************/





#if 0 /*测试语句*/
	MessageBox(NULL, TEXT("这里是正确的"), TEXT("测试"), MB_ICONERROR | MB_OK);
#endif
	char	ac当前目录[ MAX_PATH ] ;
	RECT	主窗口的矩形 ;
	int	nret = 0 ;

	memset( ac当前目录 , 0x00 , sizeof(ac当前目录) );
	GetCurrentDirectory( sizeof(ac当前目录)-1 , ac当前目录 );
	设置日志文件( (char*)"%s\\log\\ZHIYIN.log" , ac当前目录 );
	设置日志级别( 日志级别_无日志 );
	调试_日志( "--- ZHIYIN 开始调试 ---" )

	UNREFERENCED_PARAMETER(上一个实例);
	全_argv = 命令行转换成ArgvA( 命令行参数 , & 全_argc ) ;

	// 初始化全局字符串
	LoadStringA(当前实例句柄, IDS_APP_TITLE, 全_应用程序名称, 100);
	LoadStringA(当前实例句柄, IDC_EUX, 全_窗口类名称, 100);
	LoadStringW(当前实例句柄, IDS_APP_TITLE, 全_标题栏文本, 最大字符串长度);
	LoadStringW(当前实例句柄, IDC_EUX, 全_主窗口类名, 最大字符串长度);

	{
		memset( 全_模块文件名 , 0x00 , sizeof(全_模块文件名) );
		GetModuleFileName( NULL , 全_模块文件名 , sizeof(全_模块文件名) );
		strcpy( 全_模块路径名 , 全_模块文件名 );
		char *p = strrchr( 全_模块路径名 , '\\' ) ;
		if( p )
			*(p) = '\0' ;
	}

	HANDLE 互斥锁 = CreateMutex( NULL , TRUE , "您好 , 知音" ) ;
	if( 互斥锁 == NULL ){错误框( "创建互斥量失败[%d]" , GetLastError() );return -1;}
	if( GetLastError() == ERROR_ALREADY_EXISTS ){WaitForSingleObject( 互斥锁 , INFINITE );}
	if( 全_argc == 1 && 全_argv[0] )
	{
		HWND hwnd = FindWindow( 全_窗口类名称 , NULL ) ;
		if( hwnd )
		{
			COPYDATASTRUCT	cpd ;
			memset( & cpd , 0x00 , sizeof(COPYDATASTRUCT) );
			cpd.lpData = 全_argv[0] ;
			cpd.cbData = (DWORD)strlen(全_argv[0]) ;
			SendMessage( hwnd , WM_COPYDATA , 0 , (LPARAM)&cpd );
			SendMessage( hwnd , WM_ACTIVATE , 0 , 0 );
			ReleaseMutex( 互斥锁 );
			CloseHandle( 互斥锁 );
			return 0;
		}
	}

	设置默认主配置( & 全_风格主配置 );
	初始化列表头( & 远程文件服务器列表 );
	初始化导航返回下一个跟踪列表();
	加载配置();

	HMODULE hmod = LoadLibrary(TEXT("SciLexer32.DLL")) ;/*装载Scintilla控件*/
	if (hmod == NULL){MessageBox(NULL, TEXT("不能装载Scintilla控件"), TEXT("错误"), MB_ICONERROR | MB_OK);CloseHandle( 互斥锁 );return 1;}

	curl_global_init( CURL_GLOBAL_DEFAULT );
	if(!注册窗口类(当前实例句柄)){MessageBox(0, "注册窗口失败", "错误信息",MB_ICONEXCLAMATION | MB_OK | MB_SYSTEMMODAL);return 0;}
	if(!初始化实例 (当前实例句柄, 窗口的实现方式) ){CloseHandle( 互斥锁 );MessageBox(0, "创建窗口失败", "错误信息",MB_ICONEXCLAMATION | MB_OK | MB_SYSTEMMODAL);return FALSE;}
	更新窗口主题菜单();
	更新文件类型菜单();
	更新所有菜单( 全_主窗口句柄 , NULL );

	HACCEL h加速键表 = LoadAccelerators(当前实例句柄, MAKEINTRESOURCE(IDC_EUX));

	if( 全_argc >= 1 )
	{
		size_t	路径文件名称长度 ;

		for( int i = 0 ; i < 全_argc ; i++ )
		{
			路径文件名称长度 = strlen(全_argv[i]) ;

			if( 路径文件名称长度 > 2 && 全_argv[i][路径文件名称长度-2] == '\\' && 全_argv[i][路径文件名称长度-1] == '\\' )
				加载目录到文件树( 全_文件树句柄 , 全_argv[i] );
			else if( 路径文件名称长度 > 1 && 全_argv[i][路径文件名称长度-1] == '\\' )
				定位目录( 全_argv[i] );
			else
				直接打开文件( 全_argv[i] );
		}
	}
	else if( 全_风格主配置.打开退出时打开的文件 == TRUE )
	{
		for( int index = 0 ; index < 启动时打开文件最大数量M ; index++ )
		{
			if( 全_风格主配置.启动时打开文件[index][0] == '\0' )
				break;

			直接打开文件( 全_风格主配置.启动时打开文件[index] );
		}

		if( 全_风格主配置.启动时的活动文件[0] )
		{
			int		n选项卡页面数量 , n选项卡页面索引 ;
			TCITEM		tci ;
			struct 选项卡页面S	*p = NULL ;

			n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
			for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
			{
				memset( & tci , 0x00 , sizeof(TCITEM) );
				tci.mask = TCIF_PARAM ;
				TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
				p = (struct 选项卡页面S *)(tci.lParam);
				if( strcmp( p->acPathFilename , 全_风格主配置.启动时的活动文件 ) == 0 ){以索引选择选项卡页面( n选项卡页面索引 );break;}
			}
		}
	}
	ReleaseMutex( 互斥锁 );
	CloseHandle( 互斥锁 );
	if( 全_风格主配置.在启动时创建新文件 == TRUE && TabCtrl_GetItemCount( 全_选项卡页面句柄 ) == 0 ){PostMessage( 全_主窗口句柄 , WM_COMMAND , (WPARAM)IDM_FILE_NEW , (LPARAM)0 );}

	GetWindowRect( 全_主窗口句柄 , & 主窗口的矩形 );
	PostMessage( 全_主窗口句柄 , WM_SIZE , (WPARAM)SIZE_MAXSHOW , (LPARAM)MAKELONG(主窗口的矩形.right-主窗口的矩形.left,主窗口的矩形.bottom-主窗口的矩形.top) );

	if( 全_风格主配置.重新加载符号列表或树间隔 > 0 )
	{
		重新加载符号列表或树线程处理程序 = _beginthread( 重新加载符号列表或树线程条目 , 0 , NULL ) ;
		if( 重新加载符号列表或树线程处理程序 == -1L ){错误框( "创建定时刷新符号列表或符号树线程失败" );return 1;}
	}

	MSG msg;
	// 主消息循环:
	while( GetMessage( & msg , NULL , 0 , 0 ) )
	{
		if ( !IsDialogMessage( 查找框句柄, &msg ) && !IsDialogMessage( 替换对话框句柄, & msg ) )
		{
			nret = 主窗口过程之前( & msg ) ;
			if( nret > 0 )continue;
			if( ! TranslateAccelerator( 全_主窗口句柄 , h加速键表 , & msg ) )
			{
				TranslateMessage( & msg );
				nret = 发送消息之前( & msg ) ;
				if( nret > 0 )continue;
				DispatchMessage( & msg );
			}
			nret = 主窗口过程之后( & msg ) ;
			if( nret > 0 )continue;
		}
	}
	保存主配置文件();
	保存样式主题配置文件();
	return (int) msg.wParam;
}
