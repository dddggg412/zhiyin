#include "framework.h"

int CreateQueryResultEditCtl( struct 选项卡页面S *选项卡页节点 , HFONT hFont )
{
	if( 选项卡页节点->hwndQueryResultEdit )
	{
		DestroyWindow( 选项卡页节点->hwndQueryResultEdit );
	}

	/* 创建结果消息控件 */
	// 选项卡页节点->hwndQueryResultEdit = CreateWindow( "EDIT" , NULL , WS_BORDER|WS_CHILD|WS_VISIBLE|WS_VSCROLL|ES_LEFT|ES_MULTILINE|ES_AUTOVSCROLL , 选项卡页节点->rectQueryResultEdit.left , 选项卡页节点->rectQueryResultEdit.top , 选项卡页节点->rectQueryResultEdit.right-选项卡页节点->rectQueryResultEdit.left , 选项卡页节点->rectQueryResultEdit.bottom-选项卡页节点->rectQueryResultEdit.top , g_hwndMainClient , NULL , 全_当前实例 , NULL ) ;
	选项卡页节点->hwndQueryResultEdit = CreateWindow( "EDIT" , NULL , WS_CHILD|WS_VISIBLE|WS_VSCROLL|ES_LEFT|ES_MULTILINE|ES_AUTOVSCROLL , 选项卡页节点->rectQueryResultEdit.left , 选项卡页节点->rectQueryResultEdit.top , 选项卡页节点->rectQueryResultEdit.right-选项卡页节点->rectQueryResultEdit.left , 选项卡页节点->rectQueryResultEdit.bottom-选项卡页节点->rectQueryResultEdit.top , 全_主窗口句柄 , NULL , 全_当前实例 , NULL ) ;
	if( 选项卡页节点->hwndQueryResultEdit == NULL )
	{
		MessageBox(NULL, TEXT("不能创建结果消息控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	SendMessage( 选项卡页节点->hwndQueryResultEdit , WM_SETFONT , (WPARAM)hFont, 0);

	return 0;
}

int AppendSqlQueryResultEditText( HWND hWnd , char *format , ... )
{
	va_list		valist ;
	SYSTEMTIME	systime ;
	char		buf[ 1024 ] ;
	int		l , len ;

	memset( buf , 0x00 , sizeof(buf) );
	len = 0 ;

	GetLocalTime( & systime );

	l = snprintf( buf+len , sizeof(buf)-1-len , "%04d-%02d-%02d %02d:%02d:%02d.%06d | " , systime.wYear , systime.wMonth , systime.wDay , systime.wHour , systime.wMinute , systime.wSecond , systime.wMilliseconds*1000 ) ;
	if( l > 0 )
		len += l ;

	va_start( valist , format );
	l = vsnprintf( buf+len , sizeof(buf)-1-len , format , valist ) ;
	if( l > 0 )
		len += l ;
	va_end( valist );

	l = snprintf( buf+len , sizeof(buf)-1-len , "\r\n" ) ;
	if( l > 0 )
		len += l ;

	SendMessage( hWnd , EM_SETSEL , -2 , -1 );
	SendMessage( hWnd , EM_REPLACESEL , TRUE , (LPARAM)buf );

	return len;
}

