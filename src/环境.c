#include "framework.h"

BOOL 全_已选择环境文件弹出菜单 ;
BOOL 全_已选择环境目录弹出菜单 ;

#define 使用_知音编辑器_打开_文件	"用 ZHIYIN 打开文件"

int OnEnvFilePopupMenu()
{
	HKEY		注册表键_ZhiYin ;
	HKEY		regkey_EditUltra_command ;
	long		lsret ;

	if( 全_已选择环境文件弹出菜单 == FALSE )
	{
		char		acCommand[ MAX_PATH ] ;

		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "*\\shell\\ZHIYIN" , & 注册表键_ZhiYin ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表创建项\"*\\shell\\ZHIYIN\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表创建项\"*\\shell\\ZHIYIN\""), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
		}

		lsret = RegSetValue( 注册表键_ZhiYin , NULL , REG_SZ , 使用_知音编辑器_打开_文件 , (DWORD)sizeof(使用_知音编辑器_打开_文件) ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"*\\shell\\ZHIYIN:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		lsret = RegSetKeyValue( 注册表键_ZhiYin , NULL , "Icon" , REG_SZ , 全_模块文件名 , (DWORD)strlen(全_模块文件名)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"*\\shell\\ZHIYIN:Icon\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		RegCloseKey( 注册表键_ZhiYin );

		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "*\\shell\\ZHIYIN\\command" , & regkey_EditUltra_command ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表创建项\"*\\shell\\ZHIYIN\\command\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		memset( acCommand , 0x00 , sizeof(acCommand) );
		snprintf( acCommand , sizeof(acCommand)-1 , "\"%s\" \"%%1\"" , 全_模块文件名 );
		lsret = RegSetValue( regkey_EditUltra_command , NULL , REG_SZ , acCommand , (DWORD)strlen(acCommand)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"*\\shell\\ZHIYIN:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra_command );
			return -1;
		}

		RegCloseKey( regkey_EditUltra_command );

		MessageBox(NULL, TEXT("在资源管理器注册右键弹出菜单成功"), TEXT("操作结果"), MB_ICONINFORMATION | MB_OK);

		设置菜单项已选择( 全_主窗口句柄, IDM_ENV_FILE_POPUPMENU, TRUE);

		全_已选择环境文件弹出菜单 = TRUE ;
	}
	else
	{
		/* 注销兼容老版名字EditUltra */
		RegDeleteKey( HKEY_CLASSES_ROOT , "*\\shell\\EditUltra\\command" ) ;
		RegDeleteKey( HKEY_CLASSES_ROOT , "*\\shell\\EditUltra" ) ;

		/* 注销右键文件菜单项 */
		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "*\\shell\\ZHIYIN\\command" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"*\\shell\\ZHIYIN\\command\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"*\\shell\\ZHIYIN\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "*\\shell\\ZHIYIN" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"*\\shell\\ZHIYIN\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"*\\shell\\ZHIYIN\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		if( lsret == ERROR_SUCCESS )
			MessageBox(NULL, TEXT("在资源管理器卸载右键弹出菜单成功"), TEXT("操作结果"), MB_ICONINFORMATION | MB_OK);

		设置菜单项已选择( 全_主窗口句柄, IDM_ENV_FILE_POPUPMENU, FALSE);

		全_已选择环境文件弹出菜单 = FALSE ;
	}

	return 0;
}

#define 使用_知音编辑器_打开_目录中所有文件	"用 ZHIYIN 打开目录中所有文件"
#define 使用_知音编辑器_打开_目录		"用 ZHIYIN 打开目录"
#define 使用_知音编辑器_定位_目录		"用 ZHIYIN 定位目录"

int OnEnvDirectoryPopupMenu()
{
	HKEY		注册表键_ZhiYin ;
	HKEY		regkey_EditUltra_command ;
	long		lsret ;

	if( 全_已选择环境目录弹出菜单 == FALSE )
	{
		char		acCommand[ MAX_PATH ] ;

		/* Directory注册EUX */
		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Directory\\shell\\ZHIYIN" , & 注册表键_ZhiYin ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表创建项\"Directory\\shell\\ZHIYIN\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表创建项\"Directory\\shell\\ZHIYIN\""), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
		}

		lsret = RegSetValue( 注册表键_ZhiYin , NULL , REG_SZ , 使用_知音编辑器_打开_目录中所有文件 , (DWORD)sizeof(使用_知音编辑器_打开_目录中所有文件) ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\ZHIYIN:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		lsret = RegSetKeyValue( 注册表键_ZhiYin , NULL , "Icon" , REG_SZ , 全_模块文件名 , (DWORD)strlen(全_模块文件名)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\ZHIYIN:Icon\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		RegCloseKey( 注册表键_ZhiYin );

		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Directory\\shell\\ZHIYIN\\command" , & regkey_EditUltra_command ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表创建项\"Directory\\shell\\ZHIYIN\\command\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		memset( acCommand , 0x00 , sizeof(acCommand) );
		snprintf( acCommand , sizeof(acCommand)-1 , "\"%s\" \"%%1\\*\"" , 全_模块文件名 );
		lsret = RegSetValue( regkey_EditUltra_command , NULL , REG_SZ , acCommand , (DWORD)strlen(acCommand)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\ZHIYIN:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra_command );
			return -1;
		}

		RegCloseKey( regkey_EditUltra_command );

		/* Directory注册EUX2 */
		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EUX2" , & 注册表键_ZhiYin ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表创建项\"Directory\\shell\\EUX2\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表创建项\"Directory\\shell\\EUX2\""), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
		}

		lsret = RegSetValue( 注册表键_ZhiYin , NULL , REG_SZ , 使用_知音编辑器_定位_目录 , (DWORD)sizeof(使用_知音编辑器_定位_目录) ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\EUX2:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		lsret = RegSetKeyValue( 注册表键_ZhiYin , NULL , "Icon" , REG_SZ , 全_模块文件名 , (DWORD)strlen(全_模块文件名)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\EUX2:Icon\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		RegCloseKey( 注册表键_ZhiYin );

		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EUX2\\command" , & regkey_EditUltra_command ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表创建项\"Directory\\shell\\EUX2\\command\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		memset( acCommand , 0x00 , sizeof(acCommand) );
		snprintf( acCommand , sizeof(acCommand)-1 , "\"%s\" \"%%1\\\"" , 全_模块文件名 );
		lsret = RegSetValue( regkey_EditUltra_command , NULL , REG_SZ , acCommand , (DWORD)strlen(acCommand)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\EUX2:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra_command );
			return -1;
		}

		RegCloseKey( regkey_EditUltra_command );

		/* Directory注册EUX3 */
		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EUX3" , & 注册表键_ZhiYin ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表创建项\"Directory\\shell\\EUX3\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表创建项\"Directory\\shell\\EUX3\""), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
		}

		lsret = RegSetValue( 注册表键_ZhiYin , NULL , REG_SZ , 使用_知音编辑器_打开_目录 , (DWORD)sizeof(使用_知音编辑器_打开_目录) ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\EUX3:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		lsret = RegSetKeyValue( 注册表键_ZhiYin , NULL , "Icon" , REG_SZ , 全_模块文件名 , (DWORD)strlen(全_模块文件名)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\EUX3:Icon\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		RegCloseKey( 注册表键_ZhiYin );

		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EUX3\\command" , & regkey_EditUltra_command ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表创建项\"Directory\\shell\\EUX3\\command\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		memset( acCommand , 0x00 , sizeof(acCommand) );
		snprintf( acCommand , sizeof(acCommand)-1 , "\"%s\" \"%%1\\\\\"" , 全_模块文件名 );
		lsret = RegSetValue( regkey_EditUltra_command , NULL , REG_SZ , acCommand , (DWORD)strlen(acCommand)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Directory\\shell\\EUX3:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra_command );
			return -1;
		}

		RegCloseKey( regkey_EditUltra_command );

		/* Drive注册EUX */
		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Drive\\shell\\ZHIYIN" , & 注册表键_ZhiYin ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表创建项\"Drive\\shell\\ZHIYIN\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表创建项\"Drive\\shell\\ZHIYIN\""), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
		}

		lsret = RegSetValue( 注册表键_ZhiYin , NULL , REG_SZ , 使用_知音编辑器_打开_目录中所有文件 , (DWORD)sizeof(使用_知音编辑器_打开_目录中所有文件) ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Drive\\shell\\ZHIYIN:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		lsret = RegSetKeyValue( 注册表键_ZhiYin , NULL , "Icon" , REG_SZ , 全_模块文件名 , (DWORD)strlen(全_模块文件名)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Drive\\shell\\ZHIYIN:Icon\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		RegCloseKey( 注册表键_ZhiYin );

		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Drive\\shell\\ZHIYIN\\command" , & regkey_EditUltra_command ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表创建项\"Drive\\shell\\ZHIYIN\\command\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		memset( acCommand , 0x00 , sizeof(acCommand) );
		snprintf( acCommand , sizeof(acCommand)-1 , "\"%s\" \"%%1\\*\"" , 全_模块文件名 );
		lsret = RegSetValue( regkey_EditUltra_command , NULL , REG_SZ , acCommand , (DWORD)strlen(acCommand)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Drive\\shell\\ZHIYIN:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra_command );
			return -1;
		}

		RegCloseKey( regkey_EditUltra_command );

		/* Drive注册EUX2 */
		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Drive\\shell\\EUX2" , & 注册表键_ZhiYin ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表创建项\"Drive\\shell\\EUX2\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表创建项\"Drive\\shell\\EUX2\""), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
		}

		lsret = RegSetValue( 注册表键_ZhiYin , NULL , REG_SZ , 使用_知音编辑器_定位_目录 , (DWORD)sizeof(使用_知音编辑器_打开_目录中所有文件) ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Drive\\shell\\EUX2:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		lsret = RegSetKeyValue( 注册表键_ZhiYin , NULL , "Icon" , REG_SZ , 全_模块文件名 , (DWORD)strlen(全_模块文件名)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Drive\\shell\\EUX2:Icon\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( 注册表键_ZhiYin );
			return -1;
		}

		RegCloseKey( 注册表键_ZhiYin );

		lsret = RegCreateKey( HKEY_CLASSES_ROOT , "Drive\\shell\\EUX2\\command" , & regkey_EditUltra_command ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表创建项\"Drive\\shell\\EUX2\\command\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		memset( acCommand , 0x00 , sizeof(acCommand) );
		snprintf( acCommand , sizeof(acCommand)-1 , "\"%s\" \"%%1\\\"" , 全_模块文件名 );
		lsret = RegSetValue( regkey_EditUltra_command , NULL , REG_SZ , acCommand , (DWORD)strlen(acCommand)+1 ) ;
		if( lsret != ERROR_SUCCESS )
		{
			MessageBox(NULL, TEXT("不能在注册表设置键\"Drive\\shell\\EUX2:(default)\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			RegCloseKey( regkey_EditUltra_command );
			return -1;
		}

		RegCloseKey( regkey_EditUltra_command );

		/* 完成 */
		MessageBox(NULL, TEXT("在资源管理器注册右键弹出菜单成功"), TEXT("操作结果"), MB_ICONINFORMATION | MB_OK);

		设置菜单项已选择( 全_主窗口句柄, IDM_ENV_DIRECTORY_POPUPMENU, TRUE);

		全_已选择环境目录弹出菜单 = TRUE ;
	}
	else
	{
		/* 注销兼容老版名字EditUltra */
		RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EditUltra\\command" ) ;
		RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EditUltra" ) ;
		RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EditUltra2\\command" ) ;
		RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EditUltra2" ) ;
		RegDeleteKey( HKEY_CLASSES_ROOT , "Drive\\shell\\EditUltra\\command" ) ;
		RegDeleteKey( HKEY_CLASSES_ROOT , "Drive\\shell\\EditUltra" ) ;
		RegDeleteKey( HKEY_CLASSES_ROOT , "Drive\\shell\\EditUltra2\\command" ) ;
		RegDeleteKey( HKEY_CLASSES_ROOT , "Drive\\shell\\EditUltra2" ) ;

		/* Directory注销EUX */
		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\ZHIYIN\\command" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"Directory\\shell\\ZHIYIN\\command\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"Directory\\shell\\ZHIYIN\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\ZHIYIN" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"Directory\\shell\\ZHIYIN\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"Directory\\shell\\ZHIYIN\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		/* Directory注销EUX2 */
		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EUX2\\command" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"Directory\\shell\\EUX2\\command\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"Directory\\shell\\EUX2\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EUX2" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"Directory\\shell\\EUX2\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"Directory\\shell\\EUX2\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		/* Directory注销EUX3 */
		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EUX3\\command" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"Directory\\shell\\EUX3\\command\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"Directory\\shell\\EUX3\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Directory\\shell\\EUX3" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"Directory\\shell\\EUX3\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"Directory\\shell\\EUX3\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		/* Drive注销EUX */
		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Drive\\shell\\ZHIYIN\\command" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"Drive\\shell\\ZHIYIN\\command\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"Drive\\shell\\ZHIYIN\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Drive\\shell\\ZHIYIN" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"Drive\\shell\\ZHIYIN\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"Drive\\shell\\ZHIYIN\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		/* Drive注销EUX2 */
		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Drive\\shell\\EUX2\\command" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"Drive\\shell\\EUX2\\command\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"Drive\\shell\\EUX2\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		lsret = RegDeleteKey( HKEY_CLASSES_ROOT , "Drive\\shell\\EUX2" ) ;
		if( lsret != ERROR_SUCCESS )
		{
			if( lsret == 5 )
			{
				MessageBox(NULL, TEXT("没有权限在注册表删除项\"Drive\\shell\\EUX2\"，请\"用管理员身份运行\"再尝试作此设置"), TEXT("错误"), MB_ICONERROR | MB_OK);
				return -1;
			}
			else
			{
				MessageBox(NULL, TEXT("不能在注册表删除项\"Drive\\shell\\EUX2\""), TEXT("错误"), MB_ICONERROR | MB_OK);
			}
		}

		/* 完成 */
		if( lsret == ERROR_SUCCESS )
			MessageBox(NULL, TEXT("在资源管理器卸载右键弹出菜单成功"), TEXT("操作结果"), MB_ICONINFORMATION | MB_OK);

		设置菜单项已选择( 全_主窗口句柄, IDM_ENV_DIRECTORY_POPUPMENU, FALSE);

		全_已选择环境目录弹出菜单 = FALSE ;
	}

	return 0;
}

int OnEnvSetProcessFileCommand()
{
	char	进程文件命令[ sizeof(全_风格主配置.进程文件命令) ] ;
	int	nret = 0 ;

	memset( 进程文件命令 , 0x00 , sizeof(进程文件命令) );
	strcpy( 进程文件命令 , 全_风格主配置.进程文件命令 );
	nret = InputBox( 全_主窗口句柄 , "请输入处理文件命令行\n（可以用\"%F\"占位文件名）：" , "输入窗口" , 0 , 进程文件命令 , sizeof(进程文件命令)-1 ) ;
	if( nret == IDOK )
	{
		if( 进程文件命令[0] == '\0' )
		{
			return 0;
		}
	}
	else if( nret == IDCANCEL )
	{
		return 0;
	}
	else
	{
		return 0;
	}

	strcpy( 全_风格主配置.进程文件命令 , 进程文件命令 );

	保存主配置文件();

	return 0;
}

int OnEnvExecuteProcessFileCommand( struct 选项卡页面S *选项卡页节点 )
{
	char	*p = NULL ;
	char	cmd[ sizeof(全_风格主配置.进程文件命令) * 2 ] ;
	BOOL	bret ;

	if( 选项卡页节点 == NULL )
		return 0;
	if( 全_风格主配置.进程文件命令[0] == '\0' )
		return 0;

	memset( cmd , 0x00 , sizeof(cmd) );
	p = strstr( 全_风格主配置.进程文件命令 , "%F" ) ;
	if( p )
	{
		snprintf( cmd , sizeof(cmd)-1 , "%.*s%s%s" , (int)(p-全_风格主配置.进程文件命令),全_风格主配置.进程文件命令 , 选项卡页节点->acPathFilename , p+2 );
	}
	else
	{
		strcpy( cmd , 全_风格主配置.进程文件命令 );
	}

	STARTUPINFO si ;
	PROCESS_INFORMATION pi ;
	memset( & si , 0x00 , sizeof(STARTUPINFO) );
	si.cb = sizeof(STARTUPINFO) ;
	memset( & pi , 0x00 , sizeof(PROCESS_INFORMATION) );
	bret = CreateProcess( NULL , cmd , NULL , NULL , 0 , 0 , NULL , NULL , & si , & pi ) ;
	if( bret == TRUE )
		CloseHandle( pi.hProcess );

	return 0;
}

int OnEnvSetProcessTextCommand()
{
	char	进程文本命令[ sizeof(全_风格主配置.进程文本命令) ] ;
	int	nret = 0 ;

	memset( 进程文本命令 , 0x00 , sizeof(进程文本命令) );
	strcpy( 进程文本命令 , 全_风格主配置.进程文本命令 );
	nret = InputBox( 全_主窗口句柄 , "请输入处理文件命令行\n（可以用\"%T\"占位文本）：" , "输入窗口" , 0 , 进程文本命令 , sizeof(进程文本命令)-1 ) ;
	if( nret == IDOK )
	{
		if( 进程文本命令[0] == '\0' )
		{
			return 0;
		}
	}
	else if( nret == IDCANCEL )
	{
		return 0;
	}
	else
	{
		return 0;
	}

	strcpy( 全_风格主配置.进程文本命令 , 进程文本命令 );

	保存主配置文件();

	return 0;
}

int OnEnvExecuteProcessTextCommand( struct 选项卡页面S *选项卡页节点 )
{
	int	nSelStartPos ;
	int	nSelEndPos ;
	int	nSelTextLength ;
	char	*acSelText = NULL ;

	char	*p = NULL ;
	char	cmd[ sizeof(全_风格主配置.进程文件命令) * 2 ] ;

	BOOL	bret ;

	if( 选项卡页节点 == NULL )
		return 0;
	if( 全_风格主配置.进程文本命令[0] == '\0' )
		return 0;

	nSelStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 );
	nSelEndPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETSELECTIONEND , 0 , 0 );
	nSelTextLength = nSelEndPos - nSelStartPos ;
	acSelText = (char*)malloc( nSelTextLength+1 ) ;
	if( acSelText == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存以存放SQL"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acSelText , 0x00 , nSelTextLength+1 );
	GetTextByRange( 选项卡页节点 , nSelStartPos , nSelEndPos , acSelText );

	memset( cmd , 0x00 , sizeof(cmd) );
	p = strstr( 全_风格主配置.进程文本命令 , "%T" ) ;
	if( p )
	{
		snprintf( cmd , sizeof(cmd)-1 , "%.*s%s%s" , (int)(p-全_风格主配置.进程文本命令),全_风格主配置.进程文本命令 , acSelText , p+2 );
	}
	else
	{
		strcpy( cmd , 全_风格主配置.进程文本命令 );
	}

	free( acSelText );

	STARTUPINFO si ;
	PROCESS_INFORMATION pi ;
	memset( & si , 0x00 , sizeof(STARTUPINFO) );
	si.cb = sizeof(STARTUPINFO) ;
	memset( & pi , 0x00 , sizeof(PROCESS_INFORMATION) );
	bret = CreateProcess( NULL , cmd , NULL , NULL , 0 , 0 , NULL , NULL , & si , & pi ) ;
	if( bret == TRUE )
		CloseHandle( pi.hProcess );

	return 0;
}
