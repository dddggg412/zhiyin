#ifndef _WINDOW_
#define _WINDOW_

#include "framework.h"

int CreateScintillaControl( struct 选项卡页面S *选项卡页节点 );
void DestroyScintillaControl( struct 选项卡页面S *选项卡页节点 );

int InitTabPageControlsCommonStyle( struct 选项卡页面S *选项卡页节点 );
int InitTabPageControlsBeforeLoadFile( struct 选项卡页面S *选项卡页节点 );
int InitTabPageControlsAfterLoadFile( struct 选项卡页面S *选项卡页节点 );
int CleanTabPageControls( struct 选项卡页面S *选项卡页节点 );

void GetTextByRange( struct 选项卡页面S *选项卡页节点 , size_t start , size_t end , char *text );
void GetTextByLine( struct 选项卡页面S *选项卡页节点 , size_t nLineNo , char *acText , size_t nTextBufsize );

BOOL IsDocumentModified( struct 选项卡页面S *选项卡页节点 );

int QueryIndexFromTabPage( struct 选项卡页面S *选项卡页节点 );

int OnSavePointReached( struct 选项卡页面S *选项卡页节点 );
int OnSavePointLeft( struct 选项卡页面S *选项卡页节点 );
int OnMarginClick( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify );
int OnCharAdded( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify );
int OnUpdateUI( struct 选项卡页面S *选项卡页节点 );

int AutosetLineNumberMarginWidth( struct 选项卡页面S *选项卡页节点 );

#endif
