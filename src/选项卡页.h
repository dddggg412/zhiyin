#ifndef _TABPAGES_
#define _TABPAGES_

#include "framework.h"

extern HWND	全_选项卡页面句柄 ;
extern RECT	全_选项卡页矩形 ;
extern HMENU	全_h选项卡页弹出菜单 ;
extern HMENU	全_h编辑框弹出菜单 ;
extern HMENU	全_h符号列表弹出菜单 ;
extern HMENU	全_h符号树弹出菜单 ;
extern int	全_n选项卡高度 ;

extern HWND	全_h选项卡关闭按钮句柄 ;
extern struct 选项卡页面S	*全_h选项卡页关闭按钮节点 ;
/*
extern HBRUSH	g_brushCurrentTabCloseButton ;
*/
extern HBRUSH	全_选项卡关闭按钮画刷 ;

struct 选项卡页面S
{
	HWND				Scintilla句柄 ;
	SciFnDirect			pfuncScintilla ;
	sptr_t				pScintilla ;
	RECT				Scintilla矩形 ;
	BOOL				文本已被选择 ;

	HWND				符号列表句柄 ;
	RECT				符号列表矩形 ;

	pcre				*pcreSymbolRe ;

	HWND				符号树句柄 ;
	RECT				符号树矩形 ;

	HWND				hwndQueryResultEdit ;
	RECT				rectQueryResultEdit ;
	HWND				hwndQueryResultTable ;
	RECT				rectQueryResultListView ;

	struct DatabaseConnectionConfig		stDatabaseConnectionConfig ;
	struct DatabaseConnectionHandles	stDatabaseConnectionHandles ;
	BOOL					bIsDatabaseConnected ;

	struct Redis连接配置		stRedisConnectionConfig ;
	struct Redis连接句柄		stRedisConnectionHandles ;
	BOOL					bIsRedisConnected ;

	char				acPathName[ MAX_PATH ] ;
	char				acPathFilename[ MAX_PATH ] ;
	char				acFilename[ MAX_PATH ] ;
	size_t				nFilenameLen ;
	char				p扩展名[ _MAX_EXT ] ;
	time_t				st_mtime ;

	struct 文档类型配置S		*pst文档类型配置 ;

	struct EncodingProbe		stEncodingProbe ;
	UINT				nCodePage ;
	char				acPreFileContext[ 4 + 1 ] ;
	size_t				nPreFileContextLen ;
	BOOL				bNeedWrotePreFileContext ;

	struct NewLineProbe		stNewLineModeProbe ;
	char				acEndOfLine[ 2+1 ] ;

	struct RemoteFileServer		stRemoteFileServer ;

	size_t				nFileSize ;
	size_t				nWroteLen ;
	char				*acWriteFileBuffer ;

	BOOL				bHexEditMode ;
	int				nByteCountInLastLine ;

	BOOL				bNotPromptWhereAutoUpdate ;
};

extern struct 选项卡页面S			*全_当前选项卡页面节点 ;

int 创建选项卡页( HWND hWnd );

void 调整选项卡页面();
void 调整选项卡页面框( struct 选项卡页面S *选项卡页节点 );

struct 选项卡页面S *AddTabPage( struct RemoteFileServer *pstRemoteFileServer , char *pcPathFilename , char *pcFilename , char *pcExtname );
int RemoveTabPage( struct 选项卡页面S *选项卡页节点 );
void SetTabPageTitle( int nTabPageNo , char *title );
void SelectTabPage( struct 选项卡页面S *选项卡页节点 , int n选项卡页面索引 );
struct 选项卡页面S *GetTabPageByScintilla( void *Scintilla句柄 );
struct 选项卡页面S *以索引获取选项卡页面( int n选项卡页面索引 );
struct 选项卡页面S *以索引选择选项卡页面( int n选项卡页面索引 );
void SetCurrentTabPageHighLight( int nCurrentTabPageIndex );

int 选项卡选择改变时();
int OnSymbolListDbClick( struct 选项卡页面S *选项卡页节点 );

int 计算选项卡页面高度();

#define CONFIG_KEY_MATERIAL_TABPAGES		"TABPAG"

#endif
