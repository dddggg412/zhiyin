#ifndef _MENU_
#define _MENU_

#include "framework.h"

void 推送最近打开路径文件名( char *acPathFilename );
void 更新最近打开路径文件名();
void 设置当前窗口主题已检查( HWND hWnd );
void 设置视图选项卡宽度菜单文本( int nMenuId );
void 设置源代码自动完成输入字符后显示菜单文本( int nMenuId );
void 更新所有菜单( HWND hWnd , struct 选项卡页面S *选项卡页节点 );

#endif
