#include "framework.h"

int CreateScintillaControl( struct 选项卡页面S *选项卡页节点 )
{
	调整选项卡页面框( 选项卡页节点 );

	// 选项卡页节点->Scintilla句柄 = CreateWindow( "Scintilla" , "Scintilla" , WS_BORDER|WS_CHILD|WS_VSCROLL|WS_HSCROLL|WS_CLIPCHILDREN , 选项卡页节点->Scintilla矩形.left , 选项卡页节点->Scintilla矩形.top , 选项卡页节点->Scintilla矩形.right-选项卡页节点->Scintilla矩形.left , 选项卡页节点->Scintilla矩形.bottom-选项卡页节点->Scintilla矩形.top , g_hwndMainClient , 0 , 全_当前实例 , 0) ;
	选项卡页节点->Scintilla句柄 = CreateWindow( "Scintilla" , "Scintilla" , WS_CHILD|WS_VSCROLL|WS_HSCROLL|WS_CLIPCHILDREN , 选项卡页节点->Scintilla矩形.left , 选项卡页节点->Scintilla矩形.top , 选项卡页节点->Scintilla矩形.right-选项卡页节点->Scintilla矩形.left , 选项卡页节点->Scintilla矩形.bottom-选项卡页节点->Scintilla矩形.top , 全_主窗口句柄 , 0 , 全_当前实例 , 0) ;
	if( 选项卡页节点->Scintilla句柄 == NULL )
	{
		MessageBox(全_主窗口句柄, TEXT("不能创建Scintilla控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	ShowWindow( 选项卡页节点->Scintilla句柄 , SW_SHOW);
	UpdateWindow( 选项卡页节点->Scintilla句柄 );

	选项卡页节点->pfuncScintilla = (SciFnDirect)SendMessage( 选项卡页节点->Scintilla句柄, SCI_GETDIRECTFUNCTION, 0, 0);
	选项卡页节点->pScintilla = (sptr_t)SendMessage( 选项卡页节点->Scintilla句柄, SCI_GETDIRECTPOINTER, 0, 0);

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_USEPOPUP , 0 , 0 );

	全_缩放重置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETZOOM , 0 , 0 );

	return 0;
}

void DestroyScintillaControl( struct 选项卡页面S *选项卡页节点 )
{
	DestroyWindow( 选项卡页节点->Scintilla句柄 );

	return;
}

int InitTabPageControlsCommonStyle( struct 选项卡页面S *选项卡页节点 )
{
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLERESETDEFAULT , 0 , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFONT , STYLE_DEFAULT , (sptr_t)(GetFontNameUtf8FromGb(g_pstWindowTheme->stStyleTheme.text.font)) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETSIZE , STYLE_DEFAULT , g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_EDITOR_ADJUSTVALUE );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , STYLE_DEFAULT , g_pstWindowTheme->stStyleTheme.text.color );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , STYLE_DEFAULT , g_pstWindowTheme->stStyleTheme.text.bgcolor );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBOLD , STYLE_DEFAULT , g_pstWindowTheme->stStyleTheme.text.bold );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLECLEARALL , 0 , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINS , 3 , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINTYPEN , 边框_行号_索引 , SC_MARGIN_NUMBER );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINWIDTHN , 边框_行号_索引 , (全_风格主配置.启用行号==TRUE?边框_行号_宽度:0) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETFORE , STYLE_LINENUMBER , g_pstWindowTheme->stStyleTheme.linenumber.color );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_STYLESETBACK , STYLE_LINENUMBER , g_pstWindowTheme->stStyleTheme.linenumber.bgcolor );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINTYPEN, 边框_书签_索引 , SC_MARGIN_SYMBOL );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINMASKN, 边框_书签_索引 , 边框_书签_MASKN );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINWIDTHN, 边框_书签_索引 , (全_风格主配置.启用书签==TRUE?边框_书签_宽度:0) );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINSENSITIVEN, 边框_书签_索引 , TRUE );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETFOLDMARGINHICOLOUR , TRUE , g_pstWindowTheme->stStyleTheme.foldmargin.color );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETFOLDMARGINCOLOUR , TRUE , g_pstWindowTheme->stStyleTheme.foldmargin.bgcolor );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCARETLINEVISIBLE , TRUE , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCARETLINEVISIBLEALWAYS , 1 , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCARETLINEBACK , g_pstWindowTheme->stStyleTheme.caretline.bgcolor , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCARETSTYLE , 1 , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCARETFORE , g_pstWindowTheme->stStyleTheme.text.color , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSELFORE , TRUE , g_pstWindowTheme->stStyleTheme.text.bgcolor );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSELBACK , TRUE , g_pstWindowTheme->stStyleTheme.text.color );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETTABWIDTH , 全_风格主配置.选项卡宽度 , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETWRAPMODE , (全_风格主配置.换行模式==TRUE?2:0) , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETUSETABS , (全_风格主配置.按下Tab键转换为空格==TRUE?FALSE:TRUE) , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETHSCROLLBAR , 1 , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETVIEWWS , (全_风格主配置.启用空格符==TRUE?SCWS_VISIBLEALWAYS:SCWS_INVISIBLE) , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETWHITESPACESIZE , 全_风格主配置.空格符大小 , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETTABDRAWMODE , SCTD_LONGARROW , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETVIEWEOL , 全_风格主配置.新行可见 , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETINDENTATIONGUIDES , (全_风格主配置.启用缩进辅助线?SC_IV_LOOKBOTH:SC_IV_NONE) , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMULTIPLESELECTION , TRUE , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETADDITIONALSELECTIONTYPING , TRUE , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETVIRTUALSPACEOPTIONS , 1 , 0 );

	/* 使用了反而字体发虚，不知道为什么 */
	/*
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETTECHNOLOGY , SC_TECHNOLOGY_DIRECTWRITEDC , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETFONTQUALITY , SC_EFF_QUALITY_LCD_OPTIMIZED , 0 );
	*/

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMODEVENTMASK , SC_MOD_INSERTTEXT , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSCROLLWIDTHTRACKING , TRUE , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETENDATLASTLINE , FALSE , 0 );

	return 0;
}

int InitTabPageControlsBeforeLoadFile( struct 选项卡页面S *选项卡页节点 )
{
	InitTabPageControlsCommonStyle( 选项卡页节点 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CANCEL , 0 , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , EM_EMPTYUNDOBUFFER , 0 , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETUNDOCOLLECTION , 0 , 0 );

	if( 选项卡页节点->pst文档类型配置 && 选项卡页节点->pst文档类型配置->pfuncInitTabPageControlsBeforeLoadFile )
	{
		选项卡页节点->pst文档类型配置->pfuncInitTabPageControlsBeforeLoadFile( 选项卡页节点 ) ;
	}

	return 0;
}

int InitTabPageControlsAfterLoadFile( struct 选项卡页面S *选项卡页节点 )
{
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCODEPAGE , 选项卡页节点->nCodePage , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETUNDOCOLLECTION, 1, 0);
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSAVEPOINT, 0, 0);

	int n换行符模式 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETEOLMODE , 0 , 0 ) ;
	if( n换行符模式 == 0 )
		strcpy( 选项卡页节点->acEndOfLine , "\r\n" );
	else if( n换行符模式 == 1 )
		strcpy( 选项卡页节点->acEndOfLine , "\r" );
	else if( n换行符模式 == 2 )
		strcpy( 选项卡页节点->acEndOfLine , "\n" );
	else
		strcpy( 选项卡页节点->acEndOfLine , "\r\n" );

	if( 选项卡页节点->pst文档类型配置 && 选项卡页节点->pst文档类型配置->pfuncInitTabPageControlsAfterLoadFile )
	{
		选项卡页节点->pst文档类型配置->pfuncInitTabPageControlsAfterLoadFile( 选项卡页节点 ) ;
	}

	AutosetLineNumberMarginWidth( 选项卡页节点 );

	return 0;
}

int CleanTabPageControls( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点->pst文档类型配置 && 选项卡页节点->pst文档类型配置->pfuncCleanTabPageControls )
	{
		选项卡页节点->pst文档类型配置->pfuncCleanTabPageControls( 选项卡页节点 ) ;
	}

	return 0;
}

void GetTextByRange( struct 选项卡页面S *选项卡页节点 , size_t start , size_t end , char *text )
{
	TEXTRANGE tr;
	tr.chrg.cpMin = (LONG)start;
	tr.chrg.cpMax = (LONG)end;
	tr.lpstrText = text;
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, EM_GETTEXTRANGE, 0, (LPARAM)(&tr));
}

void GetTextByLine( struct 选项卡页面S *选项卡页节点 , size_t nLineNo , char *acText , size_t nTextBufsize )
{
	TEXTRANGE tr;
	tr.chrg.cpMin = (LONG)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_POSITIONFROMLINE , nLineNo , 0 );
	tr.chrg.cpMax = (LONG)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLINEENDPOSITION , nLineNo , 0 );
	if( (size_t)(tr.chrg.cpMax) - (size_t)(tr.chrg.cpMin) + 1 > nTextBufsize - 1 )
	{
		tr.chrg.cpMax = tr.chrg.cpMin + ( (LONG)nTextBufsize - 1 ) ;
	}
	tr.lpstrText = acText;
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, EM_GETTEXTRANGE, 0, (LPARAM)(&tr));
}

BOOL IsDocumentModified( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 && 选项卡页节点->pfuncScintilla )
		return 选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETMODIFY, 0, 0 );
	else
		return TRUE;
}

int QueryIndexFromTabPage( struct 选项卡页面S *选项卡页节点 )
{
	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0 ; n选项卡页面索引 < n选项卡页面数量 ; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		if( p == 选项卡页节点 )
			return n选项卡页面索引;
	}

	return -1;
}

int OnSavePointReached( struct 选项卡页面S *选项卡页节点 )
{
	int		n选项卡页面索引 ;

	if( 选项卡页节点 == NULL )
		return -1;

	n选项卡页面索引 = QueryIndexFromTabPage( 选项卡页节点 ) ;
	if( n选项卡页面索引 < 0 )
		return -2;

	if( 选项卡页节点->acFilename[选项卡页节点->nFilenameLen] == '*' )
	{
		选项卡页节点->acFilename[选项卡页节点->nFilenameLen] = '\0' ;
	}
	SetTabPageTitle( n选项卡页面索引 , 选项卡页节点->acFilename );
	SetWindowTitle( 选项卡页节点->acPathFilename );
	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	return 0;
}

int OnSavePointLeft( struct 选项卡页面S *选项卡页节点 )
{
	int		n选项卡页面索引 ;

	if( 选项卡页节点 == NULL )
		return -1;

	n选项卡页面索引 = QueryIndexFromTabPage( 选项卡页节点 ) ;
	if( n选项卡页面索引 < 0 )
		return -2;

	if( 选项卡页节点->acFilename[选项卡页节点->nFilenameLen] != '*' )
	{
		选项卡页节点->acFilename[选项卡页节点->nFilenameLen] = '*' ;
		选项卡页节点->acFilename[选项卡页节点->nFilenameLen+1] = '\0' ;
	}
	SetTabPageTitle( n选项卡页面索引 , 选项卡页节点->acFilename );
	SetWindowTitle( 选项卡页节点->acPathFilename );
	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	return 0;
}

int OnMarginClick( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	int nLineNumber = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_LINEFROMPOSITION , lpnotify->position , 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_TOGGLEFOLD , nLineNumber , 0 );

	return 0;
}

int OnCharAdded( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify )
{
	if( 选项卡页节点->bHexEditMode == FALSE )
	{
		if( 选项卡页节点->pst文档类型配置 && 选项卡页节点->pst文档类型配置->pfuncOnCharAdded )
			return 选项卡页节点->pst文档类型配置->pfuncOnCharAdded( 选项卡页节点 , lpnotify );
	}
	else
	{
		SyncDataOnHexEditMode( 选项卡页节点 );
	}

	return 0;
}

int AutosetLineNumberMarginWidth( struct 选项卡页面S *选项卡页节点 )
{
	int nOldMarginWidth = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETMARGINWIDTHN, 边框_行号_索引, 0 ) ;

	int nTotalLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETLINECOUNT, 0, 0 ) ;
	char acMarginWidth[20+1] ;
	memset( acMarginWidth , 0x00 , sizeof(acMarginWidth) );
	snprintf( acMarginWidth , sizeof(acMarginWidth)-1 , "_%d" , nTotalLine );
	int nMarginWidth = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_TEXTWIDTH, STYLE_LINENUMBER, (sptr_t)acMarginWidth ) ;
	if( nMarginWidth != nOldMarginWidth )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETMARGINWIDTHN , 边框_行号_索引 , (全_风格主配置.启用行号==TRUE?nMarginWidth+10:0) );
		return 0;
	}
	else
	{
		return 1;
	}
}
