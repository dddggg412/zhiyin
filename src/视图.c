#include "framework.h"

int OnViewFileTree( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_显示文件树栏 == FALSE )
	{
		全_显示文件树栏 = TRUE ;
		全_风格主配置.显示文件树栏 = TRUE ;

		全_文件树栏矩形.right = 全_风格主配置.文件树栏宽度 ;
		全_选项卡页矩形.left = 全_风格主配置.文件树栏宽度 + 分割_宽度 ;
	}
	else
	{
		全_显示文件树栏 = FALSE ;
		全_风格主配置.显示文件树栏 = FALSE ;

		全_文件树栏矩形.right = 0 ;
		全_选项卡页矩形.left = 0 ;
	}

	更新所有窗口( 全_主窗口句柄 );

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	return 0;
}

int OnViewSwitchFileType( int nFileTypeIndex )
{
	if( 全_当前选项卡页面节点 == NULL )
		return 0;

	CleanTabPageControls( 全_当前选项卡页面节点 );

	全_当前选项卡页面节点->pst文档类型配置 = 全_文档类型配置 + nFileTypeIndex ;

	InitTabPageControlsBeforeLoadFile( 全_当前选项卡页面节点 );

	InitTabPageControlsAfterLoadFile( 全_当前选项卡页面节点 );

	// 重新计算各窗口大小
	计算选项卡页面高度();
	更新所有窗口( 全_主窗口句柄 );

	return 0;
}

int OnViewSwitchStyleTheme( int nWindowThemeIndex )
{
	g_pstWindowTheme = & (g_astWindowTheme[nWindowThemeIndex]) ;

	if( nWindowThemeIndex == 0 )
		全_风格主配置.窗口的主题[0] = '\0' ;
	else
		strncpy( 全_风格主配置.窗口的主题 , g_pstWindowTheme->acThemeName , sizeof(全_风格主配置.窗口的主题)-1 );

	全_笔刷常用控件深色 = CreateSolidBrush( g_pstWindowTheme->stStyleTheme.text.bgcolor ) ;

	保存主配置文件();

	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);

		InitTabPageControlsCommonStyle( p );

		InitTabPageControlsAfterLoadFile( p );

		if( p->符号列表句柄 )
		{
			HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font);
			SendMessage( p->符号列表句柄 , WM_SETFONT , (WPARAM)hFont, 0);
			InvalidateRect( p->符号列表句柄 , NULL , TRUE );
		}

		if( p->符号树句柄 )
		{
			HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font);
			SendMessage( p->符号树句柄 , WM_SETFONT , (WPARAM)hFont, 0);
			InvalidateRect( p->符号树句柄 , NULL , TRUE );
		}

		if( p->hwndQueryResultEdit )
		{
			HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font);
			SendMessage( p->hwndQueryResultEdit , WM_SETFONT , (WPARAM)hFont, 0);
			InvalidateRect( p->hwndQueryResultEdit , NULL , TRUE );
		}

		if( p->hwndQueryResultTable )
		{
			HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font);
			SendMessage( p->hwndQueryResultTable , WM_SETFONT , (WPARAM)hFont, 0);
			InvalidateRect( p->hwndQueryResultTable , NULL , TRUE );
		}
	}

	更新所有窗口( 全_主窗口句柄 );

	更新所有菜单( 全_主窗口句柄 , 全_当前选项卡页面节点 );

	return 0;
}

int OnViewModifyThemeStyle()
{
	int		nret = 0 ;

	nret = (int)DialogBox(全_当前实例, MAKEINTRESOURCE(IDD_STYLETHEME_DIALOG), 全_主窗口句柄, StyleThemeWndProc) ;
	if( nret == IDOK )
	{
		int		n选项卡页面数量 ;
		int		n选项卡页面索引 ;
		TCITEM		tci ;
		struct 选项卡页面S	*p = NULL ;

		n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
		for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
		{
			memset( & tci , 0x00 , sizeof(TCITEM) );
			tci.mask = TCIF_PARAM ;
			TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
			p = (struct 选项卡页面S *)(tci.lParam);

			InitTabPageControlsCommonStyle( p );

			InitTabPageControlsAfterLoadFile( p );

			if( p->符号列表句柄 )
			{
				HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font);
				SendMessage( p->符号列表句柄 , WM_SETFONT , (WPARAM)hFont, 0);
				InvalidateRect( p->符号列表句柄 , NULL , TRUE );
			}

			if( p->符号树句柄 )
			{
				HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font);
				SendMessage( p->符号树句柄 , WM_SETFONT , (WPARAM)hFont, 0);
				InvalidateRect( p->符号树句柄 , NULL , TRUE );
			}

			if( p->hwndQueryResultEdit )
			{
				HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font);
				SendMessage( p->hwndQueryResultEdit , WM_SETFONT , (WPARAM)hFont, 0);
				InvalidateRect( p->hwndQueryResultEdit , NULL , TRUE );
			}

			if( p->hwndQueryResultTable )
			{
				HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), FALSE, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font);
				SendMessage( p->hwndQueryResultTable , WM_SETFONT , (WPARAM)hFont, 0);
				InvalidateRect( p->hwndQueryResultTable , NULL , TRUE );
			}
		}

		保存样式主题配置文件();
	}

	return 0;
}

int OnViewCopyNewThemeStyle()
{
	char		acBoxCaption[ MAX_PATH*2 ] ;
	char		acStyleTheme[ MAX_PATH ] ;

	int		nret = 0 ;

	if( g_nWindowThemeCount >= 视觉样式最大数量M )
	{
		错误框( "主题方案太多了" );
		return -1;
	}

	memset( acBoxCaption , 0x00 , sizeof(acBoxCaption) );
	snprintf( acBoxCaption , sizeof(acBoxCaption)-1 , "从主题方案\"%s\"复制创建到新主题方案：" , (g_pstWindowTheme==g_astWindowTheme?"":g_pstWindowTheme->acThemeName) );
	memset( acStyleTheme , 0x00 , sizeof(acStyleTheme) );
	strncpy( acStyleTheme , (g_pstWindowTheme==g_astWindowTheme?"":g_pstWindowTheme->acThemeName) , sizeof(acStyleTheme)-1 );
_GOTO_RETRY :
	nret = InputBox( 全_主窗口句柄 , acBoxCaption , "输入窗口" , 0 , acStyleTheme , sizeof(acStyleTheme)-1 ) ;
	if( nret == IDOK )
	{
		if( acStyleTheme[0] == '\0' )
		{
			错误框( "主题方案名不能为空" );
			goto _GOTO_RETRY;
		}
		else if( g_pstWindowTheme != g_astWindowTheme && strcmp( acStyleTheme , g_pstWindowTheme->acThemeName ) == 0 )
		{
			错误框( "主题方案名不能和复制源相同" );
			goto _GOTO_RETRY;
		}
		else if( strchr(acStyleTheme,' ') || strchr(acStyleTheme,'\t') || strchr(acStyleTheme,'.') )
		{
			错误框( "主题方案名不能含有' '或'\\t'或'.'" );
			goto _GOTO_RETRY;
		}
		else
		{
			nret = CopyNewThemeStyle( acStyleTheme ) ;
			if( nret )
				goto _GOTO_RETRY;
		}
	}

	return 0;
}

#define HEXEDIT_MODE_FIRSTLINE		"          | 0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F  | 0123456789ABCDEF\r\n"
#define HEXEDIT_MODE_SECONDLINE		"----------+-------------------------------------------------+-----------------\r\n"

int OnViewHexEditMode( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点->bHexEditMode == FALSE )
	{
		BOOL bIsDocumentModified = IsDocumentModified(选项卡页节点) ;

		int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		int nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
		int nOffsetInCurrentLine = 当前文本字节位置 - (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
		int nTextTotalLength = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
		if( nTextTotalLength == 0 )
			return 0;
		int nTextTotalLineCount = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
		char *pcText = (char*)malloc( nTextTotalLength + 1 ) ;
		if( pcText == NULL )
			return -1;
		memset( pcText , 0x00 , nTextTotalLength + 1 );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXT , (sptr_t)(nTextTotalLength+1) , (sptr_t)pcText ) ;

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_CLEARALL, 0, 0 );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_APPENDTEXT, sizeof(HEXEDIT_MODE_FIRSTLINE)-1, (sptr_t)HEXEDIT_MODE_FIRSTLINE );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_APPENDTEXT, sizeof(HEXEDIT_MODE_SECONDLINE)-1, (sptr_t)HEXEDIT_MODE_SECONDLINE );

		int nHexLineCount = (nTextTotalLength+15) / 16 ;
		选项卡页节点->nByteCountInLastLine = nTextTotalLength % 16 ;
		if( 选项卡页节点->nByteCountInLastLine == 0 )
			选项卡页节点->nByteCountInLastLine = 16 ;
		char *acHexText = (char*)malloc( nHexLineCount*80 + 1 ) ;
		if( acHexText == NULL )
			return -1;
		memset( acHexText , 0x00 , nHexLineCount*80 + 1 );
		char *pcHexText = acHexText ;
		size_t nHexTextLength = 0 ;
		size_t len ;

		int nHexLineBase = 0 ;
		char acHexLineOffsetBuf[ 8 + 1 + 1 ] = "" ;
		char acHexByte[ 3+1 + 1 + 1 ] = "" ;
		int x , y ;
		for( y = 0 ; y < nHexLineCount ; y++ )
		{
			len = snprintf( acHexLineOffsetBuf , sizeof(acHexLineOffsetBuf)-1 , "%08X" , nHexLineBase );
			memcpy( pcHexText , acHexLineOffsetBuf , len ); pcHexText += len ; nHexTextLength += len ;
			len = 4 ; memcpy( pcHexText , "H | " , len ); pcHexText += len ; nHexTextLength += len ;

			for( x = 0 ; x < 16 ; x++ )
			{
				if( y+1 == nHexLineCount && x >= 选项卡页节点->nByteCountInLastLine )
					break;

				len = snprintf( acHexByte , sizeof(acHexByte)-1 , "%02X " , (unsigned char)pcText[nHexLineBase+x] ) ;
				memcpy( pcHexText , acHexByte , len ); pcHexText += len ; nHexTextLength += len ;
			}
			for( ; x < 16 ; x++ )
			{
				len = 3 ; memcpy( pcHexText , "   " , len ); pcHexText += len ; nHexTextLength += len ;
			}

			pcHexText[0] = '|' ; pcHexText += 1 ; nHexTextLength += 1 ;
			pcHexText[0] = ' ' ; pcHexText += 1 ; nHexTextLength += 1 ;

			for( x = 0 ; x < 16 ; x++ )
			{
				if( y+1 == nHexLineCount && x >= 选项卡页节点->nByteCountInLastLine )
					break;

				if( pcText[nHexLineBase+x] && 32 <= pcText[nHexLineBase+x] && pcText[nHexLineBase+x] <= 126 )
				{
					pcHexText[0] = pcText[nHexLineBase+x] ; pcHexText += 1 ; nHexTextLength += 1 ;
				}
				else
				{
					pcHexText[0] = '.' ; pcHexText += 1 ; nHexTextLength += 1 ;
				}
			}
			for( ; x < 16 ; x++ )
			{
				pcHexText[0] = ' ' ; pcHexText += 1 ; nHexTextLength += 1 ;
			}

			if( y+1 < nHexLineCount )
			{
				pcHexText[0] = '\r' ; pcHexText += 1 ; nHexTextLength += 1 ;
				pcHexText[0] = '\n' ; pcHexText += 1 ; nHexTextLength += 1 ;
			}

			nHexLineBase += 16 ;
		}

		free( pcText );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_APPENDTEXT, nHexTextLength, (sptr_t)acHexText );

		int nAdujstPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine+2, 0 ) + 12 + nOffsetInCurrentLine*3 ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GOTOPOS, nAdujstPos, 0 );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_SETOVERTYPE, TRUE, 0 );

		if( bIsDocumentModified == TRUE )
		{
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSAVEPOINT, 0, 0);

			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_INSERTTEXT, 0, (sptr_t)"X" );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_DELETERANGE, 0, 1 );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

			OnSavePointLeft( 选项卡页节点 );
		}
		else
		{
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSAVEPOINT, 0, 0);

			OnSavePointReached( 选项卡页节点 );
		}

		选项卡页节点->bHexEditMode = TRUE ;
		更新所有菜单( 全_主窗口句柄 , 全_当前选项卡页面节点 );
	}
	else
	{
		BOOL bIsDocumentModified = IsDocumentModified(选项卡页节点) ;

		size_t nTextLength = 0 ;
		unsigned char *pcText = StrdupHexEditTextFold( 选项卡页节点 , & nTextLength ) ;
		if( pcText == NULL )
			return -1;

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_CLEARALL, 0, 0 );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_APPENDTEXT, nTextLength, (sptr_t)pcText );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_SETOVERTYPE, FALSE, 0 );

		if( bIsDocumentModified == TRUE )
		{
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSAVEPOINT, 0, 0);

			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_INSERTTEXT, 0, (sptr_t)"X" );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_DELETERANGE, 0, 1 );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

			OnSavePointLeft( 选项卡页节点 );
		}
		else
		{
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSAVEPOINT, 0, 0);

			OnSavePointReached( 选项卡页节点 );
		}

		选项卡页节点->bHexEditMode = FALSE ;
		更新所有菜单( 全_主窗口句柄 , 全_当前选项卡页面节点 );
	}

	return 0;
}

static int AdujstPosition( struct 选项卡页面S *选项卡页节点 , int nAdujstPos )
{
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GOTOPOS, nAdujstPos, 0 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_CHOOSECARETX, 0, 0 );
	return 0;
}

int AdujstPositionOnHexEditMode( struct 选项卡页面S *选项卡页节点 , MSG *p_信息 )
{
	if( 选项卡页节点 == NULL || 选项卡页节点->bHexEditMode == FALSE )
		return 0;

	int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	int nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
	int nTotalLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETLINECOUNT, 0, 0 ) ;
	int nBeginPosInCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
	int nOffsetInCurrentLine = 当前文本字节位置 - nBeginPosInCurrentLine ;

	if( nCurrentLine <= 1 )
	{
		if( 11 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 59 )
		{
			int nAdujstPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, 2, 0 ) + 12 ;
			AdujstPosition( 选项卡页节点 , nAdujstPos );
		}
		else if( 61 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 78 )
		{
			int nAdujstPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, 2, 0 ) + 62 ;
			AdujstPosition( 选项卡页节点 , nAdujstPos );
		}
	}
	else if( nOffsetInCurrentLine <= 10 )
	{
		int nAdujstPos = (12-nOffsetInCurrentLine) + 当前文本字节位置 ;
		AdujstPosition( 选项卡页节点 , nAdujstPos );
	}
	else if( nOffsetInCurrentLine == 11 )
	{
		if( nCurrentLine <= 2 )
		{
			int nAdujstPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, 2, 0 ) + 12 ;
			AdujstPosition( 选项卡页节点 , nAdujstPos );
		}
		else
		{
			int nAdujstPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine-1, 0 ) + 58 ;
			AdujstPosition( 选项卡页节点 , nAdujstPos );
		}
	}
	else if( 12 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 56 )
	{
		if( nCurrentLine+1 == nTotalLine && nOffsetInCurrentLine >= 12 + 选项卡页节点->nByteCountInLastLine*3 - 1 )
		{
			int nAdujstPos = nBeginPosInCurrentLine + 12 + (选项卡页节点->nByteCountInLastLine-1)*3 + 1 ;
			AdujstPosition( 选项卡页节点 , nAdujstPos );
		}
		else if( (nOffsetInCurrentLine-12)%3 == 2 )
		{
			if( p_信息->wParam == VK_LEFT )
			{
				int nAdujstPos = 当前文本字节位置-1 ;
				AdujstPosition( 选项卡页节点 , nAdujstPos );
			}
			else
			{
				int nAdujstPos = 当前文本字节位置+1 ;
				AdujstPosition( 选项卡页节点 , nAdujstPos );
			}
		}
	}
	else if( nOffsetInCurrentLine == 59 )
	{
		if( nCurrentLine+1 == nTotalLine || p_信息->wParam != VK_RIGHT )
		{
			int nAdujstPos = 当前文本字节位置 - 1 ;
			AdujstPosition( 选项卡页节点 , nAdujstPos );
		}
		else
		{
			int nAdujstPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine+1, 0 ) + 12 ;
			AdujstPosition( 选项卡页节点 , nAdujstPos );
		}
	}
	else if( nOffsetInCurrentLine == 60 )
	{
		int nAdujstPos = (62-nOffsetInCurrentLine) + 当前文本字节位置 ;
		AdujstPosition( 选项卡页节点 , nAdujstPos );
	}
	else if( nOffsetInCurrentLine == 61 )
	{
		if( nCurrentLine == 2 )
		{
			int nAdujstPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, 2, 0 ) + 62 ;
			AdujstPosition( 选项卡页节点 , nAdujstPos );
		}
		else
		{
			int nAdujstPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine-1, 0 ) + 77 ;
			AdujstPosition( 选项卡页节点 , nAdujstPos );
		}
	}
	else if( nOffsetInCurrentLine >= 62 )
	{
		if( nCurrentLine+1 == nTotalLine && nOffsetInCurrentLine >= 62 + 选项卡页节点->nByteCountInLastLine - 1 )
		{
			int nAdujstPos = nBeginPosInCurrentLine + 62 + (选项卡页节点->nByteCountInLastLine-1) ;
			AdujstPosition( 选项卡页节点 , nAdujstPos );
		}
		else if( nOffsetInCurrentLine >= 78 )
		{
			if( nCurrentLine+1 == nTotalLine || p_信息->wParam != VK_RIGHT )
			{
				int nAdujstPos = 当前文本字节位置 - (nOffsetInCurrentLine-77) ;
				AdujstPosition( 选项卡页节点 , nAdujstPos );
			}
			else
			{
				int nAdujstPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine+1, 0 ) + 62 ;
				AdujstPosition( 选项卡页节点 , nAdujstPos );
			}
		}
	}

	return 0;
}

int AdujstKeyOnHexEditMode( struct 选项卡页面S *选项卡页节点 , MSG *p_信息 )
{
	if( p_信息->wParam == VK_BACK || p_信息->wParam == VK_RETURN || p_信息->wParam == VK_INSERT || p_信息->wParam == VK_DELETE )
	{
		return 1;
	}

	return 0;
}

int AdujstCharOnHexEditMode( struct 选项卡页面S *选项卡页节点 , MSG *p_信息 )
{
	if( 选项卡页节点 == NULL || 选项卡页节点->bHexEditMode == FALSE )
		return 0;

	int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	int nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
	int nBeginPosInCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
	int nOffsetInCurrentLine = 当前文本字节位置 - nBeginPosInCurrentLine ;

	if( 11 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 59 )
	{
		if( '0' <= p_信息->wParam && p_信息->wParam <= '9' )
		{
			;
		}
		else if( 'a' <= p_信息->wParam && p_信息->wParam <= 'z' )
		{
			p_信息->wParam = toupper((int)(p_信息->wParam)) ;
		}
		else if( 'A' <= p_信息->wParam && p_信息->wParam <= 'Z' )
		{
			;
		}
		else
		{
			return 1;
		}
	}
	else if( 61 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 78 )
	{
		if( 32 <= p_信息->wParam && p_信息->wParam <= 126 )
		{
			;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		return 1;
	}

	return 0;
}

int SyncDataOnHexEditMode( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 == NULL || 选项卡页节点->bHexEditMode == FALSE )
		return 0;

	int 当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	int nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
	int nBeginPosInCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
	int nOffsetInCurrentLine = 当前文本字节位置 - nBeginPosInCurrentLine ;
	int nOffset ;
	unsigned char nByte1 , nByte2 , nByte ;
	unsigned char buf[2+1] ;

	if( 12 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 59 )
	{
		nOffset = (nOffsetInCurrentLine-1-12) / 3 ;
		nByte1 = (unsigned char)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCHARAT, nBeginPosInCurrentLine+12+nOffset*3, 0 ) ;
		nByte2 = (unsigned char)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCHARAT, nBeginPosInCurrentLine+12+nOffset*3+1, 0 ) ;
		HexByteFold( nByte1 , nByte2 , nByte );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSEL , nBeginPosInCurrentLine+62+nOffset , nBeginPosInCurrentLine+62+nOffset+1 );
		if( 32 <= nByte && nByte <= 126 )
			buf[0] = nByte ;
		else
			buf[0] = '.' ;
		buf[1] = '\0' ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)buf );
		AdujstPosition( 选项卡页节点 , 当前文本字节位置 );
	}
	else if( 62 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 78 )
	{
		nOffset = (nOffsetInCurrentLine-1-62) ;
		nByte = (unsigned char)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCHARAT, nBeginPosInCurrentLine+62+nOffset, 0 ) ;
		HexByteExpand( nByte , buf );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSEL , nBeginPosInCurrentLine+12+nOffset*3 , nBeginPosInCurrentLine+12+nOffset*3+2 );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)buf );
		AdujstPosition( 选项卡页节点 , 当前文本字节位置 );
	}

	return 0;
}

unsigned char *StrdupHexEditTextFold( struct 选项卡页面S *选项卡页节点 , size_t *pnTextLength )
{
	int nHexTextLength = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
	unsigned char *pcHexText = (unsigned char*)malloc( nHexTextLength + 1 ) ;
	if( pcHexText == NULL )
	{
		错误框( "分配内存以存放十六进制数据失败" );
		return NULL;
	}
	memset( pcHexText , 0x00 , nHexTextLength + 1 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXT , (sptr_t)(nHexTextLength+1) , (sptr_t)pcHexText ) ;

	int nHexTextLineCount = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	size_t nTextLength = (nHexTextLineCount-3)*16 + 选项卡页节点->nByteCountInLastLine ;
	unsigned char *pcText = (unsigned char*)malloc( nTextLength + 1 ) ;
	if( pcText == NULL )
	{
		free( pcHexText );
		错误框( "分配内存以存放文本数据失败" );
		return NULL;
	}
	memset( pcText , 0x00 , nTextLength + 1 );

	unsigned char *pcHexLineBegin = pcHexText ;
	unsigned char *pcTextOffset = pcText ;
	int y , x ;
	unsigned char *tmp = NULL ;
	for( y = 0 ; y < nHexTextLineCount ; y++ )
	{
		if( y > 1 )
		{
			for( x = 0 ; x < 16 ; x++ )
			{
				if( y+1 == nHexTextLineCount && x >= 选项卡页节点->nByteCountInLastLine )
					break;

				HexByteFold( pcHexLineBegin[12+x*3] , pcHexLineBegin[12+x*3+1] , (*pcTextOffset) );
				pcTextOffset++;
			}
		}
		if( y+1 == nHexTextLineCount )
			break;

		tmp = (unsigned char *)strchr( (char *)pcHexLineBegin , '\n' ) ;
		if( tmp == NULL )
		{
			错误框( "内部错误：十六进制数据提前耗尽" );
			free( pcHexText );
			free( pcText );
			return NULL;
		}
		pcHexLineBegin = tmp + 1 ;
	}

	if( pcTextOffset-pcText != nTextLength )
	{
		错误框( "内部错误：转换出来的数据的长度[%d]与估算值[%d]不一致" , pcTextOffset-pcText , nTextLength );
		free( pcHexText );
		free( pcText );
		return NULL;
	}

	free( pcHexText );

	if( pnTextLength )
		(*pnTextLength) = nTextLength ;
	return pcText;
}

int OnViewEnableWindowsVisualStyles()
{
	if( 全_风格主配置.启用窗口视觉样式 == FALSE )
	{
		全_风格主配置.启用窗口视觉样式 = TRUE ;
		InfoBox( "已启用Windows主题样式，重启后生效" );
	}
	else
	{
		全_风格主配置.启用窗口视觉样式 = FALSE ;
		InfoBox( "已禁用Windows主题样式，重启后生效" );
	}

	更新所有菜单( 全_主窗口句柄 , 全_当前选项卡页面节点 );

	保存主配置文件();

	return 0;
}

int OnViewTabWidth( struct 选项卡页面S *选项卡页节点 )
{
	char		acTabWidth[ 20 ] ;

	int		nret = 0 ;

	memset( acTabWidth , 0x00 , sizeof(acTabWidth) );
	snprintf( acTabWidth , sizeof(acTabWidth)-1 , "%d" , 全_风格主配置.选项卡宽度 );
	nret = InputBox( 全_主窗口句柄 , "请输入制表符宽度：" , "输入窗口" , 0 , acTabWidth , sizeof(acTabWidth)-1 ) ;
	if( nret == IDOK )
	{
		if( acTabWidth[0] )
		{
			int		n选项卡页面数量 ;
			int		n选项卡页面索引 ;
			TCITEM		tci ;
			struct 选项卡页面S	*p = NULL ;

			全_风格主配置.选项卡宽度 = atoi(acTabWidth) ;

			更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

			保存主配置文件();

			n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
			for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
			{
				memset( & tci , 0x00 , sizeof(TCITEM) );
				tci.mask = TCIF_PARAM ;
				TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
				p = (struct 选项卡页面S *)(tci.lParam);
				p->pfuncScintilla( p->pScintilla , SCI_SETTABWIDTH , 全_风格主配置.选项卡宽度 , 0 );
			}
		}
	}

	return 0;
}

int OnViewOnKeydownTabConvertSpaces( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.按下Tab键转换为空格 == FALSE )
	{
		全_风格主配置.按下Tab键转换为空格 = TRUE ;
	}
	else
	{
		全_风格主配置.按下Tab键转换为空格 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETUSETABS , (全_风格主配置.按下Tab键转换为空格==TRUE?FALSE:TRUE) , 0 );
	}

	return 0;
}

int OnViewWrapLineMode( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.换行模式 == FALSE )
	{
		全_风格主配置.换行模式 = TRUE ;
	}
	else
	{
		全_风格主配置.换行模式 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETWRAPMODE , (全_风格主配置.换行模式==TRUE?2:0) , 0 );
	}

	return 0;
}

int OnViewLineNumberVisiable( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.启用行号 == FALSE )
	{
		全_风格主配置.启用行号 = TRUE ;
	}
	else
	{
		全_风格主配置.启用行号 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETMARGINWIDTHN , 边框_行号_索引 , (全_风格主配置.启用行号==TRUE?边框_行号_宽度:0) );
	}

	return 0;
}

int OnViewBookmarkVisiable( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.启用书签 == FALSE )
	{
		全_风格主配置.启用书签 = TRUE ;
	}
	else
	{
		全_风格主配置.启用书签 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETMARGINWIDTHN , 边框_书签_索引 , (全_风格主配置.启用书签==TRUE?边框_书签_宽度:0) );
	}

	return 0;
}

int OnViewWhiteSpaceVisiable( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.启用空格符 == FALSE )
	{
		全_风格主配置.启用空格符 = TRUE ;
	}
	else
	{
		全_风格主配置.启用空格符 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETVIEWWS , (全_风格主配置.启用空格符==TRUE?SCWS_VISIBLEALWAYS:SCWS_INVISIBLE) , 0 );
		p->pfuncScintilla( p->pScintilla , SCI_SETWHITESPACESIZE , 全_风格主配置.空格符大小 , 0 );
		p->pfuncScintilla( p->pScintilla , SCI_SETTABDRAWMODE , SCTD_LONGARROW , 0 );
	}

	return 0;
}

int OnViewNewLineVisiable( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.新行可见 == FALSE )
	{
		全_风格主配置.新行可见 = TRUE ;
	}
	else
	{
		全_风格主配置.新行可见 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETVIEWEOL , 全_风格主配置.新行可见 , 0 );
	}

	return 0;
}

int OnViewIndentationGuidesVisiable( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.启用缩进辅助线 == FALSE )
	{
		全_风格主配置.启用缩进辅助线 = TRUE ;
	}
	else
	{
		全_风格主配置.启用缩进辅助线 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETINDENTATIONGUIDES , (全_风格主配置.启用缩进辅助线?SC_IV_LOOKBOTH:SC_IV_NONE) , 0 );
	}

	return 0;
}

int OnViewZoomOut( struct 选项卡页面S *选项卡页节点 )
{
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_ZOOMIN , 0 , 0 );

	return 0;
}

int OnViewZoomIn( struct 选项卡页面S *选项卡页节点 )
{
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_ZOOMOUT , 0 , 0 );

	return 0;
}

int OnViewZoomReset( struct 选项卡页面S *选项卡页节点 )
{
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETZOOM , 全_缩放重置 , 0 );

	return 0;
}

int OnEditorTextSelected( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 == NULL || 选项卡页节点->pst文档类型配置 == NULL )
		return 0;
	if( 选项卡页节点->bHexEditMode == TRUE )
		return 0;

	size_t	nTextLenSelected = 0 ;
	char	*pcTextSelected = StrdupEditorSelection( & nTextLenSelected , 0 ) ;
	if( pcTextSelected )
	{
		选项卡页节点->文本已被选择 = TRUE ;

		int nSelectionStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;

		int nTextTotalLength = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_INDICATORCLEARRANGE , 0 , nTextTotalLength );
		char *pcText = (char*)malloc( nTextTotalLength + 1 ) ;
		if( pcText == NULL )
			return -1;
		memset( pcText , 0x00 , nTextTotalLength + 1 );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXT , (sptr_t)(nTextTotalLength+1) , (sptr_t)pcText ) ;

		char *p = strstr( pcText , pcTextSelected ) ;
		while( p )
		{

			if( p-pcText != nSelectionStartPos )
			{
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_INDICATORFILLRANGE , p-pcText  , nTextLenSelected ) ;
			}

			p = strstr( p+nTextLenSelected , pcTextSelected ) ;
		}

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_INDICSETSTYLE , 0 , INDIC_ROUNDBOX ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_INDICSETFORE , 0 , g_pstWindowTheme->stStyleTheme.indicator.bgcolor ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_INDICSETALPHA , 0 , 100 ) ;

		free( pcTextSelected );
		free( pcText );
	}
	else
	{
		if( 选项卡页节点->文本已被选择 == TRUE )
		{
			int nTextTotalLength = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_INDICATORCLEARRANGE , 0 , nTextTotalLength );

			选项卡页节点->文本已被选择 = FALSE ;
		}
	}

	return 0;
}
