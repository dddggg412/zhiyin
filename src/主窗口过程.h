#ifndef _WNDPROC_
#define _WNDPROC_

#include "framework.h"

int 主窗口过程之前( MSG *p_信息 );
int 发送消息之前( MSG *p_信息 );
int 主窗口过程之后( MSG *p_信息 );

LRESULT CALLBACK 主窗口过程(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

#endif
