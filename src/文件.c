#include "framework.h"

int GetFileNameByOpenFileDialog( char* pcFilename, int nFilenameBufSize)
{
	OPENFILENAME ofn = { sizeof(OPENFILENAME) };
	ofn.hwndOwner = 全_主窗口句柄;
	ofn.hInstance = 全_当前实例;
	memset(pcFilename, 0x00, nFilenameBufSize);
	ofn.lpstrFile = pcFilename;
	ofn.nMaxFile = nFilenameBufSize;
	ofn.lpstrFilter = (LPCSTR)获取文件对话框筛选器指针() ;
	ofn.lpstrTitle = "打开文件";
	ofn.Flags = OFN_ENABLESIZING | OFN_EXPLORER | OFN_ALLOWMULTISELECT | OFN_HIDEREADONLY ;
	if( GetOpenFileName( & ofn ) == FALSE )
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int GetFileNameBySaveFileDialog( char *pcFilename, int nFilenameBufSize)
{
	OPENFILENAME ofn = { sizeof(ofn) };
	ofn.hwndOwner = 全_主窗口句柄;
	ofn.hInstance = 全_当前实例;
	memset(pcFilename, 0x00, nFilenameBufSize);
	ofn.lpstrFile = pcFilename;
	ofn.nMaxFile = nFilenameBufSize;
	ofn.lpstrFilter = 获取文件对话框筛选器指针() ;
	/*
	ofn.nFilterIndex = 0 ;
	*/
	ofn.lpstrTitle = "保存文件";
	ofn.Flags = OFN_ENABLESIZING | OFN_HIDEREADONLY;
	if( GetSaveFileName( & ofn ) == FALSE )
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int SplitPathFilename( char *acFullPathFilename , char *acDrivename , char *acPathname , char *acFilename , char *acMainFilename , char *acExtFilename , char *acDriveAndPathname , char *acPathFilename )
{
	char	*p1 = NULL ;
	char	*p2 = NULL ;
	char	*pcPathnameBase = NULL ;
	char	*pcFilenameBase = NULL ;

	p1 = strchr( acFullPathFilename , ':' ) ;
	if( p1 )
	{
		pcPathnameBase = p1 + 1 ;
		if( acDrivename )
			sprintf( acDrivename , "%.*s" , (int)(pcPathnameBase-acFullPathFilename) , acFullPathFilename );
	}
	else
	{
		pcPathnameBase = acFullPathFilename ;
		if( acDrivename )
			acDrivename[0] = '\0' ;
	}

	if( acDrivename )
		sprintf( acDrivename , "%.*s" , (int)(pcPathnameBase-acFullPathFilename) , acFullPathFilename );

	p1 = strrchr( pcPathnameBase , '/' ) ;
	p2 = strrchr( pcPathnameBase , '\\' ) ;
	if( p1 == NULL && p2 == NULL )
	{
		p1 = pcPathnameBase ;
	}
	else if( p1 && p2 )
	{
		if( p1 < p2 )
			p1 = p2 + 1 ;
		else
			p1++;
	}
	else
	{
		if( p2 )
			p1 = p2 + 1 ;
		else
			p1++;
	}

	pcFilenameBase = p1 ;
	if( acPathname )
		sprintf( acPathname , "%.*s" , (int)(pcFilenameBase-pcPathnameBase) , pcPathnameBase );

	if( acFilename )
		strcpy( acFilename , pcFilenameBase );

	p1 = strrchr( pcFilenameBase , '.' ) ;
	if( p1 )
	{
		if( acMainFilename )
			sprintf( acMainFilename , "%.*s" , (int)(p1-pcFilenameBase) , pcFilenameBase );
		if( acExtFilename )
			strcpy( acExtFilename , p1 );
	}
	else
	{
		if( acMainFilename )
			strcpy( acMainFilename , pcFilenameBase );
		if( acExtFilename )
			acExtFilename[0] = '\0' ;
	}

	if( acDriveAndPathname )
		sprintf( acDriveAndPathname , "%.*s" , (int)(pcFilenameBase-acFullPathFilename) , acFullPathFilename );

	if( acPathFilename )
		strcpy( acPathFilename , pcPathnameBase );

	return 0;
}

int test_SplitPathFilename()
{
	int		nret = 0 ;

	char acFullPathFilename[] = "C:\\path1\\file1.ext1" ;
	char acDrivename[MAX_PATH] = "" ;
	char acPathname[MAX_PATH] = "" ;
	char acFilename[MAX_PATH] = "" ;
	char acMainFilename[MAX_PATH] = "" ;
	char acExtFilename[MAX_PATH] = "" ;
	char acDriveAndPathname[MAX_PATH] = "" ;
	char acPathFilename[MAX_PATH] = "" ;

	nret = SplitPathFilename( acFullPathFilename , acDrivename , acPathname , acFilename , acMainFilename , acExtFilename , acDriveAndPathname , acPathFilename ) ;

	return nret;
}

int 文件_新建( struct 选项卡页面S *选项卡页节点 )
{
	int		nret = 0;

	设置状态栏进度信息( "正在新建文件..." );

	选项卡页节点 = AddTabPage( NULL , (char*)"" , (char*)"未保存的新建文档" , (char*)"" ) ;
	if( 选项卡页节点 == NULL )
	{
		清除状态栏进度信息();
		return -1;
	}

	选项卡页节点->nCodePage = 全_风格主配置.新建文件编码 ;
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCODEPAGE , 选项卡页节点->nCodePage , 0 );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETEOLMODE , 全_风格主配置.新建文件Eols , 0 ) ;

	InitTabPageControlsBeforeLoadFile( 选项卡页节点 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CLEARALL , 0 , 0 );

	SetWindowTitle(选项卡页节点->acPathFilename);

	int n选项卡页面数量 = TabCtrl_GetItemCount(全_选项卡页面句柄) ;
	以索引选择选项卡页面( n选项卡页面数量-1 );

	InitTabPageControlsAfterLoadFile( 选项卡页节点 );
	AddNavigateBackNextTrace( 选项卡页节点 , 0 );

	全_当前选项卡页面节点 = 选项卡页节点 ;

	// 重新计算各窗口大小
	计算选项卡页面高度();
	更新所有窗口( 全_主窗口句柄 );

	清除状态栏进度信息();

	return 0;
}

int LoadFileDirectly( char *acFullPathFilename , FILE *fp , struct 选项卡页面S *选项卡页节点 )
{
	size_t			sBlockNo ;
	int			nNewLineMode ;
	size_t			offset ;

	if( fp == NULL )
	{
		fp = fopen( acFullPathFilename , "rb" );
		if( fp == NULL )
		{
			错误框( "不能打开文件[%s]" , acFullPathFilename );
			return 错误_无法打开文件;
		}
	}

	InitEncodingProbe( & (选项卡页节点->stEncodingProbe) );
	InitNewLineModeProbe( & (选项卡页节点->stNewLineModeProbe) );

	选项卡页节点->nCodePage = 0 ;

	char data[ 64 * 1024 ];
	size_t len;
	for( sBlockNo = 0 ; ; )
	{
		len = fread( data , 1 , sizeof(data) , fp );
		if (len < 0)
			return -4;
		if (len == 0)
			break;

		sBlockNo++;

		if(
			sBlockNo == 1
			&&
			memcmp( data , "\xEF\xBB\xBF" , 3 ) == 0
		)
		{
			选项卡页节点->nCodePage = 65001 ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCODEPAGE , 选项卡页节点->nCodePage , 0 );
			选项卡页节点->nPreFileContextLen = 3 ;
			memcpy( 选项卡页节点->acPreFileContext , data , 选项卡页节点->nPreFileContextLen );
			offset = 选项卡页节点->nPreFileContextLen ;
		}
		else if(
			sBlockNo == 1
			&&
			(
				memcmp( data , "\x00\x00\xFE\xFF" , 4 ) == 0
				||
				memcmp( data , "\xFF\xFE\x00\x00" , 4 ) == 0
			)
		)
		{
			选项卡页节点->nCodePage = 65001 ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCODEPAGE , 选项卡页节点->nCodePage , 0 );
			选项卡页节点->nPreFileContextLen = 4 ;
			memcpy( 选项卡页节点->acPreFileContext , data , 选项卡页节点->nPreFileContextLen );
			offset = 选项卡页节点->nPreFileContextLen ;
		}
		else if( sBlockNo == 1 && memcmp( data , "\xFF\xFE" , 2 ) == 0 )
		{
			选项卡页节点->nCodePage = 1200 ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCODEPAGE , 选项卡页节点->nCodePage , 0 );
			选项卡页节点->nPreFileContextLen = 2 ;
			memcpy( 选项卡页节点->acPreFileContext , data , 选项卡页节点->nPreFileContextLen );
			offset = 选项卡页节点->nPreFileContextLen ;
		}
		else if( sBlockNo == 1 && memcmp( data , "\xFE\xFF" , 2 ) == 0 )
		{
			选项卡页节点->nCodePage = 1201 ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCODEPAGE , 选项卡页节点->nCodePage , 0 );
			选项卡页节点->nPreFileContextLen = 2 ;
			memcpy( 选项卡页节点->acPreFileContext , data , 选项卡页节点->nPreFileContextLen );
			offset = 选项卡页节点->nPreFileContextLen ;
		}
		else
		{
			DoEncodingProbe( & (选项卡页节点->stEncodingProbe) , data , len );
			offset = 0 ;
		}

		DoNewLineModeProbe( & (选项卡页节点->stNewLineModeProbe) , data , len );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_ADDTEXT, len-offset, (LPARAM)(data+offset));
	}

	fclose(fp);

	struct stat	statbuf ;
	stat( 选项卡页节点->acPathFilename , & statbuf );
	选项卡页节点->st_mtime = statbuf.st_mtime ;

	if( 选项卡页节点->nCodePage == 0 )
	{
		选项卡页节点->nCodePage = GetCodePageFromEncodingProbe( & (选项卡页节点->stEncodingProbe) ) ;
		if( 选项卡页节点->nCodePage == 0 )
			选项卡页节点->nCodePage = GetACP() ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCODEPAGE , 选项卡页节点->nCodePage , 0 );
	}

	nNewLineMode = GetNewLineModeFromProbe( & (选项卡页节点->stNewLineModeProbe) , 全_风格主配置.新建文件Eols ) ;
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETEOLMODE, nNewLineMode, 0 );

	return 0;
}

int OpenFileDirectly( char *acFullPathFilename )
{
	FILE			*fp = NULL ;
	char			acFilename[ MAX_PATH ] ;
	char			p扩展名[ MAX_PATH ] ;
	struct 选项卡页面S		*选项卡页节点 = NULL ;
	int			nret = 0 ;

	if( strncmp(acFullPathFilename,"\\\\",2) && acFullPathFilename[1]!=':' )
	{
		char tmp[MAX_PATH];
		memset( tmp , 0x00 , sizeof(tmp) );
		GetCurrentDirectory( sizeof(tmp)-1 , tmp );
		snprintf( tmp + strlen(tmp) , sizeof(tmp)-1 - strlen(tmp) , "\\%s" , acFullPathFilename );
		strcpy( acFullPathFilename , tmp );
	}

	SplitPathFilename( acFullPathFilename , NULL , NULL , acFilename , NULL , p扩展名 , NULL , NULL );

	fp = fopen( acFullPathFilename , "rb" );
	if (fp == NULL)
	{
		错误框( "不能打开文件[%s]" , acFullPathFilename );
		return -2;
	}

	选项卡页节点 = AddTabPage( NULL , acFullPathFilename , acFilename , p扩展名 ) ;
	if( 选项卡页节点 == NULL )
	{
		fclose(fp);
		return -3;
	}

	InitTabPageControlsBeforeLoadFile( 选项卡页节点 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CLEARALL , 0 , 0 );

	nret = LoadFileDirectly( acFullPathFilename , fp , 选项卡页节点 ) ;
	if( nret )
		return nret;

	/*
	strcpy( 选项卡页节点->acPathFilename, acFullPathFilename );
	strcpy( 选项卡页节点->acFilename, acFilename );
	选项卡页节点->nFilenameLen = strlen(选项卡页节点->acFilename) ;
	strcpy( 选项卡页节点->acPathName , acFullPathFilename ); 选项卡页节点->acPathName[strlen(选项卡页节点->acPathName)-选项卡页节点->nFilenameLen] = '\0' ;
	*/

	SetWindowTitle(选项卡页节点->acPathFilename);

	int n选项卡页面数量 = TabCtrl_GetItemCount(全_选项卡页面句柄) ;
	以索引选择选项卡页面( n选项卡页面数量-1 );

	InitTabPageControlsAfterLoadFile( 选项卡页节点 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOPOS, 0, 0);
	AddNavigateBackNextTrace( 选项卡页节点 , 0 );

	if( 选项卡页节点->p扩展名[0] && strword( (char*)".exe .com .dll .lib .ocx .bin .a .so" , 选项卡页节点->p扩展名 ) )
	{
		OnViewHexEditMode( 选项卡页节点 );
	}

	/* 如果设置了打开文件后锁定文件 */
	if( 全_风格主配置.打开文件后设置为只读 == TRUE )
	{
		DWORD dw文件属性 = GetFileAttributes( 选项卡页节点->acPathFilename ) ;
		dw文件属性 |= FILE_ATTRIBUTE_READONLY ;
		SetFileAttributes( 选项卡页节点->acPathFilename , dw文件属性 );
	}

	推送最近打开路径文件名( 选项卡页节点->acPathFilename );

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	// 重新计算各窗口大小
	计算选项卡页面高度();
	更新所有窗口( 全_主窗口句柄 );

	// 设置焦点到编辑控件
	SetFocus( 选项卡页节点->Scintilla句柄 );

	return 0;
}

int 直接打开文件( char *a文件名称 )
{
	char			acPathname[ MAX_PATH ] ;
	char			acMainFilename[ MAX_PATH ] ;
	char			acExtFilename[ MAX_PATH ] ;
	char			acDriveAndPathname[ MAX_PATH ] ;
	WIN32_FIND_DATA		stFindFileData ;
	HANDLE			hFindFile ;
	char			acFoundFilename[ MAX_PATH ] ;
	int			nret = 0 ;

	nret = SplitPathFilename( a文件名称 , NULL , acPathname , NULL , acMainFilename , acExtFilename , acDriveAndPathname , NULL ) ;
	if( nret )
	{
		错误框( "分解路径文件名[%s]失败" , a文件名称 );
		return nret;
	}

	hFindFile = FindFirstFile( a文件名称 , & stFindFileData ) ;
	if( hFindFile == INVALID_HANDLE_VALUE )
	{
		错误框( "文件[%s]不存在" , a文件名称 );
		return 1;
	}

	do
	{
		if( strcmp(stFindFileData.cFileName,".") == 0 || strcmp(stFindFileData.cFileName,"..") == 0 )
			continue;

		if( ! ( stFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
		{
			memset( acFoundFilename , 0x00 , sizeof(acFoundFilename) );
			if( strchr( acMainFilename,'*') || strchr(acExtFilename,'*') )
				_snprintf( acFoundFilename , sizeof(acFoundFilename)-1 , "%s%s" , acDriveAndPathname , stFindFileData.cFileName ) ;
			else
				strcpy( acFoundFilename , a文件名称 );
			OpenFileDirectly( acFoundFilename );
		}
	}
	while( FindNextFile( hFindFile , & stFindFileData ) );

	FindClose( hFindFile );

	return 0;
}

#define PATHFILENAMES_BUFSIZE	30*1024

int 文件_打开()
{
	char		*acPathFilenames = NULL ;
	char		acPathname[ MAX_PATH ] ;
	int		nPathnameLen ;
	char		*p = NULL ;
	char		acPathFilename[ MAX_PATH ] ;

	int		nret = 0;

	设置状态栏进度信息( "正在打开文件..." );

	acPathFilenames = (char*)malloc( PATHFILENAMES_BUFSIZE ) ;
	if( acPathFilenames == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存以存放打开文件名集缓冲区"), TEXT("错误"), MB_ICONERROR | MB_OK);
		清除状态栏进度信息();
		return -1;
	}
	memset( acPathFilenames , 0x00 , PATHFILENAMES_BUFSIZE );

	nret = GetFileNameByOpenFileDialog( acPathFilenames , PATHFILENAMES_BUFSIZE ) ;
	if (nret)
	{
		清除状态栏进度信息();
		return -1;
	}

	设置状态栏进度信息( "正在打开文件[%s]..." , acPathFilenames );

	DWORD dwFileAttributes = GetFileAttributes( acPathFilenames ) ;
	if( ! ( dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
	{
		nret = OpenFileDirectly( acPathFilenames ) ;
		if( nret )
		{
			清除状态栏进度信息();
			return -1;
		}
	}
	else
	{
		memset( acPathname , 0x00 , sizeof(acPathname) );
		strncpy( acPathname , acPathFilenames , sizeof(acPathname)-1 );
		nPathnameLen = (int)strlen( acPathname ) ;

		p = acPathFilenames + nPathnameLen + 1 ;
		while( *p )
		{
			memset( acPathFilename , 0x00 , sizeof(acPathFilename) );
			snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s\\%s" , acPathname , p );
			nret = OpenFileDirectly( acPathFilename ) ;
			if( nret )
			{
				清除状态栏进度信息();
				return -1;
			}

			p = p + strlen(p) + 1 ;
		}
	}

	清除状态栏进度信息();

	return 0;
}

int 清理最近打开文件的历史( struct 选项卡页面S *选项卡页节点 )
{
	memset( 全_风格主配置.最近打开路径文件名 , 0x00 , sizeof(全_风格主配置.最近打开路径文件名) );

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	return 0;
}

int 执行打开拖放文件( HDROP hDrop )
{
	int		nFileCount ;
	int		nFileIndex ;
	char		acPathFilename[ MAX_PATH ] ;

	int		nret = 0 ;

	nFileCount = DragQueryFile( hDrop , 0xFFFFFFFF , NULL , 0 );
	for( nFileIndex = 0 ; nFileIndex < nFileCount ; nFileIndex++ )
	{
		memset( acPathFilename , 0x00 , sizeof(acPathFilename) );
		DragQueryFile( hDrop , nFileIndex , acPathFilename , sizeof(acPathFilename) );

		struct _stat	stat_buf ;
	    _stat( acPathFilename , & stat_buf );
		if( stat_buf.st_mode & _S_IFDIR )
			strcat(acPathFilename,"\\*"),直接打开文件( acPathFilename );
		else
			OpenFileDirectly( acPathFilename );
	}

	DragFinish( hDrop );

	return 0;
}

static size_t ReadRemoteFile(void *buffer, size_t size, size_t nmemb, void *stream)
{
	struct 选项卡页面S		*选项卡页节点 = (struct 选项卡页面S *)stream ;
	char			*data = (char*)buffer ;
	size_t			len ;
	size_t			offset ;

	len = size * nmemb ;

	if( 选项卡页节点->nFileSize == 0 )
	{
		if(
			memcmp( data , "\xEF\xBB\xBF" , 3 ) == 0
		)
		{
			选项卡页节点->nCodePage = 65001 ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCODEPAGE , 选项卡页节点->nCodePage , 0 );
			选项卡页节点->nPreFileContextLen = 3 ;
			memcpy( 选项卡页节点->acPreFileContext , data , 选项卡页节点->nPreFileContextLen );
			offset = 选项卡页节点->nPreFileContextLen ;
		}
		else if(
			memcmp( data , "\x00\x00\xFE\xFF" , 4 ) == 0
			||
			memcmp( data , "\xFF\xFE\x00\x00" , 4 ) == 0
		)
		{
			选项卡页节点->nCodePage = 65001 ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCODEPAGE , 选项卡页节点->nCodePage , 0 );
			选项卡页节点->nPreFileContextLen = 4 ;
			memcpy( 选项卡页节点->acPreFileContext , data , 选项卡页节点->nPreFileContextLen );
			offset = 选项卡页节点->nPreFileContextLen ;
		}
		else if(
			memcmp( data , "\xFF\xFE" , 2 ) == 0
			)
		{
			选项卡页节点->nCodePage = 1200 ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCODEPAGE , 选项卡页节点->nCodePage , 0 );
			选项卡页节点->nPreFileContextLen = 2 ;
			memcpy( 选项卡页节点->acPreFileContext , data , 选项卡页节点->nPreFileContextLen );
			offset = 选项卡页节点->nPreFileContextLen ;
		}
		else if(
			memcmp( data , "\xFE\xFF" , 2 ) == 0
			)
		{
			选项卡页节点->nCodePage = 1201 ;
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETCODEPAGE , 选项卡页节点->nCodePage , 0 );
			选项卡页节点->nPreFileContextLen = 2 ;
			memcpy( 选项卡页节点->acPreFileContext , data , 选项卡页节点->nPreFileContextLen );
			offset = 选项卡页节点->nPreFileContextLen ;
		}
		else
		{
			DoEncodingProbe( & (选项卡页节点->stEncodingProbe) , data , len );
			offset = 0 ;
		}
	}
	else
	{
		DoEncodingProbe( & (选项卡页节点->stEncodingProbe) , data , len );
		offset = 0 ;
	}

	DoNewLineModeProbe( & (选项卡页节点->stNewLineModeProbe) , data , len );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_ADDTEXT, len-offset, (LPARAM)(data+offset));
	选项卡页节点->nFileSize += len-offset ;

	return len;
}

int OpenRemoteFileDirectly( struct RemoteFileServer *pstRemoteFileServer , char *acFullPathFilename )
{
	CURL			*curl = NULL ;
	CURLcode		res ;
	char			url[ 1024 ] ;
	char			userpwd[ 256 ] ;
	char			acFilename[ MAX_PATH ] ;
	char			p扩展名[ MAX_PATH ] ;
	struct 选项卡页面S		*选项卡页节点 = NULL ;
	int			nNewLineMode ;

	SplitPathFilename( acFullPathFilename , NULL , NULL , acFilename , NULL , p扩展名 , NULL , NULL );

	curl = curl_easy_init() ;
	if( curl == NULL )
	{
		MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -2;
	}

	选项卡页节点 = AddTabPage( pstRemoteFileServer , acFullPathFilename , acFilename , p扩展名 ) ;
	if( 选项卡页节点 == NULL )
	{
		return -3;
	}

	InitTabPageControlsBeforeLoadFile( 选项卡页节点 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_CLEARALL , 0 , 0 );

	InitEncodingProbe( & (选项卡页节点->stEncodingProbe) );
	InitNewLineModeProbe( & (选项卡页节点->stNewLineModeProbe) );

	选项卡页节点->nCodePage = 0 ;

	memset( url , 0x00 , sizeof(url) );
	strncpy( url , acFullPathFilename , sizeof(url)-1 );
	curl_easy_setopt( curl , CURLOPT_URL , url );
	memset( userpwd , 0x00 , sizeof(userpwd) );
	snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , pstRemoteFileServer->acLoginUser , pstRemoteFileServer->acLoginPass );
	curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
	curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
	curl_easy_setopt( curl , CURLOPT_WRITEFUNCTION , ReadRemoteFile );
	curl_easy_setopt( curl , CURLOPT_WRITEDATA , 选项卡页节点 );
	curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
	curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
	res = curl_easy_perform( curl ) ;
	if( res != CURLE_OK )
	{
		MessageBox(NULL, TEXT("连接远程文件服务器失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		curl_easy_cleanup( curl );
		return -3;
	}
	else
	{
		curl_easy_cleanup( curl );
	}

	/*
	memcpy( & (选项卡页节点->stRemoteFileServer) , pstRemoteFileServer , sizeof(struct RemoteFileServer) );
	strcpy( 选项卡页节点->acPathFilename, acFullPathFilename );
	strcpy( 选项卡页节点->acFilename, acFilename );
	选项卡页节点->nFilenameLen = strlen(选项卡页节点->acFilename) ;
	*/

	if( 选项卡页节点->nCodePage == 0 )
	{
		选项卡页节点->nCodePage = GetCodePageFromEncodingProbe( & (选项卡页节点->stEncodingProbe) ) ;
		if( 选项卡页节点->nCodePage == 0 )
			选项卡页节点->nCodePage = GetACP() ;
	}

	nNewLineMode = GetNewLineModeFromProbe( & (选项卡页节点->stNewLineModeProbe) , 全_风格主配置.新建文件Eols ) ;
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETEOLMODE, nNewLineMode, 0 );

	SetWindowTitle(选项卡页节点->acPathFilename);

	int n选项卡页面数量 = TabCtrl_GetItemCount(全_选项卡页面句柄) ;
	以索引选择选项卡页面( n选项卡页面数量-1 );

	InitTabPageControlsAfterLoadFile( 选项卡页节点 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOPOS, 0, 0);
	AddNavigateBackNextTrace( 选项卡页节点 , 0 );

	if( 选项卡页节点->p扩展名[0] && strword( (char*)".exe .com .dll .lib .ocx .bin .a .so" , 选项卡页节点->p扩展名 ) )
	{
		OnViewHexEditMode( 选项卡页节点 );
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	// 重新计算各窗口大小
	计算选项卡页面高度();
	更新所有窗口( 全_主窗口句柄 );

	// 设置焦点到编辑控件
	SetFocus( 选项卡页节点->Scintilla句柄 );

	return 0;
}

static int DoWriteFile( struct 选项卡页面S *选项卡页节点 , char* pathfilename)
{
	FILE	*fp = NULL ;
	size_t	nWriteLen ;
	char	acWriteBuffer[ 64*1024 ] ;
	char	*pcWriteBuffer = NULL ;

	/* 如果设置了打开文件后锁定文件 */
	if( 全_风格主配置.打开文件后设置为只读 == TRUE )
	{
		DWORD dw文件属性 = GetFileAttributes( pathfilename ) ;
		dw文件属性 &= ~FILE_ATTRIBUTE_READONLY ;
		SetFileAttributes( pathfilename , dw文件属性 );
	}

	fp = fopen(pathfilename, "wb");
	if( fp == NULL )
	{
		char msg[MAX_PATH + 100];
		_snprintf( msg , sizeof(msg)-1 , "不能打开文件[%s]" , pathfilename );
		MessageBox(全_主窗口句柄, msg, 全_应用程序名称, MB_OK);
		return -1;
	}

	if( 选项卡页节点->nPreFileContextLen > 0 )
	{
		size_t len = fwrite( 选项卡页节点->acPreFileContext , 选项卡页节点->nPreFileContextLen , 1 , fp );
		if( len == -1 )
		{
			MessageBox(全_主窗口句柄, "写文件失败", 全_应用程序名称, MB_OK);
			fclose(fp);
			return -2;
		}
	}

	if( 选项卡页节点->bHexEditMode == FALSE )
	{
		选项卡页节点->nFileSize = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETLENGTH, 0, 0);
	}
	else
	{
		选项卡页节点->acWriteFileBuffer = (char*)StrdupHexEditTextFold( 选项卡页节点 , & 选项卡页节点->nFileSize ) ;
		if( 选项卡页节点->acWriteFileBuffer == NULL )
			return -3;
	}

	for ( 选项卡页节点->nWroteLen = 0 ; 选项卡页节点->nWroteLen < 选项卡页节点->nFileSize ; )
	{
		nWriteLen = 选项卡页节点->nFileSize - 选项卡页节点->nWroteLen;
		if(nWriteLen > sizeof(acWriteBuffer)-1 )
			nWriteLen = sizeof(acWriteBuffer)-1 ;
		if( 选项卡页节点->bHexEditMode == FALSE )
		{
			GetTextByRange( 选项卡页节点 , 选项卡页节点->nWroteLen , 选项卡页节点->nWroteLen+nWriteLen , acWriteBuffer ) ;
			pcWriteBuffer = acWriteBuffer ;
		}
		else
		{
			pcWriteBuffer = 选项卡页节点->acWriteFileBuffer + 选项卡页节点->nWroteLen ;
		}
		size_t len = fwrite( pcWriteBuffer , nWriteLen , 1 , fp );
		if( len == -1 )
		{
			MessageBox(全_主窗口句柄, "写文件失败", 全_应用程序名称, MB_OK);
			fclose(fp);
			return -2;
		}

		选项卡页节点->nWroteLen += nWriteLen ;
	}

	if( 选项卡页节点->bHexEditMode == FALSE )
	{
		;
	}
	else
	{
		free( 选项卡页节点->acWriteFileBuffer ); 选项卡页节点->acWriteFileBuffer = NULL ;
	}

	fclose(fp);

	/* 如果设置了打开文件后锁定文件 */
	if( 全_风格主配置.打开文件后设置为只读 == TRUE )
	{
		DWORD dw文件属性 = GetFileAttributes( pathfilename ) ;
		dw文件属性 |= FILE_ATTRIBUTE_READONLY ;
		SetFileAttributes( pathfilename , dw文件属性 );
	}

	return 0;
}

static size_t WriteRemoteFile(void *buffer, size_t size, size_t nmemb, void *stream)
{
	struct 选项卡页面S		*选项卡页节点 = (struct 选项卡页面S *)stream ;
	size_t			len ;

	if( 选项卡页节点->nWroteLen >= 选项卡页节点->nFileSize )
		return 0;

	if( 选项卡页节点->bNeedWrotePreFileContext == TRUE )
	{
		memcpy( buffer , 选项卡页节点->acPreFileContext , 选项卡页节点->nPreFileContextLen );
		len = 选项卡页节点->nPreFileContextLen ;

		选项卡页节点->bNeedWrotePreFileContext = FALSE ;

		return len;
	}

	len = size * nmemb ;
	if( 选项卡页节点->nWroteLen + len > 选项卡页节点->nFileSize )
		len = 选项卡页节点->nFileSize - 选项卡页节点->nWroteLen ;

	if( 选项卡页节点->bHexEditMode == FALSE )
	{
		GetTextByRange( 选项卡页节点 , (int)(选项卡页节点->nWroteLen) , 选项卡页节点->nWroteLen+len , (char*)buffer ) ;
	}
	else
	{
		memcpy( buffer , 选项卡页节点->acWriteFileBuffer + 选项卡页节点->nWroteLen , len );
	}

	选项卡页节点->nWroteLen += len ;

	return len;
}

int 文件_保存( struct 选项卡页面S *选项卡页节点 , BOOL bSaveAs )
{
	char		acFullPathFilename[ MAX_PATH ] ;
	char		acFilename[ MAX_PATH ] ;
	char		p扩展名[ MAX_PATH ] ;

	int		nret = 0;

	if( 选项卡页节点->pScintilla == NULL )
		return -1;

	if( IsDocumentModified(选项卡页节点) == FALSE && bSaveAs != TRUE )
		return -2;

	设置状态栏进度信息( "正在保存文件[%s]..." , 选项卡页节点->acPathFilename );

	if( 选项卡页节点->stRemoteFileServer.acNetworkAddress[0] )
	{
		CURL			*curl = NULL ;
		CURLcode		res ;
		char			url[ 1024 ] ;
		char			userpwd[ 256 ] ;

		if( 选项卡页节点->bHexEditMode == FALSE )
		{
			选项卡页节点->nFileSize = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETLENGTH, 0, 0);
		}
		else
		{
			选项卡页节点->acWriteFileBuffer = (char*)StrdupHexEditTextFold( 选项卡页节点 , & 选项卡页节点->nFileSize ) ;
			if( 选项卡页节点->acWriteFileBuffer == NULL )
			{
				清除状态栏进度信息();
				return -3;
			}
		}

		选项卡页节点->nWroteLen = 0 ;
		if( 选项卡页节点->nPreFileContextLen > 0 )
			选项卡页节点->bNeedWrotePreFileContext = TRUE ;

		curl = curl_easy_init() ;
		if( curl == NULL )
		{
			MessageBox(NULL, TEXT("不能创建连接对象"), TEXT("错误"), MB_ICONERROR | MB_OK);
			清除状态栏进度信息();
			free( 选项卡页节点->acWriteFileBuffer ); 选项卡页节点->acWriteFileBuffer = NULL ;
			return -2;
		}

		memset( url , 0x00 , sizeof(url) );
		strncpy( url , 选项卡页节点->acPathFilename , sizeof(url)-1 );
		curl_easy_setopt( curl , CURLOPT_URL , url );
		memset( userpwd , 0x00 , sizeof(userpwd) );
		snprintf( userpwd , sizeof(userpwd)-1 , "%s:%s" , 选项卡页节点->stRemoteFileServer.acLoginUser , 选项卡页节点->stRemoteFileServer.acLoginPass );
		curl_easy_setopt( curl , CURLOPT_USERPWD , userpwd );
		curl_easy_setopt( curl , CURLOPT_USE_SSL , CURLUSESSL_TRY );
		curl_easy_setopt( curl , CURLOPT_UPLOAD , 1L );
		curl_easy_setopt( curl , CURLOPT_READFUNCTION , WriteRemoteFile );
		curl_easy_setopt( curl , CURLOPT_READDATA , 选项卡页节点 );
		curl_easy_setopt( curl , CURLOPT_VERBOSE , 1L );
		curl_easy_setopt( curl , CURLOPT_CONNECTTIMEOUT , 5 );
		res = curl_easy_perform( curl ) ;
		if( 选项卡页节点->bHexEditMode == FALSE )
		{
			;
		}
		else
		{
			free( 选项卡页节点->acWriteFileBuffer ); 选项卡页节点->acWriteFileBuffer = NULL ;
		}
		if( res != CURLE_OK )
		{
			MessageBox(NULL, TEXT("连接远程文件服务器失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
			清除状态栏进度信息();
			curl_easy_cleanup( curl );
			return -3;
		}
		else
		{
			curl_easy_cleanup( curl );
		}
	}
	else if( 选项卡页节点->acPathFilename[0] == '\0' || bSaveAs == TRUE )
	{
		nret = GetFileNameBySaveFileDialog( acFullPathFilename, sizeof(acFullPathFilename));
		if (nret)
		{
			清除状态栏进度信息();
			return -3;
		}

		SplitPathFilename( acFullPathFilename , NULL , NULL , acFilename , NULL , p扩展名 , NULL , NULL );

		nret = DoWriteFile( 选项卡页节点, acFullPathFilename);
		if (nret)
		{
			清除状态栏进度信息();
			return -4;
		}

		strcpy( 选项卡页节点->acPathFilename, acFullPathFilename );
		strcpy( 选项卡页节点->acFilename, acFilename );
		选项卡页节点->nFilenameLen = strlen(选项卡页节点->acFilename) ;
		SetWindowTitle( 选项卡页节点->acPathFilename );

		选项卡页节点->pst文档类型配置 = 获取文档类型配置( p扩展名 ) ;

		InitTabPageControlsBeforeLoadFile( 选项卡页节点 );

		InitTabPageControlsAfterLoadFile( 选项卡页节点 );

		推送最近打开路径文件名( 选项卡页节点->acPathFilename );
	}
	else
	{
		nret = DoWriteFile( 选项卡页节点, 选项卡页节点->acPathFilename );
		if( nret )
		{
			清除状态栏进度信息();
			return -5;
		}
	}

	struct stat	statbuf ;
	stat( 选项卡页节点->acPathFilename , & statbuf );
	选项卡页节点->st_mtime = statbuf.st_mtime ;

	OnSavePointReached( 选项卡页节点 );
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_SETSAVEPOINT, 0, 0) ;

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	// 重新计算各窗口大小
	更新所有窗口( 全_主窗口句柄 );

	if( 选项卡页节点 && 选项卡页节点->pst文档类型配置 && 选项卡页节点->pst文档类型配置->pfuncParseFileConfigHeader )
	{
		nret = 选项卡页节点->pst文档类型配置->pfuncParseFileConfigHeader( 选项卡页节点 ) ;
		if( nret )
		{
			清除状态栏进度信息();
			return nret;
		}
	}

	/*
	if( 全_风格主配置.重新加载符号列表或树间隔 > 0 )
	{
		BeginReloadSymbolListOrTreeThread();
	}
	*/

	清除状态栏进度信息();

	return 0;
}

int 文件_另存为( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 == NULL )
		return 0;

	if ( 选项卡页节点->pScintilla == NULL)
		return -1;

	文件_保存( 选项卡页节点 , TRUE );

	return 0;
}

int 保存所有的文件()
{
	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*选项卡页节点 = NULL ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0 ; n选项卡页面索引 < n选项卡页面数量 ; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		选项卡页节点 = (struct 选项卡页面S *)(tci.lParam);
		文件_保存( 选项卡页节点 , FALSE ) ;
	}

	return 0;
}

int 关闭文件时( struct 选项卡页面S *选项卡页节点 )
{
	设置状态栏进度信息( "正在关闭文件[%s]..." , 选项卡页节点->acPathFilename );

	if( IsDocumentModified(选项卡页节点) == TRUE )
	{
		char	msg[100 + MAX_PATH];
		int	decision;
		_snprintf(msg, sizeof(msg)-1, "保存到[%s] ?", 选项卡页节点->acPathFilename);
		decision = MessageBox(全_主窗口句柄, msg, 全_应用程序名称, MB_YESNOCANCEL);
		if (decision == IDCANCEL)
		{
			清除状态栏进度信息();
			return -1;
		}
		else if (decision == IDYES)
		{
			文件_保存( 选项卡页节点 , FALSE );
		}
	}

	CleanTabPageControls( 选项卡页节点 );

	/* 如果设置了打开文件后锁定文件 */
	if( 全_风格主配置.打开文件后设置为只读 == TRUE )
	{
		DWORD dw文件属性 = GetFileAttributes( 选项卡页节点->acPathFilename ) ;
		dw文件属性 &= ~FILE_ATTRIBUTE_READONLY ;
		SetFileAttributes( 选项卡页节点->acPathFilename , dw文件属性 );
	}

	/* 清理该文件的所有导航退回结构 */
	CleanNavigateBackNextTraceListByThisFile( 选项卡页节点 );

	RemoveTabPage( 选项卡页节点 );

	// 重新计算各窗口大小
	计算选项卡页面高度();
	/*
	调整选项卡页面();
	调整选项卡页面框( 选项卡页节点 );
	*/
	更新所有菜单( 全_主窗口句柄 , 全_当前选项卡页面节点 );
	更新所有窗口( 全_主窗口句柄 );

	清除状态栏进度信息();

	return 0;
}

int 关闭所有文件时()
{
	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	int		nThisTabPageIndex ;
	TCITEM		tci ;
	struct 选项卡页面S	*选项卡页节点 = NULL ;
	int		nret = 0 ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	nThisTabPageIndex = 0 ;
	for( n选项卡页面索引 = 0 ; n选项卡页面索引 < n选项卡页面数量 ; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , nThisTabPageIndex , & tci );
		选项卡页节点 = (struct 选项卡页面S *)(tci.lParam);
		nret = 关闭文件时( 选项卡页节点 ) ;
		if( nret == -1 )
			nThisTabPageIndex++;
	}

	return 0;
}

int 关闭除当前所有文件时( struct 选项卡页面S *选项卡页节点 )
{
	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	int		nThisTabPageIndex ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;
	int		nret = 0 ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	nThisTabPageIndex = 0 ;
	for( n选项卡页面索引 = 0 ; n选项卡页面索引 < n选项卡页面数量 ; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , nThisTabPageIndex , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		if( p == 选项卡页节点 )
		{
			nThisTabPageIndex++;
			continue;
		}
		nret = 关闭文件时( p ) ;
		if( nret == -1 )
			nThisTabPageIndex++;
	}

	return 0;
}

int 启动时没有打开文件则新建文件( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.在启动时创建新文件 == FALSE )
	{
		全_风格主配置.在启动时创建新文件 = TRUE ;
	}
	else
	{
		全_风格主配置.在启动时创建新文件 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	return 0;
}

int 启动时打开退出时打开的文件( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.打开退出时打开的文件 == FALSE )
	{
		全_风格主配置.打开退出时打开的文件 = TRUE ;
	}
	else
	{
		全_风格主配置.打开退出时打开的文件 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	return 0;
}

int 在打开文件后设置为只读( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.打开文件后设置为只读 == FALSE )
	{
		全_风格主配置.打开文件后设置为只读 = TRUE ;
	}
	else
	{
		全_风格主配置.打开文件后设置为只读 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	return 0;
}

int 启动时填充所有打开的文件()
{
	memset( 全_风格主配置.启动时打开文件 , 0x00 , sizeof(全_风格主配置.启动时打开文件) );
	int count = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	if( count > 启动时打开文件最大数量M )
		count = 启动时打开文件最大数量M ;
	for( int index = 0 ; index < count ; index++ )
	{
		struct 选项卡页面S	*p = 以索引获取选项卡页面( index ) ;

		strncpy( 全_风格主配置.启动时打开文件[index] , p->acPathFilename , sizeof(全_风格主配置.启动时打开文件[index])-1 );
	}

	return 0;
}

int 检查所有文件保存()
{
	int		n选项卡页面数量 ;
	TCITEM		tci ;
	struct 选项卡页面S	*选项卡页节点 = NULL ;
	int		nret = 0 ;

	while(1)
	{
		n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
		if( n选项卡页面数量 == 0 )
			break;

		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , 0 , & tci );
		选项卡页节点 = (struct 选项卡页面S *)(tci.lParam);
		nret = 关闭文件时( 选项卡页节点 ) ;
		if( nret )
			return -1;
	}

	return 0;
}

int 检测文件变动( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.检查被选中选项卡页面的更新 == FALSE )
	{
		全_风格主配置.检查被选中选项卡页面的更新 = TRUE ;
	}
	else
	{
		全_风格主配置.检查被选中选项卡页面的更新 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	return 0;
}

int 文件变动自动重载提示( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.提示自动更新 == FALSE )
	{
		全_风格主配置.提示自动更新 = TRUE ;
	}
	else
	{
		全_风格主配置.提示自动更新 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	return 0;
}

int 设置新建文件换行符风格( struct 选项卡页面S *选项卡页节点 , int nNewFileEolsMode )
{
	全_风格主配置.新建文件Eols = nNewFileEolsMode ;

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	return 0;
}

int 全文转换文件换行符风格( struct 选项卡页面S *选项卡页节点 , int nConvertEolsMode )
{
	if( 选项卡页节点 == NULL )
		return 0;

	if( 选项卡页节点->pfuncScintilla(选项卡页节点->pScintilla,SCI_GETEOLMODE,0,0) != nConvertEolsMode )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_CONVERTEOLS, nConvertEolsMode, 0 );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_SETEOLMODE, nConvertEolsMode, 0 );

		更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

		保存主配置文件();
	}

	return 0;
}

int 设置新建文件字符编码( struct 选项卡页面S *选项卡页节点 , int 新建文件编码 )
{
	全_风格主配置.新建文件编码 = 新建文件编码 ;

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	return 0;
}

int 全文转换文件字符编码( struct 选项卡页面S *选项卡页节点 , int nConvertEncoding )
{
	char		*pcret = NULL ;

	if( 选项卡页节点 == NULL )
		return 0;

	if( 选项卡页节点->pfuncScintilla(选项卡页节点->pScintilla,SCI_GETCODEPAGE,0,0) != nConvertEncoding )
	{
		int		当前文本字节位置 ;
		int		nCurrentLine ;

		size_t		nFileSize ;
		char		*acFileBuffer = NULL ;
		char		*pcInput = NULL ;
		size_t		nConvertSize ;
		char		*acConvertBuffer = NULL ;

		当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;

		nFileSize = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
		acFileBuffer = (char*)malloc( nFileSize + 1 ) ;
		if( acFileBuffer == NULL )
			return -1;
		memset( acFileBuffer , 0x00 , nFileSize + 1 );

		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXT , (sptr_t)(nFileSize+1) , (sptr_t)acFileBuffer ) ;

		nConvertSize = 3 + nFileSize * 3 ;
		acConvertBuffer = (char*)malloc( nConvertSize + 1 ) ;
		if( acConvertBuffer == NULL )
		{
			free( acFileBuffer );
			return -1;
		}
		memset( acConvertBuffer , 0x00 , nConvertSize + 1 );

		/*
		if( 选项卡页节点->nCodePage == 65001 )
		{
			pcInput = acFileBuffer + 3 ;
			nFileSize -= 3 ;
		}
		else
		{
		*/
			pcInput = acFileBuffer ;
		/*
		}
		*/
		pcret = ConvertEncodingEx( GetEncodingString(选项卡页节点->nCodePage) , GetEncodingString(nConvertEncoding) , pcInput , nFileSize , acConvertBuffer , & nConvertSize ) ;
		if( pcret == NULL )
		{
			free( acFileBuffer );
			free( acConvertBuffer );
			错误框( "全文转换字符编码，从[%s]到[%s]，失败" , GetEncodingString(选项卡页节点->nCodePage) , GetEncodingString(nConvertEncoding) );
			return -1;
		}

		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_CLEARALL, 0, 0 );

		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_SETCODEPAGE, nConvertEncoding, 0 );
		选项卡页节点->nCodePage = nConvertEncoding ;
		if( nConvertEncoding == 字符编码_UTF8 )
			全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_ADDTEXT, 3, (sptr_t)"\xEF\xBB\xBF" );
		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_ADDTEXT, nConvertSize, (sptr_t)acConvertBuffer );

		全_当前选项卡页面节点->pfuncScintilla( 全_当前选项卡页面节点->pScintilla, SCI_GOTOLINE, nCurrentLine, 0 ) ;

		if( nConvertEncoding == 字符编码_GBK || nConvertEncoding == 字符编码_BIG5 )
			全_当前选项卡页面节点->nPreFileContextLen = 0 ;

		free( acFileBuffer );
		free( acConvertBuffer );

		更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

		保存主配置文件();
	}

	return 0;
}

