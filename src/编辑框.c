#include "framework.h"

int 撤销编辑( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, WM_UNDO, 0, 0 );
	return 0;
}

int 重做编辑( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_REDO, 0, 0 );
	return 0;
}

int 剪切文本( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, WM_CUT, 0, 0 );
	return 0;
}

int 复制文本( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, WM_COPY, 0, 0 );
	return 0;
}

int 粘贴文本( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, WM_PASTE, 0, 0 );
	return 0;
}

int 删除文本( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, WM_CLEAR, 0, 0 );
	return 0;
}

int 剪切行( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINECUT, 0, 0 );
	return 0;
}

int 剪切行和粘贴行( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
		剪切行( 选项卡页节点 );
		粘贴行( 选项卡页节点 );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_ENDUNDOACTION, 0, 0 );
	}
	return 0;
}

int 复制行( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINECOPY, 0, 0 );
	return 0;
}

int 复制行和粘贴行( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_SELECTIONDUPLICATE, 0, 0 );
	return 0;
}

int 复制文件名称( struct 选项卡页面S *选项卡页节点 )
{
	HGLOBAL hgl = (char*)GlobalAlloc( GMEM_MOVEABLE , strlen(选项卡页节点->acFilename)+1 ) ;
	char *acCopyText = (char*)GlobalLock(hgl) ;
	if( acCopyText == NULL )
	{
		错误框( "不能分配内存用以复制文件名到剪贴板的临时缓冲区" );
		return (INT_PTR)TRUE;
	}
	strcpy( acCopyText , 选项卡页节点->acFilename );
	GlobalUnlock(hgl);

	OpenClipboard( 全_主窗口句柄 );
	EmptyClipboard();
	SetClipboardData( CF_TEXT , acCopyText );
	CloseClipboard();

	return 0;
}

int 复制路径名称( struct 选项卡页面S *选项卡页节点 )
{
	HGLOBAL hgl = (char*)GlobalAlloc( GMEM_MOVEABLE , strlen(选项卡页节点->acPathName)+1 ) ;
	char *acCopyText = (char*)GlobalLock(hgl) ;
	if( acCopyText == NULL )
	{
		错误框( "不能分配内存用以复制路径名到剪贴板的临时缓冲区" );
		return (INT_PTR)TRUE;
	}
	strcpy( acCopyText , 选项卡页节点->acPathName );
	GlobalUnlock(hgl);

	OpenClipboard( 全_主窗口句柄 );
	EmptyClipboard();
	SetClipboardData( CF_TEXT , acCopyText );
	CloseClipboard();

	return 0;
}

int 复制路径文件名称( struct 选项卡页面S *选项卡页节点 )
{
	HGLOBAL hgl = (char*)GlobalAlloc( GMEM_MOVEABLE , strlen(选项卡页节点->acPathFilename)+1 ) ;
	char *acCopyText = (char*)GlobalLock(hgl) ;
	if( acCopyText == NULL )
	{
		错误框( "不能分配内存用以复制路径文件名到剪贴板的临时缓冲区" );
		return (INT_PTR)TRUE;
	}
	strcpy( acCopyText , 选项卡页节点->acPathFilename );
	GlobalUnlock(hgl);

	OpenClipboard( 全_主窗口句柄 );
	EmptyClipboard();
	SetClipboardData( CF_TEXT , acCopyText );
	CloseClipboard();

	return 0;
}

int 粘贴行( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		int	当前文本字节位置 ;
		int	nCurrentLine ;
		int	nNextLineStartPos ;

		当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
		nNextLineStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine+1, 0 ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GOTOPOS, nNextLineStartPos, 0 ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_PASTE, 0, 0 );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GOTOPOS, nNextLineStartPos, 0 ) ;
	}

	return 0;
}

int 往上粘贴行( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		int	当前文本字节位置 ;
		int	nCurrentLine ;
		int	nCurrentLineStartPos ;

		当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
		nCurrentLineStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GOTOPOS, nCurrentLineStartPos, 0 ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_PASTE, 0, 0 );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GOTOPOS, nCurrentLineStartPos, 0 ) ;
	}

	return 0;
}

int 删除行( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEDELETE, 0, 0 );
	return 0;
}

int 删除行首空白字符( struct 选项卡页面S *选项卡页节点 )
{
	int	nSelectStartLine , nSelectEndLine ;
	int	nLine ;
	int	nLineStartPos , nLineEndPos , nLineStringLength ;
	char	*acLineBuffer = NULL ;
	int	LineBufferSize = 0 ;
	int	nDeleteLength ;

	if( 选项卡页节点 == NULL )
		return 0;

	GetEditorEffectStartAndEndLine( 选项卡页节点 , & nSelectStartLine , & nSelectEndLine );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
	for( nLine = nSelectStartLine ; nLine <= nSelectEndLine ; nLine++ )
	{
		nLineStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nLine, 0 ) ;
		nLineEndPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETLINEENDPOSITION, nLine, 0 ) ;
		nLineStringLength = nLineEndPos - nLineStartPos ;
		if( acLineBuffer == NULL || nLineStringLength+1 > LineBufferSize )
		{
			int nTmpBufferSize = nLineStringLength+1 ;
			char *acTmpBuffer = NULL ;

			acTmpBuffer = (char*)realloc( acLineBuffer , nTmpBufferSize ) ;
			if( acTmpBuffer == NULL )
			{
				if( acLineBuffer )
					free( acLineBuffer );
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_ENDUNDOACTION, 0, 0 );
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_UNDO, 0, 0 );
				return -1;
			}
			acLineBuffer = acTmpBuffer ;
			LineBufferSize = nTmpBufferSize ;
		}

		memset( acLineBuffer , 0x00 , LineBufferSize );
		struct Sci_TextRange	tr ;
		tr.chrg.cpMin = nLineStartPos;
		tr.chrg.cpMax = nLineEndPos;
		tr.lpstrText = acLineBuffer;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXTRANGE , 0 , (sptr_t)(&tr) );
		for( nDeleteLength = 0 ; nDeleteLength < nLineStringLength ; nDeleteLength++ )
		{
			if( acLineBuffer[nDeleteLength] == ' ' || acLineBuffer[nDeleteLength] == '\t' )
				;
			else
				break;
		}
		if( nDeleteLength > 0 )
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_DELETERANGE, nLineStartPos, nDeleteLength );
	}
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

	if( acLineBuffer )
		free( acLineBuffer );

	return 0;
}

int 删除行尾空白字符( struct 选项卡页面S *选项卡页节点 )
{
	int	nSelectStartLine , nSelectEndLine ;
	int	nLine ;
	int	nLineStartPos , nLineEndPos , nLineStringLength ;
	char	*acLineBuffer = NULL ;
	int	LineBufferSize = 0 ;
	int	nDeleteLength ;

	if( 选项卡页节点 == NULL )
		return 0;

	GetEditorEffectStartAndEndLine( 选项卡页节点 , & nSelectStartLine , & nSelectEndLine );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
	for( nLine = nSelectStartLine ; nLine <= nSelectEndLine ; nLine++ )
	{
		nLineStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nLine, 0 ) ;
		nLineEndPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETLINEENDPOSITION, nLine, 0 ) ;
		nLineStringLength = nLineEndPos - nLineStartPos ;
		if( acLineBuffer == NULL || nLineStringLength+1 > LineBufferSize )
		{
			int nTmpBufferSize = nLineStringLength+1 ;
			char *acTmpBuffer = NULL ;

			acTmpBuffer = (char*)realloc( acLineBuffer , nTmpBufferSize ) ;
			if( acTmpBuffer == NULL )
			{
				if( acLineBuffer )
					free( acLineBuffer );
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_ENDUNDOACTION, 0, 0 );
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_UNDO, 0, 0 );
				return -1;
			}
			acLineBuffer = acTmpBuffer ;
			LineBufferSize = nTmpBufferSize ;
		}

		memset( acLineBuffer , 0x00 , LineBufferSize );
		struct Sci_TextRange	tr ;
		tr.chrg.cpMin = nLineStartPos;
		tr.chrg.cpMax = nLineEndPos;
		tr.lpstrText = acLineBuffer;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXTRANGE , 0 , (sptr_t)(&tr) );
		for( nDeleteLength = 0 ; nDeleteLength < nLineStringLength ; nDeleteLength++ )
		{
			if( acLineBuffer[nLineStringLength-1-nDeleteLength] == ' ' || acLineBuffer[nLineStringLength-1-nDeleteLength] == '\t' )
				;
			else
				break;
		}
		if( nDeleteLength > 0 )
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_DELETERANGE, nLineEndPos-nDeleteLength, nDeleteLength );
	}
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

	if( acLineBuffer )
		free( acLineBuffer );

	return 0;
}

int OnDeleteBlankLine( struct 选项卡页面S *选项卡页节点 )
{
	int	nSelectStartLine , nSelectEndLine ;
	int	nLine ;
	int	nLineStartPos , nLineEndPos , nLineStringLength , nLineLength ;

	if( 选项卡页节点 == NULL )
		return 0;

	GetEditorEffectStartAndEndLine( 选项卡页节点 , & nSelectStartLine , & nSelectEndLine );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
	for( nLine = nSelectStartLine ; nLine <= nSelectEndLine ; nLine++ )
	{
		nLineStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nLine, 0 ) ;
		nLineEndPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETLINEENDPOSITION, nLine, 0 ) ;
		nLineStringLength = nLineEndPos - nLineStartPos ;
		nLineLength = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINELENGTH, nLine, 0 ) ;
		if( nLineStringLength == 0 )
		{
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_DELETERANGE, nLineStartPos, nLineLength );
			nLine--;
			nSelectEndLine--;
		}
	}
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

	return 0;
}

int OnDeleteBlankLineWithWhiteCharacter( struct 选项卡页面S *选项卡页节点 )
{
	int	nSelectStartLine , nSelectEndLine ;
	int	nLine ;
	int	nLineStartPos , nLineEndPos , nLineStringLength , nLineLength ;
	char	*acLineBuffer = NULL ;
	int	LineBufferSize = 0 ;
	int	nDeleteLength ;

	if( 选项卡页节点 == NULL )
		return 0;

	GetEditorEffectStartAndEndLine( 选项卡页节点 , & nSelectStartLine , & nSelectEndLine );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
	for( nLine = nSelectStartLine ; nLine <= nSelectEndLine ; nLine++ )
	{
		nLineStartPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_POSITIONFROMLINE, nLine, 0 ) ;
		nLineEndPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETLINEENDPOSITION, nLine, 0 ) ;
		nLineStringLength = nLineEndPos - nLineStartPos ;
		nLineLength = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINELENGTH, nLine, 0 ) ;
		if( acLineBuffer == NULL || nLineStringLength+1 > LineBufferSize )
		{
			int nTmpBufferSize = nLineStringLength+1 ;
			char *acTmpBuffer = NULL ;

			acTmpBuffer = (char*)realloc( acLineBuffer , nTmpBufferSize ) ;
			if( acTmpBuffer == NULL )
			{
				if( acLineBuffer )
					free( acLineBuffer );
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_ENDUNDOACTION, 0, 0 );
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_UNDO, 0, 0 );
				return -1;
			}
			acLineBuffer = acTmpBuffer ;
			LineBufferSize = nTmpBufferSize ;
		}

		memset( acLineBuffer , 0x00 , LineBufferSize );
		struct Sci_TextRange	tr ;
		tr.chrg.cpMin = nLineStartPos;
		tr.chrg.cpMax = nLineEndPos;
		tr.lpstrText = acLineBuffer;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GETTEXTRANGE , 0 , (sptr_t)(&tr) );
		for( nDeleteLength = 0 ; nDeleteLength < nLineStringLength ; nDeleteLength++ )
		{
			if( acLineBuffer[nLineStringLength-1-nDeleteLength] == ' ' || acLineBuffer[nLineStringLength-1-nDeleteLength] == '\t' )
				;
			else
				break;
		}
		if( nDeleteLength == nLineStringLength )
		{
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_DELETERANGE, nLineStartPos, nLineLength );
			nLine--;
			nSelectEndLine--;
		}
	}
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

	if( acLineBuffer )
		free( acLineBuffer );

	return 0;
}

int OnJoinLine( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		int	当前文本字节位置 ;
		int	nCurrentLine ;
		int	nCurrentLineEndPos ;

		当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;
		nCurrentLineEndPos = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETLINEENDPOSITION, nCurrentLine, 0 ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GOTOPOS, nCurrentLineEndPos, 0 ) ;
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_CLEAR, 0, 0 );
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GOTOPOS, 当前文本字节位置, 0 ) ;
	}

	return 0;
}

int OnLowerCaseEdit( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LOWERCASE, 0, 0 );
	return 0;
}

int OnUpperCaseEdit( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_UPPERCASE, 0, 0 );
	return 0;
}

int OnEditEnableAutoAddCloseChar( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.启用自动添加闭合字符 == FALSE )
	{
		全_风格主配置.启用自动添加闭合字符 = TRUE ;
	}
	else
	{
		全_风格主配置.启用自动添加闭合字符 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	return 0;
}

int OnEditEnableAutoIdentation( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.启用自动识别 == FALSE )
	{
		全_风格主配置.启用自动识别 = TRUE ;
	}
	else
	{
		全_风格主配置.启用自动识别 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	return 0;
}

int OnEditBase64Encoding( struct 选项卡页面S *选项卡页节点 )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	size_t		nOutputTextLength ;
	char		*acOutputText = NULL ;
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 0 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nOutputTextLength = nSelTextLength * 2 ;
	acOutputText = (char*)malloc( nOutputTextLength+1 ) ;
	if( acOutputText == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存以存放目标字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		return -1;
	}
	memset( acOutputText , 0x00 , nOutputTextLength+1 );
	nret = EVP_EncodeBlock( (unsigned char *)acOutputText , (unsigned char *)acSelText ,(int)nSelTextLength ) ;
	if( nret < 0 )
	{
		MessageBox(NULL, TEXT("BASE64编码失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		free( acOutputText );
		return -1;
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputText );

	free( acSelText );
	free( acOutputText );

	return 0;
}

int OnEditBase64Decoding( struct 选项卡页面S *选项卡页节点 )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	size_t		nOutputTextLength ;
	char		*acOutputText = NULL ;
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 0 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nOutputTextLength = nSelTextLength ;
	acOutputText = (char*)malloc( nOutputTextLength+1 ) ;
	if( acOutputText == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存以存放目标字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		return -1;
	}
	memset( acOutputText , 0x00 , nOutputTextLength+1 );
	nret = EVP_DecodeBlock( (unsigned char *)acOutputText , (unsigned char *)acSelText , (int)nSelTextLength ) ;
	if( nret < 0 )
	{
		MessageBox(NULL, TEXT("BASE64解码失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		free( acOutputText );
		return -1;
	}

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputText );

	free( acSelText );
	free( acOutputText );

	return 0;
}

int OnEditMd5( struct 选项卡页面S *选项卡页节点 )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	char		acOutputText[ MD5_DIGEST_LENGTH + 1 ] ;
	char		acOutputTextExp[ MD5_DIGEST_LENGTH * 2 + 1 ] ;
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 0 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	memset( acOutputText , 0x00 , sizeof(acOutputText) );
	MD5( (unsigned char *)acSelText , (int)nSelTextLength , (unsigned char *)acOutputText );
	memset( acOutputTextExp , 0x00 , sizeof(acOutputTextExp) );
	HexExpand( acOutputText , (int)strlen(acOutputText) , acOutputTextExp );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputTextExp );

	free( acSelText );

	return 0;
}

int OnEditSha1( struct 选项卡页面S *选项卡页节点 )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	char		acOutputText[ SHA_DIGEST_LENGTH + 1 ] ;
	char		acOutputTextExp[ SHA_DIGEST_LENGTH * 2 + 1 ] ;
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 0 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	memset( acOutputText , 0x00 , sizeof(acOutputText) );
	SHA1( (unsigned char *)acSelText , (int)nSelTextLength , (unsigned char *)acOutputText );
	memset( acOutputTextExp , 0x00 , sizeof(acOutputTextExp) );
	HexExpand( acOutputText , (int)strlen(acOutputText) , acOutputTextExp );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputTextExp );

	free( acSelText );

	return 0;
}

int OnEditSha256( struct 选项卡页面S *选项卡页节点 )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	char		acOutputText[ SHA256_DIGEST_LENGTH + 1 ] ;
	char		acOutputTextExp[ SHA256_DIGEST_LENGTH * 2 + 1 ] ;
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 0 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	memset( acOutputText , 0x00 , sizeof(acOutputText) );
	SHA256( (unsigned char *)acSelText , (int)nSelTextLength , (unsigned char *)acOutputText );
	memset( acOutputTextExp , 0x00 , sizeof(acOutputTextExp) );
	HexExpand( acOutputText , (int)strlen(acOutputText) , acOutputTextExp );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputTextExp );

	free( acSelText );

	return 0;
}

int OnEdit3DesCbcEncrypto( struct 选项卡页面S *选项卡页节点 )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	long		nOutputTextLength ;
	char		*acOutputText = NULL ;
	char		*acOutputTextExp = NULL ;
	char		key[ 24 + 1 ] ;
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 8 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nOutputTextLength = (long)nSelTextLength ;
	acOutputText = (char*)malloc( nOutputTextLength + 1 ) ;
	if( acOutputText == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存以存放目标字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acOutputText , 0x00 , nOutputTextLength + 1 );

	acOutputTextExp = (char*)malloc( nOutputTextLength * 2 + 1 ) ;
	if( acOutputTextExp == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存以存放目标字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}
	memset( acOutputTextExp , 0x00 , nOutputTextLength * 2 + 1 );

	while(1)
	{
		memset( key , 0x00 , sizeof(key) );
		nret = InputBox( 全_主窗口句柄 , "请输入加密密钥：" , "输入窗口" , 0 , key , sizeof(key)-1 ) ;
		if( nret == IDOK )
		{
			break;
		}
		else
		{
			free( acSelText );
			free( acOutputText );
			free( acOutputTextExp );
			return 0;
		}
	}

	Encrypt_3DES_CBC_192bits( (unsigned char *)key , (unsigned char *)acSelText , (int)nSelTextLength , (unsigned char *)acOutputText , & nOutputTextLength , NULL );
	memset( acOutputTextExp , 0x00 , sizeof(acOutputTextExp) );
	HexExpand( acOutputText , (int)nOutputTextLength , acOutputTextExp );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputTextExp );

	free( acSelText );
	free( acOutputText );
	free( acOutputTextExp );

	return 0;
}

int OnEdit3DesCbcDecrypto( struct 选项卡页面S *选项卡页节点 )
{
	size_t		nSelTextLength ;
	char		*acSelText = NULL ;
	size_t		nInputTextLength ;
	char		*acInputText = NULL ;
	long		nOutputTextLength ;
	char		*acOutputText = NULL ;
	char		key[ 24 + 1 ] ;
	int		nret = 0 ;

	if( 选项卡页节点 == NULL )
		return 0;

	acSelText = StrdupEditorSelection( & nSelTextLength , 8 ) ;
	if( acSelText == NULL )
	{
		if( nSelTextLength == 0 )
			return 0;
		MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nInputTextLength = nSelTextLength / 2 ;
	acInputText = (char*)malloc( nInputTextLength + 1 ) ;
	if( acInputText == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存以存放源字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		return -1;
	}
	memset( acInputText , 0x00 , nInputTextLength + 1 );
	HexFold( acSelText , (int)nSelTextLength , acInputText );

	nOutputTextLength = (long)nSelTextLength ;
	acOutputText = (char*)malloc( nInputTextLength + 1 ) ;
	if( acOutputText == NULL )
	{
		MessageBox(NULL, TEXT("不能分配内存以存放目标字符串"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( acSelText );
		free( acInputText );
		return -1;
	}
	memset( acOutputText , 0x00 , nInputTextLength + 1 );

	while(1)
	{
		memset( key , 0x00 , sizeof(key) );
		nret = InputBox( 全_主窗口句柄 , "请输入解密密钥：" , "输入窗口" , 0 , key , sizeof(key)-1 ) ;
		if( nret == IDOK )
		{
			break;
		}
		else
		{
			free( acSelText );
			free( acInputText );
			free( acOutputText );
			return 0;
		}
	}

	Decrypt_3DES_CBC_192bits( (unsigned char *)key , (unsigned char *)acInputText , (int)nInputTextLength , (unsigned char *)acOutputText , & nOutputTextLength , NULL );

	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)acOutputText );

	free( acSelText );
	free( acInputText );
	free( acOutputText );

	return 0;
}

