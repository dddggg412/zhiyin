#ifndef _ENCODING_
#define _ENCODING_

#include "framework.h"

#define �ַ�����_UTF8		65001
#define �ַ�����_UTF8_STR	"UTF-8"

#define �ַ�����_GBK		936
#define �ַ�����_GBK_STR	"GBK"

#define �ַ�����_BIG5		950
#define �ַ�����_BIG5_STR	"BIG5"


typedef void funcInitProbeReader( void *context );
typedef int funcDoProbeReader( void *context , unsigned char *p_ch , size_t *p_counter );

/* UTF8 */

funcInitProbeReader	funcInitProbeReader_UTF8 ;
funcDoProbeReader	funcDoProbeReader_UTF8 ;

struct EncodingProbeContext_UTF8
{
	int	flag ;
	int	step ;
	int	skip ;
} ;

extern struct EncodingProbeContext_UTF8	g_stEncodingProbeContext_UTF8 ;

/* GB18030 */

funcInitProbeReader	funcInitProbeReader_GB18030 ;
funcDoProbeReader	funcDoProbeReader_GB18030 ;

struct EncodingProbeContext_GB18030
{
	int	skip ;
} ;

extern struct EncodingProbeContext_GB18030	g_stEncodingProbeContext_GB18030 ;

/* BIG5 */

funcInitProbeReader	funcInitProbeReader_BIG5 ;
funcDoProbeReader	funcDoProbeReader_BIG5 ;

struct EncodingProbeContext_BIG5
{
	int	flag ;
	int	skip ;
} ;

extern struct EncodingProbeContext_BIG5	g_stEncodingProbeContext_BIG5 ;

/* Encoding probe */

struct EncodingProbe
{
	UINT			nCodePage ;
	const char		*acCodePage ;
	funcInitProbeReader	*pfuncInitProbeReader ;
	funcDoProbeReader	*pfuncDoProbeReader ;
	void			*pContext ;
	size_t			nProbeCounter ;
} ;

void InitEncodingProbe( struct EncodingProbe *ep );
void DoEncodingProbe( struct EncodingProbe *ep , char *str , size_t len );
UINT GetCodePageFromEncodingProbe( struct EncodingProbe *ep );

const char *GetEncodingString( UINT nCodePage );

/* New line probe */
struct NewLineProbe
{
	int			nNewLineMode ;
};

void InitNewLineModeProbe( struct NewLineProbe *elp );
void DoNewLineModeProbe( struct NewLineProbe *elp , char *str , size_t len );
int GetNewLineModeFromProbe( struct NewLineProbe *elp , int nNewLineModeDefault );

#endif
