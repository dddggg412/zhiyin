#ifndef _CONFIG_
#define _CONFIG_

#include "framework.h"

#define FILEBUF_MAX	1024*1024

struct 主配置S
{
	BOOL		在启动时创建新文件 ;
	BOOL		打开退出时打开的文件 ;
	char		启动时打开文件[启动时打开文件最大数量M][MAX_PATH] ;
	char		启动时的活动文件[MAX_PATH] ;
	BOOL		打开文件后设置为只读 ;
	char		最近打开路径文件名[最近打开路径文件名最大数量M][MAX_PATH] ;
	BOOL		检查被选中选项卡页面的更新 ;
	BOOL		提示自动更新 ;
	int		    新建文件Eols ;
	UINT		新建文件编码 ;
	BOOL		启用自动添加闭合字符 ;
	BOOL		启用自动识别 ;

	char		窗口的主题[MAX_PATH] ;
	BOOL		启用窗口视觉样式 ;
	BOOL		启用行号 ;
	BOOL		启用书签 ;
	BOOL		启用空格符 ;
	int		    空格符大小 ;
	BOOL		新行可见 ;
	BOOL		启用缩进辅助线 ;

	int		    选项卡宽度 ;
	BOOL		按下Tab键转换为空格 ;

	BOOL		换行模式 ;

	BOOL		显示文件树栏 ;
	int		文件树栏宽度 ;
	int		符号列表的宽度 ;
	int		符号树宽度 ;
	int		Sql查询结果编辑框高度 ;
	int		Sql查询结果列表视图高度 ;

	int		重新加载符号列表或树间隔 ;
	BOOL		启用块折叠 ;
	BOOL		启用自动完成显示 ;
	int		输入字符后自动完成显示 ;
	BOOL		启用调用提示显示 ;

	char		进程文件命令[ 1024 ] ;
	char		进程文本命令[ 1024 ] ;
} ;

void 设置默认主配置( struct 主配置S *pstEditUltraConfig );

int LoadMainConfigFile( char *filebuf );
void FoldConfigStringItemValue( char *value );
int DecomposeConfigFileBuffer( char *filebuf , char **ppcItemName , char **ppcItemValue );
int 加载配置();
void ExpandConfigStringItemValue( FILE *fp , char *value );
int 保存主配置文件();

#endif
