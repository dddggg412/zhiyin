// #pragma once

#ifndef _
#define _

#include "framework.h"

extern char		**全_argv ;
extern int		全_argc ;
extern HINSTANCE	全_当前实例;
extern HWND		全_主窗口句柄;
extern char		全_应用程序名称[100];

/*
extern HWND		g_hwndToolBar_FileMenu ;
extern HWND		g_hwndToolBar_EditMenu ;
extern HWND		g_hwndReBar ;
extern int		g_nReBarHeight ;
*/
extern HWND		全_工具栏句柄 ;
extern int		全_工具栏高度 ;

extern pcre		*pcreCppFunctionRe ;

extern char		全_模块文件名[ MAX_PATH ] ;
extern char		全_模块路径名[ MAX_PATH ] ;

extern int		全_缩放重置 ;

int 创建新窗口( HWND hWnd , WPARAM wParam , LPARAM lParam );
void 更新所有窗口( HWND hWnd );
void 重置窗口大小( HWND hWnd , int nWidth , int nHeight );

#define CONFIG_KEY_MATERIAL_EDITULTRA	"EDITUL"

#endif
