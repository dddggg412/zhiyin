#include "framework.h"

void 设置默认主配置( struct 主配置S *pstEditUltraMainConfig )
{
	pstEditUltraMainConfig->在启动时创建新文件 = TRUE ;
	pstEditUltraMainConfig->打开退出时打开的文件 = FALSE ;
	pstEditUltraMainConfig->打开文件后设置为只读 = FALSE ;
	memset( pstEditUltraMainConfig->最近打开路径文件名 , 0x00 , sizeof(pstEditUltraMainConfig->最近打开路径文件名) );
	memset( pstEditUltraMainConfig->启动时的活动文件 , 0x00 , sizeof(pstEditUltraMainConfig->启动时的活动文件) );

	pstEditUltraMainConfig->启用自动添加闭合字符 = TRUE ;
	pstEditUltraMainConfig->启用自动识别 = TRUE ;
	pstEditUltraMainConfig->检查被选中选项卡页面的更新 = FALSE ;
	pstEditUltraMainConfig->提示自动更新 = TRUE ;
	pstEditUltraMainConfig->新建文件Eols = 0 ;
	pstEditUltraMainConfig->新建文件编码 = 65001 ;

	pstEditUltraMainConfig->启用窗口视觉样式 = TRUE ;
	pstEditUltraMainConfig->启用行号 = TRUE ;
	pstEditUltraMainConfig->启用书签 = TRUE ;
	pstEditUltraMainConfig->启用空格符 = FALSE ;
	pstEditUltraMainConfig->空格符大小 = 空格_大小 ;
	pstEditUltraMainConfig->新行可见 = FALSE ;
	pstEditUltraMainConfig->启用缩进辅助线 = TRUE ;

	pstEditUltraMainConfig->启用块折叠 = TRUE ;

	strcpy( pstEditUltraMainConfig->进程文件命令 , "\"C:\\Windows\\notepad.exe\" \"%F\"" );
	strcpy( pstEditUltraMainConfig->进程文本命令 , "\"C:\\Program Files\\Mozilla Firefox\\firefox.exe\" \"https://www.baidu.com/s?wd=%T\"" );

	pstEditUltraMainConfig->显示文件树栏 = FALSE ;
	pstEditUltraMainConfig->文件树栏宽度 = 符号树栏_宽度_默认 ;
	pstEditUltraMainConfig->选项卡宽度 = 编辑框_选项卡_宽度_默认 ;
	pstEditUltraMainConfig->换行模式 = TRUE ;
	pstEditUltraMainConfig->符号列表的宽度 = 符号列表_宽度_默认 ;
	pstEditUltraMainConfig->符号树宽度 = 符号树_宽度_默认 ;
	pstEditUltraMainConfig->Sql查询结果编辑框高度 = SQL查询结果_编辑_高度_默认 ;
	pstEditUltraMainConfig->Sql查询结果列表视图高度 = SQL查询结果_列表视图_高度_默认 ;

	pstEditUltraMainConfig->启用自动完成显示 = TRUE ;
	pstEditUltraMainConfig->输入字符后自动完成显示 = 1 ;
	pstEditUltraMainConfig->启用调用提示显示 = TRUE ;

	return;
}

int LoadMainConfigFile( char *filebuf )
{
	char				pathfilename[ MAX_PATH ] ;
	FILE				*fp = NULL ;
	char				*pcItemName = NULL ;
	char				*pcItemValue = NULL ;
	char				*p1 = NULL ;
	char				*pcResult = NULL ;
	struct CallTipShowNode		*pstCallTipShowNode = NULL ;
	int				nret = 0 ;

	memset( pathfilename , 0x00 , sizeof(pathfilename) );
	_snprintf( pathfilename , sizeof(pathfilename)-1 , "%s\\conf\\eux.conf" , 全_模块路径名 );
	fp = fopen( pathfilename , "r" ) ;
	if( fp == NULL )
		return -2;

	while(1)
	{
		memset( filebuf , 0x00 , FILEBUF_MAX );
		if( fgets( filebuf , FILEBUF_MAX-1 , fp ) == NULL )
			break;

		nret = DecomposeConfigFileBuffer( filebuf , & pcItemName , & pcItemValue ) ;
		if( nret > 0 )
			continue;
		else if( nret < 0 )
			return nret;

		if( strcmp( pcItemName , "create_newfile_on_newboot" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.在启动时创建新文件 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.在启动时创建新文件 = FALSE ;
		}
		else if( strcmp( pcItemName , "openfiles_that_openning_on_exit" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.打开退出时打开的文件 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.打开退出时打开的文件 = FALSE ;
		}
		else if( strncmp( pcItemName , "openfile_on_boot_" , 17 ) == 0 )
		{
			int	index = atoi(pcItemName+17) ;

			strncpy( 全_风格主配置.启动时打开文件[index%启动时打开文件最大数量M] , pcItemValue , sizeof(全_风格主配置.启动时打开文件[index%启动时打开文件最大数量M])-1 );
		}
		else if( strcmp( pcItemName , "active_file_on_boot" ) == 0 )
		{
			strncpy( 全_风格主配置.启动时的活动文件 , pcItemValue , sizeof(全_风格主配置.启动时的活动文件)-1 );
		}
		else if( strcmp( pcItemName , "set_read_only_after_open_file" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.打开文件后设置为只读 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.打开文件后设置为只读 = FALSE ;
		}
		else if( strncmp( pcItemName , "open_pathfilename_recently_" , 27 ) == 0 )
		{
			int	index = atoi(pcItemName+27) ;

			strncpy( 全_风格主配置.最近打开路径文件名[index%最近打开路径文件名最大数量M] , pcItemValue , sizeof(全_风格主配置.最近打开路径文件名[index%最近打开路径文件名最大数量M])-1 );
		}
		else if( strcmp( pcItemName , "check_update_where_selecttabpage" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.检查被选中选项卡页面的更新 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.检查被选中选项卡页面的更新 = FALSE ;
		}
		else if( strcmp( pcItemName , "prompt_where_autoupdate" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.提示自动更新 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.提示自动更新 = FALSE ;
		}
		else if( strcmp( pcItemName , "newfile_eols" ) == 0 )
		{
			全_风格主配置.新建文件Eols = atoi( pcItemValue ) ;
		}
		else if( strcmp( pcItemName , "newfile_encoding" ) == 0 )
		{
			全_风格主配置.新建文件编码 = atoi( pcItemValue ) ;
		}
		else if( strcmp( pcItemName , "enable_auto_add_close_char" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.启用自动添加闭合字符 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.启用自动添加闭合字符 = FALSE ;
		}
		else if( strcmp( pcItemName , "enable_auto_identation" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.启用自动识别 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.启用自动识别 = FALSE ;
		}
		else if( strcmp( pcItemName , "window_theme" ) == 0 )
		{
			strncpy( 全_风格主配置.窗口的主题 , pcItemValue , sizeof(全_风格主配置.窗口的主题)-1 );
		}
		else if( strcmp( pcItemName , "enable_windows_visual_styles" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.启用窗口视觉样式 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.启用窗口视觉样式 = FALSE ;
		}
		else if( strcmp( pcItemName , "tab_width" ) == 0 )
		{
			全_风格主配置.选项卡宽度 = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "onkeydown_tab_convert_spaces" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.按下Tab键转换为空格 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.按下Tab键转换为空格 = FALSE ;
		}
		else if( strcmp( pcItemName , "wrapline_mode" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.换行模式 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.换行模式 = FALSE ;
		}
		else if( strcmp( pcItemName , "line_number_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.启用行号 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.启用行号 = FALSE ;
		}
		else if( strcmp( pcItemName , "bookmark_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.启用书签 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.启用书签 = FALSE ;
		}
		else if( strcmp( pcItemName , "white_space_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.启用空格符 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.启用空格符 = FALSE ;
		}
		else if( strcmp( pcItemName , "white_space_size" ) == 0 )
		{
			全_风格主配置.空格符大小 = atoi( pcItemValue ) ;
		}
		else if( strcmp( pcItemName , "newline_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.新行可见 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.新行可见 = FALSE ;
		}
		else if( strcmp( pcItemName , "indentation_guides_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.启用缩进辅助线 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.启用缩进辅助线 = FALSE ;
		}
		else if( strcmp( pcItemName , "file_treebar_visiable" ) == 0 )
		{
		if( _stricmp( pcItemValue , "TRUE" ) == 0 )
			全_风格主配置.显示文件树栏 = TRUE ;
		else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
			全_风格主配置.显示文件树栏 = FALSE ;
		}
		else if( strcmp( pcItemName , "file_treebar_width" ) == 0 )
		{
			全_风格主配置.文件树栏宽度 = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "symbol_list_width" ) == 0 )
		{
			全_风格主配置.符号列表的宽度 = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "symbol_tree_width" ) == 0 )
		{
			全_风格主配置.符号树宽度 = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "sqlquery_result_edit_height" ) == 0 )
		{
			全_风格主配置.Sql查询结果编辑框高度 = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "sqlquery_result_listview_height" ) == 0 )
		{
			全_风格主配置.Sql查询结果列表视图高度 = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "reload_symbollis_or_tree_interval" ) == 0 )
		{
			全_风格主配置.重新加载符号列表或树间隔 = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "block_fold_visiable" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.启用块折叠 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.启用块折叠 = FALSE ;
		}
		else if( strcmp( pcItemName , "enable_auto_completed_show" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.启用自动完成显示 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.启用自动完成显示 = FALSE ;
		}
		else if( strcmp( pcItemName , "auto_completed_show_after_input_characters" ) == 0 )
		{
			全_风格主配置.输入字符后自动完成显示 = atoi( pcItemValue );
		}
		else if( strcmp( pcItemName , "enable_call_tip_show" ) == 0 )
		{
			if( _stricmp( pcItemValue , "TRUE" ) == 0 )
				全_风格主配置.启用调用提示显示 = TRUE ;
			else if( _stricmp( pcItemValue , "FALSE" ) == 0 )
				全_风格主配置.启用调用提示显示 = FALSE ;
		}
		else if( strcmp( pcItemName , "process_file_command" ) == 0 )
		{
			strncpy( 全_风格主配置.进程文件命令 , pcItemValue , sizeof(全_风格主配置.进程文件命令)-1 );
		}
		else if( strcmp( pcItemName , "process_text_command" ) == 0 )
		{
			strncpy( 全_风格主配置.进程文本命令 , pcItemValue , sizeof(全_风格主配置.进程文本命令)-1 );
		}
	}

	return 0;
}

void FoldConfigStringItemValue( char *value )
{
	char	*p = NULL ;

	for( p = value ; *p ; p++ )
	{
		if( *p == '\\' && *(p+1) )
		{
			memmove( p , p+1 , strlen(p+1)+1 );
		}
	}

	return;
}

int DecomposeConfigFileBuffer( char *filebuf , char **ppcItemName , char **ppcItemValue )
{
	size_t		filebuf_len ;
	char		*filebuf_end = NULL ;
	char		*p1 = NULL ;
	char		*p2 = NULL ;
	char		*pcItemName = NULL ;
	char		*pcItemValue = NULL ;

	filebuf_len = strlen(filebuf) ;
	filebuf_end = filebuf + filebuf_len - 1 ;
	while( filebuf_end >= filebuf && strchr( " \t\r\n" , (*filebuf_end) ) )
	{
		filebuf_end--;
	}
	*(filebuf_end+1) = '\0' ;

	pcItemName = filebuf ;
	while( (*pcItemName) && strchr( " \t" , (*pcItemName) ) )
		pcItemName++;
	if( (*pcItemName) == '\0' )
		return 1;
	if( (*pcItemName) == '#' )
		return 1;

	p1 = pcItemName ;
	while( (*p1) && ! strchr( " \t=" , (*p1) ) )
		p1++;
	if( (*p1) == '\0' )
		return 1;

	p2 = p1 ;
	while( (*p2) && (*p2) != '=' )
		p2++;
	if( (*p2) == '\0' )
		return 1;

	pcItemValue = p2 + 1 ;
	while( (*pcItemValue) && strchr( " \t" , (*pcItemValue) ) )
		pcItemValue++;

	(*p1) = '\0' ;

	if( (*pcItemValue) == '"' )
	{
		pcItemValue++;
		for( p2 = pcItemValue ; *(p2) ; p2++ )
		{
			if( *(p2) == '\\' )
			{
				p2++;
				if( *(p2) == '\0' )
					break;
			}
			else if( *(p2) == '"' )
			{
				break;
			}
		}
		if( *(p2) == '\0' )
			return -1;

		*(p2) = '\0' ;
		FoldConfigStringItemValue( pcItemValue );
	}

	if( ppcItemName )
		(*ppcItemName) = pcItemName ;
	if( ppcItemValue )
		(*ppcItemValue) = pcItemValue ;

	return 0;
}

int 加载配置()
{
	char	*filebuf = NULL ;
	int	nret = 0 ;

	GenarateConfigKey();

	filebuf = (char*)malloc( FILEBUF_MAX ) ;
	if( filebuf == NULL )
	{
		MessageBox(NULL, TEXT("分配内存以存放配置文件行数据失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	nret = LoadMainConfigFile( filebuf ) ;
	if( nret )
	{
		MessageBox(NULL, TEXT("装载主配置失败，使用缺省配置"), TEXT("错误"), MB_ICONERROR | MB_OK);
	}

	nret = LoadAllStyleThemeConfigFiles( filebuf ) ;
	if( nret )
	{
		MessageBox(NULL, TEXT("装载风格主题配置失败，使用缺省配置"), TEXT("错误"), MB_ICONERROR | MB_OK);
	}

	for( struct 文档类型配置S *pstFileExtnameMapper = 全_文档类型配置 ; pstFileExtnameMapper->p文件扩展名 ; pstFileExtnameMapper++ )
	{
		if( pstFileExtnameMapper->p文件类型的配置文件名 == NULL )
			continue;

		nret = LoadLexerConfigFile( pstFileExtnameMapper->p文件类型的配置文件名 , pstFileExtnameMapper , filebuf ) ;
		if( nret < 0 )
		{
			free( filebuf );
			return -1;
		}
	}

	nret = LoadRemoteFileServersConfigFromFile( filebuf ) ;
	if( nret < 0 )
	{
		MessageBox(NULL, TEXT("装载远程文件服务器配置失败"), TEXT("错误"), MB_ICONERROR | MB_OK);
		free( filebuf );
		return -1;
	}

	free( filebuf );

	return 0;
}

void ExpandConfigStringItemValue( FILE *fp , char *value )
{
	char	*p = NULL ;

	fprintf( fp , "\"" );

	for( p = value ; *p ; p++ )
	{
		if( *p == '"' || *p == '\\' )
			fputc( '\\' , fp );
		fputc( *p , fp );
	}
	fprintf( fp , "\"\n" );

	return;
}

int 保存主配置文件()
{
	char		pathfilename[ MAX_PATH ] ;
	FILE		*fp = NULL ;

	memset( pathfilename , 0x00 , sizeof(pathfilename) );
	_snprintf( pathfilename , sizeof(pathfilename)-1 , "%s\\conf\\eux.conf" , 全_模块路径名 );
	fp = fopen( pathfilename , "w" ) ;
	if( fp == NULL )
		return -1;

	if( 全_风格主配置.在启动时创建新文件 == TRUE )
		fprintf( fp , "create_newfile_on_newboot = TRUE\n" );
	else if( 全_风格主配置.在启动时创建新文件 == FALSE )
		fprintf( fp , "create_newfile_on_newboot = FALSE\n" );
	else
		fprintf( fp , "create_newfile_on_newboot = FALSE\n" );

	if( 全_风格主配置.打开退出时打开的文件 == TRUE )
		fprintf( fp , "openfiles_that_openning_on_exit = TRUE\n" );
	else if( 全_风格主配置.打开退出时打开的文件 == FALSE )
		fprintf( fp , "openfiles_that_openning_on_exit = FALSE\n" );
	else
		fprintf( fp , "openfiles_that_openning_on_exit = FALSE\n" );

	for( int index = 0 ; 全_风格主配置.打开退出时打开的文件 == TRUE && index < 启动时打开文件最大数量M ; index++ )
	{
		if( 全_风格主配置.启动时打开文件[index][0] == '\0' )
			break;

		fprintf( fp , "openfile_on_boot_%d = " , index ); ExpandConfigStringItemValue( fp , 全_风格主配置.启动时打开文件[index] );
	}

	if( 全_风格主配置.打开文件后设置为只读 == TRUE )
		fprintf( fp , "set_read_only_after_open_file = TRUE\n" );
	else if( 全_风格主配置.打开文件后设置为只读 == FALSE )
		fprintf( fp , "set_read_only_after_open_file = FALSE\n" );
	else
		fprintf( fp , "set_read_only_after_open_file = FALSE\n" );

	for( int index = 0 ; index < 最近打开路径文件名最大数量M ; index++ )
	{
		if( 全_风格主配置.最近打开路径文件名[index][0] == '\0' )
			break;

		fprintf( fp , "open_pathfilename_recently_%d = " , index ); ExpandConfigStringItemValue( fp , 全_风格主配置.最近打开路径文件名[index] );
	}
	fprintf( fp , "active_file_on_boot = " ); ExpandConfigStringItemValue( fp , 全_风格主配置.启动时的活动文件 );

	if( 全_风格主配置.检查被选中选项卡页面的更新 == TRUE )
		fprintf( fp , "check_update_where_selecttabpage = TRUE\n" );
	else if( 全_风格主配置.检查被选中选项卡页面的更新 == FALSE )
		fprintf( fp , "check_update_where_selecttabpage = FALSE\n" );
	else
		fprintf( fp , "check_update_where_selecttabpage = TRUE\n" );

	if( 全_风格主配置.提示自动更新 == TRUE )
		fprintf( fp , "prompt_where_autoupdate = TRUE\n" );
	else if( 全_风格主配置.提示自动更新 == FALSE )
		fprintf( fp , "prompt_where_autoupdate = FALSE\n" );
	else
		fprintf( fp , "prompt_where_autoupdate = TRUE\n" );

	fprintf( fp , "newfile_eols = %d\n" , 全_风格主配置.新建文件Eols );

	fprintf( fp , "newfile_encoding = %d\n" , 全_风格主配置.新建文件编码 );

	if( 全_风格主配置.启用自动添加闭合字符 == TRUE )
		fprintf( fp , "enable_auto_add_close_char = TRUE\n" );
	else if( 全_风格主配置.启用自动添加闭合字符 == FALSE )
		fprintf( fp , "enable_auto_add_close_char = FALSE\n" );
	else
		fprintf( fp , "enable_auto_add_close_char = TRUE\n" );

	if( 全_风格主配置.启用自动识别 == TRUE )
		fprintf( fp , "enable_auto_identation = TRUE\n" );
	else if( 全_风格主配置.启用自动识别 == FALSE )
		fprintf( fp , "enable_auto_identation = FALSE\n" );
	else
		fprintf( fp , "enable_auto_identation = TRUE\n" );

	fprintf( fp , "window_theme = %s\n" , 全_风格主配置.窗口的主题 );

	if( 全_风格主配置.启用窗口视觉样式 == TRUE )
		fprintf( fp , "enable_windows_visual_styles = TRUE\n" );
	else if( 全_风格主配置.启用窗口视觉样式 == FALSE )
		fprintf( fp , "enable_windows_visual_styles = FALSE\n" );
	else
		fprintf( fp , "enable_windows_visual_styles = TRUE\n" );
	fprintf( fp , "tab_width = %d\n" , 全_风格主配置.选项卡宽度 );
	if( 全_风格主配置.按下Tab键转换为空格 == TRUE )
		fprintf( fp , "onkeydown_tab_convert_spaces = TRUE\n" );
	else if( 全_风格主配置.按下Tab键转换为空格 == FALSE )
		fprintf( fp , "onkeydown_tab_convert_spaces = FALSE\n" );
	else
		fprintf( fp , "onkeydown_tab_convert_spaces = FALSE\n" );

	if( 全_风格主配置.换行模式 == TRUE )
		fprintf( fp , "wrapline_mode = TRUE\n" );
	else if( 全_风格主配置.换行模式 == FALSE )
		fprintf( fp , "wrapline_mode = FALSE\n" );
	else
		fprintf( fp , "wrapline_mode = TRUE\n" );

	if( 全_风格主配置.启用行号 == TRUE )
		fprintf( fp , "line_number_visiable = TRUE\n" );
	else if( 全_风格主配置.启用行号 == FALSE )
		fprintf( fp , "line_number_visiable = FALSE\n" );
	else
		fprintf( fp , "line_number_visiable = TRUE\n" );

	if( 全_风格主配置.启用书签 == TRUE )
		fprintf( fp , "bookmark_visiable = TRUE\n" );
	else if( 全_风格主配置.启用书签 == FALSE )
		fprintf( fp , "bookmark_visiable = FALSE\n" );
	else
		fprintf( fp , "bookmark_visiable = TRUE\n" );

	if( 全_风格主配置.启用空格符 == TRUE )
		fprintf( fp , "white_space_visiable = TRUE\n" );
	else if( 全_风格主配置.启用空格符 == FALSE )
		fprintf( fp , "white_space_visiable = FALSE\n" );
	else
		fprintf( fp , "white_space_visiable = FALSE\n" );

	if( 全_风格主配置.新行可见 == TRUE )
		fprintf( fp , "newline_visiable = TRUE\n" );
	else if( 全_风格主配置.新行可见 == FALSE )
		fprintf( fp , "newline_visiable = FALSE\n" );
	else
		fprintf( fp , "newline_visiable = FALSE\n" );

	if( 全_风格主配置.启用缩进辅助线 == TRUE )
		fprintf( fp , "indentation_guides_visiable = TRUE\n" );
	else if( 全_风格主配置.启用缩进辅助线 == FALSE )
		fprintf( fp , "indentation_guides_visiable = FALSE\n" );
	else
		fprintf( fp , "indentation_guides_visiable = TRUE\n" );

	fprintf( fp , "white_space_size = %d\n" , 全_风格主配置.空格符大小 );

	if( 全_风格主配置.显示文件树栏 == TRUE )
		fprintf( fp , "file_treebar_visiable = TRUE\n" );
	else if( 全_风格主配置.显示文件树栏 == FALSE )
		fprintf( fp , "file_treebar_visiable = FALSE\n" );
	else
		fprintf( fp , "file_treebar_visiable = FALSE\n" );
	fprintf( fp , "file_treebar_width = %d\n" , 全_风格主配置.文件树栏宽度 );
	fprintf( fp , "symbol_list_width = %d\n" , 全_风格主配置.符号列表的宽度 );
	fprintf( fp , "symbol_tree_width = %d\n" , 全_风格主配置.符号树宽度 );
	fprintf( fp , "sqlquery_result_edit_height = %d\n" , 全_风格主配置.Sql查询结果编辑框高度 );
	fprintf( fp , "sqlquery_result_listview_height = %d\n" , 全_风格主配置.Sql查询结果列表视图高度 );

	fprintf( fp , "reload_symbollis_or_tree_interval = %d\n" , 全_风格主配置.重新加载符号列表或树间隔 );
	if( 全_风格主配置.启用块折叠 == TRUE )
		fprintf( fp , "block_fold_visiable = TRUE\n" );
	else if( 全_风格主配置.启用块折叠 == FALSE )
		fprintf( fp , "block_fold_visiable = FALSE\n" );
	else
		fprintf( fp , "block_fold_visiable = TRUE\n" );
	if( 全_风格主配置.启用自动完成显示 == TRUE )
		fprintf( fp , "auto_completed_show_enable = TRUE\n" );
	else if( 全_风格主配置.启用自动完成显示 == FALSE )
		fprintf( fp , "auto_completed_show_enable = FALSE\n" );
	else
		fprintf( fp , "auto_completed_show_enable = TRUE\n" );
	fprintf( fp , "auto_completed_show_after_input_characters = %d\n" , 全_风格主配置.输入字符后自动完成显示 );
	if( 全_风格主配置.启用调用提示显示 == TRUE )
		fprintf( fp , "call_tip_show_enable = TRUE\n" );
	else if( 全_风格主配置.启用调用提示显示 == FALSE )
		fprintf( fp , "call_tip_show_enable = FALSE\n" );
	else
		fprintf( fp , "call_tip_show_enable = TRUE\n" );

	fprintf( fp , "process_file_command = " ); ExpandConfigStringItemValue( fp , 全_风格主配置.进程文件命令 );
	fprintf( fp , "process_text_command = " ); ExpandConfigStringItemValue( fp , 全_风格主配置.进程文本命令 );

	fclose( fp );

	return 0;
}
