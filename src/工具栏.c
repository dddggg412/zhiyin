#include "framework.h"
void 创建主窗工具栏(HINSTANCE 当前实例句柄)
{
	全_工具栏句柄 = CreateWindow( TOOLBARCLASSNAME , "" , WS_CHILD|WS_VISIBLE|TBSTYLE_TOOLTIPS|TBSTYLE_FLAT , 0 , 0 , 0 , 0 , 全_主窗口句柄 , NULL , 当前实例句柄 , NULL ) ;
	HIMAGELIST h启用图像列表 = ImageList_Create( 16 , 16 , ILC_COLOR32|ILC_MASK , 27 , 0 ) ;
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_NEWFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_OPENFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_SAVEFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_SAVEFILEAS_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_SAVEALLFILES_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_CLOSEFILE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_REMOTEFILESERVERS_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_UNDO_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_REDO_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_CUTTEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_COPYTEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_PASTETEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_FIND_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_FINDPREV_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_FINDNEXT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_FOUNDLIST_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_REPLACE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_TOGGLE_BOOKMARK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_GOTO_PREV_BOOKMARK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_GOTO_NEXT_BOOKMARK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_NAVIGATEBACK_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_FILETREE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_STYLETHEME_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_HEXEDITMODE_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_ZOOMOUT_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_ZOOMIN_C)) , RGB(255,255,255) );
	ImageList_AddMasked( h启用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_ABOUT_C)) , RGB(255,255,255) );
	HIMAGELIST h禁用图像列表 = ImageList_Create( 16 , 16 , ILC_COLOR24|ILC_MASK , 27 , 0 ) ;
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_NEWFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_OPENFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_SAVEFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_SAVEFILEAS_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_SAVEALLFILES_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_CLOSEFILE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_REMOTEFILESERVERS_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_UNDO_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_REDO_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_CUTTEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_COPYTEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_PASTETEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_FIND_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_FINDPREV_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_FINDNEXT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_FOUNDLIST_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_REPLACE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_TOGGLE_BOOKMARK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_GOTO_PREV_BOOKMARK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_GOTO_NEXT_BOOKMARK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_NAVIGATEBACK_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_FILETREE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_STYLETHEME_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_HEXEDITMODE_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_ZOOMOUT_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_ZOOMIN_D)) , RGB(255,255,255) );
	ImageList_AddMasked( h禁用图像列表 , LoadBitmap(全_当前实例,MAKEINTRESOURCE(IDB_ABOUT_D)) , RGB(255,255,255) );
	TBBUTTON 工具栏按钮[] = {
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 0 , IDM_FILE_NEW , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"新建文件" } ,
		{ 1 , IDM_FILE_OPEN , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"打开文件..." } ,
		{ 2 , IDM_FILE_SAVE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"保存文件" } ,
		{ 3 , IDM_FILE_SAVEAS , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"另存为..." } ,
		{ 4 , IDM_FILE_SAVEALL , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"保存所有文件" } ,
		{ 5 , IDM_FILE_CLOSE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"关闭文件" } ,
		{ 6 , IDM_FILE_REMOTE_FILESERVERS , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"配置远程文件服务器..." } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 7 , IDM_EDIT_UNDO , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"撤销操作" } ,
		{ 8 , IDM_EDIT_REDO , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"重做操作" } ,
		{ 9 , IDM_EDIT_CUT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"剪切文本" } ,
		{ 10 , IDM_EDIT_COPY , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"复制文本" } ,
		{ 11 , IDM_EDIT_PASTE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"粘贴文本" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 12 , IDM_SEARCH_FIND , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"查找文本..." } ,
		{ 13 , IDM_SEARCH_FINDPREV , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"查找上一个文本" } ,
		{ 14 , IDM_SEARCH_FINDNEXT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"查找下一个文本" } ,
		{ 15 , IDM_SEARCH_FOUNDLIST , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"列出包含关键词的行..." } ,
		{ 16 , IDM_SEARCH_REPLACE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"替换文本" } ,
		{ 17 , IDM_SEARCH_TOGGLE_BOOKMARK , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"切换书签" } ,
		{ 18 , IDM_SEARCH_GOTO_PREV_BOOKMARK , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"跳到上一个书签" } ,
		{ 19 , IDM_SEARCH_GOTO_NEXT_BOOKMARK , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"跳到下一个书签" } ,
		{ 20 , IDM_SEARCH_NAVIGATEBACK_PREV_IN_THIS_FILE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"退回上一个位置" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 21 , IDM_VIEW_FILETREE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"显示/隐藏文件树" } ,
		{ 22 , IDM_VIEW_MODIFY_STYLETHEME , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"修改主题方案..." } ,
		{ 23 , IDM_VIEW_HEXEDIT_MODE , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"切换十六进制编辑视图" } ,
		{ 24 , IDM_VIEW_ZOOMOUT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"放大视图" } ,
		{ 25 , IDM_VIEW_ZOOMIN , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"缩小视图" } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
		{ 26 , IDM_ABOUT , TBSTATE_ENABLED , TBSTYLE_BUTTON , { 0 } , 0 , (INT_PTR)"关于..." } ,
		{ 0 , 0 , TBSTATE_ENABLED , TBSTYLE_SEP , { 0 } , 0 , 0 } ,
	} ;
	SendMessage( 全_工具栏句柄 , TB_BUTTONSTRUCTSIZE , (WPARAM)sizeof(TBBUTTON) , 0 );
	SendMessage( 全_工具栏句柄 , TB_ADDBUTTONS , (WPARAM)(sizeof(工具栏按钮)/sizeof(工具栏按钮[0])) , (LPARAM)&工具栏按钮 );
	SendMessage( 全_工具栏句柄 , TB_SETIMAGELIST , (WPARAM)0 , (LPARAM)h启用图像列表 );
	SendMessage( 全_工具栏句柄 , TB_SETDISABLEDIMAGELIST , (WPARAM)0 , (LPARAM)h禁用图像列表 );
	SendMessage( 全_工具栏句柄 , TB_SETMAXTEXTROWS , 0 , 0 );
	SendMessage( 全_工具栏句柄 , TB_AUTOSIZE , 0 , 0 );
}
