#include "framework.h"

int OnSourceCodeBlockFoldVisiable( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.启用块折叠 == FALSE )
	{
		全_风格主配置.启用块折叠 = TRUE ;
	}
	else
	{
		全_风格主配置.启用块折叠 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	保存主配置文件();

	int		n选项卡页面数量 ;
	int		n选项卡页面索引 ;
	TCITEM		tci ;
	struct 选项卡页面S	*p = NULL ;

	n选项卡页面数量 = TabCtrl_GetItemCount( 全_选项卡页面句柄 ) ;
	for( n选项卡页面索引 = 0; n选项卡页面索引 < n选项卡页面数量; n选项卡页面索引++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( 全_选项卡页面句柄 , n选项卡页面索引 , & tci );
		p = (struct 选项卡页面S *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETMARGINWIDTHN , 边框_折叠_索引 , (全_风格主配置.启用块折叠==TRUE?16:0) );
	}

	return 0;
}

int OnSourceCodeBlockFoldToggle( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		int	当前文本字节位置 ;
		int	nCurrentLine ;
		int	nFoldLevel ;
		int	nFoldHeaderLine ;

		当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;

		nFoldLevel = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETFOLDLEVEL, nCurrentLine, 0 ) ;
		if( nFoldLevel & SC_FOLDLEVELHEADERFLAG )
		{
			nFoldHeaderLine = nCurrentLine ;
		}
		else
		{
			nFoldHeaderLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETFOLDPARENT, nCurrentLine, 0 ) ;
		}

		if( nFoldHeaderLine >= 0 )
		{
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOLINE , nFoldHeaderLine , 0 );
			选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_TOGGLEFOLD, nFoldHeaderLine, 0 );
		}
	}

	return 0;
}

int OnSourceCodeBlockFoldContract( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		int	当前文本字节位置 ;
		int	nCurrentLine ;
		int	nFoldLevel ;
		int	nFoldHeaderLine ;
		BOOL	bIsFoldExpand ;

		当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;

		nFoldLevel = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETFOLDLEVEL, nCurrentLine, 0 ) ;
		if( nFoldLevel & SC_FOLDLEVELHEADERFLAG )
		{
			nFoldHeaderLine = nCurrentLine ;
		}
		else
		{
			nFoldHeaderLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETFOLDPARENT, nCurrentLine, 0 ) ;
		}

		if( nFoldHeaderLine >= 0 )
		{
			bIsFoldExpand = (BOOL)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETFOLDEXPANDED, nFoldHeaderLine, 0 ) ;
			if( bIsFoldExpand == TRUE )
			{
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOLINE , nFoldHeaderLine , 0 );
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_FOLDLINE, nFoldHeaderLine, SC_FOLDACTION_CONTRACT );
			}
		}
	}

	return 0;
}

int OnSourceCodeBlockFoldExpand( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		int	当前文本字节位置 ;
		int	nCurrentLine ;
		int	nFoldLevel ;
		int	nFoldHeaderLine ;
		BOOL	bIsFoldExpand ;

		当前文本字节位置 = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_LINEFROMPOSITION, 当前文本字节位置, 0 ) ;

		nFoldLevel = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETFOLDLEVEL, nCurrentLine, 0 ) ;
		if( nFoldLevel & SC_FOLDLEVELHEADERFLAG )
		{
			nFoldHeaderLine = nCurrentLine ;
		}
		else
		{
			nFoldHeaderLine = (int)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETFOLDPARENT, nCurrentLine, 0 ) ;
		}

		if( nFoldHeaderLine >= 0 )
		{
			bIsFoldExpand = (BOOL)选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_GETFOLDEXPANDED, nFoldHeaderLine, 0 ) ;
			if( bIsFoldExpand == FALSE )
			{
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_GOTOLINE , nFoldHeaderLine , 0 );
				选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_FOLDLINE, nFoldHeaderLine, SC_FOLDACTION_EXPAND );
			}
		}
	}

	return 0;
}

int OnSourceCodeBlockFoldContractAll( struct 选项卡页面S *选项卡页节点 )
{
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_FOLDALL, SC_FOLDACTION_CONTRACT, 0 );

	return 0;
}

int OnSourceCodeBlockFoldExpandAll( struct 选项卡页面S *选项卡页节点 )
{
	选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla, SCI_FOLDALL, SC_FOLDACTION_EXPAND, 0 );

	return 0;
}

int OnSetReloadSymbolListOrTreeInterval( struct 选项卡页面S *选项卡页节点 )
{
	int		nOldReloadSymbolListOrTreeInterval = 全_风格主配置.重新加载符号列表或树间隔 ;

	char		nOldReloadSymbolListOrTreeIntervalStr[ 20+1 ] ;

	int		nret = 0 ;

	memset( nOldReloadSymbolListOrTreeIntervalStr , 0x00 , sizeof(nOldReloadSymbolListOrTreeIntervalStr) );
	snprintf( nOldReloadSymbolListOrTreeIntervalStr , sizeof(nOldReloadSymbolListOrTreeIntervalStr)-1 , "%d" , 全_风格主配置.重新加载符号列表或树间隔 );
_GOTO_RETRY :
	nret = InputBox( 全_主窗口句柄 , "请输入定时刷新符号列表或符号树的时间间隔（0~600）（单位：秒）：" , "输入窗口" , 0 , nOldReloadSymbolListOrTreeIntervalStr , sizeof(nOldReloadSymbolListOrTreeIntervalStr)-1 ) ;
	if( nret == IDOK )
	{
		if( nOldReloadSymbolListOrTreeIntervalStr[0] == '\0' || atoi(nOldReloadSymbolListOrTreeIntervalStr) < 0 || atoi(nOldReloadSymbolListOrTreeIntervalStr) > 600 )
		{
			错误框( "输入数值不合法" );
			goto _GOTO_RETRY;
		}

		全_风格主配置.重新加载符号列表或树间隔 = atoi(nOldReloadSymbolListOrTreeIntervalStr) ;
		if( nOldReloadSymbolListOrTreeInterval == 0 && 全_风格主配置.重新加载符号列表或树间隔 > 0 )
		{
			BeginReloadSymbolListOrTreeThread();
		}

		更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );
	}

	return 0;
}

int OnSourceCodeEnableAutoCompletedShow( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.启用自动完成显示 == FALSE )
	{
		全_风格主配置.启用自动完成显示 = TRUE ;
	}
	else
	{
		全_风格主配置.启用自动完成显示 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	return 0;
}

int OnSourceCodeAutoCompletedShowAfterInputCharacters( struct 选项卡页面S *选项卡页节点 )
{
	char		输入字符后自动完成显示[ 2+1 ] ;

	int		nret = 0 ;

	memset( 输入字符后自动完成显示 , 0x00 , sizeof(输入字符后自动完成显示) );
	snprintf( 输入字符后自动完成显示 , sizeof(输入字符后自动完成显示)-1 , "%d" , 全_风格主配置.输入字符后自动完成显示 );
	nret = InputBox( 全_主窗口句柄 , "请输入弹出自动完成框之前输入的字符数：" , "输入窗口" , 0 , 输入字符后自动完成显示 , sizeof(输入字符后自动完成显示)-1 ) ;
	if( nret == IDOK )
	{
		if( 输入字符后自动完成显示[0] )
		{
			全_风格主配置.输入字符后自动完成显示 = atoi(输入字符后自动完成显示) ;

			更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );
		}
	}

	return 0;
}

int OnSourceCodeEnableCallTipShow( struct 选项卡页面S *选项卡页节点 )
{
	if( 全_风格主配置.启用调用提示显示 == FALSE )
	{
		全_风格主配置.启用调用提示显示 = TRUE ;
	}
	else
	{
		全_风格主配置.启用调用提示显示 = FALSE ;
	}

	更新所有菜单( 全_主窗口句柄 , 选项卡页节点 );

	return 0;
}

#define DATABASE_CONNECTION_CONFIG	BEGIN_DATABASE_CONNECTION_CONFIG"\r\n" \
					"--  DBTYPE : <MySQL|Oracle|Sqlite3|PostgreSQL>\r\n" \
					"--  DBHOST : <MySQL/PostgreSQL Server IP|(ORACLESID)\r\n" \
					"--  DBPORT : <MySQL/PostgreSQL Port|0>\r\n" \
					"--  DBUSER : <MySQL/PostgreSQL User>\r\n" \
					"--  DBPASS : (MySQL/PostgreSQL Password)\r\n" \
					"--  DBNAME : (MySQL/PostgreSQL Database|Sqlite Path Filename>)\r\n" \
					END_DATABASE_CONNECTION_CONFIG"\r\n" \
					"\r\n"

int OnInsertDataBaseConnectionConfig( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_INSERTTEXT , 0 , (sptr_t)DATABASE_CONNECTION_CONFIG );
	}

	return 0;
}

#define REDIS_CONNECTION_CONFIG		BEGIN_REDIS_CONNECTION_CONFIG"\r\n" \
					"--  HOST : 127.0.0.1\r\n" \
					"--  PORT : 6379\r\n" \
					"--  PASS : (DBPASS)\r\n" \
					"--  DBSL : 0\r\n" \
					END_REDIS_CONNECTION_CONFIG"\r\n" \
					"\r\n"

int OnInsertRedisConnectionConfig( struct 选项卡页面S *选项卡页节点 )
{
	if( 选项卡页节点 )
	{
		选项卡页节点->pfuncScintilla( 选项卡页节点->pScintilla , SCI_INSERTTEXT , 0 , (sptr_t)REDIS_CONNECTION_CONFIG );
	}

	return 0;
}
