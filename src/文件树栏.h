#ifndef _FILETREEBAR_
#define _FILETREEBAR_

#include "framework.h"

extern HWND	全_文件树栏句柄 ;
extern BOOL	全_显示文件树栏 ;
extern RECT	全_文件树栏矩形 ;

extern HWND	全_文件树句柄 ;

extern HMENU	g_hFileTreeFilePopupMenu ;
extern HMENU	g_hFileTreeDirectoryPopupMenu ;

extern int	全_文件树映像驱动器 ;
extern int	nFileTreeImageOpenFold ;
extern int	nFileTreeImageClosedFold ;
extern int	nFileTreeImageTextFile ;
extern int	nFileTreeImageGeneralFile ;
extern int	nFileTreeImageExecFile ;

struct 树视图数据S
{
	struct RemoteFileServer	*pstRemoteFileServer ;
	char			acPathName[ MAX_PATH ] ;
	char			acPathFilename[ MAX_PATH ] ;
	char			acFilename[ MAX_PATH ] ;
	HTREEITEM		hti ;
	int			nImageIndex ;
	BOOL			bIsLoadedCompleted ;
};

int 创建文件树栏( HWND hWnd );
void 调整文件树框( RECT *rectFileTreeBar , RECT *文件树矩形 );

struct 树视图数据S *从HTREEITEM获取树状视图数据( HTREEITEM hti );
struct 树视图数据S *AddFileTreeNode( HWND hwnd , HTREEITEM parent , int nImageIndex , struct RemoteFileServer *pstRemoteFileServer , char *acPathName , char *acPathFilename , char *acFilename , BOOL bIsLoadedCompleted );

int AppendFileTreeNodeChildren( HWND hwnd , struct 树视图数据S *tvdParent );
int LoadDrivesToFileTree( HWND hwnd );
int 加载目录到文件树( HWND hwnd , char *pcPathname );

int AppendRemoteFileTreeNodeChildren( HWND hwnd , struct 树视图数据S *tvdParent , CURL *curl );
int LoadRemoteDriverToFileTree( HWND hwnd , struct RemoteFileServer *pnodeRemoteFileServer );
int LoadRemoteDriversToFileTree( HWND hwnd );

int UpdateFileTreeNodeChildren( HWND hwnd , HTREEITEM htiParent );

int 文件树节点展开时执行( NMTREEVIEW *lpnmtv );
int 文件树节点被双击时执行();

int RefreshFileTree();
int FileTreeRenameDirectory();
int FileTreeDeleteDirectory();
int FileTreeCreateSubDirectory();
int FileTreeCreateFile();
int FileTreeCopyFile();
int FileTreeRenameFile();
int FileTreeDeleteFile();

int 定位目录( char *acPathName );

#define CONFIG_KEY_MATERIAL_FILETREE		"FILETR"

#define REMOTE_FILE_BUFFER_SIZE_DEFAULT		1024*1024

struct RemoteFileBuffer
{
	char	buf[ REMOTE_FILE_BUFFER_SIZE_DEFAULT ] ;
	size_t	str_len ;
	size_t	remain_len ;
};

#endif
