#ifndef _SOURCECODE_
#define _SOURCECODE_

#include "framework.h"

int OnViewFileTree( struct 选项卡页面S *选项卡页节点 );

int OnViewSwitchFileType( int nFileTypeIndex );

int OnViewSwitchStyleTheme( int nWindowThemeIndex );
int OnViewModifyThemeStyle();
int OnViewCopyNewThemeStyle();

int OnViewHexEditMode( struct 选项卡页面S *选项卡页节点 );
int AdujstPositionOnHexEditMode( struct 选项卡页面S *选项卡页节点 , MSG *p_信息 );
int AdujstKeyOnHexEditMode( struct 选项卡页面S *选项卡页节点 , MSG *p_信息 );
int AdujstCharOnHexEditMode( struct 选项卡页面S *选项卡页节点 , MSG *p_信息 );
int SyncDataOnHexEditMode( struct 选项卡页面S *选项卡页节点 );
unsigned char *StrdupHexEditTextFold( struct 选项卡页面S *选项卡页节点 , size_t *pnTextLength );

int OnViewEnableWindowsVisualStyles();

int OnViewTabWidth( struct 选项卡页面S *选项卡页节点 );
int OnViewOnKeydownTabConvertSpaces( struct 选项卡页面S *选项卡页节点 );

int OnViewWrapLineMode( struct 选项卡页面S *选项卡页节点 );

int OnViewLineNumberVisiable( struct 选项卡页面S *选项卡页节点 );
int OnViewBookmarkVisiable( struct 选项卡页面S *选项卡页节点 );

int OnViewWhiteSpaceVisiable( struct 选项卡页面S *选项卡页节点 );
int OnViewNewLineVisiable( struct 选项卡页面S *选项卡页节点 );
int OnViewIndentationGuidesVisiable( struct 选项卡页面S *选项卡页节点 );

int OnViewZoomOut( struct 选项卡页面S *选项卡页节点 );
int OnViewZoomIn( struct 选项卡页面S *选项卡页节点 );
int OnViewZoomReset( struct 选项卡页面S *选项卡页节点 );

int OnEditorTextSelected( struct 选项卡页面S *选项卡页节点 );

#endif
