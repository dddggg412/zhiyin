#ifndef _DOCTYPE_
#define _DOCTYPE_

#include "framework.h"

extern uintptr_t		重新加载符号列表或树线程处理程序 ;

struct AutoCompletedShowNode
{
	char		*acAutoCompletedString ;
	int		begin ;
	int		end ;

	struct rb_node	nodeAutoCompletedShow ;
} ;

struct AutoCompletedShowTree
{
	struct rb_root	treeAutoCompletedShow ;
} ;

int LinkAutoCompletedShowTreeNode( struct AutoCompletedShowTree *tree , struct AutoCompletedShowNode *node );
void UnlinkAutoCompletedShowTreeNode( struct AutoCompletedShowTree *tree , struct AutoCompletedShowNode *node );
int CompareAutoCompletedShowTreeNodePartlyEntry( void *pv1 , void *pv2 );
struct AutoCompletedShowNode *QueryAutoCompletedShowTreeNodePartly( struct AutoCompletedShowTree *tree , struct AutoCompletedShowNode *node );
void DestroyAutoCompletedShowTree( struct AutoCompletedShowTree *tree );

struct CallTipShowNode
{
	char		*acCallTipFuncName ;
	char		*acCallTipFuncDesc ;

	struct rb_node	nodeCallTipShow ;
};

struct CallTipShowTree
{
	struct rb_root	treeCallTipShow ;
};

int LinkCallTipShowTreeNode( struct CallTipShowTree *tree , struct CallTipShowNode *node );
void UnlinkCallTipShowTreeNode( struct CallTipShowTree *tree , struct CallTipShowNode *node );
struct CallTipShowNode *QueryCallTipShowTreeNode( struct CallTipShowTree *tree , struct CallTipShowNode *node );
void DestroyCallTipShowTree( struct CallTipShowTree *tree );

typedef int funcInitTabPageControlsBeforeLoadFile( struct 选项卡页面S *选项卡页节点 );
typedef int funcInitTabPageControlsAfterLoadFile( struct 选项卡页面S *选项卡页节点 );
typedef int funcParseFileConfigHeader( struct 选项卡页面S *选项卡页节点 );
typedef int funcOnCharAdded( struct 选项卡页面S *选项卡页节点 , SCNotification *lpnotify );
typedef int funcOnKeyDown( struct 选项卡页面S *选项卡页节点 , WPARAM wParam , LPARAM lParam );
typedef int funcOnKeyUp( struct 选项卡页面S *选项卡页节点 , WPARAM wParam , LPARAM lParam );
typedef int funcOnReloadSymbolList( struct 选项卡页面S *选项卡页节点 );
typedef int funcOnDbClickSymbolList( struct 选项卡页面S *选项卡页节点 );
typedef int funcOnReloadSymbolTree( struct 选项卡页面S *选项卡页节点 );
typedef int funcOnDbClickSymbolTree( struct 选项卡页面S *选项卡页节点 );
typedef int funcCleanTabPageControls( struct 选项卡页面S *选项卡页节点 );

enum DocType
{
	DOCTYPE_END = 0 ,
	DOCTYPE_TXT ,
	DOCTYPE_CPP ,
	DOCTYPE_CS ,
	DOCTYPE_JAVA ,
	DOCTYPE_JAVASCRIPT ,
	DOCTYPE_GO ,
	DOCTYPE_SWIFT ,
	DOCTYPE_SQL ,
	DOCTYPE_REDIS ,
	DOCTYPE_PYTHON ,
	DOCTYPE_LUA ,
	DOCTYPE_PERL ,
	DOCTYPE_SH ,
	DOCTYPE_RUST ,
	DOCTYPE_RUBY ,
	DOCTYPE_LISP ,
	DOCTYPE_ASM ,
	DOCTYPE_COBOL ,
	DOCTYPE_HTML ,
	DOCTYPE_XML ,
	DOCTYPE_CSS ,
	DOCTYPE_JSON ,
	DOCTYPE_YAML ,
	DOCTYPE_MAKEFILE ,
	DOCTYPE_CMAKE ,
	DOCTYPE_MARKDOWN ,
	DOCTYPE_LOG ,
	DOCTYPE_PROPERTIES ,
	DOCTYPE_BATCH ,
	DOCTYPE_NIM
} ;

struct 文档类型配置S
{
	enum DocType				n文档的类型 ;
	const char				*p文件类型的配置文件名 ;
	const char				*p文件扩展名 ;
	const char				*p文件类型描述 ;

	funcInitTabPageControlsBeforeLoadFile	*pfuncInitTabPageControlsBeforeLoadFile ;
	funcInitTabPageControlsAfterLoadFile	*pfuncInitTabPageControlsAfterLoadFile ;
	funcParseFileConfigHeader		*pfuncParseFileConfigHeader ;
	funcOnKeyDown				*pfuncOnKeyDown ;
	funcOnKeyDown				*pfuncOnKeyUp ;
	funcOnCharAdded				*pfuncOnCharAdded ;
	funcOnReloadSymbolList			*pfuncOnReloadSymbolList ;
	funcOnDbClickSymbolList			*pfuncOnDbClickSymbolList ;
	funcOnReloadSymbolTree			*pfuncOnReloadSymbolTree ;
	funcOnDbClickSymbolTree			*pfuncOnDbClickSymbolTree ;
	funcCleanTabPageControls		*pfuncCleanTabPageControls ;

	char				*keywords ;
	char				*keywords2 ;
	char				*keywords3 ;
	char				*keywords4 ;
	char				*keywords5 ;
	char				*keywords6 ;
	char				*autocomplete_set ;
	char				*autocomplete2_set ;

	struct AutoCompletedShowTree	stAutoCompletedShowTree ;
	size_t				sAutoCompletedBufferSize ;
	char				*acAutoCompletedBuffer ;
	char				*acAutoCompletedShowBuffer ;
	int				nAutoCompletedShowStartPos ;
	int				nAutoCompletedShowEndPos ;

	struct AutoCompletedShowTree	stAutoCompletedShowTree2 ;
	size_t				sAutoCompletedBufferSize2 ;
	char				*acAutoCompletedBuffer2 ;
	char				*acAutoCompletedShowBuffer2 ;
	int				nAutoCompletedShowStartPos2 ;
	int				nAutoCompletedShowEndPos2 ;

	struct CallTipShowTree		stCallTipShowTree ;

	char				*acSymbolReqularExp ;
} ;

extern struct 文档类型配置S	全_文档类型配置[] ;

struct 文档类型配置S *获取文档类型配置( char *pcExtname );
char *获取文件对话框筛选器指针();

int BuildAutoCompletedShowTree( struct 文档类型配置S *pstFileExtnameMapper , char *word2 );
int ExpandAutoCompletedShowTreeToBuffer( struct 文档类型配置S *pstFileExtnameMapper );

int BuildAutoCompletedShowTree2( struct 文档类型配置S *pstFileExtnameMapper , char *word2 );
int ExpandAutoCompletedShowTreeToBuffer2( struct 文档类型配置S *pstFileExtnameMapper );

int LoadLexerConfigFile( const char *filename , struct 文档类型配置S *pstFileExtnameMapper , char *filebuf );

#define CONFIG_KEY_MATERIAL_LEXER	"DOCTYP"

int BeginReloadSymbolListOrTreeThread();
void 重新加载符号列表或树线程条目( void *param );

void 更新文件类型菜单();

#endif
